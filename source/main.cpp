/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <config_tree/config_tree.hpp>
#include <qfre/fractal.hpp>
#include <qfre/pipeline.hpp>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include <QStandardPaths>
#include <application_config.hpp>
#include <message_streams.hpp>

void
setup_application_settings ( config_tree::Config_Tree & settings_n )
{
  settings_n.set_value ( "app.name", APPLICATION_NAME );
  settings_n.set_value ( "app.title", APPLICATION_TITLE );
  settings_n.set_value ( "app.version", APPLICATION_VERSION );
  {
    QString path = "file://";
    path +=
        QStandardPaths::writableLocation ( QStandardPaths::AppConfigLocation );
    path += "/gui_settings.json";
    settings_n.set_value ( "files.guiSettings", path );
  }
}

void
setup_qml_engine ( QQmlApplicationEngine & engine_n,
                   config_tree::Config_Tree & application_settings_n )
{
  // -- Register qml types
  qmlRegisterType< config_tree::Config_Tree > (
      "Settings", 1, 0, "ConfigTree" );
  qmlRegisterType< qfre::Pipeline > ( "Fractal", 1, 0, "Pipeline" );
  qmlRegisterType< qfre::Fractal > ( "Fractal", 1, 0, "FractalView" );
  qmlRegisterType< qfre::Iterator_Settings > (
      "Fractal", 1, 0, "IteratorSettings" );
  qmlRegisterType< qfre::Color_Settings > ( "Fractal", 1, 0, "ColorSettings" );

  // -- Register import paths
  engine_n.addImportPath ( "qrc:///qml" );
  if ( DEBUG_QML_ENGINE ) {
    Debug_Stream oss;
    oss << "engine ()->importPathList ()\n";
    for ( auto const & item : engine_n.importPathList () ) {
      oss << "  " << item.toStdString () << "\n";
    }
  }

  engine_n.rootContext ()->setContextProperty ( "applicationSettings",
                                                &application_settings_n );
}

int
main ( int argc, char * argv[] )
{
  int res = 0;
  {
    // -- Qt gui application class
    QGuiApplication app ( argc, argv );
    app.setApplicationName ( APPLICATION_NAME );
    app.setApplicationVersion ( APPLICATION_VERSION );

    // -- Application settings
    config_tree::Config_Tree application_settings;
    setup_application_settings ( application_settings );

    // -- Setup gui style
    QQuickStyle::setStyle ( "Imagine" );

    // -- Setup qml engine
    QQmlApplicationEngine qml_app_engine;
    setup_qml_engine ( qml_app_engine, application_settings );
    qml_app_engine.load ( QString ( "qrc:///qml/Window.qml" ) );

    res = app.exec ();
  }
  return res;
}
