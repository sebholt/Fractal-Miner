/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <config_tree/config_tree.hpp>
#include <config_tree/priv/root.hpp>
#include <config_tree/priv/watcher_relay.hpp>
#include <config_tree/watcher.hpp>
#include <QEvent>

namespace config_tree
{
// -- Static functions

static inline bool
string_shared_missmatch ( const QString & sa_n, const QString & sb_n )
{
  return ( sa_n.constData () != sb_n.constData () );
}

// -- Class methods

Watcher::Watcher ( QObject * parent_n )
: QObject ( parent_n )
, _is_notified ( ATOMIC_FLAG_INIT )
, _reference ( nullptr )
, _config ( nullptr )
{
}

Watcher::Watcher ( Config_Tree * config_n,
                   const QString & address_n,
                   QObject * parent_n )
: Watcher ( parent_n )
{
  watch ( config_n, address_n );
}

Watcher::~Watcher ()
{
  clear_config_silent ();
}

void
Watcher::clear_config_silent ()
{
  if ( _reference.is_valid () ) {
    _reference->watcher_unregister ( _address, this );
    _reference.clear ();
    _config = nullptr;
  }
}

void
Watcher::clear ()
{
  const bool cfg_changed ( _config != nullptr );
  const bool add_changed ( !_address.isNull () );
  const bool val_changed ( !_value.isNull () );
  // Clear values
  clear_config_silent ();
  if ( add_changed ) {
    _address.clear ();
  }
  if ( val_changed ) {
    _value.clear ();
  }
  // Emit signals
  forward_value_change ( cfg_changed, add_changed, val_changed );
}

void
Watcher::watch ( Config_Tree * config_n, const QString & address_n )
{
  if ( _config == config_n ) {
    // Only change address
    set_address ( address_n );
  } else {
    // State change flags
    bool add_changed ( false );
    bool val_changed ( false );
    // Update values
    {
      QString new_address ( address_n );
      QString new_value;
      // Update configuration
      clear_config_silent ();
      _config = config_n;
      if ( _config != nullptr ) {
        _reference = _config->reference ();
        _reference->watcher_register (
            new_address, new_value, address_n, this );
      }
      // Check for changes to optimize implicit sharing
      if ( string_shared_missmatch ( _address, new_address ) ) {
        _address = new_address;
        add_changed = true;
      }
      if ( string_shared_missmatch ( _value, new_value ) ) {
        _value = new_value;
        val_changed = true;
      }
    }
    // Forward change signals
    forward_value_change ( true, add_changed, val_changed );
  }
}

void
Watcher::set_config ( Config_Tree * config_n )
{
  watch ( config_n, _address );
}

void
Watcher::clear_config ()
{
  set_config ( nullptr );
}

void
Watcher::set_address ( const QString & address_n )
{
  if ( _address != address_n ) {
    bool add_changed ( false );
    bool val_changed ( false );
    {
      // Init with default values
      QString new_address ( address_n );
      QString new_value;
      if ( _reference.is_valid () ) {
        _reference->watcher_change_address (
            new_address, new_value, _address, address_n, this );
      }
      // Check for changes to optimize implicit sharing
      if ( string_shared_missmatch ( _address, new_address ) ) {
        _address = new_address;
        add_changed = true;
      }
      if ( string_shared_missmatch ( _value, new_value ) ) {
        _value = new_value;
        val_changed = true;
      }
    }
    // Forward change signals
    forward_value_change ( false, add_changed, val_changed );
  }
}

void
Watcher::clear_address ()
{
  set_address ( QString () );
}

bool
Watcher::set_value ( const QString & value_n, Error * error_n )
{
  bool val_changed ( false );
  if ( _reference.is_valid () ) {
    if ( _value != value_n ) {
      _value = value_n;
      val_changed = true;
    }
    _reference->set_value ( _address, value_n, error_n );
    forward_value_change ( false, false, val_changed );
  }
  return val_changed;
}

void
Watcher::update_value ()
{
  bool val_changed ( false );
  {
    QString new_value;
    if ( _reference.is_valid () ) {
      new_value = _reference->value ( _address );
    }
    if ( string_shared_missmatch ( _value, new_value ) ) {
      _value = new_value;
      val_changed = true;
    }
  }
  forward_value_change ( false, false, val_changed );
}

void
Watcher::setTarget ( const QQmlProperty & prop_n )
{
  _target_property = prop_n;
  // Write target property
  if ( _target_property.isWritable () ) {
    _target_property.write ( value () );
  }
}

inline void
Watcher::forward_value_change ( bool config_changed_n,
                                bool address_changed_n,
                                bool value_changed_n )
{
  if ( config_changed_n ) {
    emit config_changed ();
  }
  if ( address_changed_n ) {
    emit address_changed ();
  }
  if ( value_changed_n ) {
    // Write target property
    if ( _target_property.isWritable () ) {
      _target_property.write ( value () );
    }
    emit value_changed ();
  }
}

bool
Watcher::event ( QEvent * event_n )
{
  if ( event_n->type () == QEvent::User ) {
    // Accept new notifications
    _is_notified.clear ();
    update_value ();
    event_n->accept ();
    return true;
  }
  return QObject::event ( event_n );
}
} // namespace config_tree