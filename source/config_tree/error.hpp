/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <string_tree/error.hpp>

namespace config_tree
{
using string_tree::Error;
}
