/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <config_tree/error.hpp>
#include <string_tree/path.hpp>

namespace config_tree
{
// -- Types and statics

using string_tree::Path;
using string_tree::path_key_char_valid;
using string_tree::path_key_sep;
} // namespace config_tree
