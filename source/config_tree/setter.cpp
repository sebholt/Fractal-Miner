/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <config_tree/config_tree.hpp>
#include <config_tree/priv/root.hpp>
#include <config_tree/setter.hpp>

namespace config_tree
{
// -- Static functions

static inline bool
string_shared_missmatch ( const QString & sa_n, const QString & sb_n )
{
  return ( sa_n.constData () != sb_n.constData () );
}

// -- Class methods

Setter::Setter ( QObject * parent_n )
: QObject ( parent_n )
, _reference ( nullptr )
, _config ( nullptr )
{
}

Setter::Setter ( Config_Tree * config_n,
                 const QString & address_n,
                 QObject * parent_n )
: Setter ( parent_n )
{
  setup ( config_n, address_n );
}

Setter::~Setter ()
{
  clear_config_silent ();
}

void
Setter::clear_config_silent ()
{
  if ( _reference.is_valid () ) {
    _reference.clear ();
    _config = nullptr;
  }
}

void
Setter::clear ()
{
  const bool cfg_changed ( _config != nullptr );
  const bool add_changed ( !_address.isNull () );
  // Clear values
  clear_config_silent ();
  if ( add_changed ) {
    _address.clear ();
  }
  // Emit signals
  forward_value_change ( cfg_changed, add_changed );
}

void
Setter::setup ( Config_Tree * config_n, const QString & address_n )
{
  if ( _config == config_n ) {
    // Only change address
    set_address ( address_n );
  } else {
    // State change flags
    bool add_changed ( false );
    // Update values
    {
      QString new_address ( address_n );
      QString new_value;
      // Update configuration
      clear_config_silent ();
      _config = config_n;
      if ( _config != nullptr ) {
        _reference = _config->reference ();
      }
      // Check for changes to optimize implicit sharing
      if ( string_shared_missmatch ( _address, new_address ) ) {
        _address = new_address;
        add_changed = true;
      }
    }
    // Forward change signals
    forward_value_change ( true, add_changed );
  }
}

void
Setter::set_config ( Config_Tree * config_n )
{
  setup ( config_n, _address );
}

void
Setter::clear_config ()
{
  set_config ( nullptr );
}

void
Setter::set_address ( const QString & address_n )
{
  // Check for changes to optimize implicit sharing
  if ( string_shared_missmatch ( _address, address_n ) ) {
    _address = address_n;
    forward_value_change ( false, true );
  }
}

void
Setter::clear_address ()
{
  set_address ( QString () );
}

QString
Setter::get () const
{
  return get ( nullptr );
}

QString
Setter::get ( Error * error_n ) const
{
  QString res;
  if ( _reference.is_valid () ) {
    res = _reference->value ( _address, error_n );
  }
  return res;
}

bool
Setter::set ( const QString & value_n ) const
{
  return set ( value_n, nullptr );
}

bool
Setter::set ( const QString & value_n, Error * error_n ) const
{
  if ( _reference.is_valid () ) {
    return _reference->set_value ( _address, value_n, error_n );
  }
  return false;
}

void
Setter::update () const
{
  if ( _updater ) {
    set ( _updater () );
  }
}

inline void
Setter::forward_value_change ( bool config_changed_n, bool address_changed_n )
{
  if ( config_changed_n ) {
    emit config_changed ();
  }
  if ( address_changed_n ) {
    emit address_changed ();
  }
}
} // namespace config_tree