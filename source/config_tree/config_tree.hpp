/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <config_tree/error.hpp>
#include <config_tree/path.hpp>
#include <config_tree/reference.hpp>
#include <string_tree/branch.hpp>
#include <QObject>
#include <QString>
#include <QStringList>

namespace config_tree
{

/// @brief Thread safe configuration string tree
///
class Config_Tree : public QObject
{
  Q_OBJECT

  public:
  // -- Types

  typedef string_tree::Branch Branch;

  public:
  // -- Constructors

  /// @brief Creates a new configuration
  ///
  Config_Tree ( QObject * parent_n = nullptr );

  /// @brief Copy constructor
  ///
  /// Deep copies the remote configuration
  Config_Tree ( const Config_Tree & config_n );

  /// @brief Move constructor
  Config_Tree ( Config_Tree && config_n ) = delete;

  /// @brief Link constructor
  ///
  /// Makes this another view into the configuration referenced by Reference_n
  Config_Tree ( const Reference & Reference_n )
  : _reference ( Reference_n )
  {
  }

  /// @brief Destructor
  ~Config_Tree ();

  // -- Accessors

  /// @brief Reference to the shared private configuration
  const Reference &
  reference () const
  {
    return _reference;
  }

  // -- Clear and empty

  Q_INVOKABLE
  void
  clear ();

  Q_INVOKABLE
  bool
  is_empty () const;

  // -- Branch keys

  QStringList
  get_keys ( const Path & path_n, Error * error_n = nullptr ) const;

  Q_INVOKABLE
  QStringList
  get_keys ( const QString & address_n, Error * error_n = nullptr ) const;

  Q_INVOKABLE
  QStringList
  get_branch_keys ( const QString & address_n,
                    Error * error_n = nullptr ) const;

  Q_INVOKABLE
  QStringList
  get_leaf_keys ( const QString & address_n, Error * error_n = nullptr ) const;

  bool
  get_split_keys ( QStringList & branch_keys_n,
                   QStringList & leaf_keys_n,
                   const Path & path_n,
                   Error * error_n = nullptr ) const;

  bool
  get_split_keys ( QStringList & branch_keys_n,
                   QStringList & leaf_keys_n,
                   const QString & address_n,
                   Error * error_n = nullptr ) const;

  // -- Get value by address

  QString
  get_value ( const Path & path_n, Error * error_n = nullptr ) const;

  QString
  get_value ( const QString & address_n, Error * error_n = nullptr ) const;

  Q_INVOKABLE
  QString
  value ( const QString & address_n,
          const QString & default_n = QString () ) const;

  // -- Set value recursive by address

  bool
  set_value ( const Path & path_n,
              const QString & value_n,
              Error * error_n = nullptr );

  Q_INVOKABLE
  bool
  set_value ( const QString & address_n,
              const QString & value_n,
              Error * error_n = nullptr );

  bool
  set_value_real ( const Path & path_n,
                   double value_n,
                   Error * error_n = nullptr );

  Q_INVOKABLE
  bool
  set_value_real ( const QString & address_n,
                   double value_n,
                   Error * error_n = nullptr );

  // -- Get branch by address

  void
  get_root_branch ( Branch & branch_n ) const;

  Branch
  root_branch () const;

  // -- Get branch by address

  bool
  get_branch ( Branch & branch_n,
               const Path & path_n,
               Error * error_n = nullptr ) const;

  bool
  get_branch ( Branch & branch_n,
               const QString & address_n,
               Error * error_n = nullptr ) const;

  Branch
  branch ( const Path & path_n, Error * error_n = nullptr ) const;

  Branch
  branch ( const QString & address_n, Error * error_n = nullptr ) const;

  // -- Recursive remove

  bool
  remove ( const Path & path_n, Error * error_n = nullptr );

  Q_INVOKABLE
  bool
  remove ( const QString & address_n, Error * error_n = nullptr );

  // -- Remote branch insert

  void
  insert ( const Branch & branch_n );

  void
  insert_at ( const Path & path_n,
              const Branch & branch_n,
              Error * error_n = nullptr );

  void
  clear_insert ( const Branch & branch_n );

  // -- Json import / export

  /// @return True on success
  Q_INVOKABLE
  bool
  json_import_file ( const QUrl & filename_n );

  /// @return True on success
  Q_INVOKABLE
  bool
  json_export_file ( const QUrl & filename_n );

  // -- Cast operators

  /// @brief Cast to root reference
  operator const Reference & () const { return _reference; }

  // -- Assignment operators

  /// @brief Clears and inserts a copy of the remote configuration
  /// @arg config_n Remote source configuration
  /// @see clear_insert ( const Config_Tree & config_n )
  /// @return *this
  Config_Tree &
  operator= ( const Config_Tree & config_n )
  {
    clear_insert ( config_n.root_branch () );
    return *this;
  }

  /// @brief Drops the current reference and takes over the ownership
  ///        of the argument reference
  ///
  /// @arg config_n Is in an undefined but deconstructible state afterwards
  /// @return *this
  Config_Tree &
  operator= ( Config_Tree && config_n )
  {
    _reference = std::move ( config_n._reference );
    return *this;
  }

  /// @brief Clears and inserts a copy of the remote configuration
  /// @arg Reference_n Reference to the new configuration
  /// @see clear_insert ( const Config_Tree & config_n )
  /// @return *this
  Config_Tree &
  operator= ( const Reference & Reference_n )
  {
    _reference = Reference_n;
    return *this;
  }

  private:
  Reference _reference;
};
} // namespace config_tree
