/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <config_tree/error.hpp>
#include <config_tree/reference.hpp>
#include <QObject>
#include <QQmlProperty>
#include <QQmlPropertyValueSource>
#include <QString>

namespace config_tree
{

// -- Forward declaration
class Config_Tree;

/// @brief Configuration value or branch watcher
///
class Watcher : public QObject, public QQmlPropertyValueSource
{
  Q_OBJECT
  Q_INTERFACES ( QQmlPropertyValueSource )

  Q_PROPERTY ( config_tree::Config_Tree * config READ config WRITE set_config
                   NOTIFY config_changed )
  Q_PROPERTY (
      QString address READ address WRITE set_address NOTIFY address_changed )
  Q_PROPERTY ( QString value READ value WRITE set_value NOTIFY value_changed )

  public:
  // -- Constructors

  /// @brief Default constructor
  Watcher ( QObject * parent_n = nullptr );

  /// @brief Constructor
  Watcher ( Config_Tree * config_n,
            const QString & address_n,
            QObject * parent_n = nullptr );

  ~Watcher ();

  // -- General

  /// @brief Unwatches and clears the address
  Q_SLOT
  void
  clear ();

  /// @brief set_config() and set_address() in one method.
  void
  watch ( Config_Tree * config_n, const QString & address_n );

  // -- Configuration

  bool
  config_is_valid ()
  {
    return ( _config != nullptr );
  }

  /// @brief Starts watching config_ref_n at address().
  Config_Tree *
  config () const
  {
    return _config;
  }

  /// @brief Starts watching config_n at address().
  Q_SLOT
  void
  set_config ( Config_Tree * config_n );

  /// @brief Stops watching and clears value(). address() is unchanged.
  ///
  /// @see clear();
  Q_SLOT
  void
  clear_config ();

  /// @brief Gets called when config() changed
  Q_SIGNAL
  void
  config_changed ();

  // -- Address

  const QString &
  address () const
  {
    return _address;
  }

  Q_SLOT
  void
  set_address ( const QString & address_n );

  Q_SLOT
  void
  clear_address ();

  /// @brief Gets called when address() changed
  Q_SIGNAL
  void
  address_changed ();

  // -- Value

  QString
  value () const
  {
    return _value;
  }

  /// @brief Sets the value in this and the configuration
  ///
  /// @return True if the value changed
  bool
  set_value ( const QString & value_n, Error * error_n = nullptr );

  /// @brief Gets called when value() changed
  Q_SIGNAL
  void
  value_changed ();

  // -- Target property for QQmlPropertyValueSource

  void
  setTarget ( const QQmlProperty & prop_n ) override;

  // -- Qt event processing

  /// @brief Atomically sets the notified flag (thread safe)
  /// @return Value of the flag before
  bool
  set_notified ()
  {
    return _is_notified.test_and_set ();
  }

  bool
  event ( QEvent * event_n ) override;

  private:
  // -- Utility

  void
  clear_config_silent ();

  /// @brief Update value() from the configuration
  void
  update_value ();

  void
  forward_value_change ( bool config_changed_n,
                         bool address_changed_n,
                         bool value_changed_n );

  private:
  std::atomic_flag _is_notified;
  QString _address;
  QString _value;
  Reference _reference;
  Config_Tree * _config;
  QQmlProperty _target_property;
};
} // namespace config_tree
