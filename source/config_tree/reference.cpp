/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <config_tree/priv/root.hpp>
#include <config_tree/reference.hpp>

namespace config_tree
{
Reference::Reference ( Root * root_n )
: _root ( root_n )
{
  // This is another reference
  if ( _root != nullptr ) {
    _root->ref_count_increment ();
  }
}

Reference::Reference ( const Reference & ref_n )
: Reference ( ref_n._root )
{
}

Reference::Reference ( Reference && ref_n )
: _root ( ref_n._root )
{
  // We took over the source's reference
  if ( _root != nullptr ) {
    ref_n._root = nullptr;
  }
}

Reference::~Reference ()
{
  if ( _root != nullptr ) {
    root_unref ( _root );
  }
}

void
Reference::clear ()
{
  if ( _root != nullptr ) {
    root_unref ( _root );
    _root = nullptr;
  }
}

void
Reference::root_unref ( Root * root_n )
{
  if ( !root_n->ref_count_decrement () ) {
    delete root_n;
  }
}

void
Reference::reset ( Root * root_n )
{
  if ( _root != root_n ) {
    Root * rold ( _root );
    _root = root_n;
    if ( _root != nullptr ) {
      _root->ref_count_increment ();
    }
    // Unref old reference
    if ( rold != nullptr ) {
      root_unref ( rold );
    }
  }
}

void
Reference::move_assign ( Reference && ref_n )
{
  if ( &ref_n != this ) {
    // Unref old reference
    if ( _root != nullptr ) {
      root_unref ( _root );
    }
    _root = ref_n._root;
    // Clear source
    if ( ref_n._root != nullptr ) {
      ref_n._root = nullptr;
    }
  }
}
} // namespace config_tree