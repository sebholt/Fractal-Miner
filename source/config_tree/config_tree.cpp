/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <config_tree/config_tree.hpp>
#include <config_tree/priv/root.hpp>
#include <string_tree/json_export/exporter.hpp>
#include <string_tree/json_import/importer.hpp>
#include <QDir>
#include <QSaveFile>
#include <QUrl>

namespace config_tree
{
Config_Tree::Config_Tree ( QObject * parent_n )
: QObject ( parent_n )
, _reference ( new config_tree::priv::Root )
{
}

Config_Tree::Config_Tree ( const Config_Tree & config_n )
: Config_Tree ()
{
  clear_insert ( config_n.root_branch () );
}

Config_Tree::~Config_Tree () {}

/// @brief Clears all entries
void
Config_Tree::clear ()
{
  _reference->clear ();
}

bool
Config_Tree::is_empty () const
{
  return _reference->is_empty ();
}

/// @return List of keys in the addressed branch
QStringList
Config_Tree::get_keys ( const Path & path_n, Error * error_n ) const
{
  return _reference->get_keys ( path_n, error_n );
}

/// @return List of keys in the addressed branch
QStringList
Config_Tree::get_keys ( const QString & address_n, Error * error_n ) const
{
  return _reference->get_keys ( Path ( address_n, error_n ), error_n );
}

/// @return List of branch keys in the addressed branch
QStringList
Config_Tree::get_branch_keys ( const QString & address_n,
                               Error * error_n ) const
{
  return _reference->get_branch_keys ( Path ( address_n, error_n ), error_n );
}

/// @return List of leaf keys in the addressed branch
QStringList
Config_Tree::get_leaf_keys ( const QString & address_n, Error * error_n ) const
{
  return _reference->get_leaf_keys ( Path ( address_n, error_n ), error_n );
}

/// @return True if the address was valid and the branch existed
bool
Config_Tree::get_split_keys ( QStringList & branch_keys_n,
                              QStringList & leaf_keys_n,
                              const Path & path_n,
                              Error * error_n ) const
{
  return _reference->get_split_keys (
      branch_keys_n, leaf_keys_n, path_n, error_n );
}

/// @return True if the address was valid and the branch existed
bool
Config_Tree::get_split_keys ( QStringList & branch_keys_n,
                              QStringList & leaf_keys_n,
                              const QString & address_n,
                              Error * error_n ) const
{
  return _reference->get_split_keys (
      branch_keys_n, leaf_keys_n, Path ( address_n, error_n ), error_n );
}

/// @return On success the value from the addressed leaf or an empty value
/// otherwise
QString
Config_Tree::get_value ( const Path & path_n, Error * error_n ) const
{
  return _reference->value ( path_n, error_n );
}

/// @return On success the value from the addressed leaf or an empty value
/// otherwise
QString
Config_Tree::get_value ( const QString & address_n, Error * error_n ) const
{
  return _reference->value ( Path ( address_n, error_n ), error_n );
}

QString
Config_Tree::value ( const QString & address_n,
                     const QString & default_n ) const
{
  Error error;
  QString res = get_value ( address_n, &error );
  if ( error.is_valid () ) {
    res = default_n;
  }
  return res;
}

/// @return True if the value was changed
bool
Config_Tree::set_value ( const Path & path_n,
                         const QString & value_n,
                         Error * error_n )
{
  return _reference->set_value ( path_n, value_n, error_n );
}

/// @return True if the value was changed
bool
Config_Tree::set_value ( const QString & address_n,
                         const QString & value_n,
                         Error * error_n )
{
  return _reference->set_value (
      Path ( address_n, error_n ), value_n, error_n );
}

/// @return True if the value was changed
bool
Config_Tree::set_value_real ( const Path & path_n,
                              double value_n,
                              Error * error_n )
{
  return set_value (
      path_n,
      QString::number (
          value_n, 'g', std::numeric_limits< double >::max_digits10 ),
      error_n );
}

/// @return True if the value was changed
bool
Config_Tree::set_value_real ( const QString & address_n,
                              double value_n,
                              Error * error_n )
{
  return set_value (
      address_n,
      QString::number (
          value_n, 'g', std::numeric_limits< double >::max_digits10 ),
      error_n );
}

void
Config_Tree::get_root_branch ( Branch & branch_n ) const
{
  _reference->get_root_branch ( branch_n );
}

Config_Tree::Branch
Config_Tree::root_branch () const
{
  Branch res;
  _reference->get_root_branch ( res );
  return res;
}

bool
Config_Tree::get_branch ( Branch & branch_n,
                          const Path & path_n,
                          Error * error_n ) const
{
  return _reference->get_branch ( branch_n, path_n, error_n );
}

bool
Config_Tree::get_branch ( Branch & branch_n,
                          const QString & address_n,
                          Error * error_n ) const
{
  return _reference->get_branch (
      branch_n, Path ( address_n, error_n ), error_n );
}

Config_Tree::Branch
Config_Tree::branch ( const Path & path_n, Error * error_n ) const
{
  Branch res;
  _reference->get_branch ( res, path_n, error_n );
  return res;
}

Config_Tree::Branch
Config_Tree::branch ( const QString & address_n, Error * error_n ) const
{
  Branch res;
  _reference->get_branch ( res, Path ( address_n, error_n ), error_n );
  return res;
}

/// @brief Finds an existing child node and removes it recursively.
/// @return True if the node did exist and was removed
bool
Config_Tree::remove ( const Path & path_n, Error * error_n )
{
  return _reference->remove_node ( path_n, error_n );
}

/// @brief Finds an existing child node and removes it recursively.
/// @return True if the node did exist and was removed
bool
Config_Tree::remove ( const QString & address_n, Error * error_n )
{
  return _reference->remove_node ( Path ( address_n, error_n ), error_n );
}

/// @brief Inserts a copy of a remote branch
/// @arg branch_n Remote source branch
void
Config_Tree::insert ( const Branch & branch_n )
{
  _reference->insert_at ( Path (), branch_n, nullptr );
}

/// @brief Inserts a copy of a remote branch into a local branch
/// @arg address_n Address of the local branch
/// @arg branch_n Remote source branch
/// @arg error_n Path error of address_n
void
Config_Tree::insert_at ( const Path & path_n,
                         const Branch & branch_n,
                         Error * error_n )
{
  _reference->insert_at ( path_n, branch_n, error_n );
}

/// @brief Clears and sets to a copy of a remote branch
/// @arg branch_n Remote source branch
/// @see operator= ( const Config_Tree & config_n )
void
Config_Tree::clear_insert ( const Branch & branch_n )
{
  _reference->clear_and_insert ( branch_n );
}

bool
Config_Tree::json_import_file ( const QUrl & filename_n )
{
  bool success = filename_n.isLocalFile ();
  if ( success ) {
    QByteArray content;
    {
      QFile file ( filename_n.path () );
      success = file.open ( QIODevice::ReadOnly );
      if ( success ) {
        content = file.readAll ();
      }
    }
    if ( success ) {
      Branch branch;
      {
        string_tree::json_import::Importer importer;
        success = importer.import ( branch, content );
        content.clear ();
      }
      if ( success ) {
        insert ( branch );
      }
    }
  }
  return success;
}

bool
Config_Tree::json_export_file ( const QUrl & filename_n )
{
  bool success = filename_n.isLocalFile ();
  if ( success ) {
    QDir dir = QFileInfo ( filename_n.path () ).dir ();
    success = dir.mkpath ( dir.path () );
  }
  if ( success ) {
    QString content;
    {
      string_tree::json_export::Exporter exporter;
      content = exporter.serialize ( root_branch () );
    }
    {
      QSaveFile file ( filename_n.path () );
      if ( file.open ( QIODevice::WriteOnly | QIODevice::Truncate ) ) {
        file.write ( content.toUtf8 () );
        success = file.commit ();
      } else {
        success = false;
      }
    }
  }
  return success;
}
} // namespace config_tree