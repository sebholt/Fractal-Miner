/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

namespace config_tree
{

// -- Forward declaration
namespace priv
{
class Root;
}

/// @brief Reference to a shared configuration
///
/// The last destroyed reference destroys the shared configuration
class Reference
{
  public:
  // -- Types
  typedef priv::Root Root;

  public:
  // -- Constructors

  /// @brief Makes this another reference into the given configuration
  Reference ( Root * root_n );

  /// @brief Makes this another reference into the same configuration
  Reference ( const Reference & ref_n );

  /// @brief Takes over the reference and leaves it in an undefined but
  ///        destructible state
  Reference ( Reference && ref_n );

  ~Reference ();

  // -- Root reference

  bool
  is_valid () const
  {
    return ( _root != nullptr );
  }

  Root *
  root () const
  {
    return _root;
  }

  void
  clear ();

  /// @brief Drops old and sets new root reference
  void
  reset ( Root * root_n );

  /// @brief Takes over the remote root reference
  void
  move_assign ( Reference && ref_n );

  // -- Type cast operators

  operator Root * () const { return _root; }

  Root *
  operator-> () const
  {
    return _root;
  }

  // -- Comparison operators

  bool
  operator== ( const Reference & ref_n ) const
  {
    return ( _root == ref_n._root );
  }

  bool
  operator!= ( const Reference & ref_n ) const
  {
    return ( _root != ref_n._root );
  }

  bool
  operator== ( Root * root_n ) const
  {
    return ( _root == root_n );
  }

  bool
  operator!= ( Root * root_n ) const
  {
    return ( _root != root_n );
  }

  // -- Assign operators

  /// @brief Makes this another reference into the same shared configuration
  Reference &
  operator= ( const Reference & ref_n )
  {
    reset ( ref_n._root );
    return *this;
  }

  /// @brief Takes over the reference and leaves it in an undefined but
  ///        destructible state
  Reference &
  operator= ( Reference && ref_n )
  {
    move_assign ( static_cast< Reference && > ( ref_n ) );
    return *this;
  }

  private:
  static void
  root_unref ( Root * root_n );

  private:
  Root * _root;
};
} // namespace config_tree
