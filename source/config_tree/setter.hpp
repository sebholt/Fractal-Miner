/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <config_tree/error.hpp>
#include <config_tree/reference.hpp>
#include <QObject>
#include <QString>
#include <functional>
#include <utility>

namespace config_tree
{

// -- Forward declaration
class Config_Tree;

/// @brief Configuration value setter
///
class Setter : public QObject
{
  Q_OBJECT
  Q_PROPERTY ( config_tree::Config_Tree * config READ config WRITE set_config
                   NOTIFY config_changed )
  Q_PROPERTY (
      QString address READ address WRITE set_address NOTIFY address_changed )

  public:
  // -- Constructors

  /// @brief Default constructor
  Setter ( QObject * parent_n = nullptr );

  /// @brief Constructor
  Setter ( Config_Tree * config_n,
           const QString & address_n,
           QObject * parent_n = nullptr );

  ~Setter ();

  // -- General

  /// @brief Unwatches and clears the address
  Q_SLOT
  void
  clear ();

  /// @brief set_config() and set_address() in one method.
  void
  setup ( Config_Tree * config_n, const QString & address_n );

  // -- Configuration

  bool
  config_is_valid ()
  {
    return ( _config != nullptr );
  }

  /// @brief Starts watching config_ref_n at address().
  Config_Tree *
  config () const
  {
    return _config;
  }

  /// @brief Starts watching config_n at address().
  Q_SLOT
  void
  set_config ( Config_Tree * config_n );

  /// @brief Stops watching and clears value(). address() is unchanged.
  ///
  /// @see clear();
  Q_SLOT
  void
  clear_config ();

  /// @brief Gets called when config() changed
  Q_SIGNAL
  void
  config_changed ();

  // -- Address

  const QString &
  address () const
  {
    return _address;
  }

  Q_SLOT
  void
  set_address ( const QString & address_n );

  Q_SLOT
  void
  clear_address ();

  /// @brief Gets called when address() changed
  Q_SIGNAL
  void
  address_changed ();

  // -- Value

  /// @brief Reads the value from the configuration tree at address()
  ///
  /// @return The value
  Q_INVOKABLE
  QString
  get () const;

  /// @brief Reads the value from the configuration tree at address()
  ///
  /// @return The value
  QString
  get ( Error * error_n ) const;

  /// @brief Sets the value in the configuration tree at address()
  ///
  /// @return True if the value changed
  Q_SLOT
  bool
  set ( const QString & value_n ) const;

  /// @brief Sets the value in the configuration tree at address()
  ///
  /// @return True if the value changed
  bool
  set ( const QString & value_n, Error * error_n ) const;

  // -- Value updater function

  /// @brief Updater used by the update() slot to set() the configuration value
  const std::function< QString ( void ) > &
  updater () const
  {
    return _updater;
  }

  void
  set_updater ( const std::function< QString ( void ) > & updater_n )
  {
    _updater = updater_n;
  }

  void
  set_updater ( std::function< QString ( void ) > && updater_n )
  {
    _updater = std::move ( updater_n );
  }

  /// @brief Calls the updater() object to set() the configuration value
  Q_SLOT
  void
  update () const;

  private:
  // -- Utility

  void
  clear_config_silent ();

  void
  forward_value_change ( bool config_changed_n, bool address_changed_n );

  private:
  QString _address;
  Reference _reference;
  Config_Tree * _config;
  std::function< QString ( void ) > _updater;
};
} // namespace config_tree
