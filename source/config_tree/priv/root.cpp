/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <config_tree/priv/root.hpp>
#include <QJsonObject>
#include <algorithm>
#include <stack>

namespace config_tree::priv
{

// -- Utility

typedef std::lock_guard< std::mutex > Lock_Guard;

static inline void
error_set_type ( Error * error_n, Error::Type type_n )
{
  if ( error_n != nullptr ) {
    error_n->set_type ( type_n );
  }
}

// -- Class methods

Root::Root () {}

Root::~Root ()
{
  {
    Lock_Guard glock ( mutex () );
    _branch.clear ();
  }
}

void
Root::clear ()
{
  // Value watcher list
  Watcher_Unique_List watchers;
  // Lock and delete all children
  {
    Lock_Guard glock ( mutex () );
    delete_children_recursive ( &_branch, watchers );
    // Notify value watchers
    watchers_notify ( watchers );
  }
}

QStringList
Root::get_keys ( const Path & path_n, Error * error_n ) const
{
  QStringList res;
  if ( path_n ) {
    Lock_Guard glock ( mutex () );
    const Branch * branch ( _branch.find_branch ( path_n, error_n ) );
    if ( branch != nullptr ) {
      res = branch->children ().keys ();
    }
  } else {
    error_set_type ( error_n, Error::Type::ADDRESS_INVALID );
  }
  return res;
}

QStringList
Root::get_branch_keys ( const Path & path_n, Error * error_n ) const
{
  QStringList res;
  if ( path_n ) {
    Lock_Guard glock ( mutex () );
    const Branch * branch ( _branch.find_branch ( path_n, error_n ) );
    if ( branch != nullptr ) {
      branch->get_branch_keys ( res );
    }
  } else {
    error_set_type ( error_n, Error::Type::ADDRESS_INVALID );
  }
  return res;
}

QStringList
Root::get_leaf_keys ( const Path & path_n, Error * error_n ) const
{
  QStringList res;
  if ( path_n ) {
    Lock_Guard glock ( mutex () );
    const Branch * branch ( _branch.find_branch ( path_n, error_n ) );
    if ( branch != nullptr ) {
      branch->get_leaf_keys ( res );
    }
  } else {
    error_set_type ( error_n, Error::Type::ADDRESS_INVALID );
  }
  return res;
}

bool
Root::get_split_keys ( QStringList & branch_keys_n,
                       QStringList & leaf_keys_n,
                       const Path & path_n,
                       Error * error_n ) const
{
  bool res ( false );
  if ( path_n ) {
    Lock_Guard glock ( mutex () );
    const Branch * branch ( _branch.find_branch ( path_n, error_n ) );
    if ( branch != nullptr ) {
      branch->get_split_keys ( branch_keys_n, leaf_keys_n );
      res = true;
    }
  } else {
    error_set_type ( error_n, Error::Type::ADDRESS_INVALID );
  }
  return res;
}

inline void
Root::get_value_unlocked ( QString & value_n,
                           const Path & path_n,
                           Error * error_n ) const
{
  const Leaf * leaf ( _branch.find_sub_leaf ( path_n, error_n ) );
  if ( leaf != nullptr ) {
    value_n = leaf->value ();
  }
}

QString
Root::value ( const Path & path_n, Error * error_n ) const
{
  QString res;
  if ( path_n ) {
    Lock_Guard glock ( mutex () );
    get_value_unlocked ( res, path_n, error_n );
  } else {
    error_set_type ( error_n, Error::Type::ADDRESS_INVALID );
  }
  return res;
}

bool
Root::set_value ( const Path & path_n,
                  const QString & value_n,
                  Error * error_n )
{
  bool changed ( false );
  if ( path_n ) {
    // Value watcher list
    Watcher_Unique_List watchers;
    // Lock and manipulate
    {
      Lock_Guard glock ( mutex () );
      Leaf * leaf ( acquire_leaf ( path_n, watchers, error_n ) );
      if ( leaf != nullptr ) {
        if ( leaf_set_value ( leaf, value_n, watchers ) ) {
          changed = true;
        }
      }
      // Notify value watchers
      watchers_notify ( watchers );
    }
  } else {
    error_set_type ( error_n, Error::Type::ADDRESS_INVALID );
  }
  return changed;
}

void
Root::get_root_branch ( Branch & branch_n ) const
{
  Lock_Guard glock ( mutex () );
  branch_n = _branch;
}

bool
Root::get_branch ( Branch & branch_n,
                   const Path & path_n,
                   Error * error_n ) const
{
  bool success ( false );
  if ( path_n ) {
    Lock_Guard glock ( mutex () );
    const Branch * branch ( _branch.find_branch ( path_n, error_n ) );
    if ( branch != nullptr ) {
      branch_n = *branch;
      success = true;
    }
  } else {
    error_set_type ( error_n, Error::Type::ADDRESS_INVALID );
  }
  return success;
}

bool
Root::remove_node ( const Path & path_n, Error * error_n )
{
  bool removed ( false );
  // Value watcher list
  Watcher_Unique_List watchers;
  // Lock and manipulate
  {
    Lock_Guard glock ( mutex () );
    Node * node ( _branch.find_sub ( path_n, error_n ) );
    if ( node != nullptr ) {
      delete_node_recursive ( node, watchers );
      removed = true;
    }
    // Notify value watchers
    watchers_notify ( watchers );
  }
  return removed;
}

void
Root::insert_at ( const Path & path_n,
                  const Branch & branch_n,
                  Error * error_n )
{
  // Value watcher list
  if ( path_n ) {
    Watcher_Unique_List watchers;
    // Lock and copy
    {
      Lock_Guard glock ( mutex () );
      Branch * branch ( acquire_branch ( path_n, watchers, error_n ) );
      if ( branch != nullptr ) {
        insert_branch_recursive ( branch, &branch_n, watchers );
      }
      // Notify value watchers
      watchers_notify ( watchers );
    }
  } else {
    error_set_type ( error_n, Error::Type::ADDRESS_INVALID );
  }
}

void
Root::clear_and_insert ( const Branch & branch_n )
{
  // Value watcher list
  Watcher_Unique_List watchers;
  // Lock and manipulate
  {
    Lock_Guard glock ( mutex () );
    // Clear root branch
    delete_children_recursive ( &_branch, watchers );
    insert_branch_recursive ( &_branch, &branch_n, watchers );
    // Notify value watchers
    watchers_notify ( watchers );
  }
}

Branch *
Root::acquire_branch ( const Path & path_n,
                       Watcher_Unique_List & watchers_n,
                       Error * error_n )
{
  Branch * branch ( &_branch );
  if ( !path_n.empty () ) {
    // Path iterator
    Path::const_iterator pit ( path_n.begin () );
    // Walk over existing nodes
    for ( ; pit != path_n.end (); ++pit ) {
      Node * node ( branch->child ( *pit ) );
      if ( node != nullptr ) {
        // Child with matching key found
        if ( node->is_branch () ) {
          // Continue with this branch
          branch = &node->as_branch ();
        } else {
          // Node not a branch. Abort.
          error_set_type ( error_n, Error::Type::NODE_NOT_BRANCH );
          branch = nullptr;
          break;
        }
      } else {
        // No child branch with matching key.
        break;
      }
    }
    // Generate new nodes
    if ( branch != nullptr ) {
      for ( ; pit != path_n.end (); ++pit ) {
        // Generate new branch
        branch = branch_new_child_branch ( branch, *pit, watchers_n );
      }
    }
  }
  return branch;
}

Leaf *
Root::acquire_leaf ( const Path & path_n,
                     Watcher_Unique_List & watchers_n,
                     Error * error_n )
{
  Leaf * leaf ( nullptr );
  if ( path_n.empty () ) {
    error_set_type ( error_n, Error::Type::NODE_NOT_LEAF );
  } else {
    Branch * branch ( &_branch );
    // Path iterator
    Path::const_iterator pit ( path_n.begin () );
    Path::const_iterator pit_last ( pit + ( path_n.size () - 1 ) );

    // Walk over existing branches
    for ( ; pit != pit_last; ++pit ) {
      Node * node ( branch->child ( *pit ) );
      if ( node != nullptr ) {
        if ( node->is_branch () ) {
          // Continue with this branch
          branch = &node->as_branch ();
        } else {
          // Node not a branch. Error abort.
          error_set_type ( error_n, Error::Type::NODE_NOT_BRANCH );
          branch = nullptr;
          break;
        }
      } else {
        break;
      }
    }
    if ( branch != nullptr ) {
      // Generate missing branches
      for ( ; pit != pit_last; ++pit ) {
        // Generate new branch
        branch = branch_new_child_branch ( branch, *pit, watchers_n );
      }
      // Take or create leaf
      {
        Node * node ( branch->child ( *pit ) );
        if ( node != nullptr ) {
          // Final child with matching key found
          if ( node->is_leaf () ) {
            // Finish: Use existing child leaf
            leaf = &node->as_leaf ();
          } else {
            // Node is not a leaf. Error abort.
            error_set_type ( error_n, Error::Type::NODE_NOT_LEAF );
          }
        } else {
          // Leaf does not exist
          leaf = branch_new_child_leaf ( branch, *pit, watchers_n );
        }
      }
    }
  }
  return leaf;
}

void
Root::insert_branch_recursive ( Branch * branch_n,
                                const Branch * branch_ext_n,
                                Watcher_Unique_List & watchers_n )
{
  // Stack item struct
  struct Stack_Item
  {
    Stack_Item ( Branch * blocal, const Branch * bext )
    : local ( blocal )
    , ext_cit ( bext->children ().cbegin () )
    , ext_end ( bext->children ().cend () )
    {
    }

    Branch * local;
    Branch::Children::const_iterator ext_cit;
    const Branch::Children::const_iterator ext_end;
  };

  // Insert tracking stack
  std::stack< Stack_Item > stack;

  // Push root branches to stack
  stack.emplace ( branch_n, branch_ext_n );
  while ( !stack.empty () ) {
    Stack_Item & sitem ( stack.top () );
    if ( sitem.ext_cit == sitem.ext_end ) {
      stack.pop ();
    } else {
      // Local iterator data copies
      Branch * branch_own ( sitem.local );
      const QString name_ext ( sitem.ext_cit.key () );
      const Node * child_ext ( sitem.ext_cit.value () );
      // Increment iterator before manipulating the stack
      ++sitem.ext_cit;
      // Find existing child node
      Node * child_own ( branch_own->child ( name_ext ) );
      if ( child_own != nullptr ) {
        // Named child exists already
        if ( child_own->type () == child_ext->type () ) {
          switch ( child_ext->type () ) {
          case Node::Type::BRANCH:
            // Push branches to stack
            stack.emplace ( &child_own->as_branch (),
                            &child_ext->as_branch () );
            break;
          case Node::Type::LEAF:
            // Update existing leaf value
            leaf_set_value ( &child_own->as_leaf (),
                             child_ext->as_leaf ().value (),
                             watchers_n );
            break;
          }
        } else {
          // Type missmatch
        }
      } else {
        // Named child does not exist
        switch ( child_ext->type () ) {
        case Node::Type::BRANCH: {
          // Generate new branch
          Branch * branch_new (
              branch_new_child_branch ( branch_own, name_ext, watchers_n ) );
          // Push branches to stack
          stack.emplace ( branch_new, &child_ext->as_branch () );
        } break;
        case Node::Type::LEAF: {
          // Generate new leaf
          Leaf * leaf_new (
              branch_new_child_leaf ( branch_own, name_ext, watchers_n ) );
          // Se leaf value
          leaf_set_value (
              leaf_new, child_ext->as_leaf ().value (), watchers_n );
        } break;
        }
      }
    }
  }
}

inline bool
Root::leaf_set_value ( Leaf * leaf_n,
                       const QString & value_n,
                       Watcher_Unique_List & watchers_n )
{
  if ( leaf_n->set_value_testing ( value_n ) ) {
    _tracker.acquire_leaf_watchers ( watchers_n, leaf_n );
    return true;
  }
  return false;
}

Leaf *
Root::branch_new_child_leaf ( Branch * branch_n,
                              const QString & key_n,
                              Watcher_Unique_List & watchers_n )
{
  Leaf * leaf ( branch_n->new_child_leaf ( key_n ) );
  // Tracking
  _tracker.acquire_branch_watchers ( watchers_n, branch_n );
  return leaf;
}

Branch *
Root::branch_new_child_branch ( Branch * branch_n,
                                const QString & key_n,
                                Watcher_Unique_List & watchers_n )
{
  Branch * branch ( branch_n->new_child_branch ( key_n ) );
  // Tracking
  _tracker.acquire_branch_watchers ( watchers_n, branch_n );
  return branch;
}

Leaf *
Root::branch_acquire_child_leaf ( Branch * branch_n,
                                  const QString & key_n,
                                  Watcher_Unique_List & watchers_n )
{
  Node * node ( branch_n->child ( key_n ) );
  if ( ( node != nullptr ) && ( node->is_leaf () ) ) {
    return &node->as_leaf ();
  }
  return branch_new_child_leaf ( branch_n, key_n, watchers_n );
}

Branch *
Root::branch_acquire_child_branch ( Branch * branch_n,
                                    const QString & key_n,
                                    Watcher_Unique_List & watchers_n )
{
  Node * node ( branch_n->child ( key_n ) );
  if ( ( node != nullptr ) && ( node->is_branch () ) ) {
    return &node->as_branch ();
  }
  return branch_new_child_branch ( branch_n, key_n, watchers_n );
}

void
Root::branch_delete_child_branch ( Branch * branch_n,
                                   Branch * child_n,
                                   Watcher_Unique_List & watchers_n )
{
  if ( branch_n != nullptr ) {
    // Branch tracking
    _tracker.acquire_branch_watchers ( watchers_n, branch_n );
    // Drop child
    branch_n->remove_child ( child_n->key () );
  }
}

void
Root::branch_delete_child_leaf ( Branch * branch_n,
                                 Leaf * child_n,
                                 Watcher_Unique_List & watchers_n )
{
  if ( branch_n != nullptr ) {
    // Leaf tracking
    if ( !child_n->value ().isEmpty () ) {
      _tracker.acquire_leaf_watchers ( watchers_n, child_n );
    }
    // Branch tracking
    _tracker.acquire_branch_watchers ( watchers_n, branch_n );
    // Drop child
    branch_n->remove_child ( child_n->key () );
  }
}

void
Root::delete_node_recursive ( Node * node_n, Watcher_Unique_List & watchers_n )
{
  if ( node_n != nullptr ) {
    Branch * parent ( node_n->parent () );
    // Delete recursive uppwards from node
    switch ( node_n->type () ) {
    case Node::Type::BRANCH:
      delete_branch_recursive ( &node_n->as_branch (), watchers_n );
      break;
    case Node::Type::LEAF:
      branch_delete_child_leaf ( parent, &node_n->as_leaf (), watchers_n );
      break;
    }
  }
}

void
Root::delete_branch_recursive ( Branch * branch_n,
                                Watcher_Unique_List & watchers_n )
{
  std::stack< Branch * > stack;
  stack.push ( branch_n );

  while ( !stack.empty () ) {
    Branch * btop ( stack.top () );
    if ( btop->is_empty () ) {
      // Delete only empty branches
      stack.pop ();
      branch_delete_child_branch ( btop->parent (), btop, watchers_n );
      break;
    } else {
      // Delete the first child leaf immediately or push a branch
      Node * nfirst ( btop->children ().begin ().value () );
      switch ( nfirst->type () ) {
      case Node::Type::BRANCH:
        stack.push ( &nfirst->as_branch () );
        break;
      case Node::Type::LEAF:
        branch_delete_child_leaf (
            nfirst->parent (), &nfirst->as_leaf (), watchers_n );
        break;
      }
    }
  }
}

void
Root::delete_children_recursive ( Branch * branch_n,
                                  Watcher_Unique_List & watchers_n )
{
  // Delete all children
  const Branch::Children & chi ( branch_n->children () );
  while ( !chi.empty () ) {
    delete_node_recursive ( chi.begin ().value (), watchers_n );
  }
}

void
Root::watcher_register ( QString & address_shared_n,
                         QString & value_shared_n,
                         const QString & address_n,
                         const Watcher * watcher_n )
{
  const Path spath ( address_n );
  {
    Lock_Guard glock ( mutex () );
    // Register relay
    _tracker.leaf_watcher_register (
        this, address_shared_n, address_n, watcher_n );
    // Update value
    value_shared_n.clear ();
    get_value_unlocked ( value_shared_n, spath );
  }
}

void
Root::watcher_unregister ( const QString & address_n,
                           const Watcher * watcher_n )
{
  {
    Lock_Guard glock ( mutex () );
    _tracker.leaf_watcher_unregister ( address_n, watcher_n );
  }
}

void
Root::watcher_change_address ( QString & address_shared_n,
                               QString & value_shared_n,
                               const QString & address_old_n,
                               const QString & address_new_n,
                               const Watcher * watcher_n )
{
  const Path spath_old ( address_old_n );
  const Path spath_new ( address_new_n );
  {
    Lock_Guard glock ( mutex () );
    // Register relay
    _tracker.leaf_watcher_unregister ( address_old_n, watcher_n );
    _tracker.leaf_watcher_register (
        this, address_shared_n, address_new_n, watcher_n );
    // Update value
    value_shared_n.clear ();
    get_value_unlocked ( value_shared_n, spath_new );
  }
}
} // namespace config_tree::priv
