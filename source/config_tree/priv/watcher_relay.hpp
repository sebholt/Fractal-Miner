/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <config_tree/priv/watcher_list.hpp>
#include <config_tree/reference.hpp>
#include <QAtomicInteger>

namespace config_tree::priv
{

// -- Forward declaration
class Watcher_Relay_Ref;
class Tracker;

/// @brief Configuration tree node
///
class Watcher_Relay
{
  // Public types
  public:
  friend class config_tree::priv::Watcher_Relay_Ref;
  friend class config_tree::priv::Tracker;

  // Public methods
  public:
  // -- Constructors

  /// @brief Default constructor
  Watcher_Relay ( Root * root_n, const QString & address_n );

  Watcher_Relay ( const Watcher_Relay & relay_n ) = delete;

  Watcher_Relay ( Watcher_Relay && relay_n ) = delete;

  ~Watcher_Relay ();

  // -- Accessors

  const Reference &
  config_ref () const
  {
    return _config_ref;
  }

  const QString &
  address ()
  {
    return _address;
  }

  // -- Watcher list

  bool
  is_empty () const
  {
    return _watchers.isEmpty ();
  }

  const Watcher_List &
  watchers () const
  {
    return _watchers;
  }

  // Private methods
  private:
  void
  ref_count_increment ()
  {
    _ref_count.ref ();
  }

  /// @return False if this was the last reference
  bool
  ref_count_decrement ()
  {
    return _ref_count.deref ();
  }

  void
  add_watcher ( const Watcher * watcher_n )
  {
    _watchers.append ( watcher_n );
  }

  void
  remove_watcher ( const Watcher * watcher_n )
  {
    _watchers.removeOne ( watcher_n );
  }

  // Private attributes
  private:
  QAtomicInteger< uintptr_t > _ref_count;
  Reference _config_ref;
  const QString _address;
  Watcher_List _watchers;
};
} // namespace config_tree::priv
