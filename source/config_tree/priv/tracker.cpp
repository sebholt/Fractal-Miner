/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <config_tree/priv/tracker.hpp>
#include <config_tree/priv/watcher_relay.hpp>

namespace config_tree::priv
{

Tracker::Tracker () {}

Tracker::~Tracker () {}

void
Tracker::leaf_watcher_register ( Root * root_n,
                                 QString & address_shared_n,
                                 const QString & address_n,
                                 const Watcher * watcher_n )
{
  watcher_register (
      root_n, _relays_leaf, address_shared_n, address_n, watcher_n );
}

void
Tracker::leaf_watcher_unregister ( const QString & address_n,
                                   const Watcher * watcher_n )
{
  watcher_unregister ( _relays_leaf, address_n, watcher_n );
}

void
Tracker::branch_watcher_register ( Root * root_n,
                                   QString & address_shared_n,
                                   const QString & address_n,
                                   const Watcher * watcher_n )
{
  watcher_register (
      root_n, _relays_branch, address_shared_n, address_n, watcher_n );
}

void
Tracker::branch_watcher_unregister ( const QString & address_n,
                                     const Watcher * watcher_n )
{
  watcher_unregister ( _relays_branch, address_n, watcher_n );
}

void
Tracker::watcher_register ( Root * root_n,
                            Relay_Map & map_n,
                            QString & address_shared_n,
                            const QString & address_n,
                            const Watcher * watcher_n )
{
  Relay_Map::iterator it ( map_n.find ( address_n ) );
  if ( it == map_n.end () ) {
    it = map_n.insert ( address_n, new Watcher_Relay ( root_n, address_n ) );
  }
  if ( it != map_n.end () ) {
    Watcher_Relay * relay ( it.value () );
    relay->add_watcher ( watcher_n );
    // Update
    address_shared_n = relay->address ();
  } else {
    address_shared_n.clear ();
  }
}

void
Tracker::watcher_unregister ( Relay_Map & map_n,
                              const QString & address_n,
                              const Watcher * watcher_n )
{
  Relay_Map::iterator it ( map_n.find ( address_n ) );
  if ( it != map_n.end () ) {
    Watcher_Relay * relay ( it.value () );
    relay->remove_watcher ( watcher_n );
    if ( relay->is_empty () ) {
      map_n.erase ( it );
    }
  }
}

void
Tracker::acquire_leaf_watchers ( Watcher_Unique_List & watchers_n,
                                 const Node * node_n ) const
{
  acquire_watchers ( watchers_n, _relays_leaf, node_n );
}

void
Tracker::acquire_branch_watchers ( Watcher_Unique_List & watchers_n,
                                   const Node * node_n ) const
{
  acquire_watchers ( watchers_n, _relays_branch, node_n );
}

void
Tracker::acquire_watchers ( Watcher_Unique_List & watchers_n,
                            const Relay_Map & map_n,
                            const Node * node_n )
{
  Relay_Map::const_iterator it ( map_n.find ( node_n->acquire_address () ) );
  if ( it != map_n.end () ) {
    const Watcher_Relay * relay ( it.value () );
    watchers_n.append ( relay->watchers () );
  }
}
} // namespace config_tree::priv
