/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <config_tree/path.hpp>
#include <config_tree/priv/branch.hpp>
#include <config_tree/priv/leaf.hpp>
#include <config_tree/priv/tracker.hpp>
#include <config_tree/priv/watcher_list.hpp>
#include <QAtomicInteger>
#include <mutex>

// -- Forward declaration
namespace config_tree
{
class Reference;
}

namespace config_tree::priv
{

/// @brief Root
///
class Root
{
  public:
  // -- Types

  friend class config_tree::Reference;
  friend class Watcher_Relay;

  public:
  // -- Constructors

  /// Default constructor
  Root ();

  /// @brief Copy constructor
  Root ( const Root & root_n ) = delete;

  /// @brief Destructor
  ~Root ();

  // -- Accessors

  /// @brief Mutex accessor - const casted
  std::mutex &
  mutex () const
  {
    return _mutex;
  }

  /// @brief Usafe branch access
  const Branch &
  branch () const
  {
    return _branch;
  }

  // -- Status

  /// @brief Deletes all child nodes
  void
  clear ();

  bool
  is_empty () const
  {
    return _branch.is_empty ();
  }

  // -- Branch keys

  /// @return List of keys in the addressed branch
  QStringList
  get_keys ( const Path & path_n, Error * error_n = nullptr ) const;

  /// @return List of branch keys in the addressed branch
  QStringList
  get_branch_keys ( const Path & path_n, Error * error_n = nullptr ) const;

  /// @return List of leaf keys in the addressed branch
  QStringList
  get_leaf_keys ( const Path & path_n, Error * error_n = nullptr ) const;

  /// @return True if the address was valid and the branch existed
  bool
  get_split_keys ( QStringList & branch_keys_n,
                   QStringList & leaf_keys_n,
                   const Path & path_n,
                   Error * error_n = nullptr ) const;

  // -- Get / set value

  QString
  value ( const Path & path_n, Error * error_n = nullptr ) const;

  /// @return True if the value was changed
  bool
  set_value ( const Path & path_n,
              const QString & value_n,
              Error * error_n = nullptr );

  // -- Get / set branch

  void
  get_root_branch ( Branch & branch_n ) const;

  bool
  get_branch ( Branch & branch_n,
               const Path & path_n,
               Error * error_n = nullptr ) const;

  // -- Remove nodes recursive

  /// @brief Finds an existing child node and removes it recursively.
  /// @return True if the node did exist and was removed
  bool
  remove_node ( const Path & path_n, Error * error_n = nullptr );

  // -- Insert nodes recursive

  /// @brief Clears an insert a copy root_n into the root branch
  void
  clear_and_insert ( const Branch & branch_n );

  /// @brief Insert a copy of root_n into the branch at address_n
  /// @arg address_n Address of the destination branch
  void
  insert_at ( const Path & path_n,
              const Branch & branch_n,
              Error * error_n = nullptr );

  // -- Watchers

  void
  watcher_register ( QString & address_shared_n,
                     QString & value_shared_n,
                     const QString & address_n,
                     const Watcher * watcher_n );

  void
  watcher_unregister ( const QString & address_n, const Watcher * watcher_n );

  void
  watcher_change_address ( QString & address_shared_n,
                           QString & value_shared_n,
                           const QString & address_old_n,
                           const QString & address_new_n,
                           const Watcher * watcher_n );

  private:
  // -- Reference count

  void
  ref_count_increment ()
  {
    _reference_count.ref ();
  }

  bool
  ref_count_decrement ()
  {
    return _reference_count.deref ();
  }

  // -- Unsafe access

  /// @brief Does not set a default value
  void
  get_value_unlocked ( QString & value_n,
                       const Path & path_n,
                       Error * error_n = nullptr ) const;

  // -- Recursive node generation

  /// @brief Finds an existing branch node or creates a new one. May return
  /// this.
  /// @return Non nullptr on success
  Branch *
  acquire_branch ( const Path & path_n,
                   Watcher_Unique_List & watchers_n,
                   Error * error_n = nullptr );

  Leaf *
  acquire_leaf ( const Path & path_n,
                 Watcher_Unique_List & watchers_n,
                 Error * error_n = nullptr );

  void
  insert_branch_recursive ( Branch * branch_n,
                            const Branch * branch_ext_n,
                            Watcher_Unique_List & watchers_n );

  // -- Leaf manipulation

  /// @return True if the value changed
  bool
  leaf_set_value ( Leaf * leaf_n,
                   const QString & value_n,
                   Watcher_Unique_List & watchers_n );

  // -- Branch children new / delete

  Leaf *
  branch_new_child_leaf ( Branch * branch_n,
                          const QString & key_n,
                          Watcher_Unique_List & watchers_n );

  Branch *
  branch_new_child_branch ( Branch * branch_n,
                            const QString & key_n,
                            Watcher_Unique_List & watchers_n );

  /// @brief Find an existing leaf or generate a new one
  Leaf *
  branch_acquire_child_leaf ( Branch * branch_n,
                              const QString & key_n,
                              Watcher_Unique_List & watchers_n );

  /// @brief Find an existing leaf or generate a new one
  Branch *
  branch_acquire_child_branch ( Branch * branch_n,
                                const QString & key_n,
                                Watcher_Unique_List & watchers_n );

  void
  branch_delete_child_branch ( Branch * branch_n,
                               Branch * child_n,
                               Watcher_Unique_List & watchers_n );

  void
  branch_delete_child_leaf ( Branch * branch_n,
                             Leaf * child_n,
                             Watcher_Unique_List & watchers_n );

  // -- Delete child nodes

  void
  delete_node_recursive ( Node * node_n, Watcher_Unique_List & watchers_n );

  void
  delete_branch_recursive ( Branch * branch_n,
                            Watcher_Unique_List & watchers_n );

  void
  delete_children_recursive ( Branch * branch_n,
                              Watcher_Unique_List & watchers_n );

  private:
  QAtomicInteger< uintptr_t > _reference_count;
  mutable std::mutex _mutex;
  Branch _branch;
  config_tree::priv::Tracker _tracker;
};
} // namespace config_tree::priv
