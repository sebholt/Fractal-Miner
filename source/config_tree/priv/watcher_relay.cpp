/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <config_tree/priv/watcher_relay.hpp>

namespace config_tree::priv
{

Watcher_Relay::Watcher_Relay ( Root * root_n, const QString & address_n )
: _config_ref ( root_n )
, _address ( address_n )
{
}

Watcher_Relay::~Watcher_Relay () {}
} // namespace config_tree::priv
