/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <string_tree/node.hpp>

namespace config_tree::priv
{

using string_tree::Node;
}
