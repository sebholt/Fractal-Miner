/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <config_tree/priv/watcher_relay.hpp>
#include <config_tree/priv/watcher_relay_ref.hpp>

namespace config_tree::priv
{

Watcher_Relay_Ref::Watcher_Relay_Ref ( Watcher_Relay * relay_n )
: _relay ( relay_n )
{
  // This is another reference
  if ( _relay != nullptr ) {
    _relay->ref_count_increment ();
  }
}

Watcher_Relay_Ref::Watcher_Relay_Ref ( const Watcher_Relay_Ref & value_n )
: Watcher_Relay_Ref ( value_n._relay )
{
}

Watcher_Relay_Ref::Watcher_Relay_Ref ( Watcher_Relay_Ref && value_n )
: _relay ( value_n._relay )
{
  // We took over the source's reference
  if ( _relay != nullptr ) {
    value_n._relay = nullptr;
  }
}

Watcher_Relay_Ref::~Watcher_Relay_Ref ()
{
  unref_watcher ( _relay );
}

void
Watcher_Relay_Ref::reset ( Watcher_Relay * relay_n )
{
  if ( _relay != relay_n ) {
    Watcher_Relay * rold ( _relay );
    _relay = relay_n;
    if ( _relay != nullptr ) {
      _relay->ref_count_increment ();
    }
    unref_watcher ( rold );
  }
}

void
Watcher_Relay_Ref::clear ()
{
  if ( _relay != nullptr ) {
    Watcher_Relay * rold ( _relay );
    _relay = nullptr;
    unref_watcher ( rold );
  }
}

void
Watcher_Relay_Ref::unref_watcher ( Watcher_Relay * relay_n )
{
  if ( relay_n != nullptr ) {
    if ( !relay_n->ref_count_decrement () ) {
      delete relay_n;
    }
  }
}

Watcher_Relay_Ref &
Watcher_Relay_Ref::operator= ( Watcher_Relay_Ref && ref_n )
{
  // drop own reference
  reset ( nullptr );
  // take over the source's reference
  if ( ref_n._relay != nullptr ) {
    _relay = ref_n._relay;
    ref_n._relay = nullptr;
  }
  return *this;
}
} // namespace config_tree::priv
