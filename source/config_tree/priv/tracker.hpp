/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <config_tree/priv/node.hpp>
#include <config_tree/priv/watcher_list.hpp>
#include <config_tree/priv/watcher_relay_ref.hpp>
#include <QHash>
#include <QString>

// -- Forward declaration
namespace config_tree
{
class Watcher;
}

namespace config_tree::priv
{

// -- Forward declaration
class Root;

/// @brief Watcher tracker
///
class Tracker
{
  // Public types
  public:
  typedef QHash< QString, Watcher_Relay_Ref > Relay_Map;

  // Public methods
  public:
  // -- Constructors

  /// @brief Default constructor
  Tracker ();

  /// @brief Copy constructor
  Tracker ( const Tracker & tracker_n ) = delete;

  /// @brief Move constructor
  Tracker ( Tracker && tracker_n ) = delete;

  ~Tracker ();

  // -- Registration

  void
  leaf_watcher_register ( Root * root_n,
                          QString & address_shared_n,
                          const QString & address_n,
                          const Watcher * watcher_n );

  void
  leaf_watcher_unregister ( const QString & address_n,
                            const Watcher * watcher_n );

  void
  branch_watcher_register ( Root * root_n,
                            QString & address_shared_n,
                            const QString & address_n,
                            const Watcher * watcher_n );

  void
  branch_watcher_unregister ( const QString & address_n,
                              const Watcher * watcher_n );

  // -- List acquire

  void
  acquire_leaf_watchers ( Watcher_Unique_List & watchers_n,
                          const Node * node_n ) const;

  void
  acquire_branch_watchers ( Watcher_Unique_List & watchers_n,
                            const Node * node_n ) const;

  // -- Operators

  Tracker &
  operator= ( const Tracker & tracker_n ) = delete;

  Tracker &
  operator= ( Tracker && tracker_n ) = delete;

  // Private methods
  private:
  static void
  watcher_register ( Root * root_n,
                     Relay_Map & map_n,
                     QString & address_shared_n,
                     const QString & address_n,
                     const Watcher * watcher_n );

  static void
  watcher_unregister ( Relay_Map & map_n,
                       const QString & address_n,
                       const Watcher * watcher_n );

  static void
  acquire_watchers ( Watcher_Unique_List & watchers_n,
                     const Relay_Map & map_n,
                     const Node * node_n );

  // Private attributes
  private:
  Relay_Map _relays_leaf;
  Relay_Map _relays_branch;
};
} // namespace config_tree::priv
