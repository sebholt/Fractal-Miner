/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <config_tree/priv/watcher_list.hpp>
#include <config_tree/watcher.hpp>
#include <QCoreApplication>

namespace config_tree::priv
{

void
watchers_notify ( const Watcher_Unique_List & watchers_n )
{
  QCoreApplication * qapp ( QCoreApplication::instance () );
  if ( qapp != nullptr ) {
    for ( const config_tree::Watcher * watcher : watchers_n.list () ) {
      config_tree::Watcher * wat (
          const_cast< config_tree::Watcher * > ( watcher ) );
      if ( !wat->set_notified () ) {
        qapp->postEvent ( wat, new QEvent ( QEvent::User ) );
      }
    }
  }
}
} // namespace config_tree::priv
