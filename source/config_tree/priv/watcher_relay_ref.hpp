/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

namespace config_tree::priv
{

// -- Forward declaration
class Watcher_Relay;

/// @brief Reference to a shared configuration that is never invalid
///
/// The last destroyed reference destroys the shared configuration
///
class Watcher_Relay_Ref
{
  // Public methods
  public:
  // -- Constructors

  /// @brief Makes this another reference into the given configuration
  Watcher_Relay_Ref ( Watcher_Relay * relay_n );

  /// @brief Makes this another reference into the same configuration
  Watcher_Relay_Ref ( const Watcher_Relay_Ref & ref_n );

  /// @brief Takes over the reference and leaves it in an undefined but
  ///        destructible state
  Watcher_Relay_Ref ( Watcher_Relay_Ref && ref_n );

  ~Watcher_Relay_Ref ();

  // -- Accessors

  bool
  is_valid () const
  {
    return ( _relay != nullptr );
  }

  /// @brief Changes the relay reference
  void
  reset ( Watcher_Relay * relay_n );

  /// @brief Clears the relay reference
  void
  clear ();

  Watcher_Relay *
  relay () const
  {
    return _relay;
  }

  // -- Type cast operators

  operator Watcher_Relay * () const { return _relay; }

  Watcher_Relay *
  operator-> () const
  {
    return _relay;
  }

  // -- Comparison operators

  bool
  operator== ( const Watcher_Relay_Ref & ref_n ) const
  {
    return ( _relay == ref_n._relay );
  }

  bool
  operator!= ( const Watcher_Relay_Ref & ref_n ) const
  {
    return ( _relay != ref_n._relay );
  }

  bool
  operator== ( const Watcher_Relay * relay_n ) const
  {
    return ( _relay == relay_n );
  }

  bool
  operator!= ( const Watcher_Relay * relay_n ) const
  {
    return ( _relay != relay_n );
  }

  // -- Assign operators

  /// @brief Makes this another reference into the same shared configuration
  Watcher_Relay_Ref &
  operator= ( const Watcher_Relay_Ref & ref_n )
  {
    reset ( ref_n._relay );
    return *this;
  }

  /// @brief Takes over the reference and leaves it in an undefined but
  ///        destructible state
  Watcher_Relay_Ref &
  operator= ( Watcher_Relay_Ref && ref_n );

  // Private methods
  private:
  static void
  unref_watcher ( Watcher_Relay * relay_n );

  // Private attributes
  private:
  Watcher_Relay * _relay;
};
} // namespace config_tree::priv
