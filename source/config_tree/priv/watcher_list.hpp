/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <QList>

// -- Forward declaration
namespace config_tree
{
class Watcher;
}

namespace config_tree::priv
{

/// @brief Simple watcher list
typedef QList< const config_tree::Watcher * > Watcher_List;

/// @brief List that ensures entries are unique
///
class Watcher_Unique_List
{
  // Public methods
  public:
  Watcher_Unique_List (){};

  bool
  is_empty () const
  {
    return _list.isEmpty ();
  }

  std::size_t
  size () const
  {
    return _list.size ();
  }

  void
  append ( const config_tree::Watcher * watcher_n )
  {
    if ( !_list.contains ( watcher_n ) ) {
      _list.append ( watcher_n );
    }
  }

  void
  append ( const Watcher_List & list_n )
  {
    for ( const config_tree::Watcher * item : list_n ) {
      append ( item );
    }
  }

  void
  remove ( const config_tree::Watcher * watcher_n )
  {
    _list.removeOne ( watcher_n );
  }

  const Watcher_List &
  list () const
  {
    return _list;
  }

  // Private attributes
  private:
  Watcher_List _list;
};

/// @brief Notifies all watchers in the list
void
watchers_notify ( const Watcher_Unique_List & watchers_n );
} // namespace config_tree::priv
