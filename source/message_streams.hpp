/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <iostream>
#include <sstream>

class Debug_Stream : public std::ostringstream
{
  public:
  using std::ostringstream::ostringstream;

  ~Debug_Stream () { std::cout << str (); }
};

class Error_Stream : public std::ostringstream
{
  public:
  using std::ostringstream::ostringstream;

  ~Error_Stream () { std::cerr << str (); }
};
