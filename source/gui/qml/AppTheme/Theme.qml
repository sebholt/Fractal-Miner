import QtQuick 2.7
pragma Singleton

QtObject {
  property var sysPal: SystemPalette {
    colorGroup: SystemPalette.Active
  }
  property color headlineColor: sysPal.text
  property color textColor: sysPal.text
}
