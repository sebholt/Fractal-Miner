import QtQuick 2.7
import AppTheme 1.0

Text {
  font.bold: true;
  color: Theme.headlineColor;
}
