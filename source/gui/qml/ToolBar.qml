
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

ToolPanel {
  id: toolBar

  implicitWidth: layout.implicitWidth
  implicitHeight: layout.implicitHeight

  signal panelSelected ( string panel )

  ButtonGroup {
    id: radioGroup
  }

  ColumnLayout {
    id: layout
    anchors.fill: parent

    RowLayout {
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.topMargin: 2
      Layout.bottomMargin: 2
      Layout.leftMargin: 8
      Layout.rightMargin: 8
      spacing: 6

      ToolBarButton {
        id: colorPlayButton
        ButtonGroup.group: radioGroup
        text: "Play"
        display: AbstractButton.IconOnly
        icon.source: fractalView.colorSettings.colorCycling ? "qrc:///icons/stop.svg" : "qrc:///icons/play.svg"
        icon.color: "transparent"

        onClicked: {
          fractalView.colorSettings.colorCycling = !fractalView.colorSettings.colorCycling;
        }
      }

      Item { width: 10 }

      ToolBarButton {
        id: iterationsButton
        ButtonGroup.group: radioGroup
        text: "Iteration"
        display: AbstractButton.IconOnly
        icon.source: "qrc:///icons/iteration.svg"
        icon.color: "transparent"

        onClicked: { toolBar.panelSelected ( "iterationColors" ) }
      }

      ToolBarButton {
        id: renderButton
        ButtonGroup.group: radioGroup
        text: "Render"
        display: AbstractButton.IconOnly
        icon.source: "qrc:///icons/render.svg"
        icon.color: "transparent"

        onClicked: { toolBar.panelSelected ( "render" ) }
      }

      ToolBarButton {
        id: infoButton
        ButtonGroup.group: radioGroup
        text: "Info"
        display: AbstractButton.IconOnly
        icon.source: "qrc:///icons/info.svg"
        icon.color: "transparent"

        onClicked: { toolBar.panelSelected ( "info" ) }
      }
    }
  }
}
