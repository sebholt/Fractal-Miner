
import QtQuick 2.7
import QtQuick.Layouts 1.3
import AppTheme 1.0

Item {
  id: root

  implicitWidth: layout.implicitWidth
  implicitHeight: layout.implicitHeight

  ColumnLayout {
    id: layout
    anchors.fill: parent
    Headline { text: "OpenGL info" }

    GridLayout {
      id: grid
      Layout.fillWidth: true
      columns: 2
      rowSpacing : 0

      Label { text: "Renderable type"; }
      Label {
        text: {
          if (OpenGLInfo.renderableType == OpenGLInfo.OpenGLES ) {
            return "OpenGL ES"
          } else if (OpenGLInfo.renderableType == OpenGLInfo.OpenGL ) {
            return "OpenGL"
          }
          return "Unspecified"
        }
      }

      Label { text: "Version"; }
      Label {
        text: OpenGLInfo.majorVersion + "." + OpenGLInfo.minorVersion;
      }

      Label { text: "Profile"; }
      Label {
        text: {
          if (OpenGLInfo.profile == OpenGLInfo.CoreProfile ) {
            return "CoreProfile"
          } else if (OpenGLInfo.profile == OpenGLInfo.CompatibilityProfile ) {
            return "CompatibilityProfile"
          }
          return "NoProfile"
        }
      }

    }
  }
}
