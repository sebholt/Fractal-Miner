
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import AppTheme 1.0

Item {
  id: root

  implicitWidth: layout.implicitWidth
  implicitHeight: layout.implicitHeight

  ListModel {
    id: iterators

    ListElement {
      key: "mandelbrot"
      name: "Mandelbrot"
    }
  }

  ColumnLayout {
    id: layout
    anchors.fill: parent

    Headline { text: "Iteration" }

    GridLayout {
      id: grid
      Layout.fillWidth: true
      columns: 2

      Label { text: "Iterator"; }
      ComboBox {
        id: iteratorBox
        Layout.fillWidth: true
        model: iterators
        textRole: "name"

        function getModelIndex(key) {
          if ( model === iterators ) {
            for ( var ii = 0; ii < iterators.count; ii++ ) {
              var elem = iterators.get(ii);
              if ( fractalView.iteratorSettings.iteratorId == elem.key) {
                return ii;
              }
            }
          }
          return -1;
        }

        currentIndex: getModelIndex ( fractalView.iteratorSettings.iteratorId )

        onCurrentIndexChanged: {
          if ( model === iterators ) {
            if ( (currentIndex >= 0) && (currentIndex < iterators.count) ) {
              fractalView.iteratorSettings.iteratorId = iterators.get(currentIndex).key
            }
          }
        }
      }

      Label { text: "Iterations"; }
      SpinBox {
        Layout.fillWidth: true
        value: fractalView.iteratorSettings.iterationsMax
        from: 0
        to: 65535;
        stepSize: 50
        editable: true
        wheelEnabled: true
        onValueChanged: {
          fractalView.iteratorSettings.iterationsMax = value;
        }
      }

      Label { text: "Supersampling"; }
      ComboBox {
        id: supersamplingBox
        Layout.fillWidth: true
        model: ListModel {
          id: supersamplings
          ListElement { key: 4; name: "4x4"; }
          ListElement { key: 2; name: "2x2"; }
          ListElement { key: 1; name: "1x1"; }
        }
        textRole: "name"
        wheelEnabled: true

        function getModelIndex(key) {
          if ( model === supersamplings ) {
            for ( var ii = 0; ii < supersamplings.count; ii++ ) {
              var elem = supersamplings.get ( ii );
              if ( fractalView.renderSettings.supersampling == elem.key) {
                return ii;
              }
            }
          }
          return -1;
        }

        currentIndex: getModelIndex ( fractalView.renderSettings.supersampling )

        onCurrentIndexChanged: {
          if ( model === supersamplings ) {
            if ( ( currentIndex >= 0 ) && ( currentIndex < supersamplings.count ) ) {
              fractalView.renderSettings.supersampling = supersamplings.get ( currentIndex ).key
            }
          }
        }
      }

    }
  }
}
