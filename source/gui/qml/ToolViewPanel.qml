
import QtQuick 2.7
import QtQuick.Layouts 1.3

ToolPanel {
  id: panel
  property string panelId: ""

  implicitWidth: layout.implicitWidth
  implicitHeight: layout.implicitHeight

  function selectPanel ( panel )
  {
    if ( visible ) {
      if ( panelId == panel ) {
        visible = false;
      }
    } else {
      visible = true;
    }
    panelId = panel

    if ( visible ) {
      if ( panelId == "iterationColors" ) {
        loader.source = "PanelIterationColors.qml"
      } else if ( panelId == "render" ) {
        loader.source = "PanelRenderSettings.qml"
      } else if ( panelId == "info" ) {
        loader.source = "PanelSystemInfo.qml"
      } else {
        console.log ( "Unknown panel: " + panelId )
      }
    }
  }

  RowLayout {
    id: layout
    Loader {
      id: loader
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.topMargin: 10
      Layout.bottomMargin: 10
      Layout.leftMargin: 16
      Layout.rightMargin: 16
    }
  }
}
