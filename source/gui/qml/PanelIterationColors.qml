
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import AppTheme 1.0

RowLayout {
  id: root

  spacing: 16

  PanelIteration {
    Layout.alignment: Qt.AlignTop
  }
  Rectangle {
    color: Theme.textColor;
    opacity: 0.33
    width: 1
    Layout.fillHeight: true
  }
  PanelColors {
    Layout.alignment: Qt.AlignTop
  }
}
