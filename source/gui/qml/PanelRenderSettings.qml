
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import AppTheme 1.0

Item {
  id: root

  implicitWidth: layout.implicitWidth
  implicitHeight: layout.implicitHeight

  ColumnLayout {
    id: layout
    anchors.fill: parent

    Headline { text: "Render settings" }

    GridLayout {
      id: grid
      Layout.fillWidth: true
      columns: 2

      Label { text: "Draw front"; }
      CheckBox {
        id: drawFrontCheck
        checked: fractalView.renderSettings.drawForeground
        Binding {
          target: fractalView.renderSettings
          property: "drawForeground"
          value: drawFrontCheck.checked
        }
      }

      Label { text: "Draw back"; }
      CheckBox {
        id: drawBackCheck
        checked: fractalView.renderSettings.drawBackgrounds
        Binding {
          target: fractalView.renderSettings
          property: "drawBackgrounds"
          value: drawBackCheck.checked
        }
      }

      Label { text: "Upload"; }
      CheckBox {
        id: uploadCheck
        checked: fractalView.renderSettings.upload
        Binding {
          target: fractalView.renderSettings
          property: "upload"
          value: uploadCheck.checked
        }
      }

      Label { text: "Wireframe"; }
      CheckBox {
        id: wireCheck
        checked: fractalView.renderSettings.wireframe
        Binding {
          target: fractalView.renderSettings
          property: "wireframe"
          value: wireCheck.checked
        }
      }

    }
  }
}
