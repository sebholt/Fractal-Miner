
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import AppTheme 1.0

Item {
  id: root

  implicitWidth: layout.implicitWidth
  implicitHeight: layout.implicitHeight

  ListModel {
    id: colorSchemes

    ListElement {
      key: "color_steps"
      name: "Color steps"
    }
    ListElement {
      key: "color_wave"
      name: "Color wave"
    }
    ListElement {
      key: "tentacle"
      name: "Tentacle"
    }
    ListElement {
      key: "grey_steps"
      name: "Grey steps"
    }
    ListElement {
      key: "grey_wave"
      name: "Grey wave"
    }
  }

  ColumnLayout {
    id: layout
    anchors.fill: parent

    Headline { text: "Colors" }

    GridLayout {
      id: grid
      Layout.fillWidth: true
      columns: 2

      Label { text: "Scheme"; }
      ComboBox {
        id: colorSchemeBox
        Layout.fillWidth: true
        model: colorSchemes
        textRole: "name"
        wheelEnabled: true

        function getModelIndex(key) {
          if ( model === colorSchemes ) {
            for ( var ii = 0; ii < colorSchemes.count; ii++ ) {
              var elem = colorSchemes.get(ii);
              if ( fractalView.colorSettings.schemeId == elem.key) {
                return ii;
              }
            }
          }
          return -1;
        }

        currentIndex: getModelIndex(fractalView.colorSettings.schemeId)

        onCurrentIndexChanged: {
          //console.log ( "Current index " + currentIndex )
          //console.log ( "Current scheme " + fractalView.colorScheme )
          if ( (currentIndex >= 0) && (currentIndex < colorSchemes.count) ) {
            fractalView.colorSettings.schemeId = colorSchemes.get(currentIndex).key
          }
        }
      }

      Label { text: "Offset"; }
      SpinBox {
        id: offsetBox
        Layout.fillWidth: true

        from: 0
        to: { fractalView.colorSettings.colorCount - 1 }
        value: fractalView.colorSettings.offset
        wrap: true;
        editable: true
        wheelEnabled: true

        Binding {
          target: fractalView.colorSettings
          property: "offset"
          value: offsetBox.value
        }
      }

      Label { text: "Colors per second"; }
      SpinBox {
        id: colorsPerSecondBox
        Layout.fillWidth: true

        value: fractalView.colorSettings.colorCyclingCPS
        from: 0
        to: 120
        editable: true
        wheelEnabled: true

        Binding {
          target: fractalView.colorSettings
          property: "colorCyclingCPS"
          value: colorsPerSecondBox.value
        }
      }

      Label { text: "Cycle forward"; }
      CheckBox {
        id: colorCyclingForwardBox
        checked: fractalView.colorSettings.colorCyclingForward
        onCheckedChanged: {
          fractalView.colorSettings.colorCyclingForward = colorCyclingForwardBox.checked
        }
      }
    }

  }
}
