
import QtQuick 2.7

Item {
  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.AllButtons
    onWheel: { wheel.accepted = true }
  }

  SystemPalette {
    id: systemPalette
  }

  Rectangle {
    anchors.fill: parent
    color: systemPalette.window
  }
}
