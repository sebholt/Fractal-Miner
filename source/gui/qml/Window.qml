
import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3
import Fractal 1.0
import Settings 1.0

Window {
  id: window
  property string guiSettingsFile: applicationSettings.value ( "files.guiSettings" );
  title: applicationSettings.value ( "app.title" );

  function init()
  {
    init_default();
    if ( guiSettings.json_import_file ( guiSettingsFile ) ) {
      init_from_settings();
    }
    window.visible = true
  }

  function init_default()
  {
    window.width = 800
    window.height = 600
  }

  function init_from_settings()
  {
    window.x = guiSettings.value ( "window.x", window.x );
    window.y = guiSettings.value ( "window.y", window.y );
    window.width = guiSettings.value ( "window.width", window.width );
    window.height = guiSettings.value ( "window.height", window.height );
  }

  function write_settings()
  {
    if ( !guiSettings.json_export_file ( guiSettingsFile ) ) {
      console.log ( "Gui settings writing failed" )
    }
  }

  function settings_store_geometry()
  {
    if ( visibility == Window.Windowed ) {
      guiSettings.set_value ( "window.x", x );
      guiSettings.set_value ( "window.y", y );
      guiSettings.set_value ( "window.width", width );
      guiSettings.set_value ( "window.height", height );
    }
  }

  onVisibilityChanged: { settings_store_geometry() }
  onXChanged: { settings_store_geometry() }
  onYChanged: { settings_store_geometry() }
  onWidthChanged: { settings_store_geometry() }
  onHeightChanged: { settings_store_geometry() }

  onClosing: {
    write_settings();
  }

  // Gui settings
  ConfigTree {
    id: guiSettings
    Component.onCompleted: {
      window.init();
    }
  }

  // Window key handler
  Item {
    id: windowKeyHandler
    anchors.fill: parent
    focus: true

    Keys.onPressed: {
      if ( event.key == Qt.Key_F ) {
        console.log("F")
        if ( window.visibility == Window.FullScreen ) {
          console.log("showNormal")
          window.showNormal();
        } else {
          console.log("showFullScreen")
          window.showFullScreen()
        }
      } else if ( event.key == Qt.Key_D ) {
        console.log ( "docker.visible " + docker.visible );
        if ( docker.visible ) {
          docker.visible = false;
        } else {
          docker.visible = true;
        }
      } else if ( event.key == Qt.Key_Q ) {
        window.close()
      } else if ( event.key == Qt.Key_Escape ) {
        if ( !focus ) {
          focus = true
        } else if ( toolViewPanel.visible ) {
          toolViewPanel.visible = false
        }
      }
    }
  }

  Pipeline {
    id: fractalPipeline
  }
  FractalView {
    id: fractalView
    anchors.fill: parent

    Keys.forwardTo: windowKeyHandler

    pipeline: fractalPipeline
    iteratorSettings.iteratorId: "mandelbrot"
    iteratorSettings.iterationsMax: 200
    colorSettings.schemeId: "color_steps"
  }

  ToolBar {
    id: toolBar
    anchors.bottom: parent.bottom
    anchors.horizontalCenter: parent.horizontalCenter

    Keys.forwardTo: windowKeyHandler
  }

  ToolViewPanel {
    id: toolViewPanel
    visible: false
    anchors.bottom: toolBar.top
    anchors.horizontalCenter: toolBar.horizontalCenter
    anchors.bottomMargin: 8

    Keys.forwardTo: windowKeyHandler

    Component.onCompleted: {
      toolBar.panelSelected.connect ( selectPanel )
    }
  }

}
