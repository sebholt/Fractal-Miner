/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <geo/pair.hpp>
#include <array>
#include <cstdint>
#include <ostream>

namespace fre
{

/// @brief Matrix resizing changes
///
class Resize_Deltas
{
  public:
  Resize_Deltas ()
  : _dim{ { { 0, 0 }, { 0, 0 } } }
  {
  }

  void
  reset ()
  {
    for ( auto & item : _dim ) {
      item.fill ( 0 );
    }
  }

  bool
  any_changes () const
  {
    return ( _dim[ 0 ].first () != 0 ) || ( _dim[ 0 ].second () != 0 ) ||
           ( _dim[ 1 ].first () != 0 ) || ( _dim[ 1 ].second () != 0 );
  }

  // -- Dimensions

  geo::Int32_Pair const &
  dim ( std::size_t index_n ) const
  {
    return _dim[ index_n ];
  }

  geo::Int32_Pair &
  dim_ref ( std::size_t index_n )
  {
    return _dim[ index_n ];
  }

  geo::Int32_Pair const &
  horizontal () const
  {
    return _dim[ 0 ];
  }

  geo::Int32_Pair &
  horizontal_ref ()
  {
    return _dim[ 0 ];
  }

  geo::Int32_Pair const &
  vertical () const
  {
    return _dim[ 1 ];
  }

  geo::Int32_Pair &
  vertical_ref ()
  {
    return _dim[ 1 ];
  }

  // -- Dimensional change sum

  std::int32_t
  dim_sum ( std::size_t index_n ) const
  {
    return ( _dim[ index_n ].first () + _dim[ index_n ].second () );
  }

  std::int32_t
  horizontal_sum () const
  {
    return dim_sum ( 0 );
  }

  std::int32_t
  vertical_sum () const
  {
    return dim_sum ( 1 );
  }

  private:
  std::array< geo::Int32_Pair, 2 > _dim;
};

// -- ostream adaptors
std::ostream &
operator<< ( std::ostream & os_n, Resize_Deltas const & deltas_n );
} // namespace fre
