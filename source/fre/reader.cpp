/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "reader.hpp"
#include <event/bit_collectors/condition.hpp>
#include <fre/hub.hpp>
#include <fre/uploader/events.hpp>
#include <fre/uploader/uploader.hpp>
#include <application_config.hpp>
#include <message_streams.hpp>
#include <utility>

namespace fre
{

Reader::Reader ( std::shared_ptr< Hub > const & hub_n )
: _hub ( hub_n )
{
  // There is only one event type
  std::uint32_t event_bits ( 1 );
  _evqc.in ().set_bit_notifier ( event_bits, &_bit_collector );
  _evqc.connect ( _hub->uploader ()->queues_reader () );
}

Reader::~Reader ()
{
  {
    // Local waiter
    event::bit_collectors::Condition waiter;
    // Replace process callback to local lambda
    set_callback ( [ &waiter ] () { waiter.notify_events ( 1 ); } );
    // Call initially
    waiter.notify_events ( 1 );
    // Wait until all events have returned
    while ( !is_aborted () ) {
      waiter.wait_for_events ();
      fetch_begin ();
      while ( fetch_next () != nullptr ) {
        fetch_accept ();
      }
      fetch_end ();
    }
    // Clear process callback
    set_callback ( Callback () );
  }
}

void
Reader::set_callback ( Callback const & callback_n )
{
  _bit_collector.set_callback ( callback_n );
}

void
Reader::set_callback ( Callback && callback_n )
{
  _bit_collector.set_callback ( std::move ( callback_n ) );
}

bool
Reader::fetch_begin ()
{
  // There is only one event type
  _bit_collector.clear_events ();
  _evqc.read_from_queue ();
  return !_evqc.in ().is_empty ();
}

fre::image_matrix::Tile_Buffer const *
Reader::fetch_next ()
{
  using namespace fre::uploader;
  while ( !_evqc.in ().is_empty () ) {
    event::Event * event ( _evqc.in ().front () );
    switch ( static_cast< events::Type > ( event->type () ) ) {
    case events::Type::TILE: {
      fre::image_matrix::Tile_Buffer const * tile =
          static_cast< events::Tile * > ( event )->buffer_ref.get ();

      if ( DEBUG_FRE_READER ) {
        Debug_Stream oss;
        oss << "Reader::fetch_next: image_code: "
            << tile->tile_id ().image_code () << "\n";
      }

      if ( tile->tile_id ().image_code () == image_code () ) {
        return tile;
      } else {
        _evqc.out ().push ( _evqc.in ().pop_not_empty () );
      }
    } break;
    case events::Type::ABORTED:
      // This is the last event to get
      if ( DEBUG_FRE_READER ) {
        Debug_Stream oss;
        oss << "Reader::fetch_next: Aborted\n";
      }
      _evqc.out ().push ( _evqc.in ().pop_not_empty () );
      _evqc.feed_into_queue ();
      _evqc.disconnect ();
      break;
    }
  }
  return nullptr;
}

void
Reader::fetch_accept ()
{
  if ( !_evqc.in ().is_empty () ) {
    _evqc.out ().push ( _evqc.in ().pop_not_empty () );
  }
}

void
Reader::fetch_end ()
{
  _evqc.feed_into_queue ();
}
} // namespace fre
