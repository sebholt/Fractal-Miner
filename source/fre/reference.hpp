/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <utility>

namespace fre
{

/// @brief Abstract reference counting object reference
///
template < class T >
class Reference
{
  public:
  Reference () = default;

  /// @brief Makes this another reference to the object
  Reference ( T * object_n )
  : _object ( object_n )
  {
    if ( _object != nullptr ) {
      ++_object->_reference_count;
    }
  }

  /// @brief Makes this another reference to the object
  Reference ( const Reference & ref_n )
  : Reference ( ref_n._object )
  {
  }

  /// @brief Takes over the reference and leaves it invalid
  Reference ( Reference && ref_n )
  : _object ( ref_n._object )
  {
    ref_n._object = nullptr;
  }

  ~Reference ()
  {
    if ( _object != nullptr ) {
      decrement_ref_count ( _object );
    }
  }

  // -- Referred object

  bool
  is_valid () const
  {
    return ( _object != nullptr );
  }

  T *
  get () const
  {
    return _object;
  }

  void
  reset ()
  {
    if ( _object != nullptr ) {
      decrement_ref_count ( _object );
      _object = nullptr;
    }
  }

  void
  reset ( T * object_n )
  {
    T * object_prev ( _object );
    _object = object_n;
    if ( _object != nullptr ) {
      ++_object->_reference_count;
    }
    if ( object_prev != nullptr ) {
      decrement_ref_count ( object_prev );
    }
  }

  /// @brief Create a new object
  template < typename... Args >
  static Reference
  created ( Args &&... args_n )
  {
    return Reference ( new T ( std::forward< Args > ( args_n )... ) );
  }

  /// @brief Moves the reference to this
  void
  move_assign ( Reference && ref_n )
  {
    if ( &ref_n != this ) {
      if ( _object != nullptr ) {
        decrement_ref_count ( _object );
      }
      _object = ref_n._object;
      ref_n._object = nullptr;
    }
  }

  void
  swap ( Reference & ref_n )
  {
    std::swap ( _object, ref_n._object );
  }

  // -- Type cast operators

  operator T * () const { return _object; }

  T *
  operator-> () const
  {
    return _object;
  }

  // -- Assignment operators

  /// @brief Makes this another reference to the queue
  Reference &
  operator= ( const Reference & ref_n )
  {
    reset ( ref_n._object );
    return *this;
  }

  /// @brief Takes over the reference and leaves it invalid
  Reference &
  operator= ( Reference && ref_n )
  {
    move_assign ( std::move ( ref_n ) );
    return *this;
  }

  // -- Comparison operators

  bool
  operator== ( const Reference & ref_n ) const
  {
    return ( _object == ref_n._object );
  }

  bool
  operator!= ( const Reference & ref_n ) const
  {
    return ( _object != ref_n._object );
  }

  bool
  operator== ( T * ptr_n ) const
  {
    return ( _object == ptr_n );
  }

  bool
  operator!= ( T * ptr_n ) const
  {
    return ( _object != ptr_n );
  }

  bool
  operator< ( const Reference & ref_n ) const
  {
    return ( _object < ref_n._object );
  }

  bool
  operator<= ( const Reference & ref_n ) const
  {
    return ( _object <= ref_n._object );
  }

  bool
  operator> ( const Reference & ref_n ) const
  {
    return ( _object > ref_n._object );
  }

  bool
  operator>= ( const Reference & ref_n ) const
  {
    return ( _object >= ref_n._object );
  }

  private:
  static void
  decrement_ref_count ( T * object_n )
  {
    if ( --object_n->_reference_count == 0 ) {
      delete object_n;
    }
  }

  private:
  T * _object = nullptr;
};

/// @brief Swap function
template < class T >
inline void
swap ( Reference< T > & ref_a_n, Reference< T > & ref_b_n )
{
  ref_a_n.swap ( ref_b_n );
}
} // namespace fre
