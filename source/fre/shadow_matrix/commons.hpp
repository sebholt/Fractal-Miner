/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/fractal/iterator.hpp>
#include <geo/geo.hpp>
#include <complex>
#include <memory>
#include <vector>

namespace fre::shadow_matrix
{

// -- Forward dewclaration
class Commons;
typedef std::shared_ptr< Commons > Commons_Reference;
typedef std::vector< std::uint16_t > Pixel_Offsets;
typedef std::shared_ptr< Pixel_Offsets > Pixel_Offsets_Reference;
typedef std::vector< std::complex< double > > Complex_Offsets;
typedef std::shared_ptr< Complex_Offsets > Complex_Offsets_Reference;

/// @brief Shadow tile commons
///
class Commons
{
  public:
  // -- Constructors
  Commons ( geo::UInt16_Size tile_size_n,
            std::uint32_t image_code_n,
            Pixel_Offsets_Reference const & pixel_offsets_n,
            Complex_Offsets_Reference const & complex_offsets_n,
            fre::fractal::Iterator_Reference const & iterator_n );

  // -- Accessors

  /// @brief Tile size
  geo::UInt16_Size const &
  tile_size () const
  {
    return _tile_size;
  }

  /// @brief Tile width
  std::int16_t
  tile_width () const
  {
    return _tile_size.width ();
  }

  /// @brief Tile height
  std::int16_t
  tile_height () const
  {
    return _tile_size.height ();
  }

  // -- Pixel count

  /// @brief Number of pixels in a tile
  std::uint32_t
  pixel_count () const
  {
    return _pixel_count;
  }

  // -- Pixel count

  /// @brief Maximum number of iterations to perform on a single tile render
  ///        session
  std::uint32_t
  session_iterations_limit () const
  {
    return _session_iterations_limit;
  }

  // -- Image code

  /// @brief Image code
  ///
  std::uint32_t
  image_code () const
  {
    return _image_code;
  }

  // -- Offsets

  Pixel_Offsets_Reference const &
  pixel_offsets () const
  {
    return _pixel_offsets;
  }

  Complex_Offsets_Reference const &
  complex_offsets () const
  {
    return _complex_offsets;
  }

  // -- Iterator

  fre::fractal::Iterator_Reference const &
  iterator () const
  {
    return _iterator;
  }

  private:
  // -- Sizes
  geo::UInt16_Size _tile_size;
  std::uint32_t _pixel_count = 0;
  // -- Iteration
  std::uint32_t _image_code = 0;
  std::uint32_t _session_iterations_limit = 32 * 32 * 20;
  Pixel_Offsets_Reference _pixel_offsets;
  Complex_Offsets_Reference _complex_offsets;
  fre::fractal::Iterator_Reference _iterator;
};
} // namespace fre::shadow_matrix
