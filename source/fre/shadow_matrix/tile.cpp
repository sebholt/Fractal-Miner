/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "tile.hpp"

namespace fre::shadow_matrix
{

Tile::Tile ( geo::UInt16_Size tile_size_n )
: _buffer ( tile_size_n )
{
}
} // namespace fre::shadow_matrix
