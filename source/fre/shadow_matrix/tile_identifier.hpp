/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/id_code.hpp>
#include <geo/point.hpp>
#include <cstdint>

namespace fre::shadow_matrix
{

/// @brief Shadow tile identifier
///
class Tile_Identifier
{
  public:
  // -- Constructors

  Tile_Identifier () = default;

  // -- State

  bool
  is_valid () const
  {
    return _image_code.is_valid ();
  }

  /// @brief Reset to construction state
  void
  reset ()
  {
    _pos.reset ();
    _image_code = 0;
    _tile_code = 0;
  }

  // -- Column / Row

  /// @brief Column and row
  geo::Int32_Point const &
  pos () const
  {
    return _pos;
  }

  void
  set_pos ( geo::Int32_Point const & pos_n )
  {
    _pos = pos_n;
  }

  /// @brief Column
  std::int32_t
  column () const
  {
    return _pos.x ();
  }

  /// @brief Set the column
  void
  set_column ( std::int32_t column_n )
  {
    _pos.set_x ( column_n );
  }

  /// @brief Row
  std::int32_t
  row () const
  {
    return _pos.y ();
  }

  /// @brief Set the row
  void
  set_row ( std::int32_t row_n )
  {
    _pos.set_y ( row_n );
  }

  // -- Image code

  /// @brief Image code
  Id_Code const &
  image_code () const
  {
    return _image_code;
  }

  /// @brief Sets the image code
  void
  set_image_code ( std::uint32_t code_n )
  {
    _image_code = code_n;
  }

  // -- Tile code

  /// @brief Tile code
  ///
  /// Gets changed when the position of the tile in the matrix changes
  Id_Code const &
  tile_code () const
  {
    return _tile_code;
  }

  /// @brief Increments the tile code
  void
  increment_tile_code ()
  {
    _tile_code.increment ();
  }

  // -- Utility

  bool
  any_code_older_than ( Tile_Identifier const & id_n ) const
  {
    return ( image_code () < id_n.image_code () ) ||
           ( tile_code () < id_n.tile_code () );
  }

  bool
  any_code_newer_than ( Tile_Identifier const & id_n ) const
  {
    return ( image_code () > id_n.image_code () ) ||
           ( tile_code () > id_n.tile_code () );
  }

  // -- Comparison operators

  bool
  operator== ( Tile_Identifier const & id_n ) const
  {
    return ( ( _pos == id_n._pos ) && ( _image_code == id_n._image_code ) &&
             ( _tile_code == id_n._tile_code ) );
  }

  bool
  operator!= ( Tile_Identifier const & id_n ) const
  {
    return ( ( _pos != id_n._pos ) || ( _image_code != id_n._image_code ) ||
             ( _tile_code != id_n._tile_code ) );
  }

  private:
  geo::Int32_Point _pos;
  Id_Code _image_code;
  Id_Code _tile_code;
};
} // namespace fre::shadow_matrix
