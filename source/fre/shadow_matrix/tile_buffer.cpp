/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "tile_buffer.hpp"

namespace fre::shadow_matrix
{

Tile_Buffer::Tile_Buffer ( geo::UInt16_Size tile_size_n )
: _buffer ( tile_size_n.area< std::uint32_t > () )
{
}
} // namespace fre::shadow_matrix
