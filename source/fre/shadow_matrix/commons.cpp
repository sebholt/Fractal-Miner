/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "commons.hpp"

namespace fre::shadow_matrix
{

Commons::Commons ( geo::UInt16_Size tile_size_n,
                   std::uint32_t image_code_n,
                   std::shared_ptr< Pixel_Offsets > const & pixel_offsets_n,
                   Complex_Offsets_Reference const & complex_offsets_n,
                   fre::fractal::Iterator_Reference const & iterator_n )
: _tile_size ( tile_size_n )
, _pixel_count ( tile_size ().area< std::uint32_t > () )
, _image_code ( image_code_n )
, _pixel_offsets ( pixel_offsets_n )
, _complex_offsets ( complex_offsets_n )
, _iterator ( iterator_n )
{
}
} // namespace fre::shadow_matrix
