/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "matrix.hpp"
#include "debug_assert.hpp"

namespace fre::shadow_matrix
{

Matrix::Matrix ()
{
  generate_pixel_offsets ();
  generate_complex_offsets ();
  generate_commons ();
}

void
Matrix::generate_pixel_offsets ()
{
  // Initialize pixel offsets
  _pixel_offsets =
      std::make_shared< Pixel_Offsets > ( _tile_size.area< std::uint32_t > () );
  if ( !_pixel_offsets->empty () ) {
    // Pixel dithering
    {
      std::uint32_t const tile_width ( _tile_size.width () );
      std::uint32_t const tile_height ( _tile_size.height () );
      std::vector< bool > picked ( _pixel_offsets->size (), false );
      std::uint16_t * it_pp = _pixel_offsets->data ();
      auto pick = [ &picked, &it_pp, tile_width ] ( std::uint32_t x_n,
                                                    std::uint32_t y_n ) {
        std::uint32_t const index = y_n * tile_width + x_n;
        if ( !picked[ index ] ) {
          picked[ index ] = true;
          *it_pp = index;
          ++it_pp;
        }
      };
      std::uint32_t lmax = std::max ( tile_width, tile_height );
      std::uint32_t length = 1;
      while ( length < lmax ) {
        length *= 2;
      }
      while ( length != 0 ) {
        std::uint32_t length2 = length * 2;
        // -- A
        for ( std::uint32_t yy = length; yy < tile_height; yy += length2 ) {
          for ( std::uint32_t xx = length; xx < tile_width; xx += length2 ) {
            pick ( xx, yy );
          }
        }
        // -- B
        for ( std::uint32_t yy = 0; yy < tile_height; yy += length2 ) {
          for ( std::uint32_t xx = 0; xx < tile_width; xx += length2 ) {
            pick ( xx, yy );
          }
        }
        // -- C
        for ( std::uint32_t yy = length; yy < tile_height; yy += length2 ) {
          for ( std::uint32_t xx = 0; xx < tile_width; xx += length2 ) {
            pick ( xx, yy );
          }
        }
        // -- D
        for ( std::uint32_t yy = 0; yy < tile_height; yy += length2 ) {
          for ( std::uint32_t xx = length; xx < tile_width; xx += length2 ) {
            pick ( xx, yy );
          }
        }
        length /= 2;
      }
      DEBUG_ASSERT ( it_pp ==
                     ( _pixel_offsets->data () + _pixel_offsets->size () ) );
    }
  }
}

void
Matrix::generate_complex_offsets ()
{
  _complex_offsets = std::make_shared< Complex_Offsets > (
      _tile_size.area< std::uint32_t > () );
  {
    std::complex< double > const cp_dy = _cp_origin.dy ();
    std::complex< double > const cp_dx = _cp_origin.dx ();
    std::complex< double > * offset = _complex_offsets->data ();
    std::uint32_t const yend ( tile_height () );
    std::uint32_t const xend ( tile_width () );
    for ( std::uint32_t yy = 0; yy != yend; ++yy ) {
      std::complex< double > cp_row = cp_dy;
      cp_row *= double ( yy ) + 0.5;
      for ( std::uint32_t xx = 0; xx != xend; ++xx ) {
        *offset = cp_dx;
        *offset *= double ( xx ) + 0.5;
        *offset += cp_row;
        ++offset;
      }
    }
  }
}

void
Matrix::generate_commons ()
{
  _commons = std::make_shared< Commons > (
      _tile_size, _image_code, _pixel_offsets, _complex_offsets, _iterator );
}

void
Matrix::reset ( std::uint32_t image_code_n )
{
  if ( !_tile_rows.empty () ) {
    for ( Tile_Row const & row : _tile_rows ) {
      for ( Tile_Reference const & ref : row ) {
        tile_invalidate ( ref );
      }
    }
    _tile_rows.clear ();
  }
  _image_code = image_code_n;
  _pixels_rendered = 0;
  _cp_origin.reset ();
  Matrix_Base::reset ();

  generate_pixel_offsets ();
  generate_complex_offsets ();
  generate_commons ();
}

void
Matrix::set_tile_size ( Tile_Sizes const & tile_sizes_n,
                        std::uint32_t image_code_n )
{
  // Reset
  reset ( image_code_n );
  // Change
  _tile_size = tile_sizes_n.shadow_tile_size;
  generate_pixel_offsets ();
  generate_complex_offsets ();
  generate_commons ();
}

void
Matrix::set_complex ( geo::Double_Point const & px_base_n,
                      fre::Complex_Origin const & cp_origin_n,
                      std::uint32_t image_code_n )
{
  // Reset tile statistics
  _column_id_ends.set ( -1, columns () );
  _row_id_ends.set ( -1, rows () );
  _image_code = image_code_n;
  _pixels_rendered = 0;
  // Reset tiles column, row and image_code
  {
    std::int32_t irow ( 0 );
    for ( Tile_Row const & row : _tile_rows ) {
      std::int32_t icol ( 0 );
      for ( Tile_Reference const & ref : row ) {
        ref->id_ref ().set_column ( icol );
        ref->id_ref ().set_row ( irow );
        ref->id_ref ().set_image_code ( image_code_n );
        ref->set_pixels_rendered ( 0 );
        ++icol;
      }
      ++irow;
    }
  }

  {
    // Add offset of left-bottom tile
    geo::Double_Point px_base ( px_base_n );
    px_base.add ( ( _column_id_ends.first () + 1 ) * tile_width (),
                  ( _row_id_ends.first () + 1 ) * tile_height () );

    // Set complex vectors
    _cp_origin.set ( cp_origin_n.at ( -px_base.x (), -px_base.y () ),
                     cp_origin_n.dx (),
                     cp_origin_n.dy () );
  }

  // Recreate commons
  generate_complex_offsets ();
  generate_commons ();
}

void
Matrix::set_image_code ( std::uint32_t code_n )
{
  _image_code = code_n;
  _pixels_rendered = 0;
  for ( Tile_Row const & row : _tile_rows ) {
    for ( Tile_Reference const & ref : row ) {
      ref->id_ref ().set_image_code ( code_n );
      ref->set_pixels_rendered ( 0 );
    }
  }
}

void
Matrix::set_iterator ( fre::fractal::Iterator_Reference const & iterator_n,
                       std::uint32_t image_code_n )
{
  _iterator = iterator_n;
  set_image_code ( image_code_n );
  generate_commons ();
}

void
Matrix::compute_size_px ()
{
  _size_px.set_width ( columns () * tile_width () );
  _size_px.set_height ( rows () * tile_height () );
}

Tile_Reference
Matrix::tile_create ( geo::Int32_Point const & pos_n )
{
  Tile_Reference tile ( Tile_Reference::created ( tile_size () ) );
  // Initialize id
  tile->id_ref ().set_pos ( pos_n );
  tile->id_ref ().set_image_code ( _image_code );
  return tile;
}

inline void
Matrix::acquire_repaintable_tile (
    shadow_matrix::Tile_Reference_Vector & tiles_n,
    Tile_Reference const & ref_n )
{
  if ( !ref_n->is_paint_only () ) {
    if ( ref_n->is_pipelined () ) {
      // Will be set pickable when returning to the hub
      ref_n->set_is_paint_only ( true );
    } else {
      ref_n->set_is_paint_only ( true );
      tiles_n.emplace_back ( ref_n );
    }
  }
}

void
Matrix::acquire_repaintables_column (
    shadow_matrix::Tile_Reference_Vector & tiles_n, std::int32_t column_n )
{
  if ( ( column_n >= 0 ) && ( column_n < std::int32_t ( columns () ) ) ) {
    for ( Tile_Row const & row : _tile_rows ) {
      acquire_repaintable_tile ( tiles_n, row[ column_n ] );
    }
  }
}

void
Matrix::acquire_repaintables_row (
    shadow_matrix::Tile_Reference_Vector & tiles_n, std::int32_t row_n )
{
  if ( ( row_n >= 0 ) && ( row_n < std::int32_t ( rows () ) ) ) {
    Tile_Row const & row = _tile_rows[ row_n ];
    for ( Tile_Reference const & ref : row ) {
      acquire_repaintable_tile ( tiles_n, ref );
    }
  }
}
} // namespace fre::shadow_matrix
