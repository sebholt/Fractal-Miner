/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/reference.hpp>
#include <fre/shadow_matrix/tile_buffer.hpp>
#include <fre/shadow_matrix/tile_identifier.hpp>
#include <geo/geo.hpp>
#include <vector>

namespace fre::shadow_matrix
{

// -- Forward declaration
class Tile;
typedef fre::Reference< Tile > Tile_Reference;
typedef std::vector< shadow_matrix::Tile_Reference > Tile_Reference_Vector;

/// @brief Shadow tile
///
class Tile
{
  public:
  // -- Types
  typedef std::vector< std::uint32_t > Buffer;
  static const std::uint8_t pickable_list_invalid = 255;
  // -- Friends
  friend Tile_Reference;

  public:
  // -- Constructors
  Tile ( geo::UInt16_Size tile_size_n );

  // -- Tile identifier

  Tile_Identifier &
  id_ref ()
  {
    return _id;
  }

  Tile_Identifier const &
  id () const
  {
    return _id;
  }

  // -- Pipeline status variables

  /// @brief Flags if the tile was picked and is in the render pipeline
  bool
  is_paint_only () const
  {
    return _is_paint_only;
  }

  void
  set_is_paint_only ( bool flag_n )
  {
    _is_paint_only = flag_n;
  }

  /// @brief Flags if the tile is in the pickable list
  bool
  is_pickable () const
  {
    return ( _pickable_list != pickable_list_invalid );
  }

  /// @brief Index of the list in which the tile is registered
  std::uint8_t
  pickable_list () const
  {
    return _pickable_list;
  }

  void
  set_pickable_list ( std::uint8_t list_index_n )
  {
    _pickable_list = list_index_n;
  }

  void
  clear_pickable_list ()
  {
    _pickable_list = pickable_list_invalid;
  }

  /// @brief Flags if the tile was picked and is in the render pipeline
  bool
  is_pipelined () const
  {
    return _is_pipelined;
  }

  void
  set_is_pipelined ( bool flag_n )
  {
    _is_pipelined = flag_n;
  }

  /// @brief Returns true if the tile is either pickable or pipelined
  bool
  is_busy () const
  {
    return ( is_pipelined () || is_pickable () );
  }

  /// @brief Amount of rendered pixels in the tile
  std::uint32_t
  pixels_rendered () const
  {
    return _pixels_rendered;
  }

  void
  set_pixels_rendered ( std::uint32_t value_n )
  {
    _pixels_rendered = value_n;
  }

  // -- Buffer

  Tile_Buffer &
  buffer_ref ()
  {
    return _buffer;
  }

  const Tile_Buffer &
  buffer () const
  {
    return _buffer;
  }

  private:
  Tile_Identifier _id;
  // -- Pipeline status variables
  std::uint8_t _pickable_list = pickable_list_invalid;
  bool _is_pipelined = false;
  bool _is_paint_only = false;
  std::uint32_t _pixels_rendered = 0;
  /// -- Iterations buffer
  Tile_Buffer _buffer;
  /// -- Reference count
  std::uint32_t _reference_count = 0;
};
} // namespace fre::shadow_matrix
