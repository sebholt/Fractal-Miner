/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/complex_origin.hpp>
#include <fre/fractal/iterator.hpp>
#include <fre/matrix_base.hpp>
#include <fre/resize_deltas.hpp>
#include <fre/shadow_matrix/commons.hpp>
#include <fre/shadow_matrix/tile.hpp>
#include <fre/tile_sizes.hpp>
#include <debug_assert.hpp>
#include <deque>

// -- Forward declaration
namespace fre
{
template < class MT, bool RN >
class Matrix_Manipulator;
}

namespace fre::shadow_matrix
{

/// @brief Shadow matrix
///
class Matrix : public Matrix_Base
{
  public:
  // -- Types
  typedef fre::shadow_matrix::Tile_Reference Tile_Reference;
  typedef std::deque< Tile_Reference > Tile_Row;
  typedef std::deque< Tile_Row > Tile_Rows;
  typedef Matrix_Manipulator< Matrix, true > Manipulator;
  // -- Friends
  friend Manipulator;

  public:
  // -- Constructors

  Matrix ();

  // -- Sile size

  /// @brief Tile size
  geo::UInt16_Size const &
  tile_size () const
  {
    return _tile_size;
  }

  /// @brief Tile width
  std::int16_t
  tile_width () const
  {
    return _tile_size.width ();
  }

  /// @brief Tile height
  std::int16_t
  tile_height () const
  {
    return _tile_size.height ();
  }

  void
  set_tile_size ( Tile_Sizes const & tile_sizes_n, std::uint32_t image_code_n );

  // -- Tile commons

  Commons_Reference const &
  commons () const
  {
    return _commons;
  }

  // -- Iterator

  fre::fractal::Iterator_Reference const &
  iterator () const
  {
    return _iterator;
  }

  void
  set_iterator ( fre::fractal::Iterator_Reference const & iterator_n,
                 std::uint32_t image_code_n );

  // -- Complex plane

  /// @brief Complex origin of the center of left-bottom pixel of the tile (0,0)
  ///
  fre::Complex_Origin const &
  cp_origin () const
  {
    return _cp_origin;
  }

  /// @brief Sets the complex vectors and updates the complex offsets
  ///
  void
  set_complex ( geo::Double_Point const & px_base_n,
                fre::Complex_Origin const & cp_origin_n,
                std::uint32_t image_code_n );

  // -- Tile pixel position / region

  std::int32_t
  column_from_px ( std::int32_t px_pos_n ) const
  {
    return ( px_pos_n / std::int32_t ( tile_width () ) );
  }

  std::int32_t
  row_from_px ( std::int32_t px_pos_n ) const
  {
    return ( px_pos_n / std::int32_t ( tile_height () ) );
  }

  geo::Int32_Point
  tile_pos_px ( std::int32_t col_n, std::int32_t row_n ) const
  {
    return geo::Int32_Point ( col_n * std::int32_t ( tile_width () ),
                              row_n * std::int32_t ( tile_height () ) );
  }

  geo::Int32_Point
  tile_pos_px ( Tile_Identifier const & id_n ) const
  {
    return tile_pos_px ( id_n.column () - _column_id_ends.first () - 1,
                         id_n.row () - _row_id_ends.first () - 1 );
  }

  geo::Int32_Region
  tile_region_px ( std::int32_t col_n, std::int32_t row_n ) const
  {
    return geo::Int32_Region ( col_n * std::int32_t ( tile_width () ),
                               row_n * std::int32_t ( tile_height () ),
                               tile_width (),
                               tile_height () );
  }

  geo::Int32_Region
  tile_region_px ( Tile_Identifier const & id_n ) const
  {
    return tile_region_px ( id_n.column () - _column_id_ends.first () - 1,
                            id_n.row () - _row_id_ends.first () - 1 );
  }

  // -- Tile coomplex

  std::complex< double >
  tile_complex_base ( Tile_Identifier const & id_n ) const
  {
    return _cp_origin.at ( id_n.column () * std::int32_t ( tile_width () ),
                           id_n.row () * std::int32_t ( tile_height () ) );
  }

  // -- Image code

  std::uint32_t
  image_code () const
  {
    return _image_code;
  }

  // -- Tile access

  Tile_Rows const &
  tile_rows () const
  {
    return _tile_rows;
  }

  void
  acquire_repaintable_tile ( shadow_matrix::Tile_Reference_Vector & tiles_n,
                             Tile_Reference const & ref_n );

  void
  acquire_repaintables_column ( shadow_matrix::Tile_Reference_Vector & tiles_n,
                                std::int32_t column_n );

  void
  acquire_repaintables_row ( shadow_matrix::Tile_Reference_Vector & tiles_n,
                             std::int32_t row_n );

  // -- Pixel statistics

  std::uint32_t
  pixels_rendered () const
  {
    return _pixels_rendered;
  }

  void
  increment_pixels_rendered ( std::uint32_t delta_n )
  {
    _pixels_rendered += delta_n;
    DEBUG_ASSERT ( _pixels_rendered <= size_px ().area< std::uint32_t > () );
  }

  private:
  // -- Utility

  void
  reset ( std::uint32_t image_code_n );

  /// @brief Sets the image code in the matrix and all tiles
  void
  set_image_code ( std::uint32_t code_n );

  void
  generate_pixel_offsets ();

  void
  generate_complex_offsets ();

  void
  generate_commons ();

  void
  decrement_pixels_rendered ( Tile_Reference const & ref_n )
  {
    if ( ref_n->id ().image_code () == image_code () ) {
      DEBUG_ASSERT ( _pixels_rendered >= ref_n->pixels_rendered () );
      _pixels_rendered -= ref_n->pixels_rendered ();
    }
  }

  // -- Matrix manipulator interface

  void
  compute_size_px ();

  Tile_Reference
  tile_create ( geo::Int32_Point const & pos_n );

  void
  tile_invalidate ( Tile_Reference const & ref_n )
  {
    decrement_pixels_rendered ( ref_n );
    ref_n->id_ref ().set_image_code ( 0 );
  }

  void
  tile_change_column ( Tile_Reference const & ref_n, std::int32_t column_n )
  {
    decrement_pixels_rendered ( ref_n );
    ref_n->id_ref ().set_column ( column_n );
    ref_n->id_ref ().increment_tile_code ();
    ref_n->set_pixels_rendered ( 0 );
    ref_n->buffer_ref ().reset_render_statistics ();
  }

  void
  tile_change_row ( Tile_Reference const & ref_n, std::int32_t row_n )
  {
    decrement_pixels_rendered ( ref_n );
    ref_n->id_ref ().set_row ( row_n );
    ref_n->id_ref ().increment_tile_code ();
    ref_n->set_pixels_rendered ( 0 );
    ref_n->buffer_ref ().reset_render_statistics ();
  }

  private:
  geo::UInt16_Size _tile_size;
  Pixel_Offsets_Reference _pixel_offsets;
  Complex_Offsets_Reference _complex_offsets;
  Commons_Reference _commons;
  std::shared_ptr< fractal::Iterator > _iterator;
  std::uint32_t _image_code = 0;
  std::uint32_t _pixels_rendered = 0;
  fre::Complex_Origin _cp_origin;
  Tile_Rows _tile_rows;
};
} // namespace fre::shadow_matrix
