/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/shadow_matrix/tile_identifier.hpp>
#include <geo/geo.hpp>
#include <complex>
#include <vector>

namespace fre::shadow_matrix
{

/// @brief Shamdow matrix tile pixel buffer
///
class Tile_Buffer
{
  public:
  // -- Types
  typedef std::vector< std::uint16_t > Buffer;

  public:
  // -- Constructors

  Tile_Buffer ( geo::UInt16_Size tile_size_n );

  // -- Tile identifier

  Tile_Identifier &
  id_ref ()
  {
    return _id;
  }

  Tile_Identifier const &
  id () const
  {
    return _id;
  }

  // -- Render statistics

  /// @brief Number of times the buffer was rendered into
  std::uint32_t
  render_sessions () const
  {
    return _render_sessions;
  }

  void
  increment_render_sessions ()
  {
    ++_render_sessions;
  }

  void
  clear_render_sessions ()
  {
    _render_sessions = 0;
  }

  /// @brief Total number of pixels rendered
  std::uint32_t
  pixels_rendered () const
  {
    return _pixels_rendered;
  }

  void
  set_pixels_rendered ( std::uint32_t value_n )
  {
    _pixels_rendered = value_n;
  }

  void
  reset_render_statistics ()
  {
    _render_sessions = 0;
    _pixels_rendered = 0;
  }

  // -- Complex base

  /// @brief Complex base of the bottom-left pixel
  std::complex< double > const &
  complex_base () const
  {
    return _complex_base;
  }

  void
  set_complex_base ( std::complex< double > const & value_n )
  {
    _complex_base = value_n;
  }

  // -- Pixel buffer

  std::size_t
  buffer_size () const
  {
    return _buffer.size ();
  }

  Buffer::iterator
  buffer_begin ()
  {
    return _buffer.begin ();
  }

  Buffer::iterator
  buffer_end ()
  {
    return _buffer.end ();
  }

  Buffer::value_type *
  data ()
  {
    return _buffer.data ();
  }

  Buffer::value_type const *
  data () const
  {
    return _buffer.data ();
  }

  Buffer::value_type const *
  cdata () const
  {
    return _buffer.data ();
  }

  private:
  Tile_Identifier _id;
  std::uint32_t _render_sessions = 0;
  std::uint32_t _pixels_rendered = 0;
  std::complex< double > _complex_base;
  Buffer _buffer;
};
} // namespace fre::shadow_matrix
