/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "dispatcher.hpp"
#include <event/queue/pair.hpp>
#include <fre/dispatcher/events_in.hpp>
#include <fre/hub.hpp>
#include <message_streams.hpp>

namespace fre
{

Dispatcher::Dispatcher ( Hub & hub_n )
: _hub ( hub_n )
{
}

Dispatcher::~Dispatcher ()
{
  if ( is_running () ) {
    // Join thread
    _thread.join ();
    // Clean up
    _evqc_engine_in.clear ();
    _evqc_engine_out.clear ();
  }
}

bool
Dispatcher::start ( const event::queue::Reference_Pair & evq_in_n,
                    const event::queue::Reference_Pair & evq_out_n,
                    const event::queue::Reference & evq_pipeline_n )
{
  if ( !is_running () ) {
    // Clear loop events
    _loop_events = 0;
    // Setup engine queue connection
    {
      _evqc_engine_in.in ().set_bit_notifier ( LEvent::ENGINE_IN,
                                               &_bit_collector );
      _evqc_engine_in.connect ( evq_in_n );

      _evqc_engine_out.in ().set_bit_notifier ( LEvent::ENGINE_OUT_RETURN,
                                                &_bit_collector );
      _evqc_engine_out.connect ( evq_out_n );
    }
    // Setup pipeline queue connection
    {
      _evqc_pipeline_return.set_bit_notifier ( LEvent::PIPELINE_RETURN,
                                               &_bit_collector );
      _evqc_pipeline_return.connect ( evq_pipeline_n );
    }
    // Start the thread
    _thread = std::thread ( &Dispatcher::run, this );
  }
  return is_running ();
}

void
Dispatcher::run ()
{
  while ( true ) {
    // Wait for bit events
    _bit_collector.wait_for_events ( _loop_events );

    // Process pipeline events
    if ( _evqc_pipeline_return.read_from_queue_checked ( _loop_events ) ) {
      process_pipeline_events ();
    }

    // Process engine events (pocess after pipeline events!)
    _evqc_engine_in.in ().read_from_queue_checked ( _loop_events );
    _evqc_engine_out.in ().read_from_queue_checked ( _loop_events );
    // Process events
    process_engine_events ();
    // Feed events into queues
    _evqc_engine_in.out ().feed_into_queue ();
    _evqc_engine_out.out ().feed_into_queue ();

    if ( _abort && _hub.dispatcher_abort () ) {
      break;
    }
  }
}

void
Dispatcher::process_pipeline_events ()
{
  _hub.dispatcher_pipeline_return ( _evqc_pipeline_return );
}

void
Dispatcher::process_engine_events ()
{
  while ( !_evqc_engine_in.in ().is_empty () ) {
    bool processed ( true );
    event::Event * event ( _evqc_engine_in.in ().pop_not_empty () );
    switch ( static_cast< dispatcher::events_in::Type > ( event->type () ) ) {
    case dispatcher::events_in::Type::ABORT:
      engine_event_abort ( event );
      break;
    case dispatcher::events_in::Type::TILE_SIZES:
      engine_event_tile_sizes ( event );
      break;
    case dispatcher::events_in::Type::PIPELINE:
      processed = engine_event_pipeline ( event );
      break;
    case dispatcher::events_in::Type::PIPELINE_LOCK:
      engine_event_pipeline_lock ( event );
      break;
    case dispatcher::events_in::Type::RESIZE:
      engine_event_resize ( event );
      break;
    case dispatcher::events_in::Type::LOOP:
      engine_event_loop ( event );
      break;
    case dispatcher::events_in::Type::COMPLEX:
      engine_event_complex ( event );
      break;
    case dispatcher::events_in::Type::ITERATOR:
      engine_event_iterator ( event );
      break;
    case dispatcher::events_in::Type::ITERATIONS:
      engine_event_iterations ( event );
      break;
    }
    if ( processed ) {
      _evqc_engine_in.out ().push ( event );
    } else {
      _evqc_engine_in.in ().repush ( event );
      break;
    }
  }

  if ( !_evqc_engine_out.in ().is_empty () ) {
    _hub.dispatcher_engine_return ( _evqc_engine_out.in () );
  }
}

void
Dispatcher::engine_event_abort ( event::Event * event_n )
{
  (void)event_n;
  _abort = true;
  _hub.dispatcher_abort ();
}

void
Dispatcher::engine_event_tile_sizes ( event::Event * event_n )
{
  using namespace dispatcher;
  auto & cevent ( *static_cast< events_in::Tile_Sizes const * > ( event_n ) );
  _hub.dispatcher_tile_sizes ( cevent.tile_sizes, cevent.image_code );
}

bool
Dispatcher::engine_event_pipeline ( event::Event * event_n )
{
  using namespace dispatcher;
  auto & cevent ( *static_cast< events_in::Pipeline const * > ( event_n ) );
  return _hub.dispatcher_pipeline ( cevent.pipeline );
}

void
Dispatcher::engine_event_pipeline_lock ( event::Event * event_n )
{
  using namespace dispatcher;
  auto & cevent (
      *static_cast< events_in::Pipeline_Lock const * > ( event_n ) );
  _hub.dispatcher_pipeline_lock ( cevent.lock );
}

void
Dispatcher::engine_event_resize ( event::Event * event_n )
{
  using namespace dispatcher;
  auto & cevent ( *static_cast< events_in::Resize const * > ( event_n ) );
  hub ().dispatcher_resize ( cevent.deltas );
}

void
Dispatcher::engine_event_loop ( event::Event * event_n )
{
  using namespace dispatcher;
  auto & cevent ( *static_cast< events_in::Loop const * > ( event_n ) );
  hub ().dispatcher_loop ( cevent.loops );
}

void
Dispatcher::engine_event_complex ( event::Event * event_n )
{
  using namespace dispatcher;
  auto & cevent ( *static_cast< events_in::Complex const * > ( event_n ) );
  hub ().dispatcher_set_complex (
      cevent.px_base, cevent.cp_origin, cevent.image_code );
}

void
Dispatcher::engine_event_iterator ( event::Event * event_n )
{
  using namespace dispatcher;
  auto & cevent ( *static_cast< events_in::Iterator const * > ( event_n ) );
  hub ().dispatcher_set_iterator ( cevent.address, cevent.image_code );
}

void
Dispatcher::engine_event_iterations ( event::Event * event_n )
{
  using namespace dispatcher;
  auto & cevent ( *static_cast< events_in::Iterations const * > ( event_n ) );
  hub ().dispatcher_set_iterations ( cevent.count, cevent.image_code );
}
} // namespace fre
