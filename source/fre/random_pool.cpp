/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <fre/random_pool.hpp>
#include <algorithm>
#include <chrono>
#include <numeric>
#include <random>

namespace fre
{

Random_Pool::Random_Pool ( std::uint32_t size_n )
{
  resize ( size_n );
}

void
Random_Pool::resize ( std::uint32_t size_n )
{
  if ( size_n == 0 ) {
    size_n = 1;
  }
  if ( size () != size_n ) {
    _numbers.resize ( size_n );
    std::iota ( _numbers.begin (), _numbers.end (), 0 );
    shuffle ();
  }
}

void
Random_Pool::shuffle ()
{
  std::uint32_t seed =
      std::chrono::system_clock::now ().time_since_epoch ().count ();
  std::shuffle ( _numbers.begin (), _numbers.end (), std::mt19937_64 ( seed ) );
  // Reset index
  _index = size () - 1;
}
} // namespace fre
