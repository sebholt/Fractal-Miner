/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/event.hpp>
#include <fre/progress.hpp>

namespace fre::dispatcher::events_out
{

enum class Type
{
  NONE,
  PROGRESS
};

class Progress : public event::Event
{
  public:
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::PROGRESS );

  public:
  Progress ()
  : event::Event ( etype )
  {
  }

  public:
  fre::Progress progress;
};
} // namespace fre::dispatcher::events_out
