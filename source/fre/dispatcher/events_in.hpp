/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/event.hpp>
#include <fre/complex_origin.hpp>
#include <fre/resize_deltas.hpp>
#include <fre/tile_sizes.hpp>
#include <geo/geo.hpp>
#include <array>
#include <complex>
#include <memory>

// -- Forward declaration
namespace fre
{
class Pipeline;
}

namespace fre::dispatcher::events_in
{

enum class Type
{
  // -- Runtime
  ABORT,
  // -- Tile sizes
  TILE_SIZES,
  // -- Pipeline connection
  PIPELINE,
  PIPELINE_LOCK,
  // -- Manipulation
  RESIZE,
  LOOP,
  COMPLEX,
  ITERATOR,
  ITERATIONS
};

/// @brief Request thread abort
///
class Abort : public event::Event
{
  public:
  // -- Statics
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::ABORT );

  public:
  // -- Constructors

  Abort ()
  : event::Event ( etype )
  {
  }
};

class Tile_Sizes : public event::Event
{
  public:
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::TILE_SIZES );

  public:
  Tile_Sizes ()
  : event::Event ( etype )
  {
  }

  public:
  fre::Tile_Sizes tile_sizes;
  std::uint32_t image_code = 0;
};

class Pipeline : public event::Event
{
  public:
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::PIPELINE );

  public:
  Pipeline ()
  : event::Event ( etype )
  {
  }

  void
  reset ()
  {
    pipeline.reset ();
  }

  public:
  std::shared_ptr< fre::Pipeline > pipeline;
};

class Pipeline_Lock : public event::Event
{
  public:
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::PIPELINE_LOCK );

  public:
  Pipeline_Lock ()
  : event::Event ( etype )
  {
  }

  public:
  bool lock = false;
};

class Resize : public event::Event
{
  public:
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::RESIZE );

  public:
  Resize ()
  : event::Event ( etype )
  {
  }

  public:
  Resize_Deltas deltas;
};

class Loop : public event::Event
{
  public:
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::LOOP );

  public:
  Loop ()
  : event::Event ( etype )
  {
  }

  public:
  /// @brief positive means smaller to larger
  geo::Int32_Pair loops;
};

class Complex : public event::Event
{
  public:
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::COMPLEX );

  public:
  Complex ()
  : event::Event ( etype )
  {
  }

  public:
  geo::Double_Point px_base;
  fre::Complex_Origin cp_origin;
  std::uint32_t image_code = 0;
};

class Iterator : public event::Event
{
  public:
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::ITERATOR );

  public:
  Iterator ()
  : event::Event ( etype )
  {
  }

  void
  reset ()
  {
    address.clear ();
    address.shrink_to_fit ();
  }

  public:
  std::string address;
  std::uint32_t image_code = 0;
};

class Iterations : public event::Event
{
  public:
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::ITERATIONS );

  public:
  Iterations ()
  : event::Event ( etype )
  {
  }

  public:
  std::uint32_t count = 0;
  std::uint32_t image_code = 0;
};
} // namespace fre::dispatcher::events_in
