/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "complex_origin.hpp"

namespace fre
{

// -- ostream adaptors
std::ostream &
operator<< ( std::ostream & os_n, Complex_Origin const & cp_n )
{
  return os_n << "base=(" << cp_n.base ().real () << ',' << cp_n.base ().imag ()
              << ") dx=(" << cp_n.dx ().real () << ',' << cp_n.dx ().imag ()
              << ") dy=(" << cp_n.dy ().real () << ',' << cp_n.dy ().imag ()
              << ')';
}
} // namespace fre
