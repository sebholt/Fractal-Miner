/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/event.hpp>
#include <event/pool.hpp>
#include <event/pool_tracker.hpp>
#include <event/queue/connection_in.hpp>
#include <event/queue/reference.hpp>
#include <fre/complex_origin.hpp>
#include <fre/dispatcher/events_out.hpp>
#include <fre/fractal/iterator.hpp>
#include <fre/id_code.hpp>
#include <fre/matrices.hpp>
#include <fre/pipeline/events.hpp>
#include <fre/pipeline_client.hpp>
#include <fre/random_pool.hpp>
#include <fre/tile_sizes.hpp>
#include <geo/geo.hpp>
#include <array>
#include <condition_variable>
#include <cstdint>
#include <mutex>

namespace fre
{

// -- Forward declaration
namespace uploader
{
class Uploader;
}
class Dispatcher;
class Pipeline;
class Pipeline_Returner;

/// @brief Shared data hub
///
class Hub
{
  public:
  // -- Constructors

  Hub ( const event::queue::Reference_Pair & evq_in_n,
        const event::queue::Reference_Pair & evq_out_n );

  ~Hub ();

  // -- Dispatcher interface

  /// @return True if all work is done
  bool
  dispatcher_abort ();

  void
  dispatcher_tile_sizes ( Tile_Sizes const & tile_sizes_n,
                          std::uint32_t image_code_n );

  bool
  dispatcher_pipeline ( std::shared_ptr< fre::Pipeline > const & pipeline_n );

  void
  dispatcher_pipeline_lock ( bool lock_n );

  void
  dispatcher_resize ( Resize_Deltas const & deltas_n );

  void
  dispatcher_loop ( geo::Int32_Pair const & loops_n );

  void
  dispatcher_set_complex ( geo::Double_Point const & px_base_n,
                           fre::Complex_Origin const & cp_origin_n,
                           std::uint32_t image_code_n );

  void
  dispatcher_set_iterator ( std::string const & address_n,
                            std::uint32_t image_code_n );

  void
  dispatcher_set_iterations ( std::uint32_t count_n,
                              std::uint32_t image_code_n );

  void
  dispatcher_pipeline_return ( event::queue::Connection_In & evqc_n );

  void
  dispatcher_engine_return ( event::queue::Connection_In & evqc_n );

  // -- Feeder interface

  event::Event *
  feeder_acquire ();

  // -- Painter interface

  void
  painter_return ( event::Event * event_n );

  std::unique_ptr< uploader::Uploader > const &
  uploader ()
  {
    return _uploader;
  }

  private:
  // -- Locked utility

  void
  locked_set_image_code ( std::uint32_t image_code_n );

  fre::fractal::Iterator_Reference
  locked_generate_iterator ();

  // -- Pipeline event return

  void
  locked_pipeline_return ( event::Event * event_n );

  // -- Locked utility methods (require the main mutex to be locked)

  std::uint32_t
  locked_pickable_list_index (
      shadow_matrix::Tile_Reference const & ref_n ) const
  {
    return std::min< std::uint32_t > ( ref_n->buffer ().render_sessions (),
                                       _pickable_tiles.size () - 1 );
  }

  void
  locked_unregister_pickable ( shadow_matrix::Tile_Reference const & ref_n );

  void
  locked_register_pickable_move (
      shadow_matrix::Tile_Reference_Vector & vec_n );

  void
  locked_register_pickable ( shadow_matrix::Tile_Reference const & ref_n );

  void
  locked_register_pickable_again (
      shadow_matrix::Tile_Reference const & ref_n );

  void
  locked_clear_pickables ();

  void
  locked_register_all_pickable ();

  void
  locked_send_progress ();

  void
  notify_feeder ()
  {
    _pipeline_client.notify ();
  }

  // -- Feeder utility methods (require the main mutex to be locked)

  bool
  feeder_locked_event_setup ( pipeline::events::Tile & event_n );

  private:
  // -- Miscellaneous
  bool _abort = false;
  bool _feeder_lock = false;
  // -- Mutexes
  std::mutex _main_mutex;
  // -- Image
  std::uint32_t _image_code = 0;
  std::string _iterator_id = "mandelbrot";
  std::uint32_t _iterations_max = 200;
  // -- Matrices
  Matrices _matrices;
  // -- Shadow tile picking
  Random_Pool _pickable_random;
  std::array< shadow_matrix::Tile_Reference_Vector, 8 > _pickable_tiles;
  std::uint32_t _pickable_tiles_count = 0;
  // -- Pipeline return
  event::queue::Connection_In _pipeline_return_evqc;
  std::unique_ptr< Pipeline_Returner > _pipeline_returner;
  // -- Uploading
  std::unique_ptr< uploader::Uploader > _uploader;
  // -- Progress
  bool _progress_send;
  Progress _progress_latest;
  // -- Event pools
  event::Pool_Tracker _epool_tracker_pipeline;
  event::Pool< pipeline::events::Tile > _epool_tile;
  event::Pool_Tracker _epool_tracker_engine;
  event::Pool< dispatcher::events_out::Progress > _epool_progress;
  // -- Pipeline
  Pipeline_Client _pipeline_client;
  // -- Dispatcher thread
  std::unique_ptr< Dispatcher > _dispatcher;
};
} // namespace fre
