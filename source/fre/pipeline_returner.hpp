/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/event.hpp>
#include <event/queue/reference.hpp>
#include <mutex>

namespace fre
{

/// @brief Painter thread interface to return pipeline events
///
class Pipeline_Returner
{
  public:
  // -- Constructors

  Pipeline_Returner ();

  ~Pipeline_Returner () = default;

  // -- Interface

  event::queue::Reference const &
  queue () const
  {
    return _queue;
  }

  /// @return a tile to the hub
  void
  feed ( event::Event * event_n );

  private:
  std::mutex _mutex;
  event::queue::Reference _queue;
};
} // namespace fre
