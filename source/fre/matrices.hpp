/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/fractal/iterator.hpp>
#include <fre/image_matrix/matrix.hpp>
#include <fre/shadow_matrix/matrix.hpp>
#include <fre/tile_sizes.hpp>
#include <geo/geo.hpp>
#include <array>
#include <complex>

namespace fre
{

/// @brief Matrices
///
class Matrices
{
  public:
  // -- Constructors

  Matrices ();

  ~Matrices ();

  // -- Accessors

  shadow_matrix::Matrix const &
  shadow_matrix () const
  {
    return _shadow_matrix;
  }

  shadow_matrix::Matrix &
  shadow_matrix_ref ()
  {
    return _shadow_matrix;
  }

  image_matrix::Matrix const &
  image_matrix () const
  {
    return _image_matrix;
  }

  geo::Int32_Size const &
  image_matrix_offset () const
  {
    return _image_matrix_offset;
  }

  // -- Readers

  /// @brief Shadow tile region in image matrix coordinates
  geo::Int32_Region
  mapped_shadow_tile_region (
      shadow_matrix::Tile_Identifier const & id_n ) const
  {
    geo::Int32_Region reg = shadow_matrix ().tile_region_px ( id_n );
    reg.move ( -_image_matrix_offset.width (),
               -_image_matrix_offset.height () );
    return reg;
  }

  // -- Manipulation

  void
  set_tile_sizes ( Tile_Sizes const & tile_sizes_n,
                   std::uint32_t image_code_n );

  void
  resize_image_matrix ( Resize_Deltas const & deltas_n,
                        shadow_matrix::Tile_Reference_Vector & pickables_n );

  void
  loop_horizontal ( std::int32_t loops_n,
                    shadow_matrix::Tile_Reference_Vector & pickables_n );

  void
  loop_vertical ( std::int32_t loops_n,
                  shadow_matrix::Tile_Reference_Vector & pickables_n );

  void
  set_image_matrix_complex ( geo::Double_Point const & px_base_n,
                             fre::Complex_Origin const & cp_origin_n,
                             std::uint32_t image_code_n );

  fre::fractal::Iterator_Reference const &
  iterator () const
  {
    return _shadow_matrix.commons ()->iterator ();
  }

  void
  set_iterator ( fre::fractal::Iterator_Reference const & iterator_n,
                 std::uint32_t image_code_n );

  private:
  // -- Border tiles

  void
  acquire_repaintables_columns ( shadow_matrix::Tile_Reference_Vector & tiles_n,
                                 std::int32_t sh_px_min_n,
                                 std::int32_t sh_px_max_n );

  void
  acquire_repaintables_rows ( shadow_matrix::Tile_Reference_Vector & tiles_n,
                              std::int32_t sh_px_min_n,
                              std::int32_t sh_px_max_n );
  void
  acquire_repaintables_left ( shadow_matrix::Tile_Reference_Vector & tiles_n );

  void
  acquire_repaintables_right ( shadow_matrix::Tile_Reference_Vector & tiles_n );

  void
  acquire_repaintables_bottom (
      shadow_matrix::Tile_Reference_Vector & tiles_n );

  void
  acquire_repaintables_top ( shadow_matrix::Tile_Reference_Vector & tiles_n );

  // -- Shadow matrix adaption

  void
  shadow_left_grow ( shadow_matrix::Matrix::Manipulator & manip_n );

  void
  shadow_left_retreat ( shadow_matrix::Matrix::Manipulator & manip_n );

  void
  shadow_right_grow ( shadow_matrix::Matrix::Manipulator & manip_n );

  void
  shadow_right_retreat ( shadow_matrix::Matrix::Manipulator & manip_n );

  void
  shadow_bottom_grow ( shadow_matrix::Matrix::Manipulator & manip_n );

  void
  shadow_bottom_retreat ( shadow_matrix::Matrix::Manipulator & manip_n );

  void
  shadow_top_grow ( shadow_matrix::Matrix::Manipulator & manip_n );

  void
  shadow_top_retreat ( shadow_matrix::Matrix::Manipulator & manip_n );

  private:
  // -- Matrices
  shadow_matrix::Matrix _shadow_matrix;
  image_matrix::Matrix _image_matrix;
  /// @brief Offset of the image matrix over the shadow matrix
  geo::Int32_Size _image_matrix_offset;
};
} // namespace fre
