/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_collectors/callback.hpp>
#include <event/pool.hpp>
#include <event/pool_tracker.hpp>
#include <event/queue/connections_io.hpp>
#include <fre/complex_origin.hpp>
#include <fre/dispatcher/events_in.hpp>
#include <fre/dispatcher/events_out.hpp>
#include <fre/id_code.hpp>
#include <fre/reader.hpp>
#include <fre/tile_sizes.hpp>
#include <geo/geo.hpp>
#include <functional>
#include <memory>

namespace fre
{

// -- Forward declaration
class Hub;
class Pipeline;

/// @brief Fractal render engine main interface class
///
class Engine
{
  public:
  // -- Types
  enum LEvent
  {
    DSP_OUT = 1 << 0,
    DSP_OUT_RETURN = 1 << 1,
    DSP_IN = 1 << 2,
    DSP_IN_RETURN = 1 << 3
  };

  typedef std::function< void () > Callback;

  public:
  // -- Constructors

  Engine ();

  ~Engine ();

  // -- Processing

  void
  set_events_callback ( Callback && function_n );

  bool
  events_begin ();

  fre::dispatcher::events_out::Type
  events_next ();

  void
  events_end ();

  // -- State

  Id_Code const &
  image_code () const
  {
    return _image_code;
  }

  fre::Progress const &
  progress () const
  {
    return _progress;
  }

  // -- Tile sizes

  void
  set_tile_sizes ( const Tile_Sizes & tile_sizes_n );

  // -- Pipeline connection

  void
  pipeline_connect ( std::shared_ptr< fre::Pipeline > const & pipeline_n );

  void
  pipeline_disconnect ();

  void
  pipeline_lock ( bool lock_n );

  void
  pipeline_lock ()
  {
    pipeline_lock ( true );
  }

  void
  pipeline_unlock ()
  {
    pipeline_lock ( false );
  }

  // -- Manipulation

  void
  matrix_resize ( Resize_Deltas const & deltas_n );

  void
  loop ( geo::Int32_Pair const & loops_n );

  /// @arg px_base_n pixel position of cp_base_n in the image_matrix
  /// @arg cp_origin_n Complex origin at px_base_n
  void
  set_complex ( geo::Double_Point const & px_base_n,
                fre::Complex_Origin const & cp_origin_n );

  void
  set_iterator ( std::string const & address_n );

  void
  set_iterations_max ( std::uint32_t num_n );

  // -- Tile reading

  std::shared_ptr< Reader > const &
  reader () const
  {
    return _reader;
  }

  private:
  void
  increment_image_code ();

  void
  send_dispatcher_event ( event::Event * event_n );

  private:
  // -- State
  fre::Id_Code _image_code;
  fre::Progress _progress;
  // -- Event loop
  event::bit_collectors::Callback _bit_collector;
  event::queue::Connections_IO _evqc_in;
  event::queue::Connections_IO _evqc_out;
  // -- Event pools
  event::Pool_Tracker _epool_tracker;
  event::Pool< dispatcher::events_in::Abort > _epool_abort;
  event::Pool< dispatcher::events_in::Tile_Sizes > _epool_tile_sizes;
  event::Pool< dispatcher::events_in::Pipeline > _epool_pipeline;
  event::Pool< dispatcher::events_in::Pipeline_Lock > _epool_pipeline_lock;
  event::Pool< dispatcher::events_in::Resize > _epool_resize;
  event::Pool< dispatcher::events_in::Loop > _epool_loop;
  event::Pool< dispatcher::events_in::Complex > _epool_complex;
  event::Pool< dispatcher::events_in::Iterator > _epool_iterator;
  event::Pool< dispatcher::events_in::Iterations > _epool_iterations;
  // -- Engine objects
  std::shared_ptr< Hub > _hub;
  std::shared_ptr< Reader > _reader;
};
} // namespace fre
