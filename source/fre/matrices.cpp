/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "matrices.hpp"
#include <fre/matrix_manipulator.hpp>
#include <application_config.hpp>
#include <message_streams.hpp>

namespace fre
{

Matrices::Matrices () {}

Matrices::~Matrices () {}

void
Matrices::set_tile_sizes ( Tile_Sizes const & tile_sizes_n,
                           std::uint32_t image_code_n )
{
  _image_matrix.set_tile_sizes ( tile_sizes_n, image_code_n );
  _shadow_matrix.set_tile_size ( tile_sizes_n, image_code_n );
  _image_matrix_offset.reset ();
}

void
Matrices::resize_image_matrix (
    Resize_Deltas const & deltas_n,
    shadow_matrix::Tile_Reference_Vector & pickables_n )
{
  std::int32_t & im_ox ( _image_matrix_offset.width_ref () );
  std::int32_t & im_oy ( _image_matrix_offset.height_ref () );
  image_matrix::Matrix::Manipulator im_man ( _image_matrix );
  shadow_matrix::Matrix::Manipulator sh_man ( _shadow_matrix, pickables_n );

  // -- Remove rows
  // Bottom
  if ( deltas_n.vertical ().first () < 0 ) {
    std::uint32_t const delta ( -deltas_n.vertical ().first () );
    DEBUG_ASSERT ( delta <= _image_matrix.rows () );
    im_man.remove_rows_bottom ( delta );
    im_oy += std::int32_t ( delta * _image_matrix.inner_tile_height () );
    shadow_bottom_retreat ( sh_man );
  }
  // Top
  if ( deltas_n.vertical ().second () < 0 ) {
    std::uint32_t const delta ( -deltas_n.vertical ().second () );
    DEBUG_ASSERT ( delta <= _image_matrix.rows () );
    im_man.remove_rows_top ( delta );
    shadow_top_retreat ( sh_man );
  }

  // -- Remove columns
  // Left
  if ( deltas_n.horizontal ().first () < 0 ) {
    std::uint32_t const delta ( -deltas_n.horizontal ().first () );
    DEBUG_ASSERT ( delta <= _image_matrix.columns () );
    im_man.remove_columns_left ( delta );
    im_ox += std::int32_t ( delta * _image_matrix.inner_tile_width () );
    shadow_left_retreat ( sh_man );
  }
  // Right
  if ( deltas_n.horizontal ().second () < 0 ) {
    std::uint32_t const delta ( -deltas_n.horizontal ().second () );
    DEBUG_ASSERT ( delta <= _image_matrix.columns () );
    im_man.remove_columns_right ( delta );
    shadow_right_retreat ( sh_man );
  }

  // -- Add columns
  // Left
  if ( deltas_n.horizontal ().first () > 0 ) {
    acquire_repaintables_left ( pickables_n );
    std::uint32_t const delta ( deltas_n.horizontal ().first () );
    im_man.add_columns_left ( delta );
    im_ox -= std::int32_t ( delta * _image_matrix.inner_tile_width () );
    shadow_left_grow ( sh_man );
  }
  // Right
  if ( deltas_n.horizontal ().second () > 0 ) {
    acquire_repaintables_right ( pickables_n );
    im_man.add_columns_right ( deltas_n.horizontal ().second () );
    shadow_right_grow ( sh_man );
  }

  // -- Add rows
  // Bottom
  if ( deltas_n.vertical ().first () > 0 ) {
    acquire_repaintables_bottom ( pickables_n );
    std::uint32_t const delta ( deltas_n.vertical ().first () );
    im_man.add_rows_bottom ( delta );
    im_oy -= std::int32_t ( delta * _image_matrix.inner_tile_height () );
    shadow_bottom_grow ( sh_man );
  }
  // Top
  if ( deltas_n.vertical ().second () > 0 ) {
    acquire_repaintables_top ( pickables_n );
    im_man.add_rows_top ( deltas_n.vertical ().second () );
    shadow_top_grow ( sh_man );
  }

  std::int32_t ex_right =
      std::int32_t ( _shadow_matrix.size_px ().width () ) -
      std::int32_t ( im_ox + _image_matrix.size_px ().width () );
  std::int32_t ex_top =
      std::int32_t ( _shadow_matrix.size_px ().height () ) -
      std::int32_t ( im_oy + _image_matrix.size_px ().height () );

  if ( DEBUG_FRE_MATRICES ) {
    Debug_Stream oss;
    oss << "Matrices::resize_image_matrix:\n";
    oss << "  Image matrix: tiles " << _image_matrix.size () << " size px "
        << _image_matrix.size_px () << " column_id_ends ("
        << _image_matrix.column_id_ends ().first () << ","
        << _image_matrix.column_id_ends ().second () << ")"
        << " row_id_ends (" << _image_matrix.row_id_ends ().first () << ","
        << _image_matrix.row_id_ends ().second () << ")\n";
    oss << "  Shadow matrix: tiles " << _shadow_matrix.size () << " size px "
        << _shadow_matrix.size_px () << " column_id_ends ("
        << _shadow_matrix.column_id_ends ().first () << ","
        << _shadow_matrix.column_id_ends ().second () << ")"
        << " row_id_ends (" << _shadow_matrix.row_id_ends ().first () << ","
        << _shadow_matrix.row_id_ends ().second () << ")\n";
    oss << "  Image matrix offset " << _image_matrix_offset << "\n";
    oss << "  Shadow matrix overlap: left " << _image_matrix_offset.width ()
        << "  right " << ex_right << "  bottom " << im_oy << "  top " << ex_top
        << "\n";
  }

  DEBUG_ASSERT ( im_ox >= 0 );
  DEBUG_ASSERT ( im_oy >= 0 );
  DEBUG_ASSERT ( ex_right >= 0 );
  DEBUG_ASSERT ( ex_top >= 0 );
}

void
Matrices::loop_horizontal ( std::int32_t loops_n,
                            shadow_matrix::Tile_Reference_Vector & pickables_n )
{
  image_matrix::Matrix::Manipulator im_man ( _image_matrix );
  shadow_matrix::Matrix::Manipulator sh_man ( _shadow_matrix, pickables_n );

  std::int32_t im_tw = _image_matrix.inner_tile_width ();
  std::int32_t sh_tw = _shadow_matrix.tile_width ();
  std::int32_t & im_ox ( _image_matrix_offset.width_ref () );
  if ( loops_n < 0 ) {
    std::int32_t const im_loops ( -loops_n );
    acquire_repaintables_left ( pickables_n );
    im_man.loop_right_to_left ( im_loops );
    // Update image matrix offset and loop shadow matrix
    im_ox -= ( im_loops * im_tw );
    if ( im_ox < 0 ) {
      std::int32_t sh_loops = ( -im_ox + sh_tw - 1 ) / sh_tw;
      im_ox += ( sh_loops * sh_tw );
      sh_man.loop_right_to_left ( sh_loops );
    }
    shadow_right_grow ( sh_man );
  } else if ( loops_n > 0 ) {
    std::int32_t const im_loops ( loops_n );
    acquire_repaintables_right ( pickables_n );
    im_man.loop_left_to_right ( im_loops );
    // Update image matrix offset and loop shadow matrix
    im_ox += im_loops * im_tw;
    if ( im_ox >= sh_tw ) {
      std::int32_t sh_loops = ( im_ox / sh_tw );
      im_ox -= ( sh_loops * sh_tw );
      sh_man.loop_left_to_right ( sh_loops );
    }
    shadow_right_grow ( sh_man );
  }
}

void
Matrices::loop_vertical ( std::int32_t loops_n,
                          shadow_matrix::Tile_Reference_Vector & pickables_n )
{
  image_matrix::Matrix::Manipulator im_man ( _image_matrix );
  shadow_matrix::Matrix::Manipulator sh_man ( _shadow_matrix, pickables_n );

  std::int32_t im_th = _image_matrix.inner_tile_height ();
  std::int32_t sh_th = _shadow_matrix.tile_height ();
  std::int32_t & im_oy ( _image_matrix_offset.height_ref () );
  if ( loops_n < 0 ) {
    // Loop top to bottom
    std::int32_t const im_loops ( -loops_n );
    acquire_repaintables_bottom ( pickables_n );
    im_man.loop_top_to_bottom ( im_loops );
    // Update image matrix offset and loop shadow matrix
    im_oy -= ( im_loops * im_th );
    if ( im_oy < 0 ) {
      std::int32_t sh_loops = ( -im_oy + sh_th - 1 ) / sh_th;
      im_oy += ( sh_loops * sh_th );
      sh_man.loop_top_to_bottom ( sh_loops );
    }
    shadow_top_grow ( sh_man );
  } else if ( loops_n > 0 ) {
    // Loop bottom to top
    std::int32_t const im_loops ( loops_n );
    acquire_repaintables_top ( pickables_n );
    im_man.loop_bottom_to_top ( im_loops );
    // Update image matrix offset and loop shadow matrix
    im_oy += im_loops * im_th;
    if ( im_oy >= sh_th ) {
      std::int32_t sh_loops = ( im_oy / sh_th );
      im_oy -= ( sh_loops * sh_th );
      sh_man.loop_bottom_to_top ( sh_loops );
    }
    shadow_top_grow ( sh_man );
  }
}

void
Matrices::set_image_matrix_complex ( geo::Double_Point const & px_base_n,
                                     fre::Complex_Origin const & cp_origin_n,
                                     std::uint32_t image_code_n )
{
  {
    geo::Double_Point px_base ( px_base_n );
    // Add offset of image_matrix left-bottom inner tile
    // to the shadow-matrix left-bottom tile
    px_base.add ( _image_matrix.tile_overlap ().width (),
                  _image_matrix.tile_overlap ().height () );
    px_base.add ( _image_matrix_offset.width (),
                  _image_matrix_offset.height () );

    _shadow_matrix.set_complex ( px_base, cp_origin_n, image_code_n );
  }
  _image_matrix.set_image_code ( image_code_n );
}

void
Matrices::set_iterator ( fre::fractal::Iterator_Reference const & iterator_n,
                         std::uint32_t image_code_n )
{
  _shadow_matrix.set_iterator ( iterator_n, image_code_n );
  _image_matrix.set_image_code ( image_code_n );
}

inline void
Matrices::acquire_repaintables_columns (
    shadow_matrix::Tile_Reference_Vector & tiles_n,
    std::int32_t sh_px_min_n,
    std::int32_t sh_px_max_n )
{
  std::int32_t cols = _shadow_matrix.columns ();
  if ( cols != 0 ) {
    std::int32_t col ( _shadow_matrix.column_from_px ( sh_px_min_n ) );
    std::int32_t col_end ( _shadow_matrix.column_from_px ( sh_px_max_n ) + 1 );
    col = std::min ( std::max ( 0, col ), cols );
    col_end = std::min ( std::max ( 0, col_end ), cols );
    for ( ; col != col_end; ++col ) {
      _shadow_matrix.acquire_repaintables_column ( tiles_n, col );
    }
  }
}

inline void
Matrices::acquire_repaintables_rows (
    shadow_matrix::Tile_Reference_Vector & tiles_n,
    std::int32_t im_px_min_n,
    std::int32_t im_px_max_n )
{
  std::int32_t rows = _shadow_matrix.rows ();
  if ( rows != 0 ) {
    std::int32_t row ( _shadow_matrix.row_from_px ( im_px_min_n ) );
    std::int32_t row_end ( _shadow_matrix.row_from_px ( im_px_max_n ) + 1 );
    row = std::min ( std::max ( 0, row ), rows );
    row_end = std::min ( std::max ( 0, row_end ), rows );
    for ( ; row != row_end; ++row ) {
      _shadow_matrix.acquire_repaintables_row ( tiles_n, row );
    }
  }
}

void
Matrices::acquire_repaintables_left (
    shadow_matrix::Tile_Reference_Vector & tiles_n )
{
  std::int32_t const tow ( _image_matrix.tile_overlap ().width () );
  if ( tow != 0 ) {
    acquire_repaintables_columns (
        tiles_n, 0, _image_matrix_offset.width () + tow - 1 );
  }
}

void
Matrices::acquire_repaintables_right (
    shadow_matrix::Tile_Reference_Vector & tiles_n )
{
  std::int32_t const tow ( _image_matrix.tile_overlap ().width () );
  if ( tow != 0 ) {
    acquire_repaintables_columns ( tiles_n,
                                   ( _image_matrix_offset.width () +
                                     _image_matrix.size_px ().width () - tow ),
                                   _shadow_matrix.size_px ().width () );
  }
}

void
Matrices::acquire_repaintables_bottom (
    shadow_matrix::Tile_Reference_Vector & tiles_n )
{
  std::int32_t const toh ( _image_matrix.tile_overlap ().height () );
  if ( toh != 0 ) {
    acquire_repaintables_rows (
        tiles_n, 0, _image_matrix_offset.height () + toh - 1 );
  }
}

void
Matrices::acquire_repaintables_top (
    shadow_matrix::Tile_Reference_Vector & tiles_n )
{
  std::int32_t const toh ( _image_matrix.tile_overlap ().height () );
  if ( toh != 0 ) {
    acquire_repaintables_rows ( tiles_n,
                                ( _image_matrix_offset.height () +
                                  _image_matrix.size_px ().height () - toh ),
                                _shadow_matrix.size_px ().height () );
  }
}

void
Matrices::shadow_left_grow ( shadow_matrix::Matrix::Manipulator & manip_n )
{
  std::int32_t & im_ox ( _image_matrix_offset.width_ref () );
  if ( im_ox < 0 ) {
    std::uint32_t const delta ( -im_ox );
    std::uint32_t const sh_tw ( _shadow_matrix.tile_width () );
    std::uint32_t const num = ( delta + sh_tw - 1 ) / sh_tw;
    manip_n.add_columns_left ( num );
    im_ox += std::int32_t ( num * sh_tw );
  }
}

void
Matrices::shadow_left_retreat ( shadow_matrix::Matrix::Manipulator & manip_n )
{
  std::int32_t & im_ox ( _image_matrix_offset.width_ref () );
  if ( im_ox > 0 ) {
    std::uint32_t const delta ( im_ox );
    std::uint32_t const sh_tw ( _shadow_matrix.tile_width () );
    if ( delta > sh_tw ) {
      std::uint32_t const num = delta / sh_tw;
      manip_n.remove_columns_left ( num );
      im_ox -= std::int32_t ( num * sh_tw );
    }
  }
}

void
Matrices::shadow_right_grow ( shadow_matrix::Matrix::Manipulator & manip_n )
{
  std::int32_t const im_end ( _image_matrix_offset.width () +
                              _image_matrix.width_px () );
  std::int32_t const sh_end ( shadow_matrix ().width_px () );
  if ( im_end > sh_end ) {
    std::uint32_t const delta ( im_end - sh_end );
    std::uint32_t const sh_tw ( _shadow_matrix.tile_width () );
    std::uint32_t const num = ( delta + sh_tw - 1 ) / sh_tw;
    manip_n.add_columns_right ( num );
  }
}

void
Matrices::shadow_right_retreat ( shadow_matrix::Matrix::Manipulator & manip_n )
{
  std::int32_t const im_end ( _image_matrix_offset.width () +
                              _image_matrix.width_px () );
  std::int32_t const sh_end ( shadow_matrix ().width_px () );
  if ( sh_end > im_end ) {
    std::uint32_t const delta ( sh_end - im_end );
    std::uint32_t const sh_tw ( _shadow_matrix.tile_width () );
    if ( delta > sh_tw ) {
      std::uint32_t const num = delta / sh_tw;
      manip_n.remove_columns_right ( num );
    }
  }
}

void
Matrices::shadow_bottom_grow ( shadow_matrix::Matrix::Manipulator & manip_n )
{
  std::int32_t & im_oy ( _image_matrix_offset.height_ref () );
  if ( im_oy < 0 ) {
    std::uint32_t const delta ( -im_oy );
    std::uint32_t const sh_th ( _shadow_matrix.tile_height () );
    std::uint32_t const num = ( delta + sh_th - 1 ) / sh_th;
    manip_n.add_rows_bottom ( num );
    im_oy += std::int32_t ( num * sh_th );
  }
}

void
Matrices::shadow_bottom_retreat ( shadow_matrix::Matrix::Manipulator & manip_n )
{
  std::int32_t & im_oy ( _image_matrix_offset.height_ref () );
  if ( im_oy > 0 ) {
    std::uint32_t const delta ( im_oy );
    std::uint32_t const sh_th ( _shadow_matrix.tile_height () );
    if ( delta > sh_th ) {
      std::uint32_t const num = delta / sh_th;
      manip_n.remove_rows_bottom ( num );
      im_oy -= std::int32_t ( num * sh_th );
    }
  }
}

void
Matrices::shadow_top_grow ( shadow_matrix::Matrix::Manipulator & manip_n )
{
  std::int32_t const im_end ( _image_matrix_offset.height () +
                              _image_matrix.height_px () );
  std::int32_t const sh_end ( shadow_matrix ().height_px () );
  if ( im_end > sh_end ) {
    std::uint32_t const delta ( im_end - sh_end );
    std::uint32_t const sh_th ( _shadow_matrix.tile_height () );
    std::uint32_t const num = ( delta + sh_th - 1 ) / sh_th;
    manip_n.add_rows_top ( num );
  }
}

void
Matrices::shadow_top_retreat ( shadow_matrix::Matrix::Manipulator & manip_n )
{
  std::int32_t const im_end ( _image_matrix_offset.height () +
                              _image_matrix.height_px () );
  std::int32_t const sh_end ( shadow_matrix ().height_px () );
  if ( sh_end > im_end ) {
    std::uint32_t const delta ( sh_end - im_end );
    std::uint32_t const sh_th ( _shadow_matrix.tile_height () );
    if ( delta > sh_th ) {
      std::uint32_t const num = delta / sh_th;
      manip_n.remove_rows_top ( num );
    }
  }
}
} // namespace fre
