/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "engine.hpp"
#include <event/bit_collectors/condition.hpp>
#include <event/queue/pair.hpp>
#include <fre/hub.hpp>
#include <message_streams.hpp>

namespace fre
{

Engine::Engine ()
: _epool_abort ( _epool_tracker )
, _epool_tile_sizes ( _epool_tracker )
, _epool_pipeline ( _epool_tracker )
, _epool_pipeline_lock ( _epool_tracker )
, _epool_resize ( _epool_tracker )
, _epool_loop ( _epool_tracker )
, _epool_complex ( _epool_tracker )
, _epool_iterator ( _epool_tracker )
, _epool_iterations ( _epool_tracker )
{
  // Connect dispatcher event queue and start the dispatcher
  {
    event::queue::Pair queues_out;
    event::queue::Pair queues_in;

    _evqc_out.out ().set_push_bit_notifier ( LEvent::DSP_OUT, &_bit_collector );
    _evqc_out.in ().set_bit_notifier ( LEvent::DSP_OUT_RETURN,
                                       &_bit_collector );
    _evqc_out.connect ( queues_out.end_a () );

    _evqc_in.out ().set_push_bit_notifier ( LEvent::DSP_IN_RETURN,
                                            &_bit_collector );
    _evqc_in.in ().set_bit_notifier ( LEvent::DSP_IN, &_bit_collector );
    _evqc_in.connect ( queues_in.end_a () );

    _hub = std::make_shared< Hub > ( queues_out.end_b (), queues_in.end_b () );
  }
  _reader = std::make_shared< Reader > ( _hub );

  _epool_abort.set_capacity ( 0 );
  _epool_tile_sizes.set_capacity ( 0 );
  _epool_pipeline.set_capacity ( 0 );
  _epool_pipeline_lock.set_capacity ( 4 );
  _epool_resize.set_capacity ( 0 );
  _epool_loop.set_capacity ( 4 );
  _epool_complex.set_capacity ( 8 );
  _epool_iterator.set_capacity ( 0 );
  _epool_iterations.set_capacity ( 0 );
}

Engine::~Engine ()
{
  // -- Send abort event and wait until all events have returned
  {
    // Local waiter
    event::bit_collectors::Condition waiter;
    // Replace process callback to local lambda
    set_events_callback ( [ &waiter ] () { waiter.notify_events ( 1 ); } );
    // Send abort avent
    send_dispatcher_event ( _epool_abort.acquire () );
    // Unlink reader
    _reader.reset ();
    // Wait until all events have returned
    while ( !_epool_tracker.all_home () ) {
      waiter.wait_for_events ();
      if ( events_begin () ) {
        while ( events_next () != fre::dispatcher::events_out::Type::NONE ) {
        }
        events_end ();
      }
    }
    // Clear process callback
    set_events_callback ( Callback () );
  }
}

void
Engine::set_events_callback ( Callback && function_n )
{
  _bit_collector.set_callback ( std::move ( function_n ) );
}

bool
Engine::events_begin ()
{
  // Fetch events from event bit collector
  event::Bit_Collector::Int_Type loop_events = 0;
  _bit_collector.fetch_events ( loop_events );

  // Fetch and release returned events
  if ( _evqc_out.in ().read_from_queue_checked ( loop_events ) ) {
    while ( !_evqc_out.in ().is_empty () ) {
      _epool_tracker.casted_reset_release ( _evqc_out.in ().pop_not_empty () );
    }
  }

  _evqc_in.in ().read_from_queue_checked ( loop_events );
  return !_evqc_in.in ().is_empty ();
}

fre::dispatcher::events_out::Type
Engine::events_next ()
{
  typedef fre::dispatcher::events_out::Type EType;
  EType res = EType::NONE;
  while ( ( res == EType::NONE ) && !_evqc_in.in ().is_empty () ) {
    event::Event * event ( _evqc_in.in ().pop_not_empty () );
    EType etype = static_cast< EType > ( event->type () );
    switch ( etype ) {
    case EType::NONE:
      break;
    case EType::PROGRESS: {
      auto * cevent =
          static_cast< fre::dispatcher::events_out::Progress * > ( event );
      if ( cevent->progress.image_code () == _image_code ) {
        _progress = cevent->progress;
        res = etype;
      }
    } break;
    }
    _evqc_in.out ().push ( event );
  }
  return res;
}

void
Engine::events_end ()
{
  _evqc_in.out ().feed_into_queue ();
}

void
Engine::increment_image_code ()
{
  _image_code.increment ();
  _progress.reset ();
}

void
Engine::send_dispatcher_event ( event::Event * event_n )
{
  _evqc_out.out ().push ( event_n );
  _evqc_out.feed_into_queue ();
}

void
Engine::pipeline_connect ( std::shared_ptr< fre::Pipeline > const & pipeline_n )
{
  auto * event = _epool_pipeline.acquire ();
  event->pipeline = pipeline_n;
  send_dispatcher_event ( event );
}

void
Engine::pipeline_disconnect ()
{
  pipeline_connect ( nullptr );
}

void
Engine::pipeline_lock ( bool lock_n )
{
  auto * event = _epool_pipeline_lock.acquire ();
  event->lock = lock_n;
  send_dispatcher_event ( event );
}

void
Engine::set_tile_sizes ( const Tile_Sizes & tile_sizes_n )
{
  increment_image_code ();
  {
    auto * event = _epool_tile_sizes.acquire ();
    event->tile_sizes = tile_sizes_n;
    event->image_code = _image_code;
    send_dispatcher_event ( event );
  }
}

void
Engine::matrix_resize ( Resize_Deltas const & deltas_n )
{
  _progress.reset ();
  {
    auto * event = _epool_resize.acquire ();
    event->deltas = deltas_n;
    send_dispatcher_event ( event );
  }
}

void
Engine::loop ( geo::Int32_Pair const & loops_n )
{
  _progress.reset ();
  {
    auto * event = _epool_loop.acquire ();
    event->loops = loops_n;
    send_dispatcher_event ( event );
  }
}

void
Engine::set_complex ( geo::Double_Point const & px_base_n,
                      fre::Complex_Origin const & cp_origin_n )
{
  increment_image_code ();
  {
    auto * event = _epool_complex.acquire ();
    event->px_base = px_base_n;
    event->cp_origin = cp_origin_n;
    event->image_code = _image_code;
    send_dispatcher_event ( event );
  }
}

void
Engine::set_iterator ( std::string const & address_n )
{
  increment_image_code ();
  {
    auto * event = _epool_iterator.acquire ();
    event->address = address_n;
    event->image_code = _image_code;
    send_dispatcher_event ( event );
  }
}

void
Engine::set_iterations_max ( std::uint32_t num_n )
{
  increment_image_code ();
  {
    auto * event = _epool_iterations.acquire ();
    event->count = num_n;
    event->image_code = _image_code;
    send_dispatcher_event ( event );
  }
}
} // namespace fre
