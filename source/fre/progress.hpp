/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>

namespace fre
{

/// @brief Render progress
///
class Progress
{
  public:
  // -- Constructors

  Progress () = default;

  ~Progress () = default;

  // -- Setters

  void
  reset ()
  {
    _image_code = 0;
    _pixel_count = 0;
    _pixels_rendered = 0;
  }

  // -- Accessors

  std::uint32_t
  image_code () const
  {
    return _image_code;
  }

  void
  set_image_code ( std::uint32_t value_n )
  {
    _image_code = value_n;
  }

  std::uint32_t
  pixel_count () const
  {
    return _pixel_count;
  }

  void
  set_pixel_count ( std::uint32_t value_n )
  {
    _pixel_count = value_n;
  }

  std::uint32_t
  pixels_rendered () const
  {
    return _pixels_rendered;
  }

  void
  set_pixels_rendered ( std::uint32_t value_n )
  {
    _pixels_rendered = value_n;
  }

  double
  normalized () const
  {
    return ( double ( _pixels_rendered ) / double ( _pixel_count ) );
  }

  // -- Comparison operators

  bool
  operator== ( Progress const & prog_n ) const
  {
    return ( ( _image_code == prog_n._image_code ) &&
             ( _pixel_count == prog_n._pixel_count ) &&
             ( _pixels_rendered == prog_n._pixels_rendered ) );
  }

  bool
  operator!= ( Progress const & prog_n ) const
  {
    return ( ( _image_code != prog_n._image_code ) ||
             ( _pixel_count != prog_n._pixel_count ) ||
             ( _pixels_rendered != prog_n._pixels_rendered ) );
  }

  private:
  std::uint32_t _image_code = 0;
  std::uint32_t _pixel_count = 0;
  std::uint32_t _pixels_rendered = 0;
};
} // namespace fre
