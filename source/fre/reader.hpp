/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_collectors/callback.hpp>
#include <event/queue/connections_io.hpp>
#include <fre/image_matrix/tile_buffer.hpp>
#include <functional>
#include <memory>

namespace fre
{

// -- Forward declaration
class Hub;

/// @brief Asynchronous image tile reader
///
class Reader
{
  public:
  // -- Types
  typedef std::function< void () > Callback;

  public:
  // -- Constructors

  Reader ( std::shared_ptr< Hub > const & hub_n );

  ~Reader ();

  // -- Callback

  void
  set_callback ( Callback const & callback_n );

  void
  set_callback ( Callback && callback_n );

  Callback const &
  callback () const
  {
    return _bit_collector.callback ();
  }

  // -- Tile fetching

  /// @return True if there is something to fetch
  bool
  fetch_begin ();

  bool
  fetchable () const
  {
    return !_evqc.in ().is_empty ();
  }

  fre::image_matrix::Tile_Buffer const *
  fetch_next ();

  void
  fetch_accept ();

  void
  fetch_end ();

  // -- State

  std::uint32_t
  image_code () const
  {
    return _image_code;
  }

  void
  set_image_code ( std::uint32_t code_n )
  {
    _image_code = code_n;
  }

  bool
  is_aborted () const
  {
    return !_evqc.in ().is_connected ();
  }

  private:
  std::uint32_t _image_code = 0;
  event::bit_collectors::Callback _bit_collector;
  event::queue::Connections_IO _evqc;
  std::shared_ptr< Hub > _hub;
};
} // namespace fre
