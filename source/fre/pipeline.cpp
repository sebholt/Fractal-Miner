/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "pipeline.hpp"
#include <event/queue/reference.hpp>
#include <fre/pipeline/feeder_thread.hpp>
#include <fre/pipeline/iterator_thread.hpp>
#include <fre/pipeline/painter_thread.hpp>
#include <fre/pipeline/pool.hpp>
#include <application_config.hpp>
#include <message_streams.hpp>

namespace fre
{

Pipeline::Pipeline ()
{
  _pool = std::make_unique< pipeline::Pool > ();
}

Pipeline::~Pipeline ()
{
  stop ();
}

bool
Pipeline::start ( std::uint32_t concurrency_n )
{
  _concurrency = concurrency_n;
  if ( _concurrency == 0 ) {
    _concurrency = std::thread::hardware_concurrency ();
  }
  _concurrency = std::max< std::uint32_t > ( _concurrency, 1 );
  _concurrency = std::min< std::uint32_t > ( _concurrency, 128 );

  if ( !is_running () && ( _concurrency != 0 ) ) {

    if ( DEBUG_FRE_PIPELINE ) {
      Debug_Stream oss;
      oss << "Pipeline::start: concurrency: " << _concurrency << "\n";
    }

    _feeders.reserve ( _concurrency );
    _iterators.reserve ( _concurrency );
    _painters.reserve ( _concurrency );

    for ( std::uint32_t ii = 0; ii != _concurrency; ++ii ) {
      auto feeder = std::make_unique< pipeline::Feeder_Thread > ( *_pool );
      auto iterator = std::make_unique< pipeline::Iterator_Thread > ();
      auto painter = std::make_unique< pipeline::Painter_Thread > ();
      // Create connecting queues and start
      {
        auto queue_feeder = event::queue::Reference::created ();
        auto queue_painter = event::queue::Reference::created ();
        // Start the threads
        feeder->start ( queue_feeder );
        iterator->start ( queue_feeder, queue_painter );
        painter->start ( queue_painter );
      }
      _feeders.emplace_back ( std::move ( feeder ) );
      _iterators.emplace_back ( std::move ( iterator ) );
      _painters.emplace_back ( std::move ( painter ) );
    }
    return true;
  }
  return false;
}

void
Pipeline::abort ()
{
  if ( is_running () ) {
    for ( auto & item : _feeders ) {
      item->abort ();
    }
    _pool->abort ();
    for ( auto & item : _iterators ) {
      item->abort ();
    }
    for ( auto & item : _painters ) {
      item->abort ();
    }
  }
}

void
Pipeline::join ()
{
  if ( is_running () ) {
    if ( DEBUG_FRE_PIPELINE ) {
      Debug_Stream () << "Pipeline: Joining pipeline threads begin\n";
    }

    if ( DEBUG_FRE_PIPELINE ) {
      Debug_Stream () << "Pipeline: Joining painter threads begin\n";
    }
    _painters.clear ();
    if ( DEBUG_FRE_PIPELINE ) {
      Debug_Stream () << "Pipeline: Joining painter threads done\n";
    }

    if ( DEBUG_FRE_PIPELINE ) {
      Debug_Stream () << "Pipeline: Joining iterator threads begin\n";
    }
    _iterators.clear ();
    if ( DEBUG_FRE_PIPELINE ) {
      Debug_Stream () << "Pipeline: Joining iterator threads done\n";
    }

    if ( DEBUG_FRE_PIPELINE ) {
      Debug_Stream () << "Pipeline: Joining feeder threads begin\n";
    }
    _feeders.clear ();
    if ( DEBUG_FRE_PIPELINE ) {
      Debug_Stream () << "Pipeline: Joining feeder threads done\n";
    }

    if ( DEBUG_FRE_PIPELINE ) {
      Debug_Stream () << "Pipeline: Joining pipeline threads done\n";
    }
  }
}

void
Pipeline::stop ()
{
  abort ();
  join ();
}
} // namespace fre
