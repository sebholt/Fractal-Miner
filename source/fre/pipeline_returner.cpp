/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "pipeline_returner.hpp"
#include <event/queue/queue.hpp>

namespace fre
{

Pipeline_Returner::Pipeline_Returner ()
: _queue ( event::queue::Reference::created () )
{
}

void
Pipeline_Returner::feed ( event::Event * event_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  _queue->push ( event_n );
}
} // namespace fre
