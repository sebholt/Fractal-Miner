/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>
#include <limits>

namespace fre
{

/// @brief Creates incrementing integer ids != 0
///
class Id_Code
{
  public:
  // -- Types
  static constexpr std::uint32_t hlimit =
      std::numeric_limits< std::uint32_t >::max () / 2;

  public:
  // -- Constructors

  Id_Code ( std::uint32_t id_n = 0 )
  : _id ( id_n )
  {
  }

  // -- Accessors

  bool
  is_valid () const
  {
    return ( _id != 0u );
  }

  void
  reset ( std::uint32_t id_n = 0u )
  {
    _id = id_n;
  }

  std::uint32_t
  get () const
  {
    return _id;
  }

  void
  increment ()
  {
    ++_id;
    if ( _id == 0u ) {
      ++_id;
    }
  }

  // -- Comparison operators

  bool
  operator< ( std::uint32_t id_n ) const
  {
    return ( _id - id_n ) > hlimit;
  }

  bool
  operator> ( std::uint32_t id_n ) const
  {
    return ( id_n - _id ) > hlimit;
  }

  bool
  operator== ( std::uint32_t id_n ) const
  {
    return _id == id_n;
  }

  bool
  operator!= ( std::uint32_t id_n ) const
  {
    return _id != id_n;
  }

  operator std::uint32_t () const { return _id; }

  private:
  std::uint32_t _id;
};
} // namespace fre
