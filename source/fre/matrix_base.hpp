/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/resize_deltas.hpp>
#include <geo/geo.hpp>

namespace fre
{

/// @brief Matrices base class
///
class Matrix_Base
{
  public:
  // -- Constructors

  Matrix_Base () = default;

  // -- Rows and Columns

  /// @brief Matrix size in columns and rows
  geo::UInt32_Size const &
  size () const
  {
    return _size;
  }

  std::uint32_t
  columns () const
  {
    return _size.width ();
  }

  std::uint32_t
  rows () const
  {
    return _size.height ();
  }

  std::uint32_t
  num_tiles () const
  {
    return _size.area< std::uint32_t > ();
  }

  geo::Int32_Pair const &
  column_id_ends () const
  {
    return _column_id_ends;
  }

  geo::Int32_Pair const &
  row_id_ends () const
  {
    return _row_id_ends;
  }

  // -- Pixels

  /// @brief Matrix size in pixel
  geo::UInt32_Size const &
  size_px () const
  {
    return _size_px;
  }

  std::uint32_t
  width_px () const
  {
    return _size_px.width ();
  }

  std::uint32_t
  height_px () const
  {
    return _size_px.height ();
  }

  protected:
  void
  reset ()
  {
    _size = { 0, 0 };
    _column_id_ends = { -1, 0 };
    _row_id_ends = { -1, 0 };
    _size_px = { 0, 0 };
  }

  protected:
  geo::UInt32_Size _size = { 0, 0 };
  geo::Int32_Pair _column_id_ends = { -1, 0 };
  geo::Int32_Pair _row_id_ends = { -1, 0 };
  geo::UInt32_Size _size_px = { 0, 0 };
};
} // namespace fre
