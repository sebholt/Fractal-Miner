/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <geo/geo.hpp>

namespace fre
{

/// @brief Matrix tile sizes
///
class Tile_Sizes
{
  public:
  geo::UInt16_Size
  image_inner_tile_size () const
  {
    return geo::UInt16_Size (
        image_tile_size.width () - 2 * image_tile_overlap.width (),
        image_tile_size.height () - 2 * image_tile_overlap.height () );
  }

  public:
  geo::UInt16_Size shadow_tile_size;
  geo::UInt16_Size image_tile_size;
  geo::UInt16_Size image_tile_overlap;
};
} // namespace fre
