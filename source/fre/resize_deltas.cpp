/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <fre/resize_deltas.hpp>

namespace fre
{

std::ostream &
operator<< ( std::ostream & os_n, Resize_Deltas const & deltas_n )
{
  return os_n << "horizontal=" << deltas_n.horizontal ()
              << " vertical=" << deltas_n.vertical ();
}
} // namespace fre
