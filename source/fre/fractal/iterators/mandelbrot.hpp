/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/fractal/iterator.hpp>

namespace fre::fractal::iterators
{

/// @brief Mandelbrot iterator
///
class Mandelbrot : public Iterator
{
  public:
  // -- Constructors

  Mandelbrot ();

  ~Mandelbrot () override;

  // -- Iteration parameters

  double
  escape_radius () const
  {
    return _escape_radius;
  }

  void
  set_escape_radius ( double radius_n )
  {
    if ( _escape_radius != radius_n ) {
      _escape_radius = radius_n;
      _escape_radius_square = radius_n * radius_n;
    }
  }

  double
  escape_radius_square () const
  {
    return _escape_radius_square;
  }

  // -- Iteration interface

  void
  iterate ( Iter_Args args_n ) const override;

  private:
  double _escape_radius_square = 4.0;
  double _escape_radius = 2.0;
};
} // namespace fre::fractal::iterators
