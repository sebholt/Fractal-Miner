/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "mandelbrot.hpp"
#include <fre/shadow_matrix/commons.hpp>
#include <fre/shadow_matrix/tile_buffer.hpp>

namespace fre::fractal::iterators
{

Mandelbrot::Mandelbrot () {}

Mandelbrot::~Mandelbrot () {}

void
Mandelbrot::iterate ( Iter_Args args_n ) const
{
  // Stack local parameter copy
  std::uint16_t * output = args_n.buffer.data ();
  std::uint_fast32_t const max_it = max_iterations ();
  double const escape_rad_square ( escape_radius_square () );
  std::complex< double > const complex_base ( args_n.buffer.complex_base () );

  std::uint_fast32_t const iterations_limit =
      args_n.com.session_iterations_limit ();
  std::uint_fast32_t iterations_sum = 0;

  auto const * cpo ( args_n.com.complex_offsets ()->data () );
  auto pxob ( args_n.com.pixel_offsets ()->cbegin () );
  auto pxoe ( args_n.com.pixel_offsets ()->cend () );
  auto pxo ( pxob + args_n.buffer.pixels_rendered () );
  while ( ( pxo != pxoe ) && ( iterations_sum < iterations_limit ) ) {
    std::uintptr_t px_index = *pxo;
    ++pxo;
    // Keep all iteration variables close together
    std::uint_fast32_t iterations = max_it;
    double const esc_rad_square = escape_rad_square;
    std::complex< double > const cp_pos ( complex_base + cpo[ px_index ] );
    std::complex< double > cpr ( 0.0, 0.0 );
    // Iterate
    while ( iterations != 0 ) {
      --iterations;
      cpr *= cpr;
      cpr += cp_pos;
      if ( std::norm ( cpr ) >= esc_rad_square ) {
        break;
      }
    }
    // Evaluate iterations
    iterations = ( max_it - iterations );
    output[ px_index ] = iterations;
    iterations_sum += iterations;
  }

  args_n.buffer.set_pixels_rendered ( std::distance ( pxob, pxo ) );
}
} // namespace fre::fractal::iterators
