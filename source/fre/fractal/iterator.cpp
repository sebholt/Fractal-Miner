/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "iterator.hpp"
#include <algorithm>

namespace fre::fractal
{

Iterator::Iterator () {}

Iterator::~Iterator () {}

void
Iterator::iterate ( Iter_Args args_n ) const
{
  (void)args_n;
}
} // namespace fre::fractal
