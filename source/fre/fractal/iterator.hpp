/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>
#include <memory>

// -- Forward declaration
namespace fre::shadow_matrix
{
class Commons;
class Tile_Buffer;
} // namespace fre::shadow_matrix

namespace fre::fractal
{

// -- Forward declaration
class Iterator;
typedef std::shared_ptr< Iterator > Iterator_Reference;

/// @brief Abstract iterator base class
///
class Iterator
{
  public:
  // -- Types
  /// @brief Iteration function arguments
  struct Iter_Args
  {
    fre::shadow_matrix::Commons const & com;
    fre::shadow_matrix::Tile_Buffer & buffer;
  };

  public:
  // -- Constructors

  Iterator ();

  virtual ~Iterator ();

  // -- Accessors

  std::uint32_t
  max_iterations () const
  {
    return _max_iterations;
  }

  void
  set_max_iterations ( std::uint32_t num_n )
  {
    _max_iterations = num_n;
  }

  // -- Iteration interface

  /// @brief Iteration function
  virtual void
  iterate ( Iter_Args args_n ) const;

  private:
  std::uint32_t _max_iterations = 0;
};

// -- Types
typedef std::shared_ptr< Iterator > Iterator_Reference;
} // namespace fre::fractal
