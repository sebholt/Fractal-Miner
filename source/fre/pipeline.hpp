/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>
#include <memory>
#include <vector>

namespace fre
{

// -- Forward declaration
namespace pipeline
{
class Pool;
class Feeder_Thread;
class Iterator_Thread;
class Painter_Thread;
} // namespace pipeline

/// @brief Parallel processing tile pipeline
///
class Pipeline
{
  public:
  // -- Constructors

  Pipeline ();

  ~Pipeline ();

  // -- Accessors

  bool
  is_running () const
  {
    return ( _feeders.size () != 0 );
  }

  std::uint32_t
  concurrency () const
  {
    return _concurrency;
  }

  pipeline::Pool *
  pool () const
  {
    return _pool.get ();
  }

  // -- Thread run control

  bool
  start ( std::uint32_t concurrency_n = 0 );

  void
  abort ();

  void
  join ();

  void
  stop ();

  private:
  std::uint32_t _concurrency = 0;
  std::unique_ptr< pipeline::Pool > _pool;
  std::vector< std::unique_ptr< pipeline::Feeder_Thread > > _feeders;
  std::vector< std::unique_ptr< pipeline::Iterator_Thread > > _iterators;
  std::vector< std::unique_ptr< pipeline::Painter_Thread > > _painters;
};
} // namespace fre
