/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/pipeline/pool_entry.hpp>
#include <cstdint>
#include <memory>

// -- Forward declaration
namespace fre::pipeline
{
class Pool;
}

namespace fre
{

// -- Forward declaration
class Hub;
class Pipeline;

/// @brief Pipeline client
///
class Pipeline_Client
{
  public:
  // -- Constructors

  Pipeline_Client ( Hub & hub_n );

  ~Pipeline_Client ();

  // -- Pool connection

  pipeline::Pool *
  pool () const
  {
    return _pool;
  }

  bool
  is_connected () const
  {
    return ( _pool != nullptr );
  }

  void
  connect ( std::shared_ptr< Pipeline > const & pipeline_n );

  void
  disconnect ();

  /// @brief Notify the pipeline that there is more to fetch
  void
  notify ();

  public:
  pipeline::Pool * _pool = nullptr;
  std::unique_ptr< pipeline::Pool_Entry > _pool_entry;
  std::shared_ptr< Pipeline > _pipeline;
};
} // namespace fre
