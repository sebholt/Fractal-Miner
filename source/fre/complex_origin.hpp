/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <array>
#include <complex>
#include <cstdint>
#include <ostream>

namespace fre
{

/// @brief Complex_Origin
///
class Complex_Origin
{
  public:
  // -- Types
  typedef std::complex< double > Complex;

  public:
  // -- Constructors

  Complex_Origin ()
  : _complex{
        { Complex ( 0.0, 0.0 ), Complex ( 1.0, 0.0 ), Complex ( 0.0, 1.0 ) } }
  {
  }

  Complex_Origin ( Complex const & base_n,
                   Complex const & dx_n,
                   Complex const & dy_n )
  : _complex{ { base_n, dx_n, dy_n } }
  {
  }

  ~Complex_Origin () = default;

  // -- Base

  Complex const &
  base () const
  {
    return _complex[ 0 ];
  }

  Complex &
  base_ref ()
  {
    return _complex[ 0 ];
  }

  void
  set_base ( Complex const & base_n )
  {
    _complex[ 0 ] = base_n;
  }

  // -- X unit

  Complex const &
  dx () const
  {
    return _complex[ 1 ];
  }

  Complex &
  dx_ref ()
  {
    return _complex[ 1 ];
  }

  void
  set_dx ( Complex const & dx_n )
  {
    _complex[ 1 ] = dx_n;
  }

  // -- Y unit

  Complex const &
  dy () const
  {
    return _complex[ 2 ];
  }

  Complex &
  dy_ref ()
  {
    return _complex[ 2 ];
  }

  void
  set_dy ( Complex const & dy_n )
  {
    _complex[ 2 ] = dy_n;
  }

  // -- Setters

  void
  reset ()
  {
    _complex[ 0 ] = Complex ( 0.0, 0.0 );
    _complex[ 1 ] = Complex ( 1.0, 0.0 );
    _complex[ 2 ] = Complex ( 0.0, 1.0 );
  }

  void
  set ( Complex const & base_n, Complex const & dx_n, Complex const & dy_n )
  {
    _complex[ 0 ] = base_n;
    _complex[ 1 ] = dx_n;
    _complex[ 2 ] = dy_n;
  }

  // -- Readers

  Complex
  at ( double x_n, double y_n ) const
  {
    Complex res = dx () * x_n;
    res += dy () * y_n;
    res += base ();
    return res;
  }

  // -- Comparison operators

  bool
  operator== ( Complex_Origin const & orig_n ) const
  {
    return ( ( _complex[ 0 ] == orig_n._complex[ 0 ] ) &&
             ( _complex[ 1 ] == orig_n._complex[ 1 ] ) &&
             ( _complex[ 2 ] == orig_n._complex[ 2 ] ) );
  }

  bool
  operator!= ( Complex_Origin const & orig_n ) const
  {
    return ( ( _complex[ 0 ] != orig_n._complex[ 0 ] ) ||
             ( _complex[ 1 ] != orig_n._complex[ 1 ] ) ||
             ( _complex[ 2 ] != orig_n._complex[ 2 ] ) );
  }

  // -- Subscript operators

  Complex const &
  operator[] ( std::size_t index_n ) const
  {
    return _complex[ index_n ];
  }

  Complex &
  operator[] ( std::size_t index_n )
  {
    return _complex[ index_n ];
  }

  private:
  std::array< Complex, 3 > _complex;
};

// -- ostream adaptors
std::ostream &
operator<< ( std::ostream & os_n, Complex_Origin const & cp_n );
} // namespace fre
