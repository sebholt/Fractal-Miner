/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "matrix.hpp"
#include <debug_assert.hpp>
#include <message_streams.hpp>

namespace fre::image_matrix
{

Matrix::Matrix () {}

Tile_Reference
Matrix::tile_create ( geo::Int32_Point const & pos_n )
{
  Tile_Reference tile ( Tile_Reference::created ( tile_size () ) );
  // Initialize state
  tile->id_ref ().set_pos ( pos_n );
  tile->id_ref ().set_image_code ( _image_code );
  return tile;
}

void
Matrix::compute_size_px ()
{
  if ( columns () == 0 ) {
    _size_px.set_width ( 0 );
  } else {
    _size_px.set_width ( ( columns () * inner_tile_width () ) +
                         2 * tile_overlap ().width () );
  }

  if ( rows () == 0 ) {
    _size_px.set_height ( 0 );
  } else {
    _size_px.set_height ( ( rows () * inner_tile_height () ) +
                          2 * tile_overlap ().height () );
  }
}

void
Matrix::reset ( std::uint32_t image_code_n )
{
  if ( !_tile_rows.empty () ) {
    for ( Tile_Row const & row : _tile_rows ) {
      for ( Tile_Reference const & ref : row ) {
        tile_invalidate ( ref );
      }
    }
    _tile_rows.clear ();
  }
  _tile_size.reset ();
  _tile_overlap.reset ();
  _inner_tile_size.reset ();
  _image_code = image_code_n;
  Matrix_Base::reset ();
}

void
Matrix::set_tile_sizes ( const Tile_Sizes & tile_sizes_n,
                         std::uint32_t image_code_n )
{
  // Reset
  reset ( image_code_n );
  // Change
  _tile_size = tile_sizes_n.image_tile_size;
  _tile_overlap = tile_sizes_n.image_tile_overlap;
  _inner_tile_size = tile_sizes_n.image_inner_tile_size ();
}

void
Matrix::set_image_code ( std::uint32_t image_code_n )
{
  if ( _image_code != image_code_n ) {
    _image_code = image_code_n;
    for ( Tile_Row const & row : _tile_rows ) {
      for ( Tile_Reference const & tile_ref : row ) {
        tile_ref->id_ref ().set_image_code ( image_code_n );
      }
    }
  }
}
} // namespace fre::image_matrix
