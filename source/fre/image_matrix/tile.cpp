/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "tile.hpp"

namespace fre::image_matrix
{

Tile::Tile ( geo::UInt16_Size tile_size_n )
: _buffer ( Tile_Buffer_Reference::created ( tile_size_n ) )
{
}
} // namespace fre::image_matrix
