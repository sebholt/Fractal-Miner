/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/image_matrix/tile.hpp>
#include <fre/matrix_base.hpp>
#include <fre/resize_deltas.hpp>
#include <fre/tile_sizes.hpp>
#include <debug_assert.hpp>
#include <deque>
#include <utility>

// -- Forward declaration
namespace fre
{
template < class MT, bool RN >
class Matrix_Manipulator;
}

namespace fre::image_matrix
{

/// @brief Image tile matrix
///
class Matrix : public Matrix_Base
{
  public:
  // -- Types
  typedef fre::image_matrix::Tile_Reference Tile_Reference;
  typedef std::deque< Tile_Reference > Tile_Row;
  typedef std::deque< Tile_Row > Tile_Rows;
  typedef Matrix_Manipulator< Matrix, false > Manipulator;
  // -- Friends
  friend Manipulator;

  public:
  // -- Constructors

  Matrix ();

  // -- Tile size

  /// @brief Tile size
  geo::UInt16_Size const &
  tile_size () const
  {
    return _tile_size;
  }

  /// @brief Tile width
  int16_t
  tile_width () const
  {
    return _tile_size.width ();
  }

  /// @brief Tile height
  int16_t
  tile_height () const
  {
    return _tile_size.height ();
  }

  // -- Tile overlap

  /// @brief Tile overlap
  geo::UInt16_Size const &
  tile_overlap () const
  {
    return _tile_overlap;
  }

  // -- Tile size

  /// @brief Tile size
  geo::UInt16_Size const &
  inner_tile_size () const
  {
    return _inner_tile_size;
  }

  // -- Inner tile size

  /// @brief Tile width
  int16_t
  inner_tile_width () const
  {
    return _inner_tile_size.width ();
  }

  /// @brief Tile height
  int16_t
  inner_tile_height () const
  {
    return _inner_tile_size.height ();
  }

  // -- Tile sizes setting

  void
  set_tile_sizes ( const Tile_Sizes & tile_sizes_n,
                   std::uint32_t image_code_n );

  // -- Column / Row from px pos

  std::int32_t
  local_column_from_px_left ( std::int32_t px_pos_n ) const
  {
    if ( px_pos_n < 0 ) {
      return -1;
    }
    std::int32_t const tow ( tile_overlap ().width () * 2 );
    std::int32_t res =
        ( px_pos_n - tow ) / std::int32_t ( inner_tile_width () );
    if ( res < std::int32_t ( _size.width () ) ) {
      return res;
    }
    return -1;
  }

  std::int32_t
  local_column_from_px_right ( std::int32_t px_pos_n ) const
  {
    if ( px_pos_n < 0 ) {
      return -1;
    }
    int res = px_pos_n / std::int32_t ( inner_tile_width () );
    if ( res < std::int32_t ( _size.width () ) ) {
      return res;
    }
    return -1;
  }

  std::int32_t
  local_row_from_px_bottom ( std::int32_t px_pos_n ) const
  {
    if ( px_pos_n < 0 ) {
      return -1;
    }
    std::int32_t const toh ( tile_overlap ().height () * 2 );
    std::int32_t res =
        ( px_pos_n - toh ) / std::int32_t ( inner_tile_height () );
    if ( res < std::int32_t ( _size.height () ) ) {
      return res;
    }
    return -1;
  }

  std::int32_t
  local_row_from_px_top ( std::int32_t px_pos_n ) const
  {
    if ( px_pos_n < 0 ) {
      return -1;
    }
    std::int32_t res = px_pos_n / std::int32_t ( inner_tile_height () );
    if ( res < std::int32_t ( _size.height () ) ) {
      return res;
    }
    return -1;
  }

  // -- Pixels

  geo::Int32_Point
  tile_pos_px ( std::int32_t col_n, std::int32_t row_n ) const
  {
    return geo::Int32_Point ( col_n * std::int32_t ( inner_tile_width () ),
                              row_n * std::int32_t ( inner_tile_height () ) );
  }

  geo::Int32_Region
  tile_region_px ( std::int32_t col_n, std::int32_t row_n ) const
  {
    return geo::Int32_Region ( col_n * std::int32_t ( inner_tile_width () ),
                               row_n * std::int32_t ( inner_tile_height () ),
                               tile_width (),
                               tile_height () );
  }

  // -- Tile access

  Tile_Reference const &
  tile ( std::uint32_t col_n, std::uint32_t row_n ) const
  {
    DEBUG_ASSERT ( col_n < columns () );
    DEBUG_ASSERT ( row_n < rows () );
    return _tile_rows[ row_n ][ col_n ];
  }

  // -- Image code

  std::uint32_t
  image_code () const
  {
    return _image_code;
  }

  /// @brief Sets the image code in the matrix and all tiles
  void
  set_image_code ( std::uint32_t image_code_n );

  private:
  // -- Utility

  void
  reset ( std::uint32_t image_code_n );

  // -- Matrix manipulator interface

  void
  compute_size_px ();

  Tile_Reference
  tile_create ( geo::Int32_Point const & pos_n );

  void
  tile_invalidate ( Tile_Reference const & tile_n )
  {
    tile_n->id_ref ().set_image_code ( 0 );
  }

  void
  tile_change_column ( Tile_Reference const & tile_n, std::int32_t column_n )
  {
    tile_n->id_ref ().set_column ( column_n );
    tile_n->id_ref ().increment_tile_code ();
  }

  void
  tile_change_row ( Tile_Reference const & tile_n, std::int32_t row_n )
  {
    tile_n->id_ref ().set_row ( row_n );
    tile_n->id_ref ().increment_tile_code ();
  }

  private:
  geo::UInt16_Size _tile_size;
  geo::UInt16_Size _tile_overlap;
  geo::UInt16_Size _inner_tile_size;
  std::uint32_t _image_code = 0;
  Tile_Rows _tile_rows;
};
} // namespace fre::image_matrix
