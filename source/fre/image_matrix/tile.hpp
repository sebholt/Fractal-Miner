/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/image_matrix/tile_buffer.hpp>
#include <fre/image_matrix/tile_identifier.hpp>
#include <fre/reference.hpp>
#include <geo/geo.hpp>
#include <memory>

namespace fre::image_matrix
{

// -- Forward declaration
class Tile;
typedef fre::Reference< Tile > Tile_Reference;

/// @brief Image tile
///
class Tile
{
  public:
  // -- Types
  typedef std::vector< std::uint32_t > Buffer;
  // -- Friends
  friend Tile_Reference;

  public:
  // -- Constructors

  Tile ( geo::UInt16_Size tile_size_n );

  ~Tile () = default;

  // -- Tile identifier

  Tile_Identifier &
  id_ref ()
  {
    return _id;
  }

  Tile_Identifier const &
  id () const
  {
    return _id;
  }

  // -- Buffer

  Tile_Buffer_Reference &
  buffer_ref ()
  {
    return _buffer;
  }

  const Tile_Buffer_Reference &
  buffer () const
  {
    return _buffer;
  }

  private:
  Tile_Identifier _id;
  Tile_Buffer_Reference _buffer;
  std::uint32_t _reference_count = 0;
};
} // namespace fre::image_matrix
