/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/image_matrix/tile_identifier.hpp>
#include <fre/reference.hpp>
#include <geo/geo.hpp>
#include <atomic>
#include <condition_variable>
#include <debug_assert.hpp>
#include <mutex>
#include <vector>

namespace fre::image_matrix
{

// -- Forward declaration
class Tile_Buffer;
typedef fre::Reference< Tile_Buffer > Tile_Buffer_Reference;

/// @brief Image tile pixel buffer
///
class Tile_Buffer
{
  public:
  // -- Types
  typedef std::vector< std::uint16_t > Buffer;
  // -- Friends
  friend Tile_Buffer_Reference;

  public:
  // -- Constructors

  Tile_Buffer ( geo::UInt16_Size tile_size_n );

  // -- Locking

  std::unique_lock< std::mutex >
  acquire_lock ()
  {
    std::unique_lock< std::mutex > lock ( _mutex );
    while ( _upload_lock ) {
      _upload_lock_cond.wait ( lock );
    }
    return lock;
  }

  bool
  upload_lock_install ( std::uint32_t image_code_n )
  {
    bool res = false;
    {
      std::lock_guard< std::mutex > lock ( _mutex );
      DEBUG_ASSERT ( _upload_registered );
      DEBUG_ASSERT ( !_upload_lock );
      _upload_registered = false;
      _upload_lock = ( tile_id ().image_code () == image_code_n );
      res = _upload_lock;
    }
    return res;
  }

  void
  upload_lock_release ()
  {
    {
      std::lock_guard< std::mutex > lock ( _mutex );
      DEBUG_ASSERT ( _upload_lock );
      _upload_lock = false;
    }
    _upload_lock_cond.notify_all ();
  }

  // -- Upload registration

  bool
  upload_registered () const
  {
    return _upload_registered;
  }

  void
  set_upload_registered ( bool flag_n )
  {
    _upload_registered = flag_n;
  }

  // -- Tile identifier

  Tile_Identifier const &
  tile_id () const
  {
    return _tile_id;
  }

  void
  set_tile_id ( Tile_Identifier const & code_n )
  {
    _tile_id = code_n;
  }

  // -- Tile size

  geo::UInt16_Size const &
  tile_size () const
  {
    return _tile_size;
  }

  std::uint16_t
  tile_width () const
  {
    return _tile_size.width ();
  }

  std::uint16_t
  tile_height () const
  {
    return _tile_size.height ();
  }

  // -- Buffer

  std::size_t
  buffer_size () const
  {
    return _buffer.size ();
  }

  Buffer::iterator
  buffer_begin ()
  {
    return _buffer.begin ();
  }

  Buffer::iterator
  buffer_end ()
  {
    return _buffer.end ();
  }

  Buffer::value_type *
  data ()
  {
    return _buffer.data ();
  }

  Buffer::value_type const *
  data () const
  {
    return _buffer.data ();
  }

  Buffer::value_type const *
  cdata () const
  {
    return _buffer.data ();
  }

  private:
  std::mutex _mutex;
  bool _upload_registered = false;
  bool _upload_lock = false;
  std::condition_variable _upload_lock_cond;
  Tile_Identifier _tile_id;
  geo::UInt16_Size _tile_size;
  Buffer _buffer;
  std::atomic_uint _reference_count = 0;
};
} // namespace fre::image_matrix
