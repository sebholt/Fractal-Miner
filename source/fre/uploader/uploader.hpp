/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_collectors/condition.hpp>
#include <event/pool.hpp>
#include <event/pool_tracker.hpp>
#include <event/queue/connections_io.hpp>
#include <event/queue/reference.hpp>
#include <event/timer_queue/client.hpp>
#include <event/timer_queue/queue.hpp>
#include <fre/image_matrix/tile_buffer.hpp>
#include <fre/uploader/events.hpp>
#include <mutex>
#include <thread>

namespace fre::uploader
{

/// @brief Uploads image tiles to the Reader
///
class Uploader
{
  public:
  // -- Types
  enum LEvent
  {
    TILES_RETURN = 1 << 0,
    TILES_SENDABLE = 1 << 1,
    TILES_SUBMIT = 1 << 2
  };

  public:
  // -- Constructors

  Uploader ();

  ~Uploader ();

  // -- Dispatcher interface

  void
  dispatcher_abort ();

  void
  dispatcher_set_image_code ( std::uint32_t image_code_n );

  void
  dispatcher_set_event_pool_capacity ( std::uint32_t capacity_n );

  // -- Painter interface

  void
  painter_upload ( fre::image_matrix::Tile_Buffer_Reference & buffer_ref_n )
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    ( this->*_m_painter_upload ) ( buffer_ref_n );
  }

  // -- Reader interface

  event::queue::Reference_Pair const &
  queues_reader () const
  {
    return _queues_reader;
  }

  private:
  bool
  is_aborting () const
  {
    return ( _m_painter_upload == &Uploader::painter_upload_abort );
  }

  void
  painter_upload_default (
      fre::image_matrix::Tile_Buffer_Reference & buffer_ref_n );

  void
  painter_upload_abort (
      fre::image_matrix::Tile_Buffer_Reference & buffer_ref_n );

  /// @brief Process returned events
  /// @return True if aborting and all events have returned
  bool
  tiles_return ();

  void
  tiles_submit ();

  void
  tiles_submit_locked ();

  void
  submit_timeout ();

  void
  thread_run ();

  private:
  std::mutex _mutex;
  // -- Method pointers
  void ( Uploader::*_m_painter_upload ) (
      fre::image_matrix::Tile_Buffer_Reference & buffer_ref_n ) =
      &Uploader::painter_upload_default;

  // -- Event pools
  event::Pool_Tracker _epool_tracker;
  event::Pool< uploader::events::Tile > _epool_tile;
  event::Pool< uploader::events::Aborted > _epool_aborted;

  // -- Event queue
  std::uint32_t _image_code = 0;
  std::uint32_t _submit_tiles_max = 6;
  std::uint32_t _submitted_tiles = 0;
  event::Chain _registered_tiles;
  event::queue::Connections_IO _evqc;
  event::queue::Reference_Pair _queues_reader;

  // -- Thread
  event::Bit_Collector::Int_Type _loop_events = 0;
  event::bit_collectors::Condition _bit_collector;
  event::timer_queue::Queue_Steady _timer_queue;
  event::timer_queue::Client_Steady _timer_submit;
  std::thread _thread;
};
} // namespace fre::uploader
