/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "uploader.hpp"
#include <event/queue/pair.hpp>
#include <event/queue/queue.hpp>
#include <application_config.hpp>
#include <message_streams.hpp>

namespace fre::uploader
{

Uploader::Uploader ()
: _epool_tile ( _epool_tracker )
, _epool_aborted ( _epool_tracker )
{
  event::queue::Reference_Pair queues_uploader;
  {
    event::queue::Pair queue_pair;
    queues_uploader = queue_pair.end_a ();
    _queues_reader = queue_pair.end_b ();
  }

  _epool_tile.set_capacity ( 0 );
  _epool_aborted.set_capacity ( 1 );

  // -- Reader queue connection
  _evqc.in ().set_bit_notifier ( LEvent::TILES_RETURN, &_bit_collector );
  _evqc.connect ( queues_uploader );

  // Thread
  _timer_submit.set_queue ( &_timer_queue );
  _timer_submit.set_callback ( std::bind ( &Uploader::submit_timeout, this ) );
  _timer_submit.set_duration ( std::chrono::milliseconds ( 1000 / 70 ) );
  _thread = std::thread ( &Uploader::thread_run, this );
}

Uploader::~Uploader ()
{
  if ( _thread.joinable () ) {
    if ( DEBUG_FRE_UPLOADER ) {
      Debug_Stream oss;
      oss << "Uploader::~Uploader: Join thread begin\n";
    }

    _thread.join ();

    if ( DEBUG_FRE_UPLOADER ) {
      Debug_Stream oss;
      oss << "Uploader::~Uploader: Join thread done\n";
    }
  }
}

void
Uploader::dispatcher_abort ()
{
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    if ( !is_aborting () ) {
      // Change method pointers
      _m_painter_upload = &Uploader::painter_upload_abort;

      // Release all registered events
      while ( !_registered_tiles.is_empty () ) {
        auto * cevent = static_cast< uploader::events::Tile * > (
            _registered_tiles.pop_front_not_empty () );
        {
          auto & buffer = *( cevent->buffer_ref );
          auto lock ( buffer.acquire_lock () );
          buffer.set_upload_registered ( false );
        }
        _epool_tile.reset_release ( cevent );
      }

      // Send aborted event to the reader
      _evqc.out ().push ( _epool_aborted.acquire () );
      _evqc.out ().feed_into_queue ();
    }
  }
}

void
Uploader::dispatcher_set_image_code ( std::uint32_t image_code_n )
{
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    _image_code = image_code_n;
  }
}

void
Uploader::dispatcher_set_event_pool_capacity ( std::uint32_t capacity_n )
{
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    _epool_tile.set_capacity ( capacity_n );
  }
}

void
Uploader::painter_upload_default (
    fre::image_matrix::Tile_Buffer_Reference & buffer_ref_n )
{
  auto * event = _epool_tile.acquire ();
  event->buffer_ref = buffer_ref_n;
  if ( _registered_tiles.is_empty () && ( _submitted_tiles == 0 ) ) {
    _bit_collector.notify_events ( LEvent::TILES_SENDABLE );
  }
  _registered_tiles.push_back ( event );
}

void
Uploader::painter_upload_abort (
    fre::image_matrix::Tile_Buffer_Reference & buffer_ref_n )
{
  // Clear flags in the image tile buffer
  {
    auto lock ( buffer_ref_n->acquire_lock () );
    buffer_ref_n->set_upload_registered ( false );
  }
  buffer_ref_n.reset ();
}

void
Uploader::tiles_submit ()
{
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    tiles_submit_locked ();
  }
}

void
Uploader::tiles_submit_locked ()
{
  if ( !_registered_tiles.is_empty () &&
       ( _submitted_tiles < _submit_tiles_max ) ) {
    // Move a number of tiles to the submit chain
    do {
      auto * cevent = static_cast< uploader::events::Tile * > (
          _registered_tiles.pop_front_not_empty () );
      // Try to set the tile state to upload locked
      if ( cevent->buffer_ref->upload_lock_install ( _image_code ) ) {
        _evqc.out ().push ( cevent );
        ++_submitted_tiles;
      } else {
        _epool_tile.reset_release ( cevent );
      }
    } while ( !_registered_tiles.is_empty () &&
              ( _submitted_tiles != _submit_tiles_max ) );
    // Feed submit chain into queue
    _evqc.out ().feed_into_queue ();
  }
}

bool
Uploader::tiles_return ()
{
  bool finished = false;
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    // -- Release returned tiles
    {
      typedef uploader::events::Type EType;
      _evqc.in ().read_from_queue ();
      while ( !_evqc.in ().is_empty () ) {
        event::Event * event ( _evqc.in ().pop_not_empty () );
        switch ( static_cast< EType > ( event->type () ) ) {

        case EType::TILE: {
          uploader::events::Tile * cevent =
              static_cast< uploader::events::Tile * > ( event );
          // Finish tile upload state
          cevent->buffer_ref->upload_lock_release ();
          _epool_tile.reset_release ( cevent );
          --_submitted_tiles;
        } break;

        case EType::ABORTED:
          _epool_aborted.casted_reset_release ( event );
          break;
        }
      }
    }
    // -- Submit new tiles
    tiles_submit_locked ();
    // -- Check if we're done
    finished = ( is_aborting () && _epool_tracker.all_home () );
  }
  return finished;
}

void
Uploader::submit_timeout ()
{
  _loop_events |= LEvent::TILES_SUBMIT;
}

void
Uploader::thread_run ()
{
  if ( DEBUG_FRE_UPLOADER ) {
    Debug_Stream oss;
    oss << "Uploader::thread_run: Main loop begin\n";
  }

  while ( true ) {
    if ( !_timer_queue.is_empty () ) {
      if ( _bit_collector.wait_for_events_until ( _loop_events,
                                                  _timer_queue.front () ) ) {
        _timer_queue.process_timeouts_now ();
      }
    } else {
      _bit_collector.wait_for_events ( _loop_events );
    }
    // Return tiles
    if ( ( _loop_events & LEvent::TILES_RETURN ) != 0 ) {
      _loop_events &= ~uint32_t ( LEvent::TILES_RETURN );
      if ( tiles_return () ) {
        break;
      }
    }
    // Submit tiles
    if ( ( _loop_events & LEvent::TILES_SUBMIT ) != 0 ) {
      _loop_events &= ~uint32_t ( LEvent::TILES_SUBMIT );
      tiles_submit ();
    }
    // Start send tiles timer
    if ( ( _loop_events & LEvent::TILES_SENDABLE ) != 0 ) {
      _loop_events &= ~uint32_t ( LEvent::TILES_SENDABLE );
      if ( !_timer_submit.is_running () ) {
        _timer_submit.start_single_period_now ();
      }
    }
  }

  if ( DEBUG_FRE_UPLOADER ) {
    Debug_Stream oss;
    oss << "Uploader::thread_run: Main loop done\n";
  }
}
} // namespace fre::uploader
