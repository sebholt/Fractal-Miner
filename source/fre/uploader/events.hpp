/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/event.hpp>
#include <fre/image_matrix/tile_buffer.hpp>

namespace fre::uploader::events
{

/// @brief Event types
enum class Type
{
  TILE,
  ABORTED
};

/// @brief Request thread abort
///
class Tile : public event::Event
{
  public:
  // -- Statics
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::TILE );

  public:
  // -- Constructors

  Tile ()
  : event::Event ( etype )
  {
  }

  void
  reset ()
  {
    buffer_ref.reset ();
  }

  public:
  fre::image_matrix::Tile_Buffer_Reference buffer_ref;
};

/// @brief The last event sent to the reader
///
class Aborted : public event::Event
{
  public:
  // -- Statics
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::ABORTED );

  public:
  // -- Constructors

  Aborted ()
  : event::Event ( etype )
  {
  }
};
} // namespace fre::uploader::events
