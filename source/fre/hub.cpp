/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "hub.hpp"
#include <fre/dispatcher.hpp>
#include <fre/fractal/iterators/mandelbrot.hpp>
#include <fre/pipeline_returner.hpp>
#include <fre/uploader/uploader.hpp>
#include <application_config.hpp>
#include <debug_assert.hpp>
#include <message_streams.hpp>

namespace fre
{

Hub::Hub ( const event::queue::Reference_Pair & evq_in_n,
           const event::queue::Reference_Pair & evq_out_n )
: _epool_tile ( _epool_tracker_pipeline )
, _epool_progress ( _epool_tracker_engine )
, _pipeline_client ( *this )
{
  _pipeline_returner = std::make_unique< Pipeline_Returner > ();
  _uploader = std::make_unique< uploader::Uploader > ();

  _epool_tile.set_capacity ( 1 );
  _epool_progress.set_size ( 1 );

  // Start dispatcher thread
  _dispatcher = std::make_unique< Dispatcher > ( *this );
  _dispatcher->start ( evq_in_n, evq_out_n, _pipeline_returner->queue () );
}

Hub::~Hub () {}

void
Hub::locked_unregister_pickable ( shadow_matrix::Tile_Reference const & ref_n )
{
  DEBUG_ASSERT ( ref_n->is_pickable () );
  {
    auto & lst = _pickable_tiles[ ref_n->pickable_list () ];
    auto it = std::find ( lst.begin (), lst.end (), ref_n );
    DEBUG_ASSERT ( it != lst.end () );
    lst.erase ( it );
    --_pickable_tiles_count;
  }
  ref_n->clear_pickable_list ();
}

void
Hub::locked_register_pickable_move (
    shadow_matrix::Tile_Reference_Vector & vec_n )
{
  for ( auto & ref_n : vec_n ) {
    DEBUG_ASSERT ( ref_n );
    if ( !ref_n->is_pipelined () ) {
      // Remove from non first pick list
      if ( ref_n->is_pickable () ) {
        locked_unregister_pickable ( ref_n );
      }
      if ( !ref_n->is_pickable () ) {
        ref_n->set_pickable_list ( 0 );
        _pickable_tiles[ 0 ].emplace_back ( std::move ( ref_n ) );
        ++_pickable_tiles_count;
      }
    }
  }
}

void
Hub::locked_register_pickable ( shadow_matrix::Tile_Reference const & ref_n )
{
  DEBUG_ASSERT ( ref_n );
  DEBUG_ASSERT ( !ref_n->is_pickable () );
  ref_n->set_pickable_list ( 0 );
  _pickable_tiles[ 0 ].emplace_back ( ref_n );
  ++_pickable_tiles_count;
}

void
Hub::locked_register_pickable_again (
    shadow_matrix::Tile_Reference const & ref_n )
{
  DEBUG_ASSERT ( ref_n );
  DEBUG_ASSERT ( !ref_n->is_pickable () );
  ref_n->set_pickable_list ( locked_pickable_list_index ( ref_n ) );
  _pickable_tiles[ ref_n->pickable_list () ].emplace_back ( ref_n );
  ++_pickable_tiles_count;
}

void
Hub::locked_clear_pickables ()
{
  if ( _pickable_tiles_count != 0 ) {
    for ( auto & vec_n : _pickable_tiles ) {
      for ( auto & ref_n : vec_n ) {
        ref_n->clear_pickable_list ();
      }
    }
    _pickable_tiles_count = 0;
  }
}

void
Hub::locked_register_all_pickable ()
{
  for ( auto const & row : _matrices.shadow_matrix ().tile_rows () ) {
    for ( auto const & ref : row ) {
      if ( !ref->is_pipelined () ) {
        // Check if the tile in a non first pickable list
        if ( ref->is_pickable () && ( ref->pickable_list () != 0 ) ) {
          locked_unregister_pickable ( ref );
        }
        if ( !ref->is_pickable () ) {
          locked_register_pickable ( ref );
        }
      }
    }
  }
}

fre::fractal::Iterator_Reference
Hub::locked_generate_iterator ()
{
  fre::fractal::Iterator_Reference res =
      std::make_shared< fre::fractal::iterators::Mandelbrot > ();
  res->set_max_iterations ( _iterations_max );
  return res;
}

bool
Hub::dispatcher_abort ()
{
  bool finished = false;
  bool first_call = false;
  // Disconnect from pipeline
  dispatcher_pipeline ( nullptr );
  {
    std::lock_guard< std::mutex > main_lock ( _main_mutex );
    if ( !_abort ) {
      _abort = true;
      first_call = true;
    }
    finished = _epool_tracker_pipeline.all_home () &&
               _epool_tracker_engine.all_home ();
  }
  if ( first_call ) {
    // Notify uploader
    _uploader->dispatcher_abort ();
    // Notify pipeline feeder
    notify_feeder ();
  }

  if ( DEBUG_FRE_DISPATCHER_EVENTS ) {
    Debug_Stream oss;
    oss << "Hub::dispatcher_abort: " << finished << "\n";
  }

  return finished;
}

void
Hub::dispatcher_tile_sizes ( Tile_Sizes const & tile_sizes_n,
                             std::uint32_t image_code_n )
{
  if ( DEBUG_FRE_DISPATCHER_EVENTS ) {
    Debug_Stream oss;
    oss << "Hub::dispatcher_tile_sizes:";
    oss << " shadow_tile_size " << tile_sizes_n.shadow_tile_size;
    oss << " image_tile_size " << tile_sizes_n.image_tile_size;
    oss << " image_tile_overlcap " << tile_sizes_n.image_tile_overlap;
    oss << " image_inner_tile_size " << tile_sizes_n.image_inner_tile_size ();
    oss << "\n";
  }
  {
    std::lock_guard< std::mutex > main_lock ( _main_mutex );
    locked_set_image_code ( image_code_n );

    _feeder_lock = true;
    locked_clear_pickables ();
    _matrices.set_tile_sizes ( tile_sizes_n, _image_code );
  }
}

bool
Hub::dispatcher_pipeline ( std::shared_ptr< fre::Pipeline > const & pipeline_n )
{
  bool change_complete = false;
  {
    std::unique_lock< std::mutex > main_lock ( _main_mutex );
    _feeder_lock = true;
    change_complete = _epool_tracker_pipeline.all_home ();
  }
  // Reconnect outside of the mutex to avoid deadlocking
  if ( change_complete ) {
    _pipeline_client.connect ( pipeline_n );
  }

  if ( DEBUG_FRE_DISPATCHER_EVENTS ) {
    Debug_Stream oss;
    oss << "Hub::dispatcher_pipeline: pipeline: " << pipeline_n
        << " change_complete: " << change_complete << "\n";
  }
  return change_complete;
}

void
Hub::dispatcher_pipeline_lock ( bool lock_n )
{
  if ( DEBUG_FRE_DISPATCHER_EVENTS ) {
    Debug_Stream oss;
    oss << "Hub::dispatcher_pipeline_lock: " << lock_n << "\n";
  }

  bool notify = false;
  if ( _pipeline_client.is_connected () ) {
    std::unique_lock< std::mutex > main_lock ( _main_mutex );
    if ( _feeder_lock != lock_n ) {
      if ( lock_n ) {
        _feeder_lock = true;

        if ( DEBUG_FRE_DISPATCHER_EVENTS ) {
          Debug_Stream oss;
          oss << "Hub::dispatcher_pipeline_lock: Locked!\n";
        }
      } else {
        // Check if picking should be allowed
        if ( !_abort && _matrices.iterator () ) {
          _feeder_lock = false;
          notify = true;

          if ( DEBUG_FRE_DISPATCHER_EVENTS ) {
            Debug_Stream oss;
            oss << "Hub::dispatcher_pipeline_lock: Unlocked!\n";
          }
        }
      }
    }
  }
  if ( notify ) {
    notify_feeder ();
  }
}

void
Hub::dispatcher_resize ( Resize_Deltas const & deltas_n )
{
  if ( DEBUG_FRE_DISPATCHER_EVENTS ) {
    Debug_Stream oss;
    oss << "Hub::dispatcher_resize: delta " << deltas_n << "\n";
  }

  {
    std::uint32_t num_image_tiles = 0;
    shadow_matrix::Tile_Reference_Vector pickables;
    {
      std::lock_guard< std::mutex > main_lock ( _main_mutex );
      _matrices.resize_image_matrix ( deltas_n, pickables );
      _pickable_random.resize ( _matrices.shadow_matrix ().num_tiles () );
      // Add new and repaintable tiles to pickable list
      locked_register_pickable_move ( pickables );
      locked_send_progress ();

      // Resize tile event capacity
      _epool_tile.set_capacity ( _matrices.shadow_matrix ().num_tiles () );
      num_image_tiles = _matrices.image_matrix ().num_tiles ();
    }
    // Notify feeder
    notify_feeder ();
    // Increase uploader capacity
    _uploader->dispatcher_set_event_pool_capacity ( num_image_tiles );
  }
}

void
Hub::dispatcher_loop ( geo::Int32_Pair const & loops_n )
{
  {
    // Create/destroy the reference vectors outside the mutex
    shadow_matrix::Tile_Reference_Vector pickables_hor;
    shadow_matrix::Tile_Reference_Vector pickables_vert;
    pickables_hor.reserve ( _matrices.shadow_matrix ().num_tiles () );
    pickables_vert.reserve ( _matrices.shadow_matrix ().num_tiles () );
    {
      std::lock_guard< std::mutex > main_lock ( _main_mutex );
      if ( loops_n.first () != 0 ) {
        _matrices.loop_horizontal ( loops_n.first (), pickables_hor );
        // Add new and repaintable tiles to pickable list
        locked_register_pickable_move ( pickables_hor );
      }
      if ( loops_n.second () != 0 ) {
        _matrices.loop_vertical ( loops_n.second (), pickables_vert );
        locked_register_pickable_move ( pickables_vert );
      }
      locked_send_progress ();
    }
    // Notify feeder
    notify_feeder ();
  }
}

void
Hub::dispatcher_set_complex ( geo::Double_Point const & px_base_n,
                              fre::Complex_Origin const & cp_origin_n,
                              std::uint32_t image_code_n )
{
  if ( DEBUG_FRE_DISPATCHER_EVENTS ) {
    Debug_Stream oss;
    oss << "Hub::dispatcher_set_complex:\n";
    oss << " px_base_n: " << px_base_n << " ";
    oss << " cp_origin_n: " << cp_origin_n << "\n";
  }

  {
    std::lock_guard< std::mutex > main_lock ( _main_mutex );
    locked_set_image_code ( image_code_n );

    _matrices.set_image_matrix_complex ( px_base_n, cp_origin_n, _image_code );
    locked_register_all_pickable ();
  }
  // Notify feeder
  notify_feeder ();
}

void
Hub::dispatcher_set_iterator ( std::string const & id_n,
                               std::uint32_t image_code_n )
{
  if ( DEBUG_FRE_DISPATCHER_EVENTS ) {
    Debug_Stream oss;
    oss << "Hub::dispatcher_set_iterator: id_n: " << id_n << "\n";
  }

  {
    std::lock_guard< std::mutex > main_lock ( _main_mutex );
    locked_set_image_code ( image_code_n );

    _iterator_id = id_n;
    _matrices.set_iterator ( locked_generate_iterator (), _image_code );
    locked_register_all_pickable ();
  }
  // Notify feeder
  notify_feeder ();
}

void
Hub::dispatcher_set_iterations ( std::uint32_t count_n,
                                 std::uint32_t image_code_n )
{
  if ( DEBUG_FRE_DISPATCHER_EVENTS ) {
    Debug_Stream oss;
    oss << "Hub::dispatcher_set_iterations: count: " << count_n << "\n";
  }

  {
    std::lock_guard< std::mutex > main_lock ( _main_mutex );
    locked_set_image_code ( image_code_n );

    _iterations_max = count_n;
    _matrices.set_iterator ( locked_generate_iterator (), _image_code );
    locked_register_all_pickable ();
  }
  // Notify feeder
  notify_feeder ();
}

void
Hub::dispatcher_pipeline_return ( event::queue::Connection_In & evqc_n )
{
  bool pickable = false;
  if ( !evqc_n.is_empty () ) {
    std::unique_lock< std::mutex > main_lock ( _main_mutex );
    if ( DEBUG_FRE_DISPATCHER_EVENTS ) {
      Debug_Stream oss;
      oss << "Hub::dispatcher_pipeline_return: "
          << evqc_n.chain ().count_events () << " events\n";
    }
    while ( !evqc_n.is_empty () ) {
      event::Event * event = evqc_n.pop_not_empty ();
      locked_pipeline_return ( event );
    }
    locked_send_progress ();
    pickable = true;
  }

  // Notify feeder
  if ( pickable ) {
    notify_feeder ();
  }
}

void
Hub::dispatcher_engine_return ( event::queue::Connection_In & evqc_n )
{
  if ( !evqc_n.is_empty () ) {
    std::unique_lock< std::mutex > main_lock ( _main_mutex );
    while ( !evqc_n.is_empty () ) {
      _epool_tracker_engine.casted_reset_release ( evqc_n.pop_not_empty () );
    }
    locked_send_progress ();
  }
}

inline void
Hub::locked_set_image_code ( std::uint32_t image_code_n )
{
  _image_code = image_code_n;
  _uploader->dispatcher_set_image_code ( image_code_n );
}

void
Hub::locked_send_progress ()
{
  if ( !_abort && !_epool_progress.is_empty () ) {
    auto * event = _epool_progress.pop_not_empty ();
    event->progress.set_image_code ( _image_code );
    event->progress.set_pixels_rendered (
        _matrices.shadow_matrix ().pixels_rendered () );
    event->progress.set_pixel_count (
        _matrices.shadow_matrix ().size_px ().area< std::uint32_t > () );

    if ( _progress_latest != event->progress ) {
      _progress_latest = event->progress;
      _dispatcher->push_out_event ( event );
    } else {
      _epool_progress.release ( event );
    }
  }
}

void
Hub::locked_pipeline_return ( event::Event * event_n )
{
  {
    auto & cevent = *static_cast< pipeline::events::Tile * > ( event_n );
    fre::shadow_matrix::Tile & sh_tile ( *cevent.shadow_tile );
    // Clear pipelined flag
    DEBUG_ASSERT ( sh_tile.is_pipelined () );
    sh_tile.set_is_pipelined ( false );
    if ( !_abort ) {
      // Check if the rendered content is still valid
      if ( sh_tile.id ().is_valid () ) {
        // Check if the tile state changed
        if ( sh_tile.buffer ().id () == sh_tile.id () ) {

          if ( !cevent.paint_only ) {
            std::uint32_t const pixel_count =
                cevent.shadow_tile_commons->pixel_count ();
            std::uint32_t const pixels_rendered =
                sh_tile.buffer ().pixels_rendered ();
            // Update pixels rendered statistics
            _matrices.shadow_matrix_ref ().increment_pixels_rendered (
                pixels_rendered - sh_tile.pixels_rendered () );
            sh_tile.set_pixels_rendered ( pixels_rendered );
            // Send through the pipeline again (on demand)
            if ( pixels_rendered != pixel_count ) {
              locked_register_pickable_again ( cevent.shadow_tile );
            }
          }
          // Check if repainting is requested
          if ( sh_tile.is_paint_only () && !sh_tile.is_pickable () ) {
            locked_register_pickable ( cevent.shadow_tile );
          }
        } else {
          // The tile changed meanwhile
          locked_register_pickable ( cevent.shadow_tile );
        }
      } else {
        // The tile was discarded
      }
    }
  }
  // Reset and release event
  _epool_tile.casted_reset_release ( event_n );
}

event::Event *
Hub::feeder_acquire ()
{
  event::Event * res ( nullptr );
  {
    // The feeder lock ensures only one feeder at a time requests the
    // main_lock
    std::unique_lock< std::mutex > main_lock ( _main_mutex );
    if ( !_feeder_lock &&
         ( _epool_tile.size_outside () != _epool_tile.capacity () ) ) {
      while ( _pickable_tiles_count != 0 ) {
        // -- Pick a tile event
        pipeline::events::Tile * tile_event = _epool_tile.acquire ( *this );
        if ( feeder_locked_event_setup ( *tile_event ) ) {
          res = tile_event;
          break;
        }
        _epool_tile.reset_release ( tile_event );
      }
    }
  }
  return res;
}

bool
Hub::feeder_locked_event_setup ( pipeline::events::Tile & event_n )
{
  auto const & shm = _matrices.shadow_matrix ();
  auto const & shm_com = shm.commons ();

  // -- Pick a tile reference
  for ( auto & lst : _pickable_tiles ) {
    if ( !lst.empty () ) {
      // Shuffle the top entry when there aremore than one items
      if ( lst.size () > 1 ) {
        std::uint32_t index_max ( lst.size () - 1 );
        std::size_t index =
            ( _pickable_random () + lst.size () / 2 ) % index_max;
        std::swap ( lst[ index ], lst[ index_max ] );
      }
      event_n.shadow_tile = std::move ( lst.back () );
      lst.pop_back ();
      --_pickable_tiles_count;
      break;
    }
  }
  auto & sh_tile = *event_n.shadow_tile;

  // Clear tile is_pickable() flag
  DEBUG_ASSERT ( sh_tile.is_pickable () );
  sh_tile.clear_pickable_list ();

  if ( sh_tile.id ().is_valid () ) {
    auto & sh_buffer = sh_tile.buffer_ref ();
    if ( sh_buffer.id () != sh_tile.id () ) {
      // First render session of the tile
      sh_buffer.id_ref () = sh_tile.id ();
      sh_buffer.reset_render_statistics ();
      sh_buffer.set_complex_base ( shm.tile_complex_base ( sh_buffer.id () ) );
    } else {
      // Paint only flag
      if ( sh_tile.is_paint_only () &&
           ( sh_buffer.pixels_rendered () == shm_com->pixel_count () ) ) {
        event_n.paint_only = true;
      }
    }
    // Clear paint only flag in the tile
    sh_tile.set_is_paint_only ( false );

    // Store a reference to the shadow tile commons
    event_n.shadow_tile_commons = shm_com;

    // -- Image tile paint jobs
    // Pixel region of the shadow tile in image matrix coordinates
    geo::Int32_Region const shm_region =
        _matrices.mapped_shadow_tile_region ( sh_buffer.id () );
    // Image tiles
    image_matrix::Matrix const & imm ( _matrices.image_matrix () );

    if ( DEBUG_FRE_TILE_PICKER ) {
      Debug_Stream oss;
      oss << "Pick shadow tile: pos " << sh_buffer.id ().pos () << " region "
          << shm_region << "\n";
    }

    // -- Create a list of image tiles that overlap wih the shadow tile
    std::uint32_t im_tiles_num = 0;
    std::array< std::array< std::int32_t, 2 >, 4 > im_tiles_cr;
    {
      // Image matrix column and row extents of the shadow tile region
      std::int32_t icol_left =
          imm.local_column_from_px_left ( shm_region.left () );
      std::int32_t icol_right =
          imm.local_column_from_px_right ( shm_region.right () );
      std::int32_t irow_bot =
          imm.local_row_from_px_bottom ( shm_region.bottom () );
      std::int32_t irow_top = imm.local_row_from_px_top ( shm_region.top () );

      if ( irow_bot >= 0 ) {
        if ( icol_left >= 0 ) {
          im_tiles_cr[ im_tiles_num++ ] = { { icol_left, irow_bot } };
        }
        if ( ( icol_right >= 0 ) && ( icol_right != icol_left ) ) {
          im_tiles_cr[ im_tiles_num++ ] = { { icol_right, irow_bot } };
        }
      }
      if ( ( irow_top >= 0 ) && ( irow_top != irow_bot ) ) {
        if ( icol_left >= 0 ) {
          im_tiles_cr[ im_tiles_num++ ] = { { icol_left, irow_top } };
        }
        if ( ( icol_right >= 0 ) && ( icol_right != icol_left ) ) {
          im_tiles_cr[ im_tiles_num++ ] = { { icol_right, irow_top } };
        }
      }

      if ( DEBUG_FRE_TILE_PICKER ) {
        Debug_Stream oss;
        oss << "icol_left " << icol_left << " icol_right " << icol_right
            << " irow_bot " << irow_bot << " irow_top " << irow_top
            << " im_tiles_num " << im_tiles_num << "\n";
      }
    }

    if ( im_tiles_num != 0 ) {
      for ( std::uint32_t ii = 0; ii != im_tiles_num; ++ii ) {
        auto const & im_tile_cr = im_tiles_cr[ ii ];
        image_matrix::Tile const * im_tile (
            imm.tile ( im_tile_cr[ 0 ], im_tile_cr[ 1 ] ).get () );
        geo::Int32_Region const imt_region (
            imm.tile_region_px ( im_tile_cr[ 0 ], im_tile_cr[ 1 ] ) );
        geo::Int32_Region const isect ( imt_region.intersected ( shm_region ) );

        if ( DEBUG_FRE_TILE_PICKER ) {
          Debug_Stream oss;
          oss << "im_tile_cr (" << im_tile_cr[ 0 ] << ", " << im_tile_cr[ 1 ]
              << ") ";
          oss << "imt_region " << imt_region << " ";
          oss << "shm_region " << shm_region << " ";
          oss << "isect " << isect << "\n";
        }
        // Test
        DEBUG_ASSERT ( isect.width () > 0 );
        DEBUG_ASSERT ( isect.height () > 0 );

        auto & job = event_n.paint_jobs[ ii ];
        job.image_tile_id = im_tile->id ();
        job.image_tile_buffer = im_tile->buffer ();
        job.shadow_region.set ( isect.left () - shm_region.left (),
                                isect.bottom () - shm_region.bottom (),
                                isect.width (),
                                isect.height () );
        job.image_pos.set ( isect.left () - imt_region.left (),
                            isect.bottom () - imt_region.bottom () );

        if ( DEBUG_FRE_TILE_PICKER ) {
          Debug_Stream oss;
          oss << "job.image_tile_id.row() " << job.image_tile_id.row () << " ";
          oss << "job.image_tile_id.column() " << job.image_tile_id.column ()
              << " ";
          oss << "job.shadow_region " << job.shadow_region << " ";
          oss << "job.image_pos " << job.image_pos << "\n";
        }

        DEBUG_ASSERT ( job.image_tile_id.image_code () ==
                       sh_buffer.id ().image_code () );
        DEBUG_ASSERT ( ( job.image_pos.x () + isect.width () ) <=
                       imm.tile_width () );
      };
    }

    // Return success
    DEBUG_ASSERT ( !sh_tile.is_pipelined () );
    sh_tile.set_is_pipelined ( true );

    return true;
  } else {
    // The tile is discarded
  }

  // Return tile not picked
  return false;
}

void
Hub::painter_return ( event::Event * event_n )
{
  _pipeline_returner->feed ( event_n );
}
} // namespace fre
