/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>
#include <vector>

namespace fre
{

/// @brief Shuffled pool of numbers
///
class Random_Pool
{
  public:
  // -- Constructors

  Random_Pool ( std::uint32_t size_n = 1 );

  ~Random_Pool () = default;

  // -- Accessors

  std::uint32_t
  size () const
  {
    return _numbers.size ();
  }

  void
  resize ( std::uint32_t size_n );

  void
  shuffle ();

  /// @brief Acquire the next random number
  std::uint32_t
  operator() ()
  {
    ++_index;
    _index %= _numbers.size ();
    return _numbers[ _index ];
  }

  private:
  std::size_t _index = 0;
  std::vector< std::uint32_t > _numbers;
};
} // namespace fre
