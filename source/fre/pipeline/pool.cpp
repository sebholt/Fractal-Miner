/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "pool.hpp"
#include <fre/hub.hpp>
#include <algorithm>
#include <message_streams.hpp>

namespace fre::pipeline
{

Pool::Pool () {}

Pool::~Pool () {}

void
Pool::abort ()
{
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    _abort = true;
  }
  _cond.notify_one ();
}

void
Pool::client_connect ( Pool_Entry * entry_n )
{
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    entry_n->pickable = true;
    _entries.push_back ( entry_n );
  }
  _cond.notify_one ();
}

void
Pool::client_disconnect ( Pool_Entry * entry_n )
{
  bool notify ( false );
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    _entries.erase (
        std::find ( _entries.begin (), _entries.end (), entry_n ) );
    notify = _abort;
  }
  if ( notify ) {
    _cond.notify_one ();
  }
}

void
Pool::client_notify ( Pool_Entry * entry_n )
{
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    entry_n->pickable = true;
  }
  _cond.notify_one ();
}

event::Event *
Pool::feeder_acquire ()
{
  event::Event * res = nullptr;
  {
    // Allow only one feeder at a time to access the main mutex
    std::lock_guard< std::mutex > feeder_lock ( _feeder_mutex );
    {
      std::unique_lock< std::mutex > ulock ( _mutex );
      while ( true ) {
        std::size_t const num_entries = _entries.size ();
        for ( std::size_t ii = 0; ii != num_entries; ++ii ) {
          ++_latest_index;
          _latest_index %= num_entries;
          Pool_Entry * entry ( _entries[ _latest_index ] );
          if ( entry->pickable ) {
            res = entry->hub.feeder_acquire ();
            if ( res != nullptr ) {
              return res;
            } else {
              entry->pickable = false;
            }
          }
        }
        if ( _abort ) {
          break;
        }
        _cond.wait ( ulock );
      }
    }
  }
  return nullptr;
}
} // namespace fre::pipeline
