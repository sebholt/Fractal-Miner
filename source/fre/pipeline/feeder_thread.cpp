/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "feeder_thread.hpp"
#include <fre/hub.hpp>
#include <fre/pipeline/events.hpp>
#include <fre/pipeline/pool.hpp>
#include <application_config.hpp>
#include <message_streams.hpp>

namespace fre::pipeline
{

Feeder_Thread::Feeder_Thread ( Pool & pool_n )
: _pool ( pool_n )
, _tiles_fetch_max ( 2 )
, _tiles_fetched ( 0 )
, _loop_events ( 0 )
{
}

Feeder_Thread::~Feeder_Thread ()
{
  join ();
}

bool
Feeder_Thread::start ( const event::queue::Reference & evq_out_n )
{
  if ( !_thread.joinable () ) {
    // Clear loop events
    _tiles_fetched = 0;
    _loop_events = 0;
    // Setup event queue connection
    _evqc_out.set_pop_bit_notifier ( LEvent::SINK_EMPTY, &_bit_collector );
    _evqc_out.connect ( evq_out_n );
    // Start the thread
    _thread = std::thread ( &Feeder_Thread::run, this );
  }
  return _thread.joinable ();
}

void
Feeder_Thread::abort ()
{
  _bit_collector.notify_events ( LEvent::ABORT );
}

void
Feeder_Thread::join ()
{
  if ( _thread.joinable () ) {
    _thread.join ();
    // Clean up
    _evqc_out.clear ();
  }
}

void
Feeder_Thread::run ()
{
  if ( DEBUG_FRE_PIPELINE ) {
    Debug_Stream oss;
    oss << "pipeline::Feeder_Thread: Main loop begin\n";
  }

  while ( true ) {
    // Fetch new events
    if ( _tiles_fetched >= _tiles_fetch_max ) {
      _bit_collector.wait_for_events ( _loop_events );
    } else {
      _bit_collector.fetch_events ( _loop_events );
    }
    // Check for abort
    if ( ( _loop_events & LEvent::ABORT ) != 0 ) {
      break;
    }
    // Check for empty sink queue
    if ( ( _loop_events & LEvent::SINK_EMPTY ) != 0 ) {
      _loop_events &= ~uint32_t ( LEvent::SINK_EMPTY );
      _tiles_fetched = 0;
    }

    // Fetch events on demand
    if ( _tiles_fetched < _tiles_fetch_max ) {
      event::Event * event = _pool.feeder_acquire ();
      if ( event != nullptr ) {
        _evqc_out.push ( event );
        ++_tiles_fetched;
      }
    }
    // Feed into sink queue
    _evqc_out.feed_into_queue ();
  }

  if ( DEBUG_FRE_PIPELINE ) {
    Debug_Stream oss;
    oss << "pipeline::Feeder_Thread: Main loop done\n";
  }
}
} // namespace fre::pipeline
