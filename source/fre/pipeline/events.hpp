/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/event.hpp>
#include <fre/image_matrix/tile_buffer.hpp>
#include <fre/shadow_matrix/commons.hpp>
#include <fre/shadow_matrix/tile.hpp>
#include <geo/geo.hpp>
#include <complex>

// -- Forward declaration
namespace fre
{
class Hub;
}

namespace fre::pipeline::events
{

/// @brief Event types
enum class Type
{
  TILE
};

/// @brief Tile iteration and paint job
///
class Tile : public event::Event
{
  public:
  // -- Types
  struct Paint_Job
  {
    fre::image_matrix::Tile_Identifier image_tile_id;
    fre::image_matrix::Tile_Buffer_Reference image_tile_buffer;
    /// @brief Region in the shadow tile
    geo::UInt16_Region shadow_region;
    /// @brief Position of the
    geo::UInt16_Point image_pos;
  };
  // -- Statics
  static constexpr std::uint32_t etype =
      static_cast< std::uint32_t > ( Type::TILE );

  public:
  // -- Constructors

  Tile ( Hub & hub_n )
  : event::Event ( etype )
  , hub ( hub_n )
  {
  }

  ~Tile () = default;

  // -- Maintanance

  void
  reset ()
  {
    paint_only = false;
    for ( auto & job : paint_jobs ) {
      job.image_tile_buffer.reset ();
    }
    shadow_tile.reset ();
    shadow_tile_commons.reset ();
  }

  public:
  Hub & hub;
  fre::shadow_matrix::Commons_Reference shadow_tile_commons;
  fre::shadow_matrix::Tile_Reference shadow_tile;
  std::array< Paint_Job, 4 > paint_jobs;
  bool paint_only = false;
};
} // namespace fre::pipeline::events
