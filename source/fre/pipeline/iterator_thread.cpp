/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "iterator_thread.hpp"
#include <fre/hub.hpp>
#include <application_config.hpp>
#include <message_streams.hpp>

namespace fre::pipeline
{

Iterator_Thread::Iterator_Thread () {}

Iterator_Thread::~Iterator_Thread ()
{
  join ();
}

bool
Iterator_Thread::start ( const event::queue::Reference & evq_in_n,
                         const event::queue::Reference & evq_out_n )
{
  if ( !_thread.joinable () ) {
    // Clear loop events
    _loop_events = 0;
    // Setup event queue connection
    _evqc_in.set_bit_notifier ( LEvent::INCOMING, &_bit_collector );
    _evqc_in.connect ( evq_in_n );
    _evqc_out.connect ( evq_out_n );
    // Start the thread
    _thread = std::thread ( &Iterator_Thread::run, this );
  }
  return _thread.joinable ();
}

void
Iterator_Thread::abort ()
{
  _bit_collector.notify_events ( LEvent::ABORT );
}

void
Iterator_Thread::join ()
{
  if ( _thread.joinable () ) {
    _thread.join ();
    // Clean up
    _evqc_in.clear ();
    _evqc_out.clear ();
  }
}

void
Iterator_Thread::run ()
{
  if ( DEBUG_FRE_PIPELINE ) {
    Debug_Stream oss;
    oss << "pipeline::Iterator_Thread: Main loop begin\n";
  }

  while ( true ) {
    // Fetch new bit events
    if ( _evqc_in.is_empty () ) {
      _bit_collector.wait_for_events ( _loop_events );
    } else {
      _bit_collector.fetch_events ( _loop_events );
    }
    // Check for abort
    if ( ( _loop_events & LEvent::ABORT ) != 0 ) {
      break;
    }

    // Check for empty sink queue
    if ( ( _loop_events & LEvent::INCOMING ) != 0 ) {
      _loop_events &= ~uint32_t ( LEvent::INCOMING );
      // Read events from source queue
      while ( _evqc_in.read_from_queue () ) {
        while ( !_evqc_in.is_empty () ) {
          event::Event * event ( _evqc_in.pop_not_empty () );
          iterate_tile ( *static_cast< events::Tile * > ( event ) );
          _evqc_out.push ( event );
          _evqc_out.feed_into_queue ();
        }
      }
    }
  }

  if ( DEBUG_FRE_PIPELINE ) {
    Debug_Stream oss;
    oss << "pipeline::Iterator_Thread: Main loop  \n";
  }
}

inline void
Iterator_Thread::iterate_tile ( events::Tile & event_n )
{
  if ( event_n.paint_only ) {
    return;
  }

  auto & sh_buffer = event_n.shadow_tile->buffer_ref ();

  // Clear buffer before first paint
  if ( sh_buffer.pixels_rendered () == 0 ) {
    std::fill ( sh_buffer.buffer_begin (), sh_buffer.buffer_end (), 0 );
  }

  // Call abstract iterator iteration method
  event_n.shadow_tile_commons->iterator ()->iterate (
      fre::fractal::Iterator::Iter_Args{ *event_n.shadow_tile_commons,
                                         sh_buffer } );

  // Increment render sessions count
  sh_buffer.increment_render_sessions ();
}
} // namespace fre::pipeline
