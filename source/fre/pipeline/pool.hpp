/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/event.hpp>
#include <fre/pipeline/pool_entry.hpp>
#include <condition_variable>
#include <cstdint>
#include <mutex>
#include <vector>

namespace fre::pipeline
{

/// @brief Pool of clients
///
class Pool
{
  public:
  // -- Constructors

  Pool ();

  ~Pool ();

  // -- Pipeline interface

  void
  abort ();

  // -- Client interface

  void
  client_connect ( Pool_Entry * entry_n );

  void
  client_disconnect ( Pool_Entry * entry_n );

  void
  client_notify ( Pool_Entry * entry_n );

  // -- Feeder interface

  event::Event *
  feeder_acquire ();

  public:
  std::mutex _feeder_mutex;
  std::mutex _mutex;
  std::condition_variable _cond;
  std::vector< Pool_Entry * > _entries;
  std::size_t _latest_index = 0;
  bool _abort = false;
};
} // namespace fre::pipeline
