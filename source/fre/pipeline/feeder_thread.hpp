/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_collectors/condition.hpp>
#include <event/queue/connection_out.hpp>
#include <thread>

namespace fre::pipeline
{

// -- Forward declaration
class Pool;

/// @brief Iterator feeder thread
///
class Feeder_Thread
{
  public:
  // -- Types

  enum LEvent
  {
    SINK_EMPTY = 1 << 0,
    ABORT = 1 << 1
  };

  public:
  // -- Constructors

  Feeder_Thread ( Pool & pool_n );

  ~Feeder_Thread ();

  // -- Thread run control

  bool
  start ( const event::queue::Reference & evq_out_n );

  void
  abort ();

  void
  join ();

  private:
  void
  run ();

  private:
  Pool & _pool;
  std::uint32_t _tiles_fetch_max;
  std::uint32_t _tiles_fetched;
  event::Bit_Collector::Int_Type _loop_events;
  event::bit_collectors::Condition _bit_collector;
  event::queue::Connection_Out _evqc_out;
  std::thread _thread;
};
} // namespace fre::pipeline
