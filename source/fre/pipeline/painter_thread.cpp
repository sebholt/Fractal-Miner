/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "painter_thread.hpp"
#include <fre/hub.hpp>
#include <fre/uploader/uploader.hpp>
#include <algorithm>
#include <application_config.hpp>
#include <message_streams.hpp>

namespace fre::pipeline
{

Painter_Thread::Painter_Thread ()
: _loop_events ( 0 )
{
}

Painter_Thread::~Painter_Thread ()
{
  join ();
}

bool
Painter_Thread::start ( const event::queue::Reference & evq_in_n )
{
  if ( !_thread.joinable () ) {
    // Clear loop events
    _loop_events = 0;
    // Setup event queue connection
    _evqc_in.set_bit_notifier ( LEvent::INCOMING, &_bit_collector );
    _evqc_in.connect ( evq_in_n );
    // Start the thread
    _thread = std::thread ( &Painter_Thread::run, this );
  }
  return _thread.joinable ();
}

void
Painter_Thread::abort ()
{
  _bit_collector.notify_events ( LEvent::ABORT );
}

void
Painter_Thread::join ()
{
  if ( _thread.joinable () ) {
    _thread.join ();
    // Clean up
    _evqc_in.clear ();
  }
}

void
Painter_Thread::run ()
{
  if ( DEBUG_FRE_PIPELINE ) {
    Debug_Stream oss;
    oss << "pipeline::Painter_Thread: Main loop begin\n";
  }

  while ( true ) {
    // Fetch new bit events
    _bit_collector.wait_for_events ( _loop_events );
    // Check for abort
    if ( ( _loop_events & LEvent::ABORT ) != 0 ) {
      break;
    }
    // Read events from source queue
    if ( _evqc_in.read_from_queue_checked ( _loop_events ) ) {
      while ( !_evqc_in.is_empty () ) {
        event::Event * event ( _evqc_in.pop_not_empty () );
        auto * cevent = static_cast< events::Tile * > ( event );
        paint_tile ( cevent );
        cevent->hub.painter_return ( cevent );
      }
    }
  }

  if ( DEBUG_FRE_PIPELINE ) {
    Debug_Stream oss;
    oss << "pipeline::Painter_Thread: Main loop done\n";
  }
}

void
Painter_Thread::paint_tile ( events::Tile * event_n )
{
  constexpr std::uint32_t const px_bytes (
      sizeof ( fre::shadow_matrix::Tile_Buffer::Buffer::value_type ) );

  fre::shadow_matrix::Tile & sh_tile ( *event_n->shadow_tile );
  std::uint32_t const sh_stride =
      event_n->shadow_tile_commons->tile_width () * px_bytes;
  for ( events::Tile::Paint_Job & job : event_n->paint_jobs ) {
    if ( !job.image_tile_buffer ) {
      continue;
    }
    bool upload ( false );
    {
      // -- Acquire image buffer lock
      fre::image_matrix::Tile_Buffer & imtb ( *job.image_tile_buffer );
      std::unique_lock< std::mutex > imt_lock ( imtb.acquire_lock () );

      // Check if the content of the buffer is newer that what we have
      if ( imtb.tile_id ().any_code_newer_than ( job.image_tile_id ) ) {
        continue;
      }

      // Check if the content of the buffer is all older that what we have
      if ( imtb.tile_id ().any_code_older_than ( job.image_tile_id ) ) {
        // Update the buffer's tile id
        imtb.set_tile_id ( job.image_tile_id );
        // Clear the pixel buffer
        std::fill (
            imtb.buffer_begin (), imtb.buffer_end (), std::uint32_t ( 0 ) );
      }

      {
        // Constant integers
        const auto & sh_reg = job.shadow_region;
        std::uint32_t const sh_row_size = ( sh_reg.width () * px_bytes );
        std::uint32_t const im_stride = ( imtb.tile_width () * px_bytes );
        // Image pointers
        std::uint8_t * im_ptr =
            reinterpret_cast< std::uint8_t * > ( imtb.data () );
        im_ptr += ( job.image_pos.y () * im_stride );
        im_ptr += ( job.image_pos.x () * px_bytes );
        // Shadow pointers
        std::uint8_t const * sh_ptr = reinterpret_cast< std::uint8_t const * > (
            sh_tile.buffer ().data () );
        sh_ptr += ( sh_reg.y () * sh_stride );
        sh_ptr += ( sh_reg.x () * px_bytes );
        std::uint8_t const * sh_ptr_end = sh_ptr;
        sh_ptr_end += ( sh_reg.height () * sh_stride );
        // Copy rows
        while ( sh_ptr != sh_ptr_end ) {
          std::copy ( sh_ptr, sh_ptr + sh_row_size, im_ptr );
          sh_ptr += sh_stride;
          im_ptr += im_stride;
        }
      }

      if ( !imtb.upload_registered () ) {
        imtb.set_upload_registered ( true );
        upload = true;
      }
    }
    // Upload the tile buffer on demand
    if ( upload ) {
      event_n->hub.uploader ()->painter_upload ( job.image_tile_buffer );
    }
  }
}
} // namespace fre::pipeline
