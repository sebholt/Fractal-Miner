/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_collectors/condition.hpp>
#include <event/queue/connection_in.hpp>
#include <fre/pipeline/events.hpp>
#include <thread>

namespace fre::pipeline
{

/// @brief Painter thread
///
class Painter_Thread
{
  public:
  // -- Types

  enum LEvent
  {
    INCOMING = 1 << 0,
    ABORT = 1 << 1
  };

  public:
  // -- Constructors

  Painter_Thread ();

  ~Painter_Thread ();

  // -- Thread run control

  bool
  start ( const event::queue::Reference & evq_in_n );

  void
  abort ();

  void
  join ();

  private:
  void
  run ();

  void
  paint_tile ( events::Tile * event_n );

  private:
  event::Bit_Collector::Int_Type _loop_events;
  event::bit_collectors::Condition _bit_collector;
  event::queue::Connection_In _evqc_in;
  std::thread _thread;
};
} // namespace fre::pipeline
