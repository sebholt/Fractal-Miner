/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_collectors/condition.hpp>
#include <event/queue/connection_in.hpp>
#include <event/queue/connection_out.hpp>
#include <fre/pipeline/events.hpp>
#include <thread>

namespace fre::pipeline
{

/// @brief Iterator thread
///
class Iterator_Thread
{
  public:
  // -- Types

  enum LEvent
  {
    INCOMING = 1 << 0,
    ABORT = 1 << 1
  };

  public:
  // -- Constructors

  Iterator_Thread ();

  ~Iterator_Thread ();

  // -- Thread run control

  bool
  start ( const event::queue::Reference & evq_in_n,
          const event::queue::Reference & evq_out_n );

  void
  abort ();

  void
  join ();

  private:
  void
  run ();

  void
  iterate_tile ( events::Tile & event_n );

  private:
  event::Bit_Collector::Int_Type _loop_events = 0;
  event::bit_collectors::Condition _bit_collector;
  event::queue::Connection_In _evqc_in;
  event::queue::Connection_Out _evqc_out;
  std::thread _thread;
};
} // namespace fre::pipeline
