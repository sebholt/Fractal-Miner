/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

// -- Forward declaration
namespace fre
{
class Hub;
}

namespace fre::pipeline
{

// -- Types
class Pool_Entry
{
  public:
  Pool_Entry ( Hub & hub_n )
  : hub ( hub_n ){};

  bool pickable = false;
  Hub & hub;
};
} // namespace fre::pipeline
