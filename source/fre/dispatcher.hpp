/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_collectors/condition.hpp>
#include <event/queue/connections_io.hpp>
#include <event/queue/reference.hpp>
#include <memory>
#include <thread>

namespace fre
{

// -- Forward declaration
class Hub;

/// @brief GUI event dispatcher and engine controller
///
class Dispatcher
{
  public:
  // -- Types
  enum LEvent
  {
    ENGINE_IN = 1 << 0,
    ENGINE_OUT_RETURN = 1 << 1,
    PIPELINE_RETURN = 1 << 2
  };

  public:
  // -- Constructors

  Dispatcher ( Hub & hub_n );

  ~Dispatcher ();

  // -- Accessors

  Hub &
  hub ()
  {
    return _hub;
  }

  // -- Thread run control

  bool
  start ( const event::queue::Reference_Pair & evq_in_n,
          const event::queue::Reference_Pair & evq_out_n,
          const event::queue::Reference & evq_pipeline_n );

  bool
  is_running () const
  {
    return _thread.joinable ();
  }

  // -- Hub interface

  void
  push_out_event ( event::Event * event_n )
  {
    _evqc_engine_out.out ().push ( event_n );
  }

  private:
  // -- Thread main loop

  /// @brief Thread main loop
  void
  run ();

  // -- Pipeline events

  void
  process_pipeline_events ();

  // -- Engine events

  void
  process_engine_events ();

  void
  engine_event_abort ( event::Event * event_n );

  void
  engine_event_tile_sizes ( event::Event * event_n );

  bool
  engine_event_pipeline ( event::Event * event_n );

  void
  engine_event_pipeline_lock ( event::Event * event_n );

  void
  engine_event_resize ( event::Event * event_n );

  void
  engine_event_loop ( event::Event * event_n );

  void
  engine_event_complex ( event::Event * event_n );

  void
  engine_event_iterator ( event::Event * event_n );

  void
  engine_event_iterations ( event::Event * event_n );

  private:
  Hub & _hub;
  bool _abort = false;
  event::Bit_Collector::Int_Type _loop_events = 0;
  event::bit_collectors::Condition _bit_collector;
  event::queue::Connections_IO _evqc_engine_in;
  event::queue::Connections_IO _evqc_engine_out;
  event::queue::Connection_In _evqc_pipeline_return;
  std::thread _thread;
};
} // namespace fre
