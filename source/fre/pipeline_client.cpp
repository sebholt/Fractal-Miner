/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "pipeline_client.hpp"
#include <fre/pipeline.hpp>
#include <fre/pipeline/pool.hpp>

namespace fre
{

Pipeline_Client::Pipeline_Client ( Hub & hub_n )
: _pool_entry ( std::make_unique< pipeline::Pool_Entry > ( hub_n ) )
{
}

Pipeline_Client::~Pipeline_Client ()
{
  disconnect ();
}

void
Pipeline_Client::connect ( std::shared_ptr< Pipeline > const & pipeline_n )
{
  disconnect ();
  if ( pipeline_n ) {
    _pool = pipeline_n->pool ();
    _pool->client_connect ( _pool_entry.get () );
    _pipeline = pipeline_n;
  }
}

void
Pipeline_Client::disconnect ()
{
  if ( is_connected () ) {
    _pool->client_disconnect ( _pool_entry.get () );
    _pool = nullptr;
    _pipeline.reset ();
  }
}

void
Pipeline_Client::notify ()
{
  if ( is_connected () ) {
    _pool->client_notify ( _pool_entry.get () );
  }
}
} // namespace fre
