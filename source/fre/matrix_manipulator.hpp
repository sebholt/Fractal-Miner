/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/resize_deltas.hpp>
#include <geo/point.hpp>
#include <cstdint>
#include <debug_assert.hpp>
#include <message_streams.hpp>
#include <vector>

namespace fre
{

/// @brief Matrix manipulator
///
template < class MT, bool RN >
class Matrix_Manipulator
{
  public:
  // -- Types
  typedef typename MT::Tile_Reference Tile_Reference;
  typedef typename MT::Tile_Row Tile_Row;
  typedef typename MT::Tile_Rows Tile_Rows;
  typedef std::vector< Tile_Reference > Tile_Reference_Vector;

  public:
  // -- Constructors

  Matrix_Manipulator ( MT & matrix_n )
  : _matrix ( matrix_n )
  {
  }

  Matrix_Manipulator ( MT & matrix_n, Tile_Reference_Vector & new_tiles_n )
  : _matrix ( matrix_n )
  , _new_tiles ( &new_tiles_n )
  {
  }

  // -- Modification

  void
  add_columns_left ( std::uint32_t num_n );

  void
  add_columns_right ( std::uint32_t num_n );

  void
  add_rows_bottom ( std::uint32_t num_n );

  void
  add_rows_top ( std::uint32_t num_n );

  void
  remove_columns_left ( std::uint32_t num_n );

  void
  remove_columns_right ( std::uint32_t num_n );

  void
  remove_rows_bottom ( std::uint32_t num_n );

  void
  remove_rows_top ( std::uint32_t num_n );

  // -- Looping

  void
  loop_left_to_right ( std::uint32_t num_n );

  void
  loop_right_to_left ( std::uint32_t num_n );

  void
  loop_bottom_to_top ( std::uint32_t num_n );

  void
  loop_top_to_bottom ( std::uint32_t num_n );

  void
  loop_horizontal ( std::int32_t num_n )
  {
    if ( num_n < 0 ) {
      loop_right_to_left ( -num_n );
    } else if ( num_n > 0 ) {
      loop_left_to_right ( num_n );
    }
  }

  void
  loop_vertical ( std::int32_t num_n )
  {
    if ( num_n < 0 ) {
      loop_top_to_bottom ( -num_n );
    } else if ( num_n > 0 ) {
      loop_bottom_to_top ( num_n );
    }
  }

  void
  loop ( geo::Int32_Pair const & loops_n )
  {
    loop_horizontal ( loops_n.first () );
    loop_vertical ( loops_n.second () );
  }

  private:
  // -- Looping

  void
  rewrite_tile_columns ();

  void
  rewrite_tile_rows ();

  // -- Creation

  Tile_Reference
  create_tile ( std::int32_t col_id_n, std::int32_t row_id_n );

  Tile_Row
  create_row ( std::int32_t row_id_n );

  private:
  MT & _matrix;
  Tile_Reference_Vector * _new_tiles = nullptr;
};

template < class MT, bool RN >
inline typename MT::Tile_Reference
Matrix_Manipulator< MT, RN >::create_tile ( std::int32_t col_id_n,
                                            std::int32_t row_id_n )
{
  Tile_Reference tile_ref (
      _matrix.tile_create ( geo::Int32_Point ( col_id_n, row_id_n ) ) );
  if ( RN ) {
    _new_tiles->emplace_back ( tile_ref );
  }
  return tile_ref;
}

template < class MT, bool RN >
inline typename MT::Tile_Row
Matrix_Manipulator< MT, RN >::create_row ( std::int32_t row_id_n )
{
  Tile_Row row;
  if ( _matrix.columns () != 0 ) {
    std::int32_t const col_id_beg ( _matrix._column_id_ends.first () + 1 );
    std::int32_t const col_id_end ( _matrix._column_id_ends.second () );
    for ( std::int32_t col_id = col_id_beg; col_id != col_id_end; ++col_id ) {
      row.emplace_back ( create_tile ( col_id, row_id_n ) );
    }
  }
  DEBUG_ASSERT ( row.size () == _matrix.columns () );
  return row;
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::add_columns_left ( std::uint32_t num_n )
{
  std::int32_t const col_id_first ( _matrix._column_id_ends.first () );
  std::int32_t const col_id_end ( col_id_first - std::int32_t ( num_n ) );
  if ( _matrix.rows () != 0 ) {
    std::int32_t row_id ( _matrix._row_id_ends.first () + 1 );
    for ( Tile_Row & row : _matrix._tile_rows ) {
      for ( std::int32_t col_id = col_id_first; col_id != col_id_end;
            --col_id ) {
        row.emplace_front ( create_tile ( col_id, row_id ) );
      }
      DEBUG_ASSERT ( row.size () == ( _matrix.columns () + num_n ) );
      ++row_id;
    }
  }
  // Update statistics
  _matrix._size.width_ref () += num_n;
  _matrix._column_id_ends.first_ref () -= std::int32_t ( num_n );
  _matrix.compute_size_px ();
  DEBUG_ASSERT ( ( _matrix._column_id_ends.second () -
                   _matrix._column_id_ends.first () - 1 ) ==
                 std::int32_t ( _matrix.columns () ) );
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::add_columns_right ( std::uint32_t num_n )
{
  std::int32_t const col_id_first ( _matrix._column_id_ends.second () );
  std::int32_t const col_id_end ( col_id_first + std::int32_t ( num_n ) );
  if ( _matrix.rows () != 0 ) {
    std::int32_t row_id ( _matrix._row_id_ends.first () + 1 );
    for ( Tile_Row & row : _matrix._tile_rows ) {
      for ( std::int32_t col_id = col_id_first; col_id != col_id_end;
            ++col_id ) {
        row.emplace_back ( create_tile ( col_id, row_id ) );
      }
      DEBUG_ASSERT ( row.size () == ( _matrix.columns () + num_n ) );
      ++row_id;
    }
  }
  // Update statistics
  _matrix._size.width_ref () += num_n;
  _matrix._column_id_ends.second_ref () += std::int32_t ( num_n );
  _matrix.compute_size_px ();
  DEBUG_ASSERT ( ( _matrix._column_id_ends.second () -
                   _matrix._column_id_ends.first () - 1 ) ==
                 std::int32_t ( _matrix.columns () ) );
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::add_rows_bottom ( std::uint32_t num_n )
{
  std::int32_t const row_id_first ( _matrix._row_id_ends.first () );
  std::int32_t const row_id_end ( row_id_first - std::int32_t ( num_n ) );
  for ( std::int32_t row_id = row_id_first; row_id != row_id_end; --row_id ) {
    _matrix._tile_rows.emplace_front ( create_row ( row_id ) );
  }
  // Update statistics
  _matrix._size.height_ref () += num_n;
  _matrix._row_id_ends.first_ref () -= std::int32_t ( num_n );
  _matrix.compute_size_px ();
  DEBUG_ASSERT ( _matrix._tile_rows.size () == _matrix.rows () );
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::add_rows_top ( std::uint32_t num_n )
{
  std::int32_t const row_id_first ( _matrix._row_id_ends.second () );
  std::int32_t const row_id_end ( row_id_first + std::int32_t ( num_n ) );
  for ( std::int32_t row_id = row_id_first; row_id != row_id_end; ++row_id ) {
    _matrix._tile_rows.emplace_back ( create_row ( row_id ) );
  }
  // Update statistics
  _matrix._size.height_ref () += num_n;
  _matrix._row_id_ends.second_ref () += std::int32_t ( num_n );
  _matrix.compute_size_px ();
  DEBUG_ASSERT ( _matrix._tile_rows.size () == _matrix.rows () );
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::remove_columns_left ( std::uint32_t num_n )
{
  DEBUG_ASSERT ( _matrix._size.width () >= num_n );
  for ( Tile_Row & row : _matrix._tile_rows ) {
    for ( std::uint32_t ii = num_n; ii != 0; --ii ) {
      _matrix.tile_invalidate ( row.front () );
      row.pop_front ();
    }
    DEBUG_ASSERT ( row.size () == ( _matrix.columns () - num_n ) );
  }
  // Update statistics
  _matrix._size.width_ref () -= num_n;
  _matrix._column_id_ends.first_ref () += std::int32_t ( num_n );
  _matrix.compute_size_px ();
  DEBUG_ASSERT ( ( _matrix._column_id_ends.second () -
                   _matrix._column_id_ends.first () - 1 ) ==
                 std::int32_t ( _matrix.columns () ) );
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::remove_columns_right ( std::uint32_t num_n )
{
  DEBUG_ASSERT ( _matrix._size.width () >= num_n );
  for ( Tile_Row & row : _matrix._tile_rows ) {
    for ( std::uint32_t ii = num_n; ii != 0; --ii ) {
      _matrix.tile_invalidate ( row.back () );
      row.pop_back ();
    }
    DEBUG_ASSERT ( row.size () == ( _matrix.columns () - num_n ) );
  }
  // Update statistics
  _matrix._size.width_ref () -= num_n;
  _matrix._column_id_ends.second_ref () -= std::int32_t ( num_n );
  _matrix.compute_size_px ();
  DEBUG_ASSERT ( ( _matrix._column_id_ends.second () -
                   _matrix._column_id_ends.first () - 1 ) ==
                 std::int32_t ( _matrix.columns () ) );
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::remove_rows_bottom ( std::uint32_t num_n )
{
  DEBUG_ASSERT ( _matrix._size.height () >= num_n );
  for ( std::uint32_t ii = num_n; ii != 0; --ii ) {
    for ( Tile_Reference const & tile : _matrix._tile_rows.front () ) {
      _matrix.tile_invalidate ( tile );
    }
    _matrix._tile_rows.pop_front ();
  }
  // Update statistics
  _matrix._size.height_ref () -= num_n;
  _matrix._row_id_ends.first_ref () += std::int32_t ( num_n );
  _matrix.compute_size_px ();
  DEBUG_ASSERT ( _matrix._tile_rows.size () == _matrix.rows () );
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::remove_rows_top ( std::uint32_t num_n )
{
  DEBUG_ASSERT ( _matrix._size.height () >= num_n );
  for ( std::uint32_t ii = num_n; ii != 0; --ii ) {
    for ( Tile_Reference const & tile : _matrix._tile_rows.back () ) {
      _matrix.tile_invalidate ( tile );
    }
    _matrix._tile_rows.pop_back ();
  }
  // Update statistics
  _matrix._size.height_ref () -= num_n;
  _matrix._row_id_ends.second_ref () -= std::int32_t ( num_n );
  _matrix.compute_size_px ();
  DEBUG_ASSERT ( _matrix._tile_rows.size () == _matrix.rows () );
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::rewrite_tile_columns ()
{
  for ( Tile_Row & tile_row : _matrix._tile_rows ) {
    std::int32_t col = _matrix._column_id_ends.first () + 1;
    for ( Tile_Reference const & tref : tile_row ) {
      _matrix.tile_change_column ( tref, col++ );
    }
  }
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::rewrite_tile_rows ()
{
  std::int32_t row = _matrix._row_id_ends.first () + 1;
  for ( Tile_Row & tile_row : _matrix._tile_rows ) {
    for ( Tile_Reference const & tref : tile_row ) {
      _matrix.tile_change_row ( tref, row );
    }
    ++row;
  }
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::loop_left_to_right ( std::uint32_t num_n )
{
  if ( ( _matrix.columns () != 0 ) && ( num_n != 0 ) ) {
    _matrix._column_id_ends.first_ref () += std::int32_t ( num_n );
    _matrix._column_id_ends.second_ref () += std::int32_t ( num_n );
    if ( num_n < _matrix.columns () ) {
      for ( Tile_Row & row : _matrix._tile_rows ) {
        std::int32_t col =
            _matrix._column_id_ends.second () - std::int32_t ( num_n );
        for ( std::uint32_t ii = num_n; ii != 0; --ii ) {
          row.emplace_back ( std::move ( row.front () ) );
          row.pop_front ();
          Tile_Reference const & tile_ref = row.back ();
          _matrix.tile_change_column ( row.back (), col++ );
          if ( RN ) {
            _new_tiles->emplace_back ( tile_ref );
          }
        }
      }
    } else {
      rewrite_tile_columns ();
    }
  }
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::loop_right_to_left ( std::uint32_t num_n )
{
  if ( ( _matrix.columns () != 0 ) && ( num_n != 0 ) ) {
    _matrix._column_id_ends.first_ref () -= std::int32_t ( num_n );
    _matrix._column_id_ends.second_ref () -= std::int32_t ( num_n );
    if ( num_n < _matrix.columns () ) {
      for ( Tile_Row & row : _matrix._tile_rows ) {
        std::int32_t col =
            _matrix._column_id_ends.first () + std::int32_t ( num_n );
        for ( std::uint32_t ii = num_n; ii != 0; --ii ) {
          row.emplace_front ( std::move ( row.back () ) );
          row.pop_back ();
          Tile_Reference const & tile_ref = row.front ();
          _matrix.tile_change_column ( tile_ref, col-- );
          if ( RN ) {
            _new_tiles->emplace_back ( tile_ref );
          }
        }
      }
    } else {
      rewrite_tile_columns ();
    }
  }
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::loop_bottom_to_top ( std::uint32_t num_n )
{
  if ( ( _matrix.rows () != 0 ) && ( num_n != 0 ) ) {
    _matrix._row_id_ends.first_ref () += std::int32_t ( num_n );
    _matrix._row_id_ends.second_ref () += std::int32_t ( num_n );
    if ( num_n < _matrix.rows () ) {
      std::int32_t row =
          _matrix._row_id_ends.second () - std::int32_t ( num_n );
      for ( std::uint32_t ii = num_n; ii != 0; --ii ) {
        _matrix._tile_rows.emplace_back (
            std::move ( _matrix._tile_rows.front () ) );
        _matrix._tile_rows.pop_front ();
        for ( Tile_Reference const & tile_ref : _matrix._tile_rows.back () ) {
          _matrix.tile_change_row ( tile_ref, row );
          if ( RN ) {
            _new_tiles->emplace_back ( tile_ref );
          }
        }
        ++row;
      }
      DEBUG_ASSERT ( row == _matrix._row_id_ends.second () );
    } else {
      rewrite_tile_rows ();
    }
  }
}

template < class MT, bool RN >
void
Matrix_Manipulator< MT, RN >::loop_top_to_bottom ( std::uint32_t num_n )
{
  if ( ( _matrix.rows () != 0 ) && ( num_n != 0 ) ) {
    _matrix._row_id_ends.first_ref () -= std::int32_t ( num_n );
    _matrix._row_id_ends.second_ref () -= std::int32_t ( num_n );
    if ( num_n < _matrix.rows () ) {
      std::int32_t row = _matrix._row_id_ends.first () + std::int32_t ( num_n );
      for ( std::uint32_t ii = num_n; ii != 0; --ii ) {
        _matrix._tile_rows.emplace_front (
            std::move ( _matrix._tile_rows.back () ) );
        _matrix._tile_rows.pop_back ();
        for ( Tile_Reference const & tile_ref : _matrix._tile_rows.front () ) {
          _matrix.tile_change_row ( tile_ref, row );
          if ( RN ) {
            _new_tiles->emplace_back ( tile_ref );
          }
        }
        --row;
      }
      DEBUG_ASSERT ( row == _matrix._row_id_ends.first () );
    } else {
      rewrite_tile_rows ();
    }
  }
}
} // namespace fre
