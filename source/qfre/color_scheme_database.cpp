/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "color_scheme_database.hpp"
#include "color_schemes/color_steps.hpp"
#include "color_schemes/color_wave.hpp"
#include "color_schemes/grey_steps.hpp"
#include "color_schemes/grey_wave.hpp"
#include "color_schemes/tentacle.hpp"

namespace qfre
{

Color_Scheme_Database::Color_Scheme_Database ()
{
  _schemes.emplace_back ( new color_schemes::Color_Steps );
  _schemes.emplace_back ( new color_schemes::Color_Wave );
  _schemes.emplace_back ( new color_schemes::Grey_Steps );
  _schemes.emplace_back ( new color_schemes::Grey_Wave );
  _schemes.emplace_back ( new color_schemes::Tentacle );
}

Color_Scheme_Database::~Color_Scheme_Database () {}

Color_Scheme_Reference
Color_Scheme_Database::acquire_color_scheme ( QString const & scheme_id_n )
{
  for ( Color_Scheme_Reference const & scheme : _schemes ) {
    if ( scheme->id_string () == scheme_id_n ) {
      return scheme;
    }
  }
  return Color_Scheme_Reference ();
}
} // namespace qfre
