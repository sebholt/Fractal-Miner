/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "events.hpp"
#include <qfre/renderer/renderer.hpp>

namespace qfre::renderer::events
{

Viewport::Viewport ()
: Base_Event ( &Renderer::process_event_viewport )
{
}

Matrix_Move::Matrix_Move ()
: Base_Event ( &Renderer::process_event_matrix_move )
{
}

Matrix_Resize::Matrix_Resize ()
: Matrix_Move ( &Renderer::process_event_matrix_resize )
{
}

Matrix_Scale::Matrix_Scale ()
: Base_Event ( &Renderer::process_event_matrix_scale )
{
}

Matrix_Loop::Matrix_Loop ()
: Matrix_Move ( &Renderer::process_event_matrix_loop )
{
}

Snapshot::Snapshot ()
: Base_Event ( &Renderer::process_event_snapshot )
{
}

Zoom::Zoom ()
: Base_Event ( &Renderer::process_event_zoom )
{
}

Clear::Clear ()
: Base_Event ( &Renderer::process_event_clear )
{
}

Clear_Foreground::Clear_Foreground ()
: Base_Event ( &Renderer::process_event_clear_foreground )
{
}

Render_Settings::Render_Settings ()
: Base_Event ( &Renderer::process_event_render_settings )
{
}

Iterator_Settings::Iterator_Settings ()
: Base_Event ( &Renderer::process_event_iterator_settings )
{
}

Color_Scheme::Color_Scheme ()
: Base_Event ( &Renderer::process_event_color_scheme )
{
}

Color_Offset::Color_Offset ()
: Base_Event ( &Renderer::process_event_color_offset )
{
}
} // namespace qfre::renderer::events
