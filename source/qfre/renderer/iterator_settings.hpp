/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>

namespace qfre::renderer
{

/// @brief Iterator settings
///
class Iterator_Settings
{
  public:
  // -- Constructors

  Iterator_Settings () = default;

  ~Iterator_Settings () = default;

  // -- Maximum iteration count

  std::uint32_t
  iterations_max () const
  {
    return _iterations_max;
  }

  void
  set_iterations_max ( std::uint32_t num_n )
  {
    _iterations_max = num_n;
  }

  private:
  std::uint32_t _iterations_max = 0;
};
} // namespace qfre::renderer
