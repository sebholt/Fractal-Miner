/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/reference.hpp>
#include <geo/geo.hpp>
#include <qfre/renderer/atlas/texture.hpp>
#include <qfre/renderer/iterator_settings.hpp>
#include <qfre/renderer/paint_tile.hpp>
#include <map>
#include <vector>

namespace qfre::renderer
{

// -- Forward declaration
class Paint_Tiles;
typedef fre::Reference< Paint_Tiles > Paint_Tiles_Reference;

/// @brief Paint_Tiles
///
class Paint_Tiles
{
  public:
  // -- Types
  typedef std::map< qfre::texture_atlas::Texture_Reference,
                    std::vector< Paint_Tile > >
      Tiles_Map;
  // -- Friends
  friend Paint_Tiles_Reference;

  public:
  // -- Constructors

  Paint_Tiles ();

  ~Paint_Tiles ();

  void
  clear ()
  {
    _tiles_map.clear ();
  }

  // -- Position

  /// @brief Position in viewport coordinates
  geo::Double_Point const &
  position () const
  {
    return _position;
  }

  void
  set_position ( geo::Double_Point const & pos_n )
  {
    _position = pos_n;
  }

  void
  set_position ( double pos_x_n, double pos_y_n )
  {
    _position.set ( pos_x_n, pos_y_n );
  }

  void
  add_position ( geo::Double_Size const & delta_n )
  {
    _position.add ( delta_n.width (), delta_n.height () );
  }

  // -- Scale

  /// @brief Scale from position()
  geo::Double_Size const &
  scale () const
  {
    return _scale;
  }

  void
  set_scale ( geo::Double_Size const & size_n )
  {
    _scale = size_n;
  }

  void
  set_scale ( double scale_x_n, double scale_y_n )
  {
    _scale.set ( scale_x_n, scale_y_n );
  }

  void
  multiply_scale ( double amount_n )
  {
    _scale.width_ref () *= amount_n;
    _scale.height_ref () *= amount_n;
  }

  // -- Tiles count

  /// @brief Number of tiles
  std::uint32_t
  tiles_count () const
  {
    return _tiles_count;
  }

  void
  set_tiles_count ( std::uint32_t num_n )
  {
    _tiles_count = num_n;
  }

  // -- Tiles map

  Tiles_Map &
  tiles_map_ref ()
  {
    return _tiles_map;
  }

  const Tiles_Map &
  tiles_map () const
  {
    return _tiles_map;
  }

  // -- Settings

  Iterator_Settings const &
  iterator_settings () const
  {
    return _iterator_settings;
  }

  void
  set_iterator_settings ( Iterator_Settings const & settings_n )
  {
    _iterator_settings = settings_n;
  }

  private:
  // -- Position and scale
  geo::Double_Point _position;
  geo::Double_Size _scale = { 1.0, 1.0 };
  // -- Tiles
  std::uint32_t _tiles_count = 0;
  Tiles_Map _tiles_map;
  // -- Settings
  Iterator_Settings _iterator_settings;
  // -- Reference count
  std::uint32_t _reference_count = 0;
};
} // namespace qfre::renderer
