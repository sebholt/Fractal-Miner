/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <geo/geo.hpp>
#include <qfre/renderer/atlas/tile_reference.hpp>

namespace qfre::renderer
{

/// @brief Paint_Tile
///
class Paint_Tile
{
  public:
  geo::Float_Point vertex_pos;
  geo::Float_Point uv_pos;
  texture_atlas::Tile_Reference tile;
};
} // namespace qfre::renderer
