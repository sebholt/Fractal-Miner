/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "matrix.hpp"
#include <fre/matrix_manipulator.hpp>
#include <debug_assert.hpp>

namespace qfre::renderer
{

Matrix::Matrix ( geo::UInt16_Size const & tile_size_n,
                 geo::UInt16_Size const & tile_overlap_n )
: _tile_size ( tile_size_n )
, _tile_overlap ( tile_overlap_n )
{
  _inner_tile_size.set ( _tile_size.width () - 2 * _tile_overlap.width (),
                         _tile_size.height () - 2 * _tile_overlap.height () );
}

void
Matrix::compute_size_px ()

{
  _size_px.set_width ( columns () * inner_tile_size ().width () );
  _size_px.set_height ( rows () * inner_tile_size ().height () );
}

void
Matrix::paintable_register ( Tile_Reference const & tile_ref_n )
{
  if ( tile_ref_n->tile () ) {
    auto & vec = _paintables_map[ tile_ref_n->tile ().texture () ];
    vec.emplace_back ( tile_ref_n );
    ++_paintables_count;
  }
}

void
Matrix::paintable_unregister ( Tile_Reference const & tile_ref_n )
{
  if ( tile_ref_n->tile () ) {
    auto itp = _paintables_map.find ( tile_ref_n->tile ().texture () );
    if ( itp != _paintables_map.end () ) {
      auto & vec = itp->second;
      auto itv = std::find ( vec.begin (), vec.end (), tile_ref_n );
      if ( itv != vec.end () ) {
        vec.erase ( itv );
        // Remove empty vector from paintables map
        if ( vec.empty () ) {
          _paintables_map.erase ( itp );
          DEBUG_ASSERT ( _paintables_count != 0 );
          --_paintables_count;
        }
      }
    }
    // Clear texture tile reference
    tile_ref_n->clear_texture_tile ();
  }
}

void
Matrix::paintables_clear ()
{
  for ( auto & tile_row : _tile_rows ) {
    for ( auto & tile_ref : tile_row ) {
      // Clear texture tile reference
      tile_ref->clear_texture_tile ();
    }
  }

  // Clear paintables
  _paintables_count = 0;
  _paintables_map.clear ();
}

void
Matrix::resize ( fre::Resize_Deltas const & deltas_n )
{
  Manipulator man ( *this );

  // -- Remove rows
  // Bottom
  if ( deltas_n.vertical ().first () < 0 ) {
    man.remove_rows_bottom ( -deltas_n.vertical ().first () );
  }
  // Top
  if ( deltas_n.vertical ().second () < 0 ) {
    man.remove_rows_top ( -deltas_n.vertical ().second () );
  }

  // -- Remove columns
  // Left
  if ( deltas_n.horizontal ().first () < 0 ) {
    man.remove_columns_left ( -deltas_n.horizontal ().first () );
  }
  // Right
  if ( deltas_n.horizontal ().second () < 0 ) {
    man.remove_columns_right ( -deltas_n.horizontal ().second () );
  }

  // -- Add columns
  // Left
  if ( deltas_n.horizontal ().first () > 0 ) {
    man.add_columns_left ( deltas_n.horizontal ().first () );
  }
  // Right
  if ( deltas_n.horizontal ().second () > 0 ) {
    man.add_columns_right ( deltas_n.horizontal ().second () );
  }

  // -- Add rows
  // Bottom
  if ( deltas_n.vertical ().first () > 0 ) {
    man.add_rows_bottom ( deltas_n.vertical ().first () );
  }
  // Top
  if ( deltas_n.vertical ().second () > 0 ) {
    man.add_rows_top ( deltas_n.vertical ().second () );
  }
}
} // namespace qfre::renderer
