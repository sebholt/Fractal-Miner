/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <geo/geo.hpp>
#include <qfre/renderer/atlas/tile.hpp>
#include <QOpenGLFunctions>
#include <debug_assert.hpp>

namespace qfre::texture_atlas
{

// -- Forward declaration
class Texture;

/// @brief Texture tile reference
///
class Tile_Reference
{
  public:
  // -- Friends
  friend Texture;

  public:
  // -- Constructors

  Tile_Reference () = default;

  Tile_Reference ( Tile_Reference const & ref_n )
  : _texture_tile ( ref_n._texture_tile )
  {
    if ( is_valid () ) {
      _texture_tile->reference ();
    }
  }

  Tile_Reference ( Tile_Reference && ref_n )
  : _texture_tile ( ref_n._texture_tile )
  {
    ref_n._texture_tile = nullptr;
  }

  ~Tile_Reference () { reset (); }

  // -- Texture tile

  Tile const *
  tile () const
  {
    return _texture_tile;
  }

  bool
  is_valid () const
  {
    return ( _texture_tile != nullptr );
  }

  Texture *
  texture () const
  {
    return _texture_tile->texture ();
  }

  // -- Tile position

  /// @brief Float uv position of the tile in the texture
  geo::Float_Point const &
  pos_uv () const
  {
    DEBUG_ASSERT ( is_valid () );
    return _texture_tile->pos_uv ();
  }

  /// @brief Integer position of the tile in the texture
  geo::UInt16_Point const &
  pos () const
  {
    DEBUG_ASSERT ( is_valid () );
    return _texture_tile->pos ();
  }

  // -- Clear

  void
  reset ()
  {
    if ( is_valid () ) {
      _texture_tile->dereference ();
      _texture_tile = nullptr;
    }
  }

  // -- Texture tile upload

  bool
  upload ( void const * data_n, QOpenGLFunctions * glf_n ) const;

  // -- Cast operators

  operator bool () const { return ( _texture_tile != nullptr ); }

  // -- Assignment operators

  Tile_Reference &
  operator= ( Tile_Reference const & ref_n )
  {
    if ( &ref_n != this ) {
      reset ();
      _texture_tile = ref_n._texture_tile;
      if ( is_valid () ) {
        _texture_tile->reference ();
      }
    }
    return *this;
  }

  Tile_Reference &
  operator= ( Tile_Reference && ref_n )
  {
    if ( &ref_n != this ) {
      reset ();
      _texture_tile = ref_n._texture_tile;
      ref_n._texture_tile = nullptr;
    }
    return *this;
  }

  private:
  Tile_Reference ( Tile * texture_tile_n )
  : _texture_tile ( texture_tile_n )
  {
    _texture_tile->reference ();
  }

  private:
  Tile * _texture_tile = nullptr;
};
} // namespace qfre::texture_atlas
