/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <qfre/renderer/atlas/texture.hpp>
#include <qfre/renderer/atlas/tile_reference.hpp>
#include <QOpenGLFunctions>

namespace qfre::texture_atlas
{

/// @brief Texture settings
///
struct Texture_Settings
{
  GLenum internal_format = GL_RGBA;
  GLenum minification_filter = GL_NEAREST;
  GLenum magnification_filter = GL_NEAREST;
  GLenum wrap_mode = GL_REPEAT;
  GLenum source_pixel_format = GL_RGBA;
  GLenum source_pixel_type = GL_UNSIGNED_BYTE;
  std::uint8_t unpack_alignment = 4;
};

/// @brief Atlas
///
class Atlas
{
  public:
  // -- Friends
  friend Texture;

  public:
  // -- Constructors

  Atlas ( geo::UInt16_Size texture_size_n, geo::UInt16_Size tile_size_n );

  ~Atlas ();

  // -- Sizes

  geo::UInt16_Size const &
  texture_size () const
  {
    return _texture_size;
  }

  std::uint16_t
  texture_width () const
  {
    return _texture_size.width ();
  }

  std::uint16_t
  texture_height () const
  {
    return _texture_size.height ();
  }

  geo::UInt16_Size const &
  tile_size () const
  {
    return _tile_size;
  }

  /// @brief Number of tile rows in a texture
  std::uint16_t
  tile_columns () const
  {
    return _tile_columns;
  }

  /// @brief Number of tile rows in a texture
  std::uint16_t
  tile_rows () const
  {
    return _tile_rows;
  }

  /// @brief Number of tiles in a texture
  std::uint32_t
  tiles () const
  {
    return _tiles;
  }

  // -- Texture settings

  Texture_Settings const &
  texture_settings () const
  {
    return _texture_settings;
  }

  void
  set_texture_settings ( Texture_Settings const & settings_n );

  // -- Tiles

  Tile_Reference
  acquire_tile ( QOpenGLFunctions * glf_n );

  private:
  void
  texture_full ( Texture_Reference const & texture_n );

  void
  texture_free ( Texture_Reference const & texture_n );

  private:
  geo::UInt16_Size _texture_size = { 0, 0 };
  geo::UInt16_Size _tile_size = { 0, 0 };
  std::uint16_t _tile_columns = 0;
  std::uint16_t _tile_rows = 0;
  std::uint16_t _tiles = 0;
  Texture_Settings _texture_settings;
  std::vector< Texture_Reference > _textures_free;
  std::vector< Texture_Reference > _textures_full;
};
} // namespace qfre::texture_atlas
