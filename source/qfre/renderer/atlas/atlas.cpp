/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "atlas.hpp"
#include <qfre/renderer/atlas/texture.hpp>
#include <algorithm>
#include <application_config.hpp>
#include <debug_assert.hpp>
#include <message_streams.hpp>

namespace qfre::texture_atlas
{

Atlas::Atlas ( geo::UInt16_Size texture_size_n, geo::UInt16_Size tile_size_n )
: _texture_size ( texture_size_n )
, _tile_size ( tile_size_n )
{
  _tile_columns = ( _texture_size.width () / _tile_size.width () );
  _tile_rows = ( _texture_size.height () / _tile_size.height () );
  _tiles = _tile_columns * _tile_rows;

  if ( DEBUG_QFRE_TEXTURE_ATLAS ) {
    Debug_Stream oss;
    oss << "Atlas::Atlas\n"
        << "  texture_size " << _texture_size << "\n"
        << "  tile_size " << _tile_size << "\n"
        << "  tile_columns " << _tile_columns << "\n"
        << "  tile_rows " << _tile_rows << "\n";
  }
}

Atlas::~Atlas () {}

Tile_Reference
Atlas::acquire_tile ( QOpenGLFunctions * glf_n )
{
  // Add a new texture on demand
  if ( _textures_free.empty () ) {
    Texture_Reference tex_ref ( Texture_Reference::created ( this ) );
    if ( tex_ref->allocate ( glf_n ) ) {
      _textures_free.emplace_back ( std::move ( tex_ref ) );
    }
  }
  if ( !_textures_free.empty () ) {
    return _textures_free.back ()->acquire_tile ();
  }
  return Tile_Reference ();
}

void
Atlas::set_texture_settings ( Texture_Settings const & settings_n )
{
  _texture_settings = settings_n;
}

void
Atlas::texture_full ( Texture_Reference const & texture_n )
{
  auto it =
      std::find ( _textures_free.begin (), _textures_free.end (), texture_n );
  if ( it != _textures_free.end () ) {
    _textures_full.emplace_back ( std::move ( *it ) );
    _textures_free.erase ( it );
  }
}

void
Atlas::texture_free ( Texture_Reference const & texture_n )
{
  auto it =
      std::find ( _textures_full.begin (), _textures_full.end (), texture_n );
  if ( it != _textures_full.end () ) {
    _textures_free.emplace_back ( std::move ( *it ) );
    _textures_full.erase ( it );
  }
}
} // namespace qfre::texture_atlas
