/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/reference.hpp>
#include <geo/geo.hpp>
#include <qfre/renderer/atlas/tile.hpp>
#include <QOpenGLFunctions>
#include <atomic>
#include <vector>

namespace qfre::texture_atlas
{

// -- Forward declaration
class Texture;
class Atlas;
typedef fre::Reference< Texture > Texture_Reference;

/// @brief Texture
///
class Texture
{
  public:
  // -- Friends
  friend Atlas;
  friend Texture_Reference;
  friend Tile;

  public:
  // -- Constructors

  Texture ( Atlas * atlas_n );

  ~Texture () = default;

  // -- Accessors

  /// @brief Opengl texture id
  std::uint32_t
  gl_id () const
  {
    return _gl_id;
  }

  Atlas *
  atlas () const
  {
    return _atlas;
  }

  // Bind / unbind

  bool
  bind ( QOpenGLFunctions * glf_n );

  bool
  release ( QOpenGLFunctions * glf_n );

  private:
  // -- Texture tile users

  Tile_Reference
  acquire_tile ();

  void
  release_tile ( Tile * tile_n );

  // -- Allocation / deallocation

  bool
  allocate ( QOpenGLFunctions * glf_n );

  void
  deallocate ( QOpenGLFunctions * glf_n );

  private:
  std::uint32_t _gl_id = 0;
  Atlas * _atlas;
  std::vector< Tile > _tiles;
  std::vector< Tile * > _tiles_avail;
  std::uint32_t _reference_count = 0;
};
} // namespace qfre::texture_atlas
