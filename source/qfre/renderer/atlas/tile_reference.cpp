/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "tile_reference.hpp"
#include <qfre/gl_utility.hpp>
#include <qfre/renderer/atlas/atlas.hpp>
#include <qfre/renderer/atlas/texture.hpp>

namespace qfre::texture_atlas
{

bool
Tile_Reference::upload ( void const * data_n, QOpenGLFunctions * glf_n ) const
{
  if ( is_valid () ) {
    Texture & tex = *texture ();
    auto const & settings = tex.atlas ()->texture_settings ();
    auto const & tile_size = tex.atlas ()->tile_size ();
    glf_n->glBindTexture ( GL_TEXTURE_2D, tex.gl_id () );
    if ( !gl_error_print ( "Texture::upload_tile: glBindTexture: ", glf_n ) ) {
      glf_n->glPixelStorei ( GL_UNPACK_ROW_LENGTH, 0 );
      glf_n->glPixelStorei ( GL_UNPACK_ALIGNMENT, settings.unpack_alignment );
      glf_n->glTexSubImage2D ( GL_TEXTURE_2D,
                               0,
                               pos ().x (),
                               pos ().y (),
                               tile_size.width (),
                               tile_size.height (),
                               settings.source_pixel_format,
                               settings.source_pixel_type,
                               data_n );
      if ( !gl_error_print ( "Texture::deallocate: glTexSubImage2D: ",
                             glf_n ) ) {
        return true;
      }
    }
  }
  return false;
}
} // namespace qfre::texture_atlas
