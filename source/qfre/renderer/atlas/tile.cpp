/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "tile.hpp"
#include <qfre/renderer/atlas/texture.hpp>

namespace qfre::texture_atlas
{

void
Tile::release ()
{
  _texture->release_tile ( this );
}
} // namespace qfre::texture_atlas
