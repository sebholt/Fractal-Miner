/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "texture.hpp"
#include <qfre/gl_utility.hpp>
#include <qfre/renderer/atlas/atlas.hpp>
#include <qfre/renderer/atlas/tile_reference.hpp>
#include <application_config.hpp>
#include <message_streams.hpp>

namespace qfre::texture_atlas
{

Texture::Texture ( Atlas * atlas_n )
: _atlas ( atlas_n )
{
  // -- Fill tile geometries
  _tiles.reserve ( atlas ()->tiles () );
  std::uint32_t const rows = atlas ()->tile_rows ();
  std::uint32_t const columns = atlas ()->tile_columns ();
  std::uint32_t const tile_width = atlas ()->tile_size ().width ();
  std::uint32_t const tile_height = atlas ()->tile_size ().height ();
  double const texture_width ( atlas ()->texture_width () );
  double const texture_height ( atlas ()->texture_height () );
  for ( std::uint32_t yy = 0; yy != rows; ++yy ) {
    for ( std::uint32_t xx = 0; xx != columns; ++xx ) {
      geo::UInt16_Point pos ( xx * tile_width, yy * tile_height );
      _tiles.emplace_back (
          this,
          pos,
          geo::Float_Point ( double ( pos.x () ) / texture_width,
                             double ( pos.y () ) / texture_height ) );
    }
  }

  // Register all tiles as available
  _tiles_avail.reserve ( _tiles.size () );
  for ( auto & tile : _tiles ) {
    _tiles_avail.push_back ( &tile );
  }
}

bool
Texture::bind ( QOpenGLFunctions * glf_n )
{
  glf_n->glBindTexture ( GL_TEXTURE_2D, _gl_id );
  return !gl_error_print ( "Texture::bind: glBindTexture: ", glf_n );
}

bool
Texture::release ( QOpenGLFunctions * glf_n )
{
  glf_n->glBindTexture ( GL_TEXTURE_2D, 0 );
  return !gl_error_print ( "Texture::release: glBindTexture: ", glf_n );
}

bool
Texture::allocate ( QOpenGLFunctions * glf_n )
{
  auto const & texs = atlas ()->texture_settings ();
  // Generate texture
  glf_n->glGenTextures ( 1, &_gl_id );
  if ( gl_error_print ( "Texture::allocate: glGenTextures: ", glf_n ) ) {
    _gl_id = 0;
    return false;
  }

  // Bind texture
  glf_n->glBindTexture ( GL_TEXTURE_2D, _gl_id );
  if ( gl_error_print ( "Texture::allocate: glBindTexture: ", glf_n ) ) {
    deallocate ( glf_n );
    return false;
  }

  // Minification / magnification filters
  glf_n->glTexParameteri (
      GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, texs.minification_filter );
  glf_n->glTexParameteri (
      GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, texs.magnification_filter );
  // Wrap mode
  glf_n->glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, texs.wrap_mode );
  glf_n->glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, texs.wrap_mode );
  // Allocate storage
  glf_n->glTexImage2D ( GL_TEXTURE_2D,
                        0,
                        texs.internal_format,
                        atlas ()->texture_size ().width (),
                        atlas ()->texture_size ().height (),
                        0,
                        texs.source_pixel_format,
                        texs.source_pixel_type,
                        0 );
  if ( gl_error_print ( "Texture::allocate: glTexImage2D: ", glf_n ) ) {
    deallocate ( glf_n );
    return false;
  }
  glf_n->glBindTexture ( GL_TEXTURE_2D, 0 );

  return true;
}

void
Texture::deallocate ( QOpenGLFunctions * glf_n )
{
  if ( _gl_id != 0 ) {
    glf_n->glBindTexture ( GL_TEXTURE_2D, 0 );
    glf_n->glDeleteTextures ( 1, &_gl_id );
    gl_error_print ( "Texture::deallocate: glDeleteTextures: ", glf_n );
    _gl_id = 0;
  }
}

Tile_Reference
Texture::acquire_tile ()
{
  DEBUG_ASSERT ( !_tiles_avail.empty () );
  Tile_Reference res ( _tiles_avail.back () );
  _tiles_avail.pop_back ();
  if ( _tiles_avail.empty () ) {
    atlas ()->texture_full ( this );
  }
  return res;
}

void
Texture::release_tile ( Tile * tile_n )
{
  DEBUG_ASSERT ( _tiles_avail.size () < _tiles.size () );
  _tiles_avail.emplace_back ( tile_n );
  if ( _tiles_avail.size () == 1 ) {
    atlas ()->texture_free ( this );
  }
}
} // namespace qfre::texture_atlas
