/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/reference.hpp>
#include <geo/geo.hpp>
#include <cstdint>
#include <debug_assert.hpp>

namespace qfre::texture_atlas
{

// -- Forward declaration
class Texture;
class Tile_Reference;

/// @brief Tile
///
class Tile
{
  public:
  // -- Friends
  friend Tile_Reference;

  public:
  // -- Constructors

  Tile ( Texture * texture_n,
         geo::UInt16_Point pos_n,
         geo::Float_Point pos_uv_n )
  : _pos_uv ( pos_uv_n )
  , _pos ( pos_n )
  , _texture ( texture_n )
  {
  }

  ~Tile () = default;

  // -- Tile in texture position

  geo::Float_Point const &
  pos_uv () const
  {
    return _pos_uv;
  }

  geo::UInt16_Point const &
  pos () const
  {
    return _pos;
  }

  // -- Texture

  Texture *
  texture () const
  {
    return _texture;
  }

  private:
  // -- Reference counting

  void
  reference ()
  {
    ++_reference_count;
  }

  void
  dereference ()
  {
    DEBUG_ASSERT ( _reference_count != 0 );
    --_reference_count;
    if ( _reference_count == 0 ) {
      release ();
    }
  }

  // -- Tile release

  void
  release ();

  private:
  geo::Float_Point _pos_uv;
  geo::UInt16_Point _pos;
  std::uint32_t _reference_count = 0;
  Texture * _texture;
};
} // namespace qfre::texture_atlas
