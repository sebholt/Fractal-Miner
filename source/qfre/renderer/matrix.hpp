/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/matrix_base.hpp>
#include <fre/resize_deltas.hpp>
#include <geo/geo.hpp>
#include <qfre/renderer/atlas/texture.hpp>
#include <qfre/renderer/matrix_tile.hpp>
#include <complex>
#include <deque>
#include <map>
#include <vector>

// -- Forward declaration
namespace fre
{
template < class MT, bool RN >
class Matrix_Manipulator;
}

namespace qfre::renderer
{

/// @brief Paint matrix
///
class Matrix : public fre::Matrix_Base
{
  public:
  // -- Types
  typedef Matrix_Tile_Reference Tile_Reference;
  typedef std::deque< Matrix_Tile_Reference > Tile_Row;
  typedef std::deque< Tile_Row > Tile_Rows;
  typedef fre::Matrix_Manipulator< Matrix, false > Manipulator;
  typedef std::map< qfre::texture_atlas::Texture_Reference,
                    std::vector< Matrix_Tile_Reference > >
      Paintables_Map;
  // -- Friends
  friend Manipulator;

  public:
  // -- Constructors

  Matrix ( geo::UInt16_Size const & tile_size_n,
           geo::UInt16_Size const & tile_overlap_n );

  // -- Tile pixel size

  const geo::UInt16_Size &
  tile_size () const
  {
    return _tile_size;
  }

  std::uint16_t
  tile_width () const
  {
    return _tile_size.width ();
  }

  std::uint16_t
  tile_height () const
  {
    return _tile_size.height ();
  }

  // -- Tile overlap

  const geo::UInt16_Size &
  tile_overlap () const
  {
    return _tile_overlap;
  }

  // -- Inner tile pixel size

  const geo::UInt16_Size &
  inner_tile_size () const
  {
    return _inner_tile_size;
  }

  // -- Scale

  const geo::UInt8_Size &
  scale () const
  {
    return _scale;
  }

  void
  set_scale ( const geo::UInt8_Size & scale_n )
  {
    _scale = scale_n;
  }

  // -- Tile access

  std::int32_t
  local_column ( std::uint32_t global_column_n ) const
  {
    return ( global_column_n - _column_id_ends.first () - 1 );
  }

  std::int32_t
  local_row ( std::uint32_t global_row_n ) const
  {
    return ( global_row_n - _row_id_ends.first () - 1 );
  }

  geo::Int32_Point
  local_inner_tile_pos_from_global (
      geo::Int32_Point const & global_pos_n ) const
  {
    return geo::Int32_Point (
        local_column ( global_pos_n.x () ) * inner_tile_size ().width (),
        local_row ( global_pos_n.y () ) * inner_tile_size ().height () );
  }

  Matrix_Tile *
  global_tile ( geo::Int32_Point const & pos_n ) const
  {
    std::int32_t col = local_column ( pos_n.x () );
    std::int32_t row = local_row ( pos_n.y () );
    if ( ( col >= 0 ) && ( row >= 0 ) &&
         ( std::uint32_t ( col ) < columns () ) &&
         ( std::uint32_t ( row ) < rows () ) ) {
      return _tile_rows[ row ][ col ].get ();
    }
    return nullptr;
  }

  Tile_Rows const &
  tile_rows () const
  {
    return _tile_rows;
  }

  // -- Manipulate

  void
  resize ( fre::Resize_Deltas const & deltas_n );

  // -- Paintable

  std::uint32_t
  paintables_count () const
  {
    return _paintables_count;
  }

  Paintables_Map const &
  paintables_map () const
  {
    return _paintables_map;
  }

  void
  paintable_register ( Matrix_Tile_Reference const & tile_ref_n );

  void
  paintables_clear ();

  private:
  // -- Utility

  void
  compute_size_px ();

  // -- Paintable

  void
  paintable_unregister ( Matrix_Tile_Reference const & tile_ref_n );

  // -- Matrix manipulator interface

  Matrix_Tile_Reference
  tile_create ( geo::Int32_Point const & pos_n )
  {
    return Matrix_Tile_Reference::created ( pos_n );
  }

  void
  tile_invalidate ( Matrix_Tile_Reference const & tile_ref_n )
  {
    paintable_unregister ( tile_ref_n );
  }

  void
  tile_change_column ( Matrix_Tile_Reference const & tile_ref_n,
                       std::int32_t column_n )
  {
    paintable_unregister ( tile_ref_n );
    tile_ref_n->set_column ( column_n );
  }

  void
  tile_change_row ( Matrix_Tile_Reference const & tile_ref_n,
                    std::int32_t row_n )
  {
    paintable_unregister ( tile_ref_n );
    tile_ref_n->set_row ( row_n );
  }

  private:
  // -- Pixel sizes
  geo::UInt16_Size _tile_size = { 0, 0 };
  geo::UInt16_Size _tile_overlap = { 0, 0 };
  geo::UInt16_Size _inner_tile_size = { 0, 0 };
  geo::UInt8_Size _scale = { 1, 1 };
  // -- Tiles
  Tile_Rows _tile_rows;
  std::uint32_t _paintables_count = 0;
  Paintables_Map _paintables_map;
};
} // namespace qfre::renderer
