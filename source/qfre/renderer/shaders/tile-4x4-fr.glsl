// Uniform
uniform mediump float iter_max;
uniform sampler2D iter_tex;
uniform mediump vec2 iter_tex_size;
uniform mediump vec2 sample_offsets[16];
uniform mediump vec4 col_blank;
uniform mediump vec4 col_end;
uniform mediump float pal_colors;
uniform mediump float pal_offset;
uniform sampler2D pal_tex;
uniform mediump vec2 pal_tex_size;
// Varying
varying mediump vec2 var_uv;
// Constants
const mediump float sample_weight = 1.0 / 16.0;

float fraction ( float x )
{
  return x - floor ( x );
}

float get_iterations ( vec2 uv_pos )
{
  vec4 tcol = texture2D ( iter_tex, uv_pos );
  // 2 x 16 bit samples in "rg" and "ba"
  vec2 uv = ( fraction ( uv_pos.x * iter_tex_size.x ) < 0.5 )
            ? tcol.xy : tcol.zw;
  return ( uv.x*255.0 + uv.y*(255.0*256.0) );
}

vec4 get_paletted_color ( float itc )
{
  float pitc = floor ( fraction ( ( itc + pal_offset ) / pal_colors )*pal_colors );
  float prow = floor ( pitc / pal_tex_size.x );
  float pcol = pitc - prow*pal_tex_size.x;
  vec2 pal_uv = vec2 (
    (pcol+0.5)/pal_tex_size.x,
    (prow+0.5)/pal_tex_size.y );
  return texture2D ( pal_tex, pal_uv );
}

void get_color_and_weights (
  in vec2 uv_pos,
  inout vec4 summed_color,
  inout float color_weight_sum )
{
  float itc = get_iterations ( uv_pos );
  if ( itc > 0.0 ) {
    vec4 color;
    if ( itc < iter_max ) {
      itc = itc - 1.0;
      color = get_paletted_color ( itc );
    } else {
      color = col_end;
    }
    summed_color += color*sample_weight;
    color_weight_sum += sample_weight;
  }
}

void main(void)
{
  vec4 summed_color = vec4 ( 0.0, 0.0, 0.0, 0.0 );
  float color_weight_sum = 0.0;

  for ( int ii=0; ii != 16; ii++ ) {
    get_color_and_weights (
      var_uv + sample_offsets[ii],
      summed_color,
      color_weight_sum );
  }

  if ( color_weight_sum == 0.0 ) {
    gl_FragColor = col_blank;
  } else {
    gl_FragColor = summed_color / color_weight_sum;
  }
}
