// Attribute
attribute vec4 vpos;
attribute vec2 vuv;
// Uniform
uniform mat4 vert_matrix;
uniform vec2 vert_pos;
uniform vec2 vert_scale;
// Varying
varying vec2 var_uv;

void main(void)
{
  gl_Position = vert_matrix * vec4(
    vert_pos.x + vert_scale.x*vpos.x,
    vert_pos.y + vert_scale.y*vpos.y,
    vpos.z,
    vpos.w);
  var_uv = vuv;
}
