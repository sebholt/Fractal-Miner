// Uniform
uniform mediump float iter_max;
uniform sampler2D iter_tex;
uniform mediump vec2 iter_tex_size;
uniform mediump vec2 filter_offsets[4];
uniform mediump vec4 col_blank;
uniform mediump vec4 col_end;
uniform mediump float pal_colors;
uniform mediump float pal_offset;
uniform sampler2D pal_tex;
uniform mediump vec2 pal_tex_size;
// Varying
varying mediump vec2 var_uv;

float fraction ( float x )
{
  return x - floor ( x );
}

float get_iterations ( vec2 uv_pos )
{
  vec4 tcol = texture2D ( iter_tex, uv_pos );
  // 2 x 16 bit samples in "rg" and "ba"
  vec2 uv = ( fraction ( uv_pos.x * iter_tex_size.x ) < 0.5 )
            ? tcol.xy : tcol.zw;
  return ( uv.x*255.0 + uv.y*(255.0*256.0) );
}

vec4 get_paletted_color ( float itc )
{
  float pitc = floor ( fraction ( ( itc + pal_offset ) / pal_colors )*pal_colors );
  float prow = floor ( pitc / pal_tex_size.x );
  float pcol = pitc - prow*pal_tex_size.x;
  vec2 pal_uv = vec2 (
    (pcol+0.5)/pal_tex_size.x,
    (prow+0.5)/pal_tex_size.y );
  return texture2D ( pal_tex, pal_uv );
}

void get_color_and_weights (
  in vec2 uv_pos,
  in float color_weight,
  inout vec4 summed_color,
  inout float color_weight_sum )
{
  float itc = get_iterations ( uv_pos );
  if ( itc > 0.0 ) {
    vec4 color;
    if ( itc < iter_max ) {
      itc = itc - 1.0;
      color = get_paletted_color ( itc );
    } else {
      color = col_end;
    }
    summed_color.x += color.x*color_weight;
    summed_color.y += color.y*color_weight;
    summed_color.z += color.z*color_weight;
    summed_color.w += color.w*color_weight;
    color_weight_sum += color_weight;
  } else {
    summed_color.w += col_blank.w * color_weight;
  }
}

void main(void)
{
  vec2 left_bottom = vec2 (
    var_uv.x * iter_tex_size.x,
    var_uv.y * iter_tex_size.y );
  left_bottom.x *= 2.0;
  left_bottom -= 0.5;

  vec2 sample_uv[4];
  sample_uv[0] = vec2 (
    floor ( left_bottom.x ) + 0.5,
    floor ( left_bottom.y ) + 0.5 );
  sample_uv[1] = vec2 ( sample_uv[0].x + 1.0, sample_uv[0].y );
  sample_uv[2] = vec2 ( sample_uv[0].x, sample_uv[0].y + 1.0 );
  sample_uv[3] = vec2 ( sample_uv[0].x + 1.0, sample_uv[0].y + 1.0 );

  vec2 dim_weights = vec2 (
    fraction ( left_bottom.x ),
    fraction ( left_bottom.y ) );

  float sample_weights[4];
  sample_weights[0] = (1.0 - dim_weights[0])*(1.0 - dim_weights[1]);
  sample_weights[1] = dim_weights[0]*(1.0 - dim_weights[1]);
  sample_weights[2] = (1.0 - dim_weights[0])*dim_weights[1];
  sample_weights[3] = dim_weights[0]*dim_weights[1];

  // Color accumulator
  vec4 summed_color = vec4 ( 0.0, 0.0, 0.0, 0.0 );
  float color_weight_sum = 0.0;

  vec2 texel_size = vec2 ( 1.0 / iter_tex_size.x / 2.0,
                           1.0 / iter_tex_size.y );
  for ( int ii=0; ii != 4; ii++ ) {
    get_color_and_weights (
      sample_uv[ii] * texel_size,
      sample_weights[ii],
      summed_color,
      color_weight_sum );
  }

  summed_color.x /= color_weight_sum;
  summed_color.y /= color_weight_sum;
  summed_color.z /= color_weight_sum;

  gl_FragColor = summed_color;
}
