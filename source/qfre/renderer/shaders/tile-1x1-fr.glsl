// Uniform
uniform mediump float iter_max;
uniform sampler2D iter_tex;
uniform mediump vec2 iter_tex_size;
uniform mediump vec4 col_blank;
uniform mediump vec4 col_end;
uniform mediump float pal_colors;
uniform mediump float pal_offset;
uniform sampler2D pal_tex;
uniform mediump vec2 pal_tex_size;
// Varying
varying mediump vec2 var_uv;

float fraction ( float x )
{
  return x - floor ( x );
}

float get_iterations ( vec2 uv_pos )
{
  vec4 tcol = texture2D ( iter_tex, uv_pos );
  // 2 x 16 bit samples in "rg" and "ba"
  vec2 uv = ( fraction ( uv_pos.x * iter_tex_size.x ) < 0.5 )
            ? tcol.xy : tcol.zw;
  return ( uv.x*255.0 + uv.y*(255.0*256.0) );
}

vec4 get_paletted_color ( float itc )
{
  float pitc = floor ( fraction ( ( itc + pal_offset ) / pal_colors )*pal_colors );
  float prow = floor ( pitc / pal_tex_size.x );
  float pcol = pitc - prow*pal_tex_size.x;
  vec2 pal_uv = vec2 (
    (pcol+0.5)/pal_tex_size.x,
    (prow+0.5)/pal_tex_size.y );
  return texture2D ( pal_tex, pal_uv );
}

vec4 get_color ( float itc )
{
  if ( itc > 0.0 ) {
    if ( itc < iter_max ) {
      itc = itc - 1.0;
      return get_paletted_color ( itc );
    } else {
      return col_end;
    }
  }
  return col_blank;
}

void main(void)
{
  float itc = get_iterations ( var_uv );
  gl_FragColor = get_color  ( itc );
}
