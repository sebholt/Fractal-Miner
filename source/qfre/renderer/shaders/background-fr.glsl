// Uniform
uniform mediump vec4 col_plane;
uniform mediump vec4 col_line;
uniform mediump vec2 quad_size;
// Varying
varying mediump vec2 var_uv;

const float line_dist = 16.0;

float fraction ( float x )
{
  return x - floor ( x );
}

void main(void)
{
  vec2 quad_px = var_uv * quad_size;
  quad_px /= line_dist;
  if ( ( fraction ( quad_px.x ) * line_dist <= 1.0 ) ||
    ( fraction ( quad_px.y ) * line_dist <= 1.0 ) )
  {
    gl_FragColor = col_line;
  } else {
    gl_FragColor = col_plane;
  }
}
