// Attribute
attribute vec4 vpos;
attribute vec2 vuv;
// Uniform
uniform mat4 vert_matrix;
uniform vec2 vert_offset;
uniform vec2 vert_pos;
uniform vec2 vert_scale;
uniform vec2 vert_uv_pos;
// Varying
varying vec2 var_uv;

void main(void)
{
  gl_Position = vert_matrix * vec4(
    vert_offset.x + vert_scale.x*( vert_pos.x + vpos.x ),
    vert_offset.y + vert_scale.y*( vert_pos.y + vpos.y ),
    vpos.z,
    vpos.w);
  var_uv = vert_uv_pos + vuv;
}
