/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <qfre/color_scheme.hpp>
#include <QColor>

namespace qfre::renderer
{

/// @brief Color settings
///
class Color_Settings
{
  public:
  // -- Constructors

  Color_Settings () = default;

  ~Color_Settings () = default;

  // -- Color scheme

  Color_Scheme_Reference const &
  scheme () const
  {
    return _scheme;
  }

  void
  set_scheme ( Color_Scheme_Reference const & scheme_n );

  // -- Color scheme id

  QString const &
  scheme_id () const
  {
    return _scheme_id;
  }

  // -- Color scheme id

  std::uint32_t
  scheme_color_count () const
  {
    return _scheme_color_count;
  }

  // -- End color

  QColor
  color_end () const
  {
    return _color_end;
  }

  void
  set_color_end ( QColor col_n )
  {
    _color_end = col_n;
  }

  // -- Color palette offset

  std::int32_t
  offset () const
  {
    return _offset;
  }

  void
  set_offset ( std::int32_t value_n )
  {
    _offset = value_n;
    update_mapped_offset ();
  }

  /// @brief Positive offset
  std::uint32_t
  mapped_offset () const
  {
    return _mapped_offset;
  }

  private:
  void
  update_mapped_offset ();

  private:
  Color_Scheme_Reference _scheme;
  QString _scheme_id;
  QColor _color_end = QColor ( 0, 0, 0, 255 );
  std::int32_t _offset = 0;
  std::uint32_t _mapped_offset = 0;
  std::uint32_t _scheme_color_count = 0;
};
} // namespace qfre::renderer
