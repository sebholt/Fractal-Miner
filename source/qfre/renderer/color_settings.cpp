/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <qfre/renderer/color_settings.hpp>

namespace qfre::renderer
{

void
Color_Settings::set_scheme ( Color_Scheme_Reference const & scheme_n )
{
  _scheme = scheme_n;
  if ( _scheme ) {
    _scheme_id = _scheme->id_string ();
    _scheme_color_count = _scheme->num_colors ();
  } else {
    _scheme_id.clear ();
    _scheme_color_count = 0;
  }
  update_mapped_offset ();
}

void
Color_Settings::update_mapped_offset ()
{
  if ( _scheme_color_count != 0 ) {
    if ( _offset >= 0 ) {
      _mapped_offset = _offset % _scheme_color_count;
    } else {
      _mapped_offset =
          _scheme_color_count - ( ( -_offset ) % _scheme_color_count );
    }
  } else {
    _mapped_offset = 0;
  }
}
} // namespace qfre::renderer
