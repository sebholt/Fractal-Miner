/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>

namespace qfre::renderer
{

/// @brief Render settings
///
class Render_Settings
{
  public:
  // -- Constructors

  Render_Settings () = default;

  ~Render_Settings () = default;

  // -- Draw foreground

  bool
  draw_foreground () const
  {
    return _draw_foreground;
  }

  void
  set_draw_foreground ( bool flag_n )
  {
    _draw_foreground = flag_n;
  }

  // -- Draw foreground

  bool
  draw_backgrounds () const
  {
    return _draw_backgrounds;
  }

  void
  set_draw_backgrounds ( bool flag_n )
  {
    _draw_backgrounds = flag_n;
  }

  // -- Upload

  bool
  upload () const
  {
    return _upload;
  }

  void
  set_upload ( bool flag_n )
  {
    _upload = flag_n;
  }

  // -- Wireframe

  bool
  wireframe () const
  {
    return _wireframe;
  }

  void
  set_wireframe ( bool flag_n )
  {
    _wireframe = flag_n;
  }

  // -- Maximum number of background tile planes

  std::uint8_t
  backgrounds () const
  {
    return _backgrounds;
  }

  void
  set_backgrounds ( std::uint8_t num_n )
  {
    _backgrounds = num_n;
  }

  // -- Supersampling

  std::uint8_t
  supersampling () const
  {
    return _supersampling;
  }

  void
  set_supersampling ( std::uint8_t num_n )
  {
    _supersampling = num_n;
  }

  private:
  bool _draw_foreground = false;
  bool _draw_backgrounds = false;
  bool _upload = false;
  bool _wireframe = false;
  std::uint8_t _backgrounds = 0;
  std::uint8_t _supersampling = 1;
};
} // namespace qfre::renderer
