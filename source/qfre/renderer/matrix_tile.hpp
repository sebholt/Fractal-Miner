/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/reference.hpp>
#include <geo/point.hpp>
#include <qfre/renderer/atlas/tile_reference.hpp>

namespace qfre::renderer
{

// -- Forward declaration
class Matrix_Tile;
typedef fre::Reference< Matrix_Tile > Matrix_Tile_Reference;

/// @brief Paint tile
///
class Matrix_Tile
{
  public:
  // -- Friends
  friend Matrix_Tile_Reference;

  public:
  // -- Constructors

  Matrix_Tile ( geo::Int32_Point const & pos_n )
  : _pos ( pos_n )
  {
  }

  // -- Column / Row

  /// @brief Column and row
  geo::Int32_Point const &
  pos () const
  {
    return _pos;
  }

  void
  set_pos ( geo::Int32_Point const & pos_n )
  {
    _pos = pos_n;
  }

  /// @brief Column
  std::int32_t
  column () const
  {
    return _pos.x ();
  }

  /// @brief Set the column
  void
  set_column ( std::int32_t column_n )
  {
    _pos.set_x ( column_n );
  }

  /// @brief Row
  std::int32_t
  row () const
  {
    return _pos.y ();
  }

  /// @brief Set the row
  void
  set_row ( std::int32_t row_n )
  {
    _pos.set_y ( row_n );
  }

  // -- Texture tile

  qfre::texture_atlas::Tile_Reference const &
  tile () const
  {
    return _texture_tile;
  }

  void
  set_texture_tile ( qfre::texture_atlas::Tile_Reference && ref_n )
  {
    _texture_tile = std::move ( ref_n );
  }

  void
  clear_texture_tile ()
  {
    _texture_tile.reset ();
  }

  // -- Vertex position

  geo::Float_Point const &
  vertex_pos () const
  {
    return _vertex_pos;
  }

  void
  set_vertex_pos ( float x_n, float y_n )
  {
    _vertex_pos.set ( x_n, y_n );
  }

  private:
  geo::Int32_Point _pos;
  qfre::texture_atlas::Tile_Reference _texture_tile;
  geo::Float_Point _vertex_pos;
  std::atomic_uint _reference_count = 0;
};
} // namespace qfre::renderer
