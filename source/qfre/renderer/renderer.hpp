/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <geo/geo.hpp>
#include <qfre/color_scheme.hpp>
#include <qfre/event_chain.hpp>
#include <qfre/renderer/atlas/atlas.hpp>
#include <qfre/renderer/color_settings.hpp>
#include <qfre/renderer/iterator_settings.hpp>
#include <qfre/renderer/matrix.hpp>
#include <qfre/renderer/paint_tiles.hpp>
#include <qfre/renderer/render_settings.hpp>
#include <QColor>
#include <QObject>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLVertexArrayObject>
#include <QQuickFramebufferObject>
#include <deque>
#include <memory>

// -- Forward declaration
namespace qfre
{
class Fractal;
}

namespace qfre::renderer
{

// -- Forward declaration
class Viewport_Matrix;
class Engine;

/// @brief The Renderer lives in the Qt Quick render thread
///
class Renderer : public QQuickFramebufferObject::Renderer
{
  public:
  // -- Types

  struct Vertex_UV : std::array< float, 5 >
  {
  };

  struct Tile_Vertices
  {
    // -- Statics
    static constexpr int stride = sizeof ( Vertex_UV );
    // -- Attributes
    QOpenGLBuffer gl_buffer;
    std::array< Vertex_UV, 4 > vertices;
  };

  struct Tile_Shader
  {
    QOpenGLShaderProgram gl_sp;
    QOpenGLVertexArrayObject gl_vao;
  };

  struct Background_Tile_Shader : public Tile_Shader
  {
    struct Shader_Locations
    {
      GLint vert_matrix;
      GLint vert_pos;
      GLint vert_scale;
      GLint quad_size;
      GLint col_plane;
      GLint col_line;
    } shader_locations;
  };

  struct Matrix_Tile_Shader : public Tile_Shader
  {
    struct Shader_Locations
    {
      GLint vert_matrix;
      GLint vert_offset;
      GLint vert_pos;
      GLint vert_scale;
      GLint vert_uv_pos;
      GLint iter_max;
      GLint iter_tex;
      GLint iter_tex_size;
      GLint sample_offsets;
      GLint col_end;
      GLint col_blank;
      GLint pal_colors;
      GLint pal_offset;
      GLint pal_tex;
      GLint pal_tex_size;
    } shader_locations;
  };

  public:
  // -- Constructors

  Renderer ( QQuickWindow * quick_window_n,
             geo::UInt16_Size tile_size_n,
             geo::UInt16_Size tile_overlap_n,
             geo::UInt16_Size texture_size_n );

  ~Renderer ();

  // -- Renderer interface

  QOpenGLFramebufferObject *
  createFramebufferObject ( const QSize & size );

  void
  synchronize ( QQuickFramebufferObject * fbo_n ) override;

  void
  render () override;

  private:
  // -- Initialization / invalidation

  bool
  initialize_palette_texture ( QOpenGLFunctions * glf_n );

  bool
  initialize_tile_vertices ( Tile_Vertices & vertices_n );

  bool
  initialize_tile_shader ( Tile_Shader & shader_n,
                           QString const & vertex_shader_n,
                           QString const & fragment_shader_n );

  bool
  initialize_tile_vertex_array_object ( Tile_Shader & shader_n,
                                        Tile_Vertices & vertices_n );

  // - Init background objects

  bool
  initialize_background_vertices ( Tile_Vertices & vertices_n );

  void
  initialize_background_objects_clear ();

  bool
  initialize_background_objects ();

  bool
  initialize_background_shader_locations ();

  // - Init matrix objects

  bool
  initialize_matrix_tile_vertices ( Tile_Vertices & vertices_n,
                                    geo::Double_Size const & px_oversize_n );

  bool
  initialize_matrix_tile_shaders ();

  bool
  initialize_matrix_tile_vertex_array_objects ();

  void
  initialize_matrix_tile_objects_clear ();

  bool
  initialize_matrix_tile_objects ();

  bool
  initialize_matrix_tile_shader_locations ( Matrix_Tile_Shader & shader_n );

  // - Shader locations

  bool
  initialize_shader_locations ();

  public:
  void
  process_events ();

  void
  process_event_viewport ( event::Event * event_n );

  void
  process_event_matrix_move ( event::Event * event_n );

  void
  process_event_matrix_resize ( event::Event * event_n );

  void
  process_event_matrix_scale ( event::Event * event_n );

  void
  process_event_matrix_loop ( event::Event * event_n );

  void
  process_event_snapshot ( event::Event * event_n );

  void
  process_event_zoom ( event::Event * event_n );

  void
  process_event_clear ( event::Event * event_n );

  void
  process_event_clear_foreground ( event::Event * event_n );

  void
  process_event_render_settings ( event::Event * event_n );

  void
  process_event_iterator_settings ( event::Event * event_n );

  void
  process_event_color_scheme ( event::Event * event_n );

  void
  process_event_color_offset ( event::Event * event_n );

  private:
  bool
  upload_palette_texture ( QOpenGLFunctions * glf_n );

  void
  upload_tiles ( Fractal * fractal_n );

  // -- Pre rendering

  void
  rebuild_paint_matrix_tiles ();

  void
  rebuild_shader_sample_offsets ();

  bool
  render_background ( QOpenGLFunctions * glf_n );

  bool
  render_matrix_tiles_begin ( QOpenGLFunctions * glf_n,
                              Matrix_Tile_Shader & shader_n );

  void
  render_matrix_tiles ( QOpenGLFunctions * glf_n,
                        Matrix_Tile_Shader & shader_n,
                        renderer::Paint_Tiles const & paint_tiles_n );

  void
  render_matrix_tiles_end ( QOpenGLFunctions * glf_n,
                            Matrix_Tile_Shader & shader_n );

  private:
  // -- Sizes and settings
  QQuickWindow * _quick_window;
  geo::UInt16_Size _tile_size;
  geo::UInt16_Size _tile_overlap;
  renderer::Render_Settings _render_settings;

  // -- Texturing and colors
  QColor _color_clear;
  QColor _color_blank;
  QColor _color_background_plane;
  QColor _color_background_line;
  renderer::Color_Settings _color_settings;
  qfre::texture_atlas::Atlas _texture_atlas;

  // -- Viewport
  geo::Int32_Size _viewport_size;
  QMatrix4x4 _viewport_projection;

  // -- Paint meta objects
  bool _rebuild_color_palette = true;
  bool _rebuild_shader_sample_offsets = true;
  bool _rebuild_paint_matrix_tiles = true;
  geo::UInt16_Size _palette_texture_size = { 256, 256 };
  std::uint32_t _palette_texture_id = 0;
  std::unique_ptr< Tile_Vertices > _background_vertices;
  std::unique_ptr< Background_Tile_Shader > _background_shader;
  std::unique_ptr< Tile_Vertices > _tile_vertices;
  std::unique_ptr< Matrix_Tile_Shader > _tile_shader_1x1;
  std::unique_ptr< Matrix_Tile_Shader > _tile_shader_2x2;
  std::unique_ptr< Matrix_Tile_Shader > _tile_shader_4x4;
  std::unique_ptr< Matrix_Tile_Shader > _tile_shader_filter;
  Matrix_Tile_Shader * _tile_shader_viewport = nullptr;
  std::vector< geo::Float_Size > _sample_offsets;

  // -- Paint objects
  renderer::Matrix _paint_matrix;
  renderer::Paint_Tiles_Reference _paint_matrix_tiles;
  std::deque< renderer::Paint_Tiles_Reference > _background_tiles;

  // -- Remote shared resources
  Event_Chain _event_chain;
};
} // namespace qfre::renderer
