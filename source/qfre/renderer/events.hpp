/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/event.hpp>
#include <fre/resize_deltas.hpp>
#include <geo/pair.hpp>
#include <geo/point.hpp>
#include <geo/size.hpp>
#include <qfre/color_scheme.hpp>
#include <qfre/renderer/iterator_settings.hpp>
#include <qfre/renderer/render_settings.hpp>

namespace qfre::renderer
{
class Renderer;
}

namespace qfre::renderer::events
{

class Base_Event : public event::Event
{
  public:
  typedef void ( Renderer::*Renderer_Function ) ( event::Event * event_n );

  public:
  Base_Event ( Renderer_Function func_n )
  : event::Event ( 0 )
  , renderer_function ( func_n )
  {
  }

  public:
  Renderer_Function renderer_function;
};

class Viewport : public Base_Event
{
  public:
  Viewport ();

  public:
  geo::Int32_Size size;
};

class Matrix_Move : public Base_Event
{
  public:
  Matrix_Move ();

  Matrix_Move ( Renderer_Function func_n )
  : Base_Event ( func_n )
  {
  }

  public:
  geo::Double_Point position;
  geo::Double_Size background_movement;
};

class Matrix_Resize : public Matrix_Move
{
  public:
  Matrix_Resize ();

  public:
  fre::Resize_Deltas deltas;
};

class Matrix_Scale : public Base_Event
{
  public:
  Matrix_Scale ();

  public:
  geo::UInt8_Size scale;
};

class Matrix_Loop : public Matrix_Move
{
  public:
  Matrix_Loop ();

  public:
  /// @brief positive means lower to upper
  geo::Int32_Pair loops;
};

class Snapshot : public Base_Event
{
  public:
  Snapshot ();
};

class Zoom : public Base_Event
{
  public:
  Zoom ();

  public:
  bool snapshot = false;
  geo::Double_Point position;
  double amount = 0.0;
};

class Clear : public Base_Event
{
  public:
  Clear ();
};

class Clear_Foreground : public Base_Event
{
  public:
  Clear_Foreground ();
};

class Render_Settings : public Base_Event
{
  public:
  Render_Settings ();

  public:
  qfre::renderer::Render_Settings settings;
};

class Iterator_Settings : public Base_Event
{
  public:
  Iterator_Settings ();

  public:
  qfre::renderer::Iterator_Settings settings;
};

class Color_Scheme : public Base_Event
{
  public:
  Color_Scheme ();

  public:
  qfre::Color_Scheme_Reference scheme;
};

class Color_Offset : public Base_Event
{
  public:
  Color_Offset ();

  public:
  std::int32_t offset = 0;
};
} // namespace qfre::renderer::events
