/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "renderer.hpp"
#include <fre/engine.hpp>
#include <fre/matrix_manipulator.hpp>
#include <fre/reader.hpp>
#include <qfre/fractal.hpp>
#include <qfre/gl_utility.hpp>
#include <qfre/renderer/atlas/texture.hpp>
#include <qfre/renderer/events.hpp>
#include <qfre/viewport_matrix.hpp>
#include <QOpenGLFramebufferObjectFormat>
#include <QQuickWindow>
#include <application_config.hpp>
#include <chrono>
#include <message_streams.hpp>

namespace qfre::renderer
{

Renderer::Renderer ( QQuickWindow * quick_window_n,
                     geo::UInt16_Size tile_size_n,
                     geo::UInt16_Size tile_overlap_n,
                     geo::UInt16_Size texture_size_n )
: _quick_window ( quick_window_n )
, _tile_size ( tile_size_n )
, _tile_overlap ( tile_overlap_n )
, _color_clear ( 0, 0, 0, 255 )
, _color_blank ( 0, 0, 0, 0 )
, _color_background_plane ( 0, 0, 0, 255 )
, _color_background_line ( 60, 60, 60, 255 )
, _texture_atlas (
      texture_size_n,
      geo::UInt16_Size ( tile_size_n.width () / 2u, tile_size_n.height () ) )
, _paint_matrix ( tile_size_n, tile_overlap_n )
, _paint_matrix_tiles ( renderer::Paint_Tiles_Reference::created () )
{
  static_assert ( sizeof ( Vertex_UV ) == sizeof ( float ) * 5 );

  QOpenGLFunctions * glf = gl_current_functions ();
  if ( glf != nullptr ) {
    initialize_palette_texture ( glf );
    initialize_background_objects ();
    initialize_matrix_tile_objects ();
    initialize_shader_locations ();
  }
}

Renderer::~Renderer () {}

bool
Renderer::initialize_palette_texture ( QOpenGLFunctions * glf_n )
{
  // Generate texture
  glf_n->glGenTextures ( 1, &_palette_texture_id );
  if ( gl_error_print ( "Renderer::initialize_palette_texture: glGenTextures: ",
                        glf_n ) ) {
    _palette_texture_id = 0;
    return false;
  }
  // Bind texture
  glf_n->glBindTexture ( GL_TEXTURE_2D, _palette_texture_id );
  if ( gl_error_print ( "Renderer::initialize_palette_texture: glBindTexture: ",
                        glf_n ) ) {
    glf_n->glBindTexture ( GL_TEXTURE_2D, 0 );
    glf_n->glDeleteTextures ( 1, &_palette_texture_id );
    return false;
  }

  // Minification / magnification filters
  glf_n->glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glf_n->glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
  // Wrap mode
  glf_n->glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
  glf_n->glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
  // Allocate storage
  glf_n->glTexImage2D ( GL_TEXTURE_2D,
                        0,
                        GL_RGBA,
                        _palette_texture_size.width (),
                        _palette_texture_size.height (),
                        0,
                        GL_RGBA,
                        GL_UNSIGNED_BYTE,
                        0 );
  if ( gl_error_print ( "Renderer::initialize_palette_texture: glTexImage2D: ",
                        glf_n ) ) {
    glf_n->glBindTexture ( GL_TEXTURE_2D, 0 );
    glf_n->glDeleteTextures ( 1, &_palette_texture_id );
    return false;
  }
  glf_n->glBindTexture ( GL_TEXTURE_2D, 0 );
  return true;
}

bool
Renderer::initialize_tile_vertices ( Tile_Vertices & vertices_n )
{
  // -- Vertex buffer
  // Create vertex buffer
  if ( !vertices_n.gl_buffer.create () ) {
    Error_Stream oss;
    oss << "Renderer::initialize_tile_vertices: Vertex buffer creation "
           "failed\n";
    return false;
  }
  // Bind vertex buffer
  if ( !vertices_n.gl_buffer.bind () ) {
    Error_Stream oss;
    oss << "Renderer::initialize_tile_vertices: Vertex buffer binding "
           "failed\n";
    return false;
  }

  // Fill vertex buffer
  vertices_n.gl_buffer.setUsagePattern ( QOpenGLBuffer::StaticDraw );
  vertices_n.gl_buffer.allocate ( vertices_n.vertices.data (),
                                  vertices_n.vertices.size () *
                                      sizeof ( Vertex_UV ) );
  return true;
}

bool
Renderer::initialize_tile_shader ( Tile_Shader & shader_n,
                                   QString const & vertex_shader_n,
                                   QString const & fragment_shader_n )
{
  // Add vertex shader
  if ( !shader_n.gl_sp.addCacheableShaderFromSourceFile ( QOpenGLShader::Vertex,
                                                          vertex_shader_n ) ) {
    {
      Error_Stream oss;
      oss << "Renderer::initialize_tile_shader\n"
          << "  Vertex shader: " << vertex_shader_n.toStdString () << "\n"
          << "  Registration failed!\n"
          << shader_n.gl_sp.log ().toStdString ();
      oss << "\n";
    }
    return false;
  }
  // Add fragment shader
  if ( !shader_n.gl_sp.addCacheableShaderFromSourceFile (
           QOpenGLShader::Fragment, fragment_shader_n ) ) {
    {
      Error_Stream oss;
      oss << "Renderer::initialize_tile_shader\n"
          << "  Fragment shader: " << fragment_shader_n.toStdString () << "\n"
          << "  Registration failed!\n"
          << shader_n.gl_sp.log ().toStdString ();
      oss << "\n";
    }
    return false;
  }
  // Link program
  if ( !shader_n.gl_sp.link () ) {
    {
      Error_Stream oss;
      oss << "Renderer::initialize_tile_shader\n"
          << "  Vertex shader: " << vertex_shader_n.toStdString () << "\n"
          << "  Fragment shader: " << fragment_shader_n.toStdString () << "\n"
          << "  Shader program linking failed!\n"
          << shader_n.gl_sp.log ().toStdString ();
      oss << "\n";
    }
    return false;
  }

  return true;
}

bool
Renderer::initialize_tile_vertex_array_object ( Tile_Shader & shader_n,
                                                Tile_Vertices & vertices_n )
{
  // Create vertex array object
  if ( !shader_n.gl_vao.create () ) {
    {
      Error_Stream oss;
      oss << "Renderer::initialize_tile_vertex_array_object: Vertex array "
             "object creation failed\n";
    }
    return false;
  }
  // Bind vertex array object
  shader_n.gl_vao.bind ();
  // Bind vertex buffer
  if ( !vertices_n.gl_buffer.bind () ) {
    shader_n.gl_vao.release ();
    {
      Error_Stream oss;
      oss << "Renderer::initialize_tile_vertex_array_object: Vertex buffer "
             "binding failed\n";
    }
    return false;
  }
  {
    GLint locPos = shader_n.gl_sp.attributeLocation ( "vpos" );
    shader_n.gl_sp.enableAttributeArray ( locPos );
    shader_n.gl_sp.setAttributeBuffer (
        locPos, GL_FLOAT, 0, 3, Tile_Vertices::stride );

    GLint locUv = shader_n.gl_sp.attributeLocation ( "vuv" );
    shader_n.gl_sp.enableAttributeArray ( locUv );
    shader_n.gl_sp.setAttributeBuffer (
        locUv, GL_FLOAT, 3 * sizeof ( float ), 2, Tile_Vertices::stride );
  }

  // Release all
  vertices_n.gl_buffer.release ();
  shader_n.gl_vao.release ();
  return true;
}

bool
Renderer::initialize_background_vertices ( Tile_Vertices & vertices_n )
{
  // Compute vertices
  {
    float const vt_min[ 2 ] = { 0.0f, 0.0f };
    float const vt_max[ 2 ] = { 1.0f, 1.0f };
    float const quad_z = 0.0;
    float const uv_min[ 2 ] = { 0.0f, 0.0f };
    float const uv_max[ 2 ] = { 1.0f, 1.0f };

    Vertex_UV bottom_left = {
        { vt_min[ 0 ], vt_min[ 1 ], quad_z, uv_min[ 0 ], uv_min[ 1 ] } };
    Vertex_UV bottom_right = {
        { vt_max[ 0 ], vt_min[ 1 ], quad_z, uv_max[ 0 ], uv_min[ 1 ] } };
    Vertex_UV top_left = {
        { vt_min[ 0 ], vt_max[ 1 ], quad_z, uv_min[ 0 ], uv_max[ 1 ] } };
    Vertex_UV top_right = {
        { vt_max[ 0 ], vt_max[ 1 ], quad_z, uv_max[ 0 ], uv_max[ 1 ] } };

    vertices_n.vertices[ 0 ] = top_left;
    vertices_n.vertices[ 1 ] = bottom_left;
    vertices_n.vertices[ 2 ] = top_right;
    vertices_n.vertices[ 3 ] = bottom_right;

    if ( DEBUG_QFRE_RENDERER ) {
      Debug_Stream oss;
      oss << "Renderer::initialize_background_vertices\n";
      for ( int ii = 0; ii != 4; ++ii ) {
        oss << "  ";
        for ( int jj = 0; jj != 5; ++jj ) {
          oss << vertices_n.vertices[ ii ][ jj ] << " ";
        }
        oss << "\n";
      }
    }
  }

  return initialize_tile_vertices ( vertices_n );
}

void
Renderer::initialize_background_objects_clear ()
{
  // Clear shaders
  _background_shader.reset ();
  // Clear vertices
  _background_vertices.reset ();
}

bool
Renderer::initialize_background_objects ()
{
  initialize_background_objects_clear ();
  bool good = true;
  if ( good ) {
    _background_vertices = std::make_unique< Tile_Vertices > ();
    good = initialize_background_vertices ( *_background_vertices );
  }
  if ( good ) {
    _background_shader = std::make_unique< Background_Tile_Shader > ();
    good = initialize_tile_shader ( *_background_shader,
                                    ":/shaders/background-vt.glsl",
                                    ":/shaders/background-fr.glsl" );
  }
  if ( good ) {
    good = initialize_tile_vertex_array_object ( *_background_shader,
                                                 *_background_vertices );
  }
  if ( !good ) {
    initialize_background_objects_clear ();
  }
  return good;
}

bool
Renderer::initialize_background_shader_locations ()
{
  // Shader program
  auto & prog = _background_shader->gl_sp;
  if ( !prog.bind () ) {
    {
      Error_Stream oss;
      oss << "Renderer::initialize_background_shader_locations\n"
          << "  Shader program binding failed!\n";
    }
    return false;
  }
  auto & shl ( _background_shader->shader_locations );
  shl.vert_matrix = prog.uniformLocation ( "vert_matrix" );
  shl.vert_pos = prog.uniformLocation ( "vert_pos" );
  shl.vert_scale = prog.uniformLocation ( "vert_scale" );
  shl.quad_size = prog.uniformLocation ( "quad_size" );
  shl.col_plane = prog.uniformLocation ( "col_plane" );
  shl.col_line = prog.uniformLocation ( "col_line" );

  if ( DEBUG_QFRE_RENDERER ) {
    Debug_Stream oss;
    oss << "Renderer::initialize_background_shader_locations:\n";
    oss << "  vert_matrix " << shl.vert_matrix << "\n";
    oss << "  vert_pos " << shl.vert_pos << "\n";
    oss << "  vert_scale " << shl.vert_scale << "\n";
    oss << "  quad_size " << shl.quad_size << "\n";
    oss << "  col_plane " << shl.col_plane << "\n";
    oss << "  col_line " << shl.col_line << "\n";
  }
  prog.release ();

  return true;
}

bool
Renderer::initialize_matrix_tile_vertices (
    Tile_Vertices & vertices_n, geo::Double_Size const & px_oversize_n )
{
  // Compute vertices
  {
    double tex_width ( _texture_atlas.texture_width () );
    double tex_height ( _texture_atlas.texture_height () );
    double inner_tile_width ( _tile_size.width () - 2 * _tile_overlap.width () +
                              2.0 * px_oversize_n.width () );
    double inner_tile_height ( _tile_size.height () -
                               2 * _tile_overlap.height () +
                               2.0 * px_oversize_n.height () );
    double uwidth = inner_tile_width / tex_width / 2.0;
    double vheight = inner_tile_height / tex_height;
    double uoff =
        ( double ( _tile_overlap.width () ) - px_oversize_n.width () ) /
        tex_width / 2.0;
    double voff =
        ( double ( _tile_overlap.height () ) - px_oversize_n.height () ) /
        tex_height;

    float const vt_min[ 2 ] = { float ( -px_oversize_n.width () ),
                                float ( -px_oversize_n.height () ) };
    float const vt_max[ 2 ] = {
        float ( inner_tile_width - px_oversize_n.width () ),
        float ( inner_tile_height - px_oversize_n.height () ) };
    float const quad_z = 0.0;
    float const uv_min[ 2 ] = { float ( uoff ), float ( voff ) };
    float const uv_max[ 2 ] = { float ( uoff + uwidth ),
                                float ( voff + vheight ) };

    Vertex_UV bottom_left = {
        { vt_min[ 0 ], vt_min[ 1 ], quad_z, uv_min[ 0 ], uv_min[ 1 ] } };
    Vertex_UV bottom_right = {
        { vt_max[ 0 ], vt_min[ 1 ], quad_z, uv_max[ 0 ], uv_min[ 1 ] } };
    Vertex_UV top_left = {
        { vt_min[ 0 ], vt_max[ 1 ], quad_z, uv_min[ 0 ], uv_max[ 1 ] } };
    Vertex_UV top_right = {
        { vt_max[ 0 ], vt_max[ 1 ], quad_z, uv_max[ 0 ], uv_max[ 1 ] } };

    vertices_n.vertices[ 0 ] = top_left;
    vertices_n.vertices[ 1 ] = bottom_left;
    vertices_n.vertices[ 2 ] = top_right;
    vertices_n.vertices[ 3 ] = bottom_right;

    if ( DEBUG_QFRE_RENDERER ) {
      Debug_Stream oss;
      oss << "Renderer::initialize_matrix_tile_vertices\n";
      for ( int ii = 0; ii != 4; ++ii ) {
        oss << "  ";
        for ( int jj = 0; jj != 5; ++jj ) {
          oss << vertices_n.vertices[ ii ][ jj ] << " ";
        }
        oss << "\n";
      }
    }
  }

  return initialize_tile_vertices ( vertices_n );
}

bool
Renderer::initialize_matrix_tile_shaders ()
{
  bool good = true;
  if ( good ) {
    _tile_shader_1x1 = std::make_unique< Matrix_Tile_Shader > ();
    good = initialize_tile_shader ( *_tile_shader_1x1,
                                    ":/shaders/tile-vt.glsl",
                                    ":/shaders/tile-1x1-fr.glsl" );
  }
  if ( good ) {
    _tile_shader_2x2 = std::make_unique< Matrix_Tile_Shader > ();
    good = initialize_tile_shader ( *_tile_shader_2x2,
                                    ":/shaders/tile-vt.glsl",
                                    ":/shaders/tile-2x2-fr.glsl" );
  }
  if ( good ) {
    _tile_shader_4x4 = std::make_unique< Matrix_Tile_Shader > ();
    good = initialize_tile_shader ( *_tile_shader_4x4,
                                    ":/shaders/tile-vt.glsl",
                                    ":/shaders/tile-4x4-fr.glsl" );
  }
  if ( good ) {
    _tile_shader_filter = std::make_unique< Matrix_Tile_Shader > ();
    good = initialize_tile_shader ( *_tile_shader_filter,
                                    ":/shaders/tile-vt.glsl",
                                    ":/shaders/tile-filter-fr.glsl" );
  }
  return good;
}

bool
Renderer::initialize_matrix_tile_vertex_array_objects ()
{
  if ( !initialize_tile_vertex_array_object ( *_tile_shader_1x1,
                                              *_tile_vertices ) ) {
    return false;
  }
  if ( !initialize_tile_vertex_array_object ( *_tile_shader_2x2,
                                              *_tile_vertices ) ) {
    return false;
  }
  if ( !initialize_tile_vertex_array_object ( *_tile_shader_4x4,
                                              *_tile_vertices ) ) {
    return false;
  }
  if ( !initialize_tile_vertex_array_object ( *_tile_shader_filter,
                                              *_tile_vertices ) ) {
    return false;
  }
  return true;
}

void
Renderer::initialize_matrix_tile_objects_clear ()
{
  // Clear shaders
  _tile_shader_filter.reset ();
  _tile_shader_4x4.reset ();
  _tile_shader_2x2.reset ();
  _tile_shader_1x1.reset ();
  // Clear vertices
  _tile_vertices.reset ();
}

bool
Renderer::initialize_matrix_tile_objects ()
{
  initialize_matrix_tile_objects_clear ();
  bool good = true;
  if ( good ) {
    _tile_vertices = std::make_unique< Tile_Vertices > ();
    good = initialize_matrix_tile_vertices ( *_tile_vertices, { 0.45, 0.45 } );
  }
  if ( good ) {
    good = initialize_matrix_tile_shaders ();
  }
  if ( good ) {
    good = initialize_matrix_tile_vertex_array_objects ();
  }
  if ( !good ) {
    initialize_matrix_tile_objects_clear ();
  }
  return good;
}

bool
Renderer::initialize_matrix_tile_shader_locations (
    Matrix_Tile_Shader & shader_n )
{
  // Shader program
  auto & prog = shader_n.gl_sp;
  if ( !prog.bind () ) {
    {
      Error_Stream oss;
      oss << "Renderer::initialize_tile_shader_locations\n"
          << "  Shader program binding failed!\n";
    }
    return false;
  }
  auto & shl ( shader_n.shader_locations );
  // Shader program uniforms
  shl.vert_matrix = prog.uniformLocation ( "vert_matrix" );
  shl.vert_offset = prog.uniformLocation ( "vert_offset" );
  shl.vert_pos = prog.uniformLocation ( "vert_pos" );
  shl.vert_scale = prog.uniformLocation ( "vert_scale" );
  shl.vert_uv_pos = prog.uniformLocation ( "vert_uv_pos" );
  shl.iter_max = prog.uniformLocation ( "iter_max" );
  shl.iter_tex = prog.uniformLocation ( "iter_tex" );
  shl.iter_tex_size = prog.uniformLocation ( "iter_tex_size" );
  shl.sample_offsets = prog.uniformLocation ( "sample_offsets" );
  shl.col_end = prog.uniformLocation ( "col_end" );
  shl.col_blank = prog.uniformLocation ( "col_blank" );
  shl.pal_colors = prog.uniformLocation ( "pal_colors" );
  shl.pal_offset = prog.uniformLocation ( "pal_offset" );
  shl.pal_tex = prog.uniformLocation ( "pal_tex" );
  shl.pal_tex_size = prog.uniformLocation ( "pal_tex_size" );

  if ( DEBUG_QFRE_RENDERER ) {
    Debug_Stream oss;
    oss << "Renderer::initialize_tile_shader_locations:\n";
    oss << "  vert_matrix " << shl.vert_matrix << "\n";
    oss << "  vert_offset " << shl.vert_offset << "\n";
    oss << "  vert_pos " << shl.vert_pos << "\n";
    oss << "  vert_scale " << shl.vert_scale << "\n";
    oss << "  vert_uv_pos " << shl.vert_uv_pos << "\n";
    oss << "  iter_max " << shl.iter_max << "\n";
    oss << "  iter_tex " << shl.iter_tex << "\n";
    oss << "  iter_tex_size " << shl.iter_tex_size << "\n";
    oss << "  sample_offsets " << shl.sample_offsets << "\n";
    oss << "  col_end " << shl.col_end << "\n";
    oss << "  col_blank " << shl.col_blank << "\n";
    oss << "  pal_colors " << shl.pal_colors << "\n";
    oss << "  pal_offset " << shl.pal_offset << "\n";
    oss << "  pal_tex " << shl.pal_tex << "\n";
    oss << "  pal_tex_size " << shl.pal_tex_size << "\n";
  }
  prog.release ();

  return true;
}

bool
Renderer::initialize_shader_locations ()
{
  if ( !_background_shader || !_tile_shader_1x1 || !_tile_shader_2x2 ||
       !_tile_shader_4x4 || !_tile_shader_filter ) {
    return false;
  }

  if ( !initialize_background_shader_locations () ) {
    return false;
  }
  if ( !initialize_matrix_tile_shader_locations ( *_tile_shader_1x1 ) ) {
    return false;
  }
  if ( !initialize_matrix_tile_shader_locations ( *_tile_shader_2x2 ) ) {
    return false;
  }
  if ( !initialize_matrix_tile_shader_locations ( *_tile_shader_4x4 ) ) {
    return false;
  }
  if ( !initialize_matrix_tile_shader_locations ( *_tile_shader_filter ) ) {
    return false;
  }
  return true;
}

QOpenGLFramebufferObject *
Renderer::createFramebufferObject ( const QSize & size )
{
  QOpenGLFramebufferObjectFormat format;
  return new QOpenGLFramebufferObject ( size, format );
}

void
Renderer::synchronize ( QQuickFramebufferObject * fbo_n )
{
  QQuickFramebufferObject::Renderer::synchronize ( fbo_n );
  {
    Fractal * fractal = static_cast< Fractal * > ( fbo_n );
    // Acquire events
    fractal->renderer_sync_get_events ( _event_chain );

    // Process events
    if ( !_event_chain.is_empty () ) {
      process_events ();
    }

    // Upload tiles
    if ( _render_settings.upload () ) {
      upload_tiles ( fractal );
    }
  }
}

bool
Renderer::upload_palette_texture ( QOpenGLFunctions * glf_n )
{
  if ( DEBUG_QFRE_RENDERER ) {
    Debug_Stream oss;
    oss << "Renderer::upload_palette_texture()\n";
  }

  if ( _color_settings.scheme () ) {
    glf_n->glBindTexture ( GL_TEXTURE_2D, _palette_texture_id );
    if ( gl_error_print ( "Renderer::upload_palette_texture: glBindTexture: ",
                          glf_n ) ) {
      glf_n->glBindTexture ( GL_TEXTURE_2D, 0 );
      return false;
    }
    {
      std::vector< std::uint32_t > buffer (
          _palette_texture_size.area< std::uint32_t > (), 0x88888888 );
      _color_settings.scheme ()->acquire_colors (
          reinterpret_cast< std::uint8_t * > ( buffer.data () ) );
      glf_n->glPixelStorei ( GL_UNPACK_ROW_LENGTH, 0 );
      glf_n->glPixelStorei ( GL_UNPACK_ALIGNMENT, 4 );
      glf_n->glTexImage2D ( GL_TEXTURE_2D,
                            0,
                            GL_RGBA,
                            _palette_texture_size.width (),
                            _palette_texture_size.height (),
                            0,
                            GL_RGBA,
                            GL_UNSIGNED_BYTE,
                            buffer.data () );
    }
    if ( gl_error_print ( "Renderer::upload_palette_texture: glTexImage2D: ",
                          glf_n ) ) {
      glf_n->glBindTexture ( GL_TEXTURE_2D, 0 );
      return false;
    }
    glf_n->glBindTexture ( GL_TEXTURE_2D, 0 );
  } else {
    return false;
  }
  return true;
}

void
Renderer::process_events ()
{
  while ( !_event_chain.is_empty () ) {
    event::Event * event ( _event_chain.front () );
    auto rfunc = static_cast< renderer::events::Base_Event * > ( event )
                     ->renderer_function;
    ( this->*rfunc ) ( event );
    _event_chain.pop_front ();
  }
}

void
Renderer::process_event_viewport ( event::Event * event_n )
{
  auto * cev = static_cast< renderer::events::Viewport * > ( event_n );
  _viewport_size = cev->size;
  _viewport_projection.setToIdentity ();
  _viewport_projection.ortho ( 0.0f,
                               float ( _viewport_size.width () ),
                               0.0f,
                               float ( _viewport_size.height () ),
                               -1.0f,
                               1.0f );
}

void
Renderer::process_event_matrix_move ( event::Event * event_n )
{
  auto * cev = static_cast< renderer::events::Matrix_Move * > ( event_n );
  _paint_matrix_tiles->set_position ( cev->position.x (), cev->position.y () );

  // Move background tiles
  for ( auto & ref : _background_tiles ) {
    ref->add_position ( cev->background_movement );
  }
}

void
Renderer::process_event_matrix_resize ( event::Event * event_n )
{
  process_event_matrix_move ( event_n );
  auto * cev = static_cast< renderer::events::Matrix_Resize * > ( event_n );
  _paint_matrix.resize ( cev->deltas );
  _rebuild_paint_matrix_tiles = true;
}

void
Renderer::process_event_matrix_scale ( event::Event * event_n )
{
  auto * cev = static_cast< renderer::events::Matrix_Scale * > ( event_n );
  _paint_matrix.set_scale ( cev->scale );
  _rebuild_paint_matrix_tiles = true;
}

void
Renderer::process_event_matrix_loop ( event::Event * event_n )
{
  process_event_matrix_move ( event_n );
  {
    auto * cev = static_cast< renderer::events::Matrix_Loop * > ( event_n );
    {
      renderer::Matrix::Manipulator pt_man ( _paint_matrix );
      pt_man.loop ( cev->loops );
    }
    _rebuild_paint_matrix_tiles = true;
  }
}

void
Renderer::process_event_snapshot ( event::Event * event_n )
{
  (void)event_n;

  if ( _paint_matrix.paintables_count () != 0 ) {
    // Clear paintables list in the matrix
    _paint_matrix.paintables_clear ();
    // Move current front tiles plane to the backgrounds list
    if ( _render_settings.backgrounds () != 0 ) {
      // Trim existing backgrounds
      while ( _background_tiles.size () >= _render_settings.backgrounds () ) {
        _background_tiles.pop_front ();
      }
      {
        renderer::Paint_Tiles_Reference ref = _paint_matrix_tiles;
        // Create new paint matrix tiles in place on the old ones
        {
          _paint_matrix_tiles = renderer::Paint_Tiles_Reference::created ();
          _paint_matrix_tiles->set_position ( ref->position () );
          _paint_matrix_tiles->set_scale ( ref->scale () );
          _paint_matrix_tiles->set_iterator_settings (
              ref->iterator_settings () );
        }
        // Move old paint matrix tiles to the background
        _background_tiles.emplace_back ( std::move ( ref ) );
      }
    }
    _rebuild_paint_matrix_tiles = true;
  }
}

void
Renderer::process_event_zoom ( event::Event * event_n )
{
  // Transform background tile planes
  auto * cev = static_cast< renderer::events::Zoom * > ( event_n );
  if ( cev->snapshot ) {
    process_event_snapshot ( nullptr );
  } else {
    process_event_clear_foreground ( nullptr );
  }

  for ( auto & ref : _background_tiles ) {
    geo::Double_Size px_delta ( ref->position ().x (), ref->position ().y () );
    px_delta.width_ref () -= cev->position.x ();
    px_delta.height_ref () -= cev->position.y ();
    px_delta.width_ref () *= cev->amount;
    px_delta.height_ref () *= cev->amount;
    ref->set_position ( cev->position.x () + px_delta.width (),
                        cev->position.y () + px_delta.height () );
    ref->multiply_scale ( cev->amount );
  }
}

void
Renderer::process_event_clear ( event::Event * event_n )
{
  (void)event_n;
  // Clear foreground
  _paint_matrix.paintables_clear ();
  _paint_matrix_tiles->clear ();
  // Clear background
  _background_tiles.clear ();
}

void
Renderer::process_event_clear_foreground ( event::Event * event_n )
{
  (void)event_n;
  _paint_matrix.paintables_clear ();
  _paint_matrix_tiles->clear ();
}

void
Renderer::process_event_render_settings ( event::Event * event_n )
{
  auto * cev = static_cast< renderer::events::Render_Settings * > ( event_n );
  _render_settings = cev->settings;

  // Reduce number of backgrounds on demand
  while ( _background_tiles.size () > _render_settings.backgrounds () ) {
    _background_tiles.pop_front ();
  }
  // Select tile object
  switch ( _render_settings.supersampling () ) {
  case 1:
    _tile_shader_viewport = _tile_shader_1x1.get ();
    break;
  case 2:
    _tile_shader_viewport = _tile_shader_2x2.get ();
    break;
  case 4:
    _tile_shader_viewport = _tile_shader_4x4.get ();
    break;
  default:
    _tile_shader_viewport = _tile_shader_1x1.get ();
  }
  // Request sample offset recalculation
  _rebuild_shader_sample_offsets = true;
}

void
Renderer::process_event_iterator_settings ( event::Event * event_n )
{
  auto * cev = static_cast< renderer::events::Iterator_Settings * > ( event_n );
  _paint_matrix_tiles->set_iterator_settings ( cev->settings );
}

void
Renderer::process_event_color_scheme ( event::Event * event_n )
{
  auto * cev = static_cast< renderer::events::Color_Scheme * > ( event_n );
  _color_settings.set_scheme ( cev->scheme );
  _rebuild_color_palette = true;
}

void
Renderer::process_event_color_offset ( event::Event * event_n )
{
  auto * cev = static_cast< renderer::events::Color_Offset * > ( event_n );
  _color_settings.set_offset ( cev->offset );
}

void
Renderer::upload_tiles ( Fractal * fractal_n )
{
  // Synchronize reader
  bool new_textured_tiles = false;
  // Fetch image tiles
  auto & fre = fractal_n->fractal_engine ();
  fre::Reader * tiles_reader = fre.reader ().get ();
  if ( tiles_reader != nullptr ) {
    auto time_start = std::chrono::system_clock::now ();
    tiles_reader->set_image_code ( fre.image_code () );
    tiles_reader->fetch_begin ();
    if ( tiles_reader->fetchable () ) {
      QOpenGLFunctions * glf = gl_current_functions ();
      glf->glEnable ( GL_TEXTURE_2D );

      fre::image_matrix::Tile_Buffer const * imt_buffer = nullptr;
      while ( ( imt_buffer = tiles_reader->fetch_next () ) != nullptr ) {
        auto const & imtb_id ( imt_buffer->tile_id () );

        if ( DEBUG_QFRE_RENDERER_UPLOAD ) {
          Debug_Stream oss;
          oss << "Renderer::upload_tiles: Fetched:  pos: " << imtb_id.pos ()
              << "  image_code: " << imtb_id.image_code () << "\n";
        }

        renderer::Matrix_Tile * pt_tile =
            _paint_matrix.global_tile ( imtb_id.pos () );
        if ( pt_tile != nullptr ) {
          // Acquire texture tile on demand
          if ( !pt_tile->tile () ) {
            pt_tile->set_texture_tile ( _texture_atlas.acquire_tile ( glf ) );
            _paint_matrix.paintable_register ( pt_tile );
            new_textured_tiles = true;
          }
          pt_tile->tile ().upload ( imt_buffer->cdata (), glf );
        }
        tiles_reader->fetch_accept ();

        // Limit the amout of time used for uploading
        {
          auto time_now = std::chrono::system_clock::now ();
          auto elapsed_us =
              std::chrono::duration_cast< std::chrono::microseconds > (
                  time_now - time_start );
          if ( elapsed_us.count () > ( 1000000 / 30 ) ) {
            fractal_n->update ();
            break;
          }
        }
      }
      tiles_reader->fetch_end ();
    }
  }

  if ( new_textured_tiles ) {
    _rebuild_paint_matrix_tiles = true;
  }
}

void
Renderer::rebuild_paint_matrix_tiles ()
{
  _paint_matrix_tiles->clear ();
  _paint_matrix_tiles->set_scale ( 1.0 / _paint_matrix.scale ().width (),
                                   1.0 / _paint_matrix.scale ().height () );
  _paint_matrix_tiles->set_tiles_count ( _paint_matrix.paintables_count () );

  for ( auto const & tex_vec_pair : _paint_matrix.paintables_map () ) {
    auto & vec = _paint_matrix_tiles->tiles_map_ref ()[ tex_vec_pair.first ];
    vec.resize ( tex_vec_pair.second.size () );

    auto its = tex_vec_pair.second.begin ();
    auto its_end = tex_vec_pair.second.end ();
    auto itd = vec.begin ();
    for ( ; its != its_end; ++its, ++itd ) {
      {
        geo::Int32_Point ipos =
            _paint_matrix.local_inner_tile_pos_from_global ( ( *its )->pos () );
        itd->vertex_pos.set ( ipos.x (), ipos.y () );
      }
      itd->tile = ( *its )->tile ();
      itd->uv_pos = itd->tile.pos_uv ();
    }
  }
}

void
Renderer::rebuild_shader_sample_offsets ()
{
  _sample_offsets.clear ();
  if ( _render_settings.supersampling () != 0 ) {
    std::uint32_t const num = _render_settings.supersampling ();
    auto const & tex_size = _texture_atlas.texture_size ();
    double dx = 1.0 / tex_size.width () / num / 2.0;
    double dy = 1.0 / tex_size.height () / num;
    double base_x = -dx * ( double ( num - 1 ) / 2.0 );
    double base_y = -dy * ( double ( num - 1 ) / 2.0 );
    for ( std::uint32_t yy = 0; yy != num; ++yy ) {
      double yy_pos = base_y + dy * double ( yy );
      for ( std::uint32_t xx = 0; xx != num; ++xx ) {
        _sample_offsets.emplace_back ( base_x + dx * double ( xx ), yy_pos );
      }
    }
  }
}

void
Renderer::render ()
{
  QOpenGLFunctions * glf = gl_current_functions ();

  glf->glDisable ( GL_DEPTH_TEST );
  glf->glEnable ( GL_CULL_FACE );
  glf->glEnable ( GL_TEXTURE_2D );
  glf->glEnable ( GL_BLEND );
  // glf->glBlendFunc ( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
  glf->glBlendFuncSeparate (
      GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ZERO, GL_ONE );

  glf->glClearColor ( _color_clear.redF (),
                      _color_clear.greenF (),
                      _color_clear.blueF (),
                      _color_clear.alphaF () );
  glf->glClear ( GL_COLOR_BUFFER_BIT );

  // Upload palette on demand
  if ( _rebuild_color_palette ) {
    if ( upload_palette_texture ( glf ) ) {
      _rebuild_color_palette = false;
    }
  }

  // Rebuild paint matrix tiles on demand
  if ( _rebuild_paint_matrix_tiles ) {
    _rebuild_paint_matrix_tiles = false;
    rebuild_paint_matrix_tiles ();
  }
  if ( _rebuild_shader_sample_offsets ) {
    _rebuild_shader_sample_offsets = false;
    rebuild_shader_sample_offsets ();
  }

  // Render background
  render_background ( glf );

  // Render matrix tiles
  if ( _palette_texture_id != 0 ) {
    // Draw background tiles
    if ( _render_settings.draw_backgrounds () && _tile_shader_filter ) {
      if ( render_matrix_tiles_begin ( glf, *_tile_shader_filter ) ) {
        for ( auto & item : _background_tiles ) {
          render_matrix_tiles ( glf, *_tile_shader_filter, *item );
        }
        render_matrix_tiles_end ( glf, *_tile_shader_filter );
      }
    }
    // Draw viewport matrix tiles
    if ( _render_settings.draw_foreground () &&
         ( _tile_shader_viewport != nullptr ) ) {
      if ( render_matrix_tiles_begin ( glf, *_tile_shader_viewport ) ) {
        render_matrix_tiles (
            glf, *_tile_shader_viewport, *_paint_matrix_tiles );
        render_matrix_tiles_end ( glf, *_tile_shader_viewport );
      }
    }
  }

  _quick_window->resetOpenGLState ();
}

bool
Renderer::render_background ( QOpenGLFunctions * glf_n )
{
  // Triangle draw type
  GLenum draw_type =
      _render_settings.wireframe () ? GL_LINE_STRIP : GL_TRIANGLE_STRIP;

  // Bind shader program
  auto & prog = _background_shader->gl_sp;
  if ( !prog.bind () ) {
    {
      Error_Stream oss;
      oss << "Renderer::render_matrix_tiles_begin\n"
          << "  Shader program binding failed!\n";
    }
    _background_shader->gl_vao.release ();
    return false;
  }

  // Bind vertex array object
  _background_shader->gl_vao.bind ();

  // -- Uniform values
  auto & shl = _background_shader->shader_locations;
  // - Vertex
  prog.setUniformValue ( shl.vert_matrix, _viewport_projection );
  prog.setUniformValue ( shl.vert_pos,
                         _paint_matrix_tiles->position ().x (),
                         _paint_matrix_tiles->position ().y () );
  prog.setUniformValue ( shl.vert_scale,
                         GLfloat ( _paint_matrix.size_px ().width () ),
                         GLfloat ( _paint_matrix.size_px ().height () ) );
  prog.setUniformValue ( shl.quad_size,
                         GLfloat ( _paint_matrix.size_px ().width () ),
                         GLfloat ( _paint_matrix.size_px ().height () ) );
  // - Colors
  prog.setUniformValue ( shl.col_plane, _color_background_plane );
  prog.setUniformValue ( shl.col_line, _color_background_line );

  // -- Draw
  glf_n->glDrawArrays ( draw_type, 0, 4 );
  gl_error_print ( "Renderer::render_background: glDrawArrays: ", glf_n );

  // -- Release objects
  _background_shader->gl_vao.release ();
  _background_shader->gl_sp.release ();

  return true;
}

bool
Renderer::render_matrix_tiles_begin ( QOpenGLFunctions * glf_n,
                                      Matrix_Tile_Shader & shader_n )
{
  // Bind palette texture
  glf_n->glActiveTexture ( GL_TEXTURE0 );
  if ( gl_error_print (
           "Renderer::render_matrix_tiles_begin: glActiveTexture: ", glf_n ) ) {
    return false;
  }
  glf_n->glBindTexture ( GL_TEXTURE_2D, _palette_texture_id );
  if ( gl_error_print ( "Renderer::render_matrix_tiles_begin: glBindTexture: ",
                        glf_n ) ) {
    return false;
  }

  // Activate iterations texture slot
  glf_n->glActiveTexture ( GL_TEXTURE1 );
  if ( gl_error_print (
           "Renderer::render_matrix_tiles_begin: glActiveTexture: ", glf_n ) ) {
    return false;
  }

  // Bind shader program
  auto & prog = shader_n.gl_sp;
  if ( !prog.bind () ) {
    {
      Error_Stream oss;
      oss << "Renderer::render_matrix_tiles_begin\n"
          << "  Shader program binding failed!\n";
    }
    return false;
  }

  // Bind vertex array object
  shader_n.gl_vao.bind ();

  // -- Uniform values
  auto & shl = shader_n.shader_locations;
  // - Vertex
  prog.setUniformValue ( shl.vert_matrix, _viewport_projection );
  // - Iteration
  prog.setUniformValue ( shl.iter_tex, 1 );
  prog.setUniformValue ( shl.iter_tex_size,
                         GLfloat ( _texture_atlas.texture_width () ),
                         GLfloat ( _texture_atlas.texture_height () ) );
  if ( shl.sample_offsets >= 0 ) {
    prog.setUniformValueArray ( shl.sample_offsets,
                                &_sample_offsets.front ()[ 0 ],
                                _sample_offsets.size (),
                                2 );
  }
  // - Colors
  prog.setUniformValue ( shl.col_blank, _color_blank );
  prog.setUniformValue ( shl.col_end, _color_settings.color_end () );
  // - Palette
  prog.setUniformValue ( shl.pal_colors,
                         GLfloat ( _color_settings.scheme_color_count () ) );
  prog.setUniformValue ( shl.pal_offset,
                         GLfloat ( _color_settings.mapped_offset () ) );
  prog.setUniformValue ( shl.pal_tex, 0 );
  prog.setUniformValue ( shl.pal_tex_size,
                         GLfloat ( _palette_texture_size.width () ),
                         GLfloat ( _palette_texture_size.height () ) );

  return true;
}

void
Renderer::render_matrix_tiles_end ( QOpenGLFunctions * glf_n,
                                    Matrix_Tile_Shader & shader_n )
{
  // Release iterations texture
  glf_n->glActiveTexture ( GL_TEXTURE1 );
  glf_n->glBindTexture ( GL_TEXTURE_2D, 0 );
  // Release palette texture
  glf_n->glActiveTexture ( GL_TEXTURE0 );
  glf_n->glBindTexture ( GL_TEXTURE_2D, 0 );

  // Release objects
  shader_n.gl_vao.release ();
  shader_n.gl_sp.release ();
}

void
Renderer::render_matrix_tiles ( QOpenGLFunctions * glf_n,
                                Matrix_Tile_Shader & shader_n,
                                renderer::Paint_Tiles const & paint_tiles_n )
{
  if ( paint_tiles_n.tiles_count () == 0 ) {
    return;
  }

  // Triangle draw type
  GLenum draw_type =
      _render_settings.wireframe () ? GL_LINE_STRIP : GL_TRIANGLE_STRIP;

  // Shader program
  auto & prog = shader_n.gl_sp;
  // -- Uniform values
  auto & shl = shader_n.shader_locations;
  // - Vertex
  prog.setUniformValue ( shl.vert_offset,
                         paint_tiles_n.position ().x (),
                         paint_tiles_n.position ().y () );
  prog.setUniformValue ( shl.vert_scale,
                         paint_tiles_n.scale ().width (),
                         paint_tiles_n.scale ().height () );
  // - Iteration
  prog.setUniformValue (
      shl.iter_max,
      GLfloat ( paint_tiles_n.iterator_settings ().iterations_max () ) );

  for ( auto const & tex_vec_pair : paint_tiles_n.tiles_map () ) {
    // Bind texture
    tex_vec_pair.first->bind ( glf_n );
    for ( auto const & tile : tex_vec_pair.second ) {
      // Shader program uniforms
      prog.setUniformValue (
          shl.vert_pos, tile.vertex_pos.x (), tile.vertex_pos.y () );
      prog.setUniformValue (
          shl.vert_uv_pos, tile.uv_pos.x (), tile.uv_pos.y () );
      // Draw
      glf_n->glDrawArrays ( draw_type, 0, 4 );
      gl_error_print ( "Renderer::render_matrix_tiles: glDrawArrays: ", glf_n );
    }
  }
}
} // namespace qfre::renderer
