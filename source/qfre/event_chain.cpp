/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "event_chain.hpp"

namespace qfre
{

void
Event_Chain::clear ()
{
  while ( !is_empty () ) {
    delete pop_front_not_empty ();
  }
}
} // namespace qfre
