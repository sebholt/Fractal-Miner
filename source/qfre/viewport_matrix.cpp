/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "viewport_matrix.hpp"
#include <fre/matrix_manipulator.hpp>
#include <message_streams.hpp>

namespace qfre
{

Viewport_Matrix::Viewport_Matrix () {}

Viewport_Matrix::~Viewport_Matrix () {}

void
Viewport_Matrix::set_tile_size ( geo::UInt16_Size const & tile_size_n )
{
  fre::Matrix_Base::reset ();
  _tile_size = tile_size_n;
  _position.reset ();
  _viewport_size.reset ();
  _cp_origin.reset ();
}

void
Viewport_Matrix::resize ( fre::Resize_Deltas const & deltas_n )
{
  _size.set_width ( std::int32_t ( _size.width () ) +
                    deltas_n.horizontal_sum () );
  _size.set_height ( std::int32_t ( _size.height () ) +
                     deltas_n.vertical_sum () );

  _column_id_ends.first_ref () -= deltas_n.horizontal ().first ();
  _column_id_ends.second_ref () += deltas_n.horizontal ().second ();
  _row_id_ends.first_ref () -= deltas_n.vertical ().first ();
  _row_id_ends.second_ref () += deltas_n.vertical ().second ();

  DEBUG_ASSERT ( _column_id_ends.first () < _column_id_ends.second () );
  DEBUG_ASSERT ( _row_id_ends.first () < _row_id_ends.second () );

  // Update matrix size
  _size_px.set ( columns () * _tile_size.width (),
                 rows () * _tile_size.height () );
}

geo::Int32_Pair
Viewport_Matrix::move_matrix_in_viewport ( geo::Int32_Size const & movement_n )
{
  geo::Int32_Pair loops;
  // Initial adjustment of the paint matrix position
  _position.x_ref () += movement_n.width ();
  _position.y_ref () += movement_n.height ();

  if ( movement_n.width () != 0 ) {
    std::int32_t const twi ( tile_size ().width () );
    if ( movement_n.width () < 0 ) {
      // Loop left to right
      loops.first_ref () = ( -matrix_extent_left () / twi );
    } else {
      // Loop right to left
      loops.first_ref () = ( -matrix_extent_right () / twi );
    }
    std::int32_t const pos_delta = ( loops.first () * twi );
    DEBUG_ASSERT ( pos_delta % _viewport_scale.width () == 0 );
    _position.x_ref () += ( pos_delta / _viewport_scale.width () );
  }

  if ( movement_n.height () != 0 ) {
    std::int32_t thi ( tile_size ().height () );
    if ( movement_n.height () < 0 ) {
      // Loop bottom to top
      loops.second_ref () = ( -matrix_extent_bottom () / thi );
    } else {
      // Loop top to bottom
      loops.second_ref () = ( -matrix_extent_top () / thi );
    }
    std::int32_t const pos_delta = ( loops.second () * thi );
    DEBUG_ASSERT ( pos_delta % _viewport_scale.height () == 0 );
    _position.y_ref () += ( pos_delta / _viewport_scale.height () );
  }

  _column_id_ends.first_ref () += loops.first ();
  _column_id_ends.second_ref () += loops.first ();
  _row_id_ends.first_ref () += loops.second ();
  _row_id_ends.second_ref () += loops.second ();

  DEBUG_ASSERT ( matrix_extent_left () <= 0 );
  DEBUG_ASSERT ( matrix_extent_right () >= 0 );
  DEBUG_ASSERT ( matrix_extent_bottom () <= 0 );
  DEBUG_ASSERT ( matrix_extent_top () >= 0 );

  return loops;
}
} // namespace qfre
