/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/reference.hpp>
#include <QColor>
#include <QString>
#include <atomic>
#include <cstdint>

namespace qfre
{

// -- Forward declaration
class Color_Scheme;
typedef fre::Reference< Color_Scheme > Color_Scheme_Reference;

/// @brief Abstract base class for color palette generators
///
class Color_Scheme
{
  public:
  // -- Types
  static constexpr std::uint32_t num_colors_max = 256 * 256;
  // -- Friends
  friend Color_Scheme_Reference;

  public:
  // -- Constructors

  Color_Scheme ( QString const & id_string_n );

  virtual ~Color_Scheme ();

  // -- Id string

  QString const &
  id_string () const
  {
    return _id_string;
  }

  // -- Id code

  std::uint32_t
  id_code () const
  {
    return _id_code;
  }

  void
  set_id_code ( std::uint32_t code_n )
  {
    _id_code = code_n;
  }

  // -- Abstract interface

  std::uint32_t
  num_colors () const
  {
    return _num_colors;
  }

  /// @brief Writes num_colors() to rgba_n
  virtual void
  acquire_colors ( std::uint8_t * rgba_n ) = 0;

  protected:
  void
  set_num_colors ( std::uint32_t num_n )
  {
    _num_colors = std::min ( num_n, num_colors_max );
  }

  private:
  QString _id_string;
  std::uint32_t _id_code = 0;
  std::uint32_t _num_colors = 0;
  QColor _end_color = QColor ( 0, 0, 0, 255 );
  std::atomic_uint _reference_count = 0;
};
} // namespace qfre
