/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <QObject>
#include <cstdint>

namespace qfre
{

/// @brief Render settings
///
class Render_Settings : public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY ( Render_Settings )

  Q_PROPERTY ( bool drawForeground READ draw_foreground WRITE
                   set_draw_foreground NOTIFY draw_foreground_changed )
  Q_PROPERTY ( bool drawBackgrounds READ draw_backgrounds WRITE
                   set_draw_backgrounds NOTIFY draw_backgrounds_changed )
  Q_PROPERTY ( bool upload READ upload WRITE set_upload NOTIFY upload_changed )
  Q_PROPERTY ( bool wireframe READ wireframe WRITE set_wireframe NOTIFY
                   wireframe_changed )
  Q_PROPERTY ( unsigned int backgrounds READ backgrounds WRITE set_backgrounds
                   NOTIFY backgrounds_changed )
  Q_PROPERTY ( unsigned int supersampling READ supersampling WRITE
                   set_supersampling NOTIFY supersampling_changed )

  public:
  // -- Constructors

  Render_Settings ( QObject * parent_n = nullptr )
  : QObject ( parent_n )
  {
  }

  ~Render_Settings () = default;

  // -- Draw foreground

  bool
  draw_foreground () const
  {
    return _draw_foreground;
  }

  void
  set_draw_foreground ( bool flag_n );

  Q_SIGNAL
  void
  draw_foreground_changed ();

  // -- Draw backgrounds

  bool
  draw_backgrounds () const
  {
    return _draw_backgrounds;
  }

  void
  set_draw_backgrounds ( bool flag_n );

  Q_SIGNAL
  void
  draw_backgrounds_changed ();

  // -- Upload

  bool
  upload () const
  {
    return _upload;
  }

  void
  set_upload ( bool flag_n );

  Q_SIGNAL
  void
  upload_changed ();

  // -- Wireframe

  bool
  wireframe () const
  {
    return _wireframe;
  }

  void
  set_wireframe ( bool flag_n );

  Q_SIGNAL
  void
  wireframe_changed ();

  // -- Maximum number of background planes

  std::uint32_t
  backgrounds () const
  {
    return _backgrounds;
  }

  void
  set_backgrounds ( std::uint32_t num_n );

  Q_SIGNAL
  void
  backgrounds_changed ();

  // -- Supersampling

  std::uint32_t
  supersampling () const
  {
    return _supersampling;
  }

  void
  set_supersampling ( std::uint32_t num_n );

  Q_SIGNAL
  void
  supersampling_changed ();

  private:
  bool _draw_foreground = true;
  bool _draw_backgrounds = true;
  bool _upload = true;
  bool _wireframe = false;
  std::uint32_t _backgrounds = 4;
  std::uint32_t _supersampling = 1;
};
} // namespace qfre
