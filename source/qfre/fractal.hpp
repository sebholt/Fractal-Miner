/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/engine.hpp>
#include <fre/id_code.hpp>
#include <fre/tile_sizes.hpp>
#include <qfre/color_scheme.hpp>
#include <qfre/color_scheme_database.hpp>
#include <qfre/color_settings.hpp>
#include <qfre/event_chain.hpp>
#include <qfre/iterator_settings.hpp>
#include <qfre/pipeline.hpp>
#include <qfre/render_settings.hpp>
#include <qfre/viewport_matrix.hpp>
#include <QQuickFramebufferObject>
#include <memory>

namespace qfre
{

// -- Forward declaration
class Pipeline;

/// @brief Qt fracal QML Item
///
class Fractal : public QQuickFramebufferObject
{
  Q_OBJECT
  Q_DISABLE_COPY ( Fractal )

  Q_PROPERTY (
      bool isManipulated READ is_manipulated NOTIFY is_manipulated_changed )
  Q_PROPERTY ( Pipeline * pipeline READ pipeline WRITE set_pipeline NOTIFY
                   pipeline_changed )
  Q_PROPERTY ( Iterator_Settings * iteratorSettings READ iterator_settings
                   NOTIFY sig_never )
  Q_PROPERTY (
      Render_Settings * renderSettings READ render_settings NOTIFY sig_never )
  Q_PROPERTY (
      Color_Settings * colorSettings READ color_settings NOTIFY sig_never )
  Q_PROPERTY ( double progress READ progress NOTIFY progress_changed )

  public:
  // -- Constructors

  Fractal ( QQuickItem * parent_n = Q_NULLPTR );

  ~Fractal ();

  // -- Accessors

  fre::Engine &
  fractal_engine ()
  {
    return _fractal_engine;
  }

  // -- Pipeline

  Pipeline *
  pipeline () const
  {
    return _pipeline;
  }

  void
  set_pipeline ( Pipeline * pipeline_n );

  Q_SIGNAL
  void
  pipeline_changed ();

  // -- Iterator settings

  Iterator_Settings *
  iterator_settings ()
  {
    return &_iterator_settings;
  }

  // -- Render settings

  Render_Settings *
  render_settings ()
  {
    return &_render_settings;
  }

  // -- Color settings

  Color_Settings *
  color_settings ()
  {
    return &_color_settings;
  }

  // -- Manipulation state

  bool
  is_manipulated () const
  {
    return _is_manipulated;
  }

  void
  set_is_manipulated ( bool flag_n );

  Q_SIGNAL void
  is_manipulated_changed ();

  // -- Manipulation

  Q_SLOT
  void
  reset_complex_area ();

  void
  zoom_at_viewport ( geo::Double_Point const & px_pos_n, double amount_n );

  void
  pan_in_viewport ( geo::Int32_Size const & movement_n );

  // -- Progress

  double
  progress () const
  {
    return _progress;
  }

  Q_SIGNAL void
  progress_changed ();

  // -- Renderer synchronization

  void
  renderer_sync_get_events ( Event_Chain & events_n )
  {
    _renderer_events.transfer_to ( events_n );
  }

  // -- Qt event handlers

  void
  geometryChanged ( const QRectF & newGeometry_n,
                    const QRectF & oldGeometry_n ) override;
  void
  mouseDoubleClickEvent ( QMouseEvent * event_n ) override;
  void
  mouseMoveEvent ( QMouseEvent * event_n ) override;
  void
  mousePressEvent ( QMouseEvent * event_n ) override;
  void
  mouseReleaseEvent ( QMouseEvent * event_n ) override;
  void
  wheelEvent ( QWheelEvent * event_n ) override;
  bool
  event ( QEvent * event_n ) override;

  // -- Renderer

  QQuickFramebufferObject::Renderer *
  createRenderer () const override;

  private:
  // -- Fractal engine

  void
  process_engine_events ();

  // -- Viewport matrix

  void
  resize_viewport_matrix ( geo::Int32_Size viewport_size_n );

  void
  rebuild_viewport_matrix ();

  // -- Renderer

  template < class T >
  T *
  create_renderer_event ()
  {
    return _renderer_events.create_back< T > ();
  }

  void
  send_renderer_settings ();

  void
  update_progress ();

  // -- Qt utility

  Q_SIGNAL
  void
  sig_never ();

  // -- Qt change slots

  Q_SLOT
  void
  iterator_id_changed ();

  Q_SLOT
  void
  iterations_max_changed ();

  Q_SLOT
  void
  supersampling_changed ();

  Q_SLOT
  void
  render_settings_changed ();

  Q_SLOT
  void
  color_scheme_changed ();

  Q_SLOT
  void
  color_offset_changed ();

  private:
  // -- Remote QML objects
  Pipeline * _pipeline = nullptr;
  // -- Fractal settings
  Color_Scheme_Database _color_scheme_database;
  Iterator_Settings _iterator_settings;
  Render_Settings _render_settings;
  Color_Settings _color_settings;
  // -- Viewport matrix
  Viewport_Matrix _viewport_matrix;
  // -- Manipulation state
  bool _is_manipulated = false;
  bool _mouse_down = false;
  QPoint _mouse_pos;
  static constexpr double _background_snapshot_limit_default = 0.4;
  double _background_snapshot_limit = _background_snapshot_limit_default;
  // -- Progress
  double _progress = 0.0;
  // -- Shared resources
  fre::Tile_Sizes _tile_sizes;
  geo::UInt16_Size _texture_size;
  fre::Engine _fractal_engine;
  Event_Chain _renderer_events;
};
} // namespace qfre
