/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "colors.hpp"

namespace qfre
{

uint32_t
Colors::least_common_multiple ( const std::uint32_t val1,
                                const std::uint32_t val2 )
{
  std::uint32_t small;
  std::uint32_t big;
  std::uint32_t res;

  if ( val1 <= val2 ) {
    small = val1;
    big = val2;
  } else {
    small = val2;
    big = val1;
  }

  res = small;
  while ( ( res % big ) != 0 ) {
    res += small;
  }

  return res;
}
} // namespace qfre