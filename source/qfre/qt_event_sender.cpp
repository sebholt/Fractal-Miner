/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "qt_event_sender.hpp"
#include <QCoreApplication>

namespace qfre
{

void
Qt_Event_Sender::operator() () const
{
  if ( _receiver ) {
    QCoreApplication * app = QCoreApplication::instance ();
    if ( app != nullptr ) {
      app->postEvent (
          _receiver,
          new QEvent ( static_cast< QEvent::Type > ( _event_type ) ) );
    }
  }
}
} // namespace qfre
