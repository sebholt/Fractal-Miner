/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "iterator_settings.hpp"

namespace qfre
{

void
Iterator_Settings::set_iterator_id ( QString const & id_n )
{
  if ( _iterator_id != id_n ) {
    _iterator_id = id_n;
    emit iterator_id_changed ();
  }
}

void
Iterator_Settings::set_iterations_max ( std::uint32_t num_n )
{
  if ( _iterations_max != num_n ) {
    _iterations_max = num_n;
    emit iterations_max_changed ();
  }
}
} // namespace qfre
