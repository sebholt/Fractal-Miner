/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <QOpenGLContext>
#include <QOpenGLFunctions>

namespace qfre
{

void
gl_error_print ( char const * prefix_n, GLenum gl_error_n );

inline bool
gl_error_print ( char const * prefix_n, QOpenGLFunctions * glf_n )
{
  GLenum error = glf_n->glGetError ();
  if ( error != GL_NO_ERROR ) {
    gl_error_print ( prefix_n, error );
    return true;
  }
  return false;
}

inline QOpenGLFunctions *
gl_current_functions ()
{
  QOpenGLContext * glc = QOpenGLContext::currentContext ();
  if ( glc != nullptr ) {
    return glc->functions ();
  }
  return nullptr;
}
} // namespace qfre
