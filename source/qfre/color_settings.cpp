/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <qfre/color_schemes/color_steps.hpp>
#include <qfre/color_settings.hpp>
#include <cmath>
#include <message_streams.hpp>

namespace qfre
{

Color_Settings::Color_Settings ( QObject * parent_n )
: QObject ( parent_n )
{
  _color_cycling_timer.setTimerType ( Qt::PreciseTimer );
  setup_color_cycling_timer ();

  connect ( &_color_cycling_timer,
            &QTimer::timeout,
            this,
            &Color_Settings::color_cycling_tick );
}

void
Color_Settings::set_scheme_id ( QString const & id_n )
{
  if ( _scheme_id != id_n ) {
    auto scheme_ref = _color_scheme_database.acquire_color_scheme ( id_n );
    if ( scheme_ref ) {
      _scheme = scheme_ref;
      _scheme_id = id_n;
      bool off_changed = ( _offset != 0 );
      _offset = 0;
      _color_cycling_offset = 0;
      bool scc_changed = update_color_count ();
      // Emit signals
      emit scheme_id_changed ();
      if ( off_changed ) {
        emit offset_changed ();
      }
      if ( scc_changed ) {
        emit color_count_changed ();
      }
    } else {
      {
        Error_Stream oss;
        oss << "Color_Settings::set_scheme_id: Unknown color scheme \""
            << id_n.toStdString () << "\"\n";
      }
    }
  }
}

void
Color_Settings::set_scheme ( Color_Scheme_Reference const & scheme_n )
{
  _scheme = scheme_n;
  if ( _scheme ) {
    _scheme_id = _scheme->id_string ();
    _color_count = _scheme->num_colors ();
  } else {
    _scheme_id.clear ();
    _color_count = 0;
  }
  set_offset ( _offset );
}

void
Color_Settings::set_offset ( std::uint32_t value_n )
{
  if ( set_offset_private ( value_n ) ) {
    _color_cycling_offset = _offset;
  }
}

bool
Color_Settings::set_offset_private ( std::uint32_t value_n )
{
  if ( _color_count != 0 ) {
    value_n = value_n % _color_count;
  }
  if ( _offset != value_n ) {
    _offset = value_n;
    emit offset_changed ();
    return true;
  }
  return false;
}

void
Color_Settings::set_color_end ( QColor col_n )
{
  if ( _color_end != col_n ) {
    _color_end = col_n;
    emit color_end_changed ();
  }
}

bool
Color_Settings::update_color_count ()
{
  std::uint32_t count = 0;
  if ( _scheme ) {
    count = _scheme->num_colors ();
  }
  if ( _color_count != count ) {
    _color_count = count;
    return true;
  }
  return false;
}

void
Color_Settings::set_color_cycling ( bool flag_n )
{
  if ( _color_cycling != flag_n ) {
    _color_cycling = flag_n;
    if ( _color_cycling ) {
      _color_cycling_offset = _offset;
      _color_cycling_time_point = std::chrono::steady_clock::now ();
      _color_cycling_timer.start ();
    } else {
      _color_cycling_timer.stop ();
    }
    emit color_cycling_changed ();
  }
}

void
Color_Settings::set_color_cycling_forward ( bool flag_n )
{
  if ( _color_cycling_forward != flag_n ) {
    _color_cycling_forward = flag_n;
    emit color_cycling_forward_changed ();
  }
}

void
Color_Settings::set_color_cycling_cps ( std::uint32_t color_cycling_cps_n )
{
  if ( color_cycling_cps_n == 0 ) {
    color_cycling_cps_n = 1;
  }
  if ( _color_cycling_cps != color_cycling_cps_n ) {
    _color_cycling_cps = color_cycling_cps_n;
    setup_color_cycling_timer ();
    emit color_cycling_cps_changed ();
  }
}

void
Color_Settings::setup_color_cycling_timer ()
{
  std::uint32_t fps_max = 61;
  std::uint32_t fps = std::min ( _color_cycling_cps, fps_max );
  std::uint32_t duration = 1000 / fps;
  _color_cycling_timer.setInterval ( std::chrono::milliseconds ( duration ) );
}

void
Color_Settings::color_cycling_tick ()
{
  auto time_latest = _color_cycling_time_point;
  _color_cycling_time_point = std::chrono::steady_clock::now ();
  auto delta = std::chrono::duration_cast< std::chrono::microseconds > (
      _color_cycling_time_point - time_latest );
  double seconds = double ( delta.count () ) / double ( 1000000 );
  double cps = _color_cycling_cps * ( _color_cycling_forward ? 1.0 : -1.0 );
  _color_cycling_offset += seconds * cps;
  _color_cycling_offset =
      std::fmod ( _color_cycling_offset, double ( _color_count ) );
  set_offset_private ( std::floor ( _color_cycling_offset + 0.5 ) );
}
} // namespace qfre
