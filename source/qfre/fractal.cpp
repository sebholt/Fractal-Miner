/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "fractal.hpp"
#include <fre/pipeline.hpp>
#include <fre/resize_deltas.hpp>
#include <qfre/gl_utility.hpp>
#include <qfre/qt_event_sender.hpp>
#include <qfre/renderer/events.hpp>
#include <qfre/renderer/renderer.hpp>
#include <application_config.hpp>
#include <message_streams.hpp>

namespace qfre
{

/// @brief QEvent::Type to request engine processing
static const int qevent_type_process = int ( QEvent::Type::User ) + 1;
static const int qevent_type_update = int ( QEvent::Type::User ) + 2;

Fractal::Fractal ( QQuickItem * parent_n )
: QQuickFramebufferObject ( parent_n )
, _iterator_settings ( this )
, _render_settings ( this )
, _color_settings ( this )
{
  // Qt assumes painting in Qt coordinates (flipped vertically)
  setMirrorVertically ( true );
  // Accept mouse buttons
  setAcceptedMouseButtons ( Qt::AllButtons );

  // -- Initialize the fractal render engine
  _fractal_engine.set_events_callback (
      Qt_Event_Sender ( this, qevent_type_process ) );
  _fractal_engine.reader ()->set_callback (
      Qt_Event_Sender ( this, qevent_type_update ) );

  // -- Initialize the tile sizes
  _tile_sizes.shadow_tile_size.set ( 32, 32 );
  _tile_sizes.image_tile_size.set ( 128, 128 );
  _tile_sizes.image_tile_overlap.set ( 2, 2 );
  _texture_size.set ( 512, 512 );

  _viewport_matrix.set_tile_size ( _tile_sizes.image_inner_tile_size () );
  _fractal_engine.set_tile_sizes ( _tile_sizes );

  // -- Connect iterator settings
  connect ( iterator_settings (),
            &Iterator_Settings::iterator_id_changed,
            this,
            &Fractal::iterator_id_changed );
  connect ( iterator_settings (),
            &Iterator_Settings::iterations_max_changed,
            this,
            &Fractal::iterations_max_changed );

  // -- Connect render settings
  connect ( render_settings (),
            &Render_Settings::draw_foreground_changed,
            this,
            &Fractal::render_settings_changed );
  connect ( render_settings (),
            &Render_Settings::draw_backgrounds_changed,
            this,
            &Fractal::render_settings_changed );
  connect ( render_settings (),
            &Render_Settings::backgrounds_changed,
            this,
            &Fractal::render_settings_changed );
  connect ( render_settings (),
            &Render_Settings::upload_changed,
            this,
            &Fractal::render_settings_changed );
  connect ( render_settings (),
            &Render_Settings::wireframe_changed,
            this,
            &Fractal::render_settings_changed );
  connect ( render_settings (),
            &Render_Settings::supersampling_changed,
            this,
            &Fractal::supersampling_changed );

  // -- Connect color settings
  connect ( color_settings (),
            &Color_Settings::scheme_id_changed,
            this,
            &Fractal::color_scheme_changed );
  connect ( color_settings (),
            &Color_Settings::offset_changed,
            this,
            &Fractal::color_offset_changed );

  send_renderer_settings ();
}

Fractal::~Fractal () {}

void
Fractal::set_is_manipulated ( bool flag_n )
{
  if ( _is_manipulated != flag_n ) {
    _is_manipulated = flag_n;
    emit is_manipulated_changed ();
  }
}

void
Fractal::set_pipeline ( Pipeline * pipeline_n )
{
  if ( _pipeline != pipeline_n ) {
    if ( _pipeline != nullptr ) {
      _fractal_engine.pipeline_disconnect ();
    }

    _pipeline = pipeline_n;

    if ( _pipeline != nullptr ) {
      _fractal_engine.pipeline_connect ( _pipeline->pipeline () );
      _fractal_engine.pipeline_unlock ();
    }

    emit pipeline_changed ();
  }
}

void
Fractal::reset_complex_area ()
{
  if ( DEBUG_QFRE_FRACTAL ) {
    Debug_Stream oss;
    oss << "Fractal::reset_complex_area\n";
  }
  {
    auto const & vps = _viewport_matrix.viewport_size ();
    std::int32_t lmin ( std::min ( vps.width (), vps.height () ) );
    if ( lmin <= 0 ) {
      lmin = 100;
    }
    double const cp_radius = 2.0;
    double px_len = ( 2.0 * cp_radius ) / double ( lmin );
    fre::Complex_Origin cp_origin (
        { 0.0, 0.0 }, { px_len, 0.0 }, { 0.0, px_len } );
    geo::Double_Point px_center ( vps.width () / 2.0, vps.height () / 2.0 );
    // Set paint matrix complex area
    _viewport_matrix.set_cp_at_viewport_px ( px_center, cp_origin );
    // Set engine complex area
    _fractal_engine.set_complex (
        _viewport_matrix.matrix_px_from_viewport_px ( px_center ), cp_origin );
  }

  // Request tile clearing from the renderer
  create_renderer_event< renderer::events::Clear > ();

  set_is_manipulated ( false );
  update_progress ();
  update ();
}

void
Fractal::resize_viewport_matrix ( geo::Int32_Size viewport_size_n )
{
  namespace rev = qfre::renderer::events;
  auto & vpm = _viewport_matrix;

  geo::Int32_Point const old_position = vpm.position ();
  geo::Int32_Point new_position = { 0, 0 };
  fre::Resize_Deltas resize_deltas;
  for ( std::uint32_t dim = 0; dim != 2; ++dim ) {
    std::int32_t scale = vpm.viewport_scale ()[ dim ];
    std::int32_t num_tiles = vpm.size ()[ dim ];
    std::int32_t tile_length = vpm.tile_size ()[ dim ];
    std::int32_t pos_lower_old = vpm.position ()[ dim ] * scale;
    std::int32_t vp_length_old = vpm.viewport_size ()[ dim ] * scale;
    std::int32_t vp_length_new = viewport_size_n[ dim ] * scale;
    geo::Int32_Pair & deltas = resize_deltas.dim_ref ( dim );

    // Offset of the old viewport within the new viewport
    // (rounded to multiples of scale)
    std::int32_t center_offset = ( vp_length_new - vp_length_old ) / 2;
    center_offset = ( center_offset + scale / 2 ) / scale * scale;
    std::int32_t const tlm1 = ( tile_length - 1 );
    std::int32_t pos_lower = ( pos_lower_old + center_offset );
    std::int32_t pos_upper = pos_lower + ( num_tiles * tile_length );
    // Low end
    {
      int const limit_top = 0;
      int const limit_bottom = -tlm1;
      if ( pos_lower > limit_top ) {
        std::int32_t num = ( ( pos_lower - limit_top ) + tlm1 ) / tile_length;
        pos_lower -= num * tile_length;
        deltas.first_ref () += num;
      }
      if ( pos_lower < limit_bottom ) {
        std::int32_t num =
            ( ( limit_bottom - pos_lower ) + tlm1 ) / tile_length;
        pos_lower += num * tile_length;
        deltas.first_ref () -= num;
      }
      DEBUG_ASSERT ( ( pos_lower <= limit_top ) &&
                     ( pos_lower >= limit_bottom ) );
    }
    // Upper end
    {
      int const limit_top = vp_length_new + tlm1;
      int const limit_bottom = vp_length_new;
      if ( pos_upper > limit_top ) {
        std::int32_t num = ( ( pos_upper - limit_top ) + tlm1 ) / tile_length;
        pos_upper -= num * tile_length;
        deltas.second_ref () -= num;
      }
      if ( pos_upper < limit_bottom ) {
        std::int32_t num =
            ( ( limit_bottom - pos_upper ) + tlm1 ) / tile_length;
        pos_upper += num * tile_length;
        deltas.second_ref () += num;
      }
      DEBUG_ASSERT ( ( pos_upper <= limit_top ) &&
                     ( pos_upper >= limit_bottom ) );
    }
    // Add extra tile on demand
    {
      std::int32_t overlap = ( pos_upper - pos_lower ) - vp_length_new;
      if ( overlap < tile_length ) {
        if ( ( -pos_lower ) < ( pos_upper - vp_length_new ) ) {
          pos_lower -= tile_length;
          ++deltas.first_ref ();
        } else {
          pos_upper += tile_length;
          ++deltas.second_ref ();
        }
      }
    }
    DEBUG_ASSERT ( pos_lower % scale == 0 );
    new_position[ dim ] = pos_lower / scale;
  }

  // -- Resize matrices
  if ( resize_deltas.any_changes () ) {
    vpm.resize ( resize_deltas );
    _fractal_engine.matrix_resize ( resize_deltas );
    _fractal_engine.pipeline_unlock ();
    {
      auto * event = create_renderer_event< rev::Matrix_Resize > ();
      event->deltas = resize_deltas;
      event->position.set ( new_position.x (), new_position.y () );
      // Compute background movement
      {
        geo::Int32_Size pxd ( resize_deltas.horizontal ().first (),
                              resize_deltas.vertical ().first () );
        pxd.width_ref () *= std::int32_t ( vpm.tile_width () );
        pxd.height_ref () *= std::int32_t ( vpm.tile_height () );
        pxd.width_ref () /= std::int32_t ( vpm.viewport_scale ()[ 0 ] );
        pxd.height_ref () /= std::int32_t ( vpm.viewport_scale ()[ 1 ] );
        pxd.add ( new_position.x (), new_position.y () );
        pxd.sub ( old_position.x (), old_position.y () );
        event->background_movement.set ( pxd.width (), pxd.height () );
      }
    }
  } else {
    auto * event = create_renderer_event< rev::Matrix_Move > ();
    event->position.set ( new_position.x (), new_position.y () );
    event->background_movement.set ( new_position.x () - old_position.x (),
                                     new_position.y () - old_position.y () );
  }

  // -- Adjust viewport position and dimensions
  vpm.set_position ( new_position );
  vpm.set_viewport_size ( viewport_size_n );
  {
    auto * event = create_renderer_event< rev::Viewport > ();
    event->size = viewport_size_n;
  }

  update_progress ();
  update ();

  if ( DEBUG_QFRE_FRACTAL ) {
    Debug_Stream oss;
    oss << "Fractal::resize_viewport: " << viewport_size_n << "\n"
        << "  X: Tiles: " << vpm.size ().width ()
        << "  Width: " << vpm.width_px ()
        << "  Offset: " << vpm.position ().x ()
        << "  Extent: " << vpm.matrix_extent_left () << " : "
        << vpm.matrix_extent_right () << "\n"
        << "  Y: Tiles: " << vpm.size ().height ()
        << "  Height: " << vpm.height_px ()
        << "  Offset: " << vpm.position ().y ()
        << "  Extent: " << vpm.matrix_extent_bottom () << " : "
        << vpm.matrix_extent_top () << "\n";
  }
}

void
Fractal::rebuild_viewport_matrix ()
{
  namespace rev = qfre::renderer::events;
  auto & vpm = _viewport_matrix;

  // Store complex origin
  geo::Double_Point px_pos ( 0.0, 0.0 );
  fre::Complex_Origin cp_origin ( vpm.cp_at_viewport_px ( px_pos ),
                                  vpm.cp_origin ().dx (),
                                  vpm.cp_origin ().dy () );

  // -- Resize old matrices to zero
  {
    fre::Resize_Deltas resize_deltas;
    resize_deltas.horizontal_ref ().set_second ( -int32_t ( vpm.columns () ) );
    resize_deltas.vertical_ref ().set_second ( -int32_t ( vpm.rows () ) );

    vpm.resize ( resize_deltas );
    vpm.set_position ( { 0, 0 } );
    _fractal_engine.matrix_resize ( resize_deltas );
    {
      auto * event = create_renderer_event< rev::Matrix_Resize > ();
      event->deltas = resize_deltas;
      event->position.reset ();
      event->background_movement.reset ();
    }
  }
  // -- Request tile clearing from the renderer
  create_renderer_event< rev::Clear > ();

  // -- Update scale
  geo::Double_Size cp_scale;
  {
    geo::UInt8_Size const new_scale ( render_settings ()->supersampling (),
                                      render_settings ()->supersampling () );
    geo::UInt8_Size const old_scale ( vpm.viewport_scale () );
    vpm.set_viewport_scale ( new_scale );
    {
      auto * event = create_renderer_event< rev::Matrix_Scale > ();
      event->scale = new_scale;
    }
    cp_scale.set (
        double ( old_scale.width () ) / double ( new_scale.width () ),
        double ( old_scale.height () ) / double ( new_scale.height () ) );
  }
  // -- Resize viewport matrix
  resize_viewport_matrix ( vpm.viewport_size () );

  // -- Restore complex origin
  {
    cp_origin.dx_ref () *= cp_scale.width ();
    cp_origin.dy_ref () *= cp_scale.height ();

    // Set viewport matrix complex area
    vpm.set_cp_at_viewport_px ( px_pos, cp_origin );
    // Set engine complex area
    _fractal_engine.set_complex ( vpm.matrix_px_from_viewport_px ( px_pos ),
                                  cp_origin );
    _fractal_engine.pipeline_unlock ();
  }
  update ();
}

void
Fractal::zoom_at_viewport ( geo::Double_Point const & px_pos_n,
                            double amount_n )
{
  namespace rev = qfre::renderer::events;

  // -- Compute new complex origin and set in vpm and engine
  fre::Complex_Origin cp_origin (
      _viewport_matrix.cp_at_viewport_px ( px_pos_n ),
      _viewport_matrix.cp_origin ().dx () / amount_n,
      _viewport_matrix.cp_origin ().dy () / amount_n );
  // Set viewport matrix complex area
  _viewport_matrix.set_cp_at_viewport_px ( px_pos_n, cp_origin );
  // Set engine complex area
  _fractal_engine.set_complex (
      _viewport_matrix.matrix_px_from_viewport_px ( px_pos_n ), cp_origin );
  // Zoom event for the renderer
  {
    auto * event = create_renderer_event< rev::Zoom > ();
    event->snapshot = ( _progress > _background_snapshot_limit );
    event->position = px_pos_n;
    event->amount = amount_n;
  }

  set_is_manipulated ( true );
  update_progress ();
  update ();

  if ( DEBUG_QFRE_FRACTAL ) {
    Debug_Stream oss;
    oss << "Fractal::zoom_at_viewport: " << px_pos_n << " amount " << amount_n
        << " cp_origin " << cp_origin << "\n";
  }
}

void
Fractal::pan_in_viewport ( geo::Int32_Size const & movement_n )
{
  namespace rev = qfre::renderer::events;

  geo::Int32_Pair loops =
      _viewport_matrix.move_matrix_in_viewport ( movement_n );

  // Set engine complex area
  if ( ( loops.first () != 0 ) || ( loops.second () != 0 ) ) {
    _fractal_engine.loop ( loops );
    auto * event = create_renderer_event< rev::Matrix_Loop > ();
    event->loops = loops;
    event->position.set ( _viewport_matrix.position ().x (),
                          _viewport_matrix.position ().y () );
    event->background_movement.set ( movement_n.width (),
                                     movement_n.height () );
  } else {
    auto * event = create_renderer_event< rev::Matrix_Move > ();
    event->position.set ( _viewport_matrix.position ().x (),
                          _viewport_matrix.position ().y () );
    event->background_movement.set ( movement_n.width (),
                                     movement_n.height () );
  }

  set_is_manipulated ( true );
  update_progress ();
  update ();

  if ( DEBUG_QFRE_FRACTAL ) {
    Debug_Stream oss;
    oss << "Fractal::pan_in_viewport: movement " << movement_n << " loops "
        << loops << "\n";
  }
}

void
Fractal::send_renderer_settings ()
{
  {
    namespace rev = qfre::renderer::events;
    auto * event = create_renderer_event< rev::Render_Settings > ();
    event->settings.set_draw_foreground (
        render_settings ()->draw_foreground () );
    event->settings.set_draw_backgrounds (
        render_settings ()->draw_backgrounds () );
    event->settings.set_upload ( render_settings ()->upload () );
    event->settings.set_wireframe ( render_settings ()->wireframe () );
    event->settings.set_backgrounds ( render_settings ()->backgrounds () );
    event->settings.set_supersampling ( render_settings ()->supersampling () );
  }
  update ();
}

void
Fractal::update_progress ()
{
  double prog ( _fractal_engine.progress ().normalized () );
  if ( _progress != prog ) {
    _progress = prog;
    emit progress_changed ();
  }
}

void
Fractal::process_engine_events ()
{
  if ( _fractal_engine.events_begin () ) {
    bool go_on ( true );
    do {
      switch ( _fractal_engine.events_next () ) {
      case fre::dispatcher::events_out::Type::NONE:
        go_on = false;
        break;
      case fre::dispatcher::events_out::Type::PROGRESS:
        break;
      }
    } while ( go_on );
    _fractal_engine.events_end ();
    update_progress ();
    update ();
  }
}

void
Fractal::iterator_id_changed ()
{
  _fractal_engine.set_iterator (
      iterator_settings ()->iterator_id ().toStdString () );
  // Request tile clearing from the renderer
  create_renderer_event< renderer::events::Clear > ();
  update_progress ();
  update ();
}

void
Fractal::iterations_max_changed ()
{
  using namespace qfre::renderer;

  std::uint32_t const it_max = iterator_settings ()->iterations_max ();
  _fractal_engine.set_iterations_max ( it_max );

  // Create a background snapshot or clear foreground
  if ( _progress > _background_snapshot_limit ) {
    create_renderer_event< events::Snapshot > ();
  } else {
    create_renderer_event< events::Clear_Foreground > ();
  }
  // Set the iterations maximum in the current render matrix
  {
    using namespace qfre::renderer;
    auto * event = create_renderer_event< events::Iterator_Settings > ();
    event->settings.set_iterations_max ( it_max );
  }

  update_progress ();
  update ();
}

void
Fractal::supersampling_changed ()
{
  _background_snapshot_limit = _background_snapshot_limit_default /
                               ( double ( _render_settings.supersampling () ) *
                                 double ( _render_settings.supersampling () ) );

  rebuild_viewport_matrix ();
  render_settings_changed ();
}

void
Fractal::render_settings_changed ()
{
  send_renderer_settings ();
}

void
Fractal::color_scheme_changed ()
{
  // Send renderer event
  {
    using namespace qfre::renderer;
    auto * event = create_renderer_event< events::Color_Scheme > ();
    event->scheme = _color_settings.scheme ();
  }
  update ();
}

void
Fractal::color_offset_changed ()
{
  // Send renderer event
  {
    using namespace qfre::renderer;
    auto * event = create_renderer_event< events::Color_Offset > ();
    event->offset = _color_settings.offset ();
  }
  update ();
}

void
Fractal::geometryChanged ( const QRectF & newGeometry_n,
                           const QRectF & oldGeometry_n )
{
  QQuickFramebufferObject::geometryChanged ( newGeometry_n, oldGeometry_n );
  resize_viewport_matrix (
      geo::Int32_Size ( newGeometry_n.width (), newGeometry_n.height () ) );
  // Reset complex area on demand
  if ( !is_manipulated () ) {
    reset_complex_area ();
  }
}

void
Fractal::mouseDoubleClickEvent ( QMouseEvent * event_n )
{
  QQuickFramebufferObject::mouseDoubleClickEvent ( event_n );
  if ( !event_n->isAccepted () ) {
    event_n->accept ();
  }
}

void
Fractal::mouseMoveEvent ( QMouseEvent * event_n )
{
  QQuickFramebufferObject::mouseMoveEvent ( event_n );
  if ( !event_n->isAccepted () && _mouse_down ) {
    event_n->accept ();
    // Pixel difference to last mouse position
    geo::Int32_Size const delta ( event_n->pos ().x () - _mouse_pos.x (),
                                  -( event_n->pos ().y () - _mouse_pos.y () ) );
    pan_in_viewport ( delta );
    _mouse_pos = event_n->pos ();
  }
}

void
Fractal::mousePressEvent ( QMouseEvent * event_n )
{
  QQuickFramebufferObject::mousePressEvent ( event_n );
  if ( !event_n->isAccepted () ) {
    event_n->accept ();
    _mouse_pos = event_n->pos ();
    _mouse_down = true;
  }
}

void
Fractal::mouseReleaseEvent ( QMouseEvent * event_n )
{
  QQuickFramebufferObject::mouseReleaseEvent ( event_n );
  event_n->accept ();
  _mouse_down = false;
}

void
Fractal::wheelEvent ( QWheelEvent * event_n )
{
  QQuickFramebufferObject::wheelEvent ( event_n );
  if ( !event_n->isAccepted () ) {
    event_n->accept ();

    double amount = 1.0;
    {
      QPoint numPixels = event_n->pixelDelta ();
      QPoint numDegrees = event_n->angleDelta () / 8;
      if ( !numPixels.isNull () ) {
        amount += ( double ( numPixels.y () ) / 1024 ) * 40.0;
      } else if ( !numDegrees.isNull () ) {
        if ( numDegrees.y () >= 0 ) {
          amount = 1.0 + ( double ( numDegrees.y () ) / 360.0 ) * 20;
        } else {
          amount = 1.0 / ( 1.0 - ( double ( numDegrees.y () ) / 360.0 ) * 20 );
        }
      }
    }

    geo::Double_Point zpos ( event_n->position ().x () + 0.5,
                             ( _viewport_matrix.viewport_size ().height () - 1 -
                               event_n->position ().y () ) +
                                 0.5 );
    zoom_at_viewport ( zpos, amount );
  }
}

bool
Fractal::event ( QEvent * event_n )
{
  switch ( int ( event_n->type () ) ) {
  case qevent_type_process:
    event_n->accept ();
    process_engine_events ();
    return true;
  case qevent_type_update:
    event_n->accept ();
    update ();
    return true;
  default:
    break;
  }
  return QQuickFramebufferObject::event ( event_n );
}

QQuickFramebufferObject::Renderer *
Fractal::createRenderer () const
{
  auto * res = new renderer::Renderer ( window (),
                                        _tile_sizes.image_tile_size,
                                        _tile_sizes.image_tile_overlap,
                                        _texture_size );
  return res;
}
} // namespace qfre
