/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "color_scheme.hpp"

namespace qfre
{

Color_Scheme::Color_Scheme ( QString const & id_string_n )
: _id_string ( id_string_n )
{
}

Color_Scheme::~Color_Scheme () {}
} // namespace qfre
