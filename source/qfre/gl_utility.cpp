/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "gl_utility.hpp"
#include <message_streams.hpp>
#include <sstream>

namespace qfre
{

void
gl_error_print ( char const * prefix_n, GLenum gl_error_n )
{
  if ( gl_error_n != GL_NO_ERROR ) {
    Error_Stream oss;
    oss << prefix_n;
    oss << "glGetError (";
    oss << std::hex << gl_error_n << ") ";
    switch ( gl_error_n ) {
    case GL_INVALID_ENUM:
      oss << "GL_INVALID_ENUM";
      break;
    case GL_INVALID_VALUE:
      oss << "GL_INVALID_VALUE";
      break;
    case GL_INVALID_OPERATION:
      oss << "GL_INVALID_OPERATION";
      break;
    case GL_OUT_OF_MEMORY:
      oss << "GL_OUT_OF_MEMORY";
      break;
    default:
      oss << "Unknown glGetError code " << gl_error_n;
      break;
    }
    oss << "\n";
  }
}
} // namespace qfre
