/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <QObject>
#include <cstdint>

namespace qfre
{

/// @brief Iterator settings
///
class Iterator_Settings : public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY ( Iterator_Settings )

  Q_PROPERTY ( QString iteratorId READ iterator_id WRITE set_iterator_id NOTIFY
                   iterator_id_changed )
  Q_PROPERTY ( unsigned int iterationsMax READ iterations_max WRITE
                   set_iterations_max NOTIFY iterations_max_changed )

  public:
  // -- Constructors

  Iterator_Settings ( QObject * parent_n = nullptr )
  : QObject ( parent_n )
  {
  }

  ~Iterator_Settings () = default;

  // -- Iterator id

  QString const &
  iterator_id () const
  {
    return _iterator_id;
  }

  void
  set_iterator_id ( QString const & id_n );

  Q_SIGNAL
  void
  iterator_id_changed ();

  // -- Maximum iterations count

  std::uint32_t
  iterations_max () const
  {
    return _iterations_max;
  }

  void
  set_iterations_max ( std::uint32_t num_n );

  Q_SIGNAL
  void
  iterations_max_changed ();

  private:
  QString _iterator_id;
  std::uint32_t _iterations_max = 0;
};
} // namespace qfre
