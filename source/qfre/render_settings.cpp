/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "render_settings.hpp"

namespace qfre
{

void
Render_Settings::set_draw_foreground ( bool flag_n )
{
  if ( _draw_foreground != flag_n ) {
    _draw_foreground = flag_n;
    emit draw_foreground_changed ();
  }
}

void
Render_Settings::set_draw_backgrounds ( bool flag_n )
{
  if ( _draw_backgrounds != flag_n ) {
    _draw_backgrounds = flag_n;
    emit draw_backgrounds_changed ();
  }
}

void
Render_Settings::set_upload ( bool flag_n )
{
  if ( _upload != flag_n ) {
    _upload = flag_n;
    emit upload_changed ();
  }
}

void
Render_Settings::set_wireframe ( bool flag_n )
{
  if ( _wireframe != flag_n ) {
    _wireframe = flag_n;
    emit wireframe_changed ();
  }
}

void
Render_Settings::set_backgrounds ( std::uint32_t num_n )
{
  if ( _backgrounds != num_n ) {
    _backgrounds = num_n;
    emit backgrounds_changed ();
  }
}

void
Render_Settings::set_supersampling ( std::uint32_t num_n )
{
  if ( _supersampling != num_n ) {
    switch ( _supersampling ) {
    case 1:
    case 2:
    case 4:
      _supersampling = num_n;
      emit supersampling_changed ();
      break;
    default:
      break;
    }
  }
}
} // namespace qfre
