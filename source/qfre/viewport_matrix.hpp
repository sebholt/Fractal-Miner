/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <fre/complex_origin.hpp>
#include <fre/matrix_base.hpp>
#include <debug_assert.hpp>

namespace qfre
{

/// @brief Matrix that only holds sizes, no tiles.
///
class Viewport_Matrix : public fre::Matrix_Base
{
  public:
  // -- Constructors

  Viewport_Matrix ();

  ~Viewport_Matrix ();

  // -- Tile size

  /// @brief Matrix tile size
  geo::UInt16_Size const &
  tile_size () const
  {
    return _tile_size;
  }

  /// @brief Matrix tile width
  std::uint16_t
  tile_width () const
  {
    return _tile_size.width ();
  }

  /// @brief Matrix tile height
  std::uint16_t
  tile_height () const
  {
    return _tile_size.height ();
  }

  void
  set_tile_size ( geo::UInt16_Size const & tile_size_n );

  // -- Matrix position

  /// @brief Position of the matrix relative to the viewport left-bottom corner
  ///
  /// The position is in unscaled pixel coordinates of the viewport.
  geo::Int32_Point const &
  position () const
  {
    return _position;
  }

  /// @brief Set the matrix position in the viewport
  void
  set_position ( geo::Int32_Point const & offset_n )
  {
    _position = offset_n;
  }

  // -- Viewport size

  /// @brief Viewport offset
  const geo::Int32_Size &
  viewport_size () const
  {
    return _viewport_size;
  }

  void
  set_viewport_size ( const geo::Int32_Size & size_n )
  {
    _viewport_size = size_n;
  }

  // -- Viewport scale

  /// @brief Viewport offset
  const geo::UInt8_Size &
  viewport_scale () const
  {
    return _viewport_scale;
  }

  void
  set_viewport_scale ( const geo::UInt8_Size & scale_n )
  {
    _viewport_scale = scale_n;
    DEBUG_ASSERT ( tile_width () % _viewport_scale.width () == 0 );
    DEBUG_ASSERT ( tile_height () % _viewport_scale.height () == 0 );
  }

  // -- Global

  std::int32_t
  global_left_column_px () const
  {
    return ( ( _column_id_ends.first () + 1 ) * _tile_size.width () );
  }

  std::int32_t
  global_bottom_row_px () const
  {
    return ( ( _row_id_ends.first () + 1 ) * _tile_size.height () );
  }

  void
  viewport_px_to_matrix_px ( geo::Double_Point & px_pos_n ) const
  {
    px_pos_n.x_ref () -= _position.x ();
    px_pos_n.y_ref () -= _position.y ();
    px_pos_n.x_ref () *= _viewport_scale.width ();
    px_pos_n.y_ref () *= _viewport_scale.height ();
  }

  geo::Double_Point
  matrix_px_from_viewport_px ( geo::Double_Point px_pos_n ) const
  {
    viewport_px_to_matrix_px ( px_pos_n );
    return px_pos_n;
  }

  // -- Paint matrix extents

  std::int32_t
  matrix_extent_left () const
  {
    return _position.x () * _viewport_scale.width ();
  }

  std::int32_t
  matrix_extent_right () const
  {
    return ( std::int32_t ( width_px () ) -
             ( _viewport_size.width () - _position.x () ) *
                 _viewport_scale.width () );
  }

  std::int32_t
  matrix_extent_bottom () const
  {
    return _position.y () * _viewport_scale.height ();
  }

  std::int32_t
  matrix_extent_top () const
  {
    return ( std::int32_t ( height_px () ) -
             ( _viewport_size.height () - _position.y () ) *
                 _viewport_scale.height () );
  }

  // -- Complex plane

  /// @brief Complex origin of the bottom-left pixel of the global tile (0,0)
  fre::Complex_Origin const &
  cp_origin () const
  {
    return _cp_origin;
  }

  // -- Matrix manipulation

  void
  resize ( fre::Resize_Deltas const & deltas_n );

  /// @return The number of matrix loops horizontally and vertically
  geo::Int32_Pair
  move_matrix_in_viewport ( geo::Int32_Size const & movement_n );

  // -- Complex plane in viewport

  void
  set_cp_at_viewport_px ( geo::Double_Point px_pos_n,
                          fre::Complex_Origin cp_origin_n )
  {
    viewport_px_to_matrix_px ( px_pos_n );
    px_pos_n.add ( global_left_column_px (), global_bottom_row_px () );
    _cp_origin.set ( cp_origin_n.at ( -px_pos_n.x (), -px_pos_n.y () ),
                     cp_origin_n.dx (),
                     cp_origin_n.dy () );
  }

  std::complex< double >
  cp_at_viewport_px ( geo::Double_Point px_pos_n ) const
  {
    viewport_px_to_matrix_px ( px_pos_n );
    px_pos_n.add ( global_left_column_px (), global_bottom_row_px () );
    return _cp_origin.at ( px_pos_n.x (), px_pos_n.y () );
  }

  private:
  // -- Matrix
  geo::UInt16_Size _tile_size;
  /// @brief Position of the paint matrix relative to the viewport
  geo::Int32_Point _position;
  // -- Viewport
  geo::Int32_Size _viewport_size;
  geo::UInt8_Size _viewport_scale = { 1, 1 };
  // -- Complex plane
  fre::Complex_Origin _cp_origin;
};
} // namespace qfre
