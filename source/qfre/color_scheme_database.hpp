/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <qfre/color_scheme.hpp>
#include <QString>
#include <cstdint>
#include <memory>
#include <vector>

namespace qfre
{

/// @brief Color palette generator database
///
class Color_Scheme_Database
{
  public:
  // -- Constructors

  Color_Scheme_Database ();

  ~Color_Scheme_Database ();

  Color_Scheme_Reference
  acquire_color_scheme ( QString const & scheme_id_n );

  private:
  std::vector< Color_Scheme_Reference > _schemes;
};
} // namespace qfre
