/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "pipeline.hpp"
#include <fre/pipeline.hpp>

namespace qfre
{

Pipeline::Pipeline ( QObject * parent_n )
: QObject ( parent_n )
{
  // -- Initialize the pipeline
  _fr_pipeline = std::make_shared< fre::Pipeline > ();
  _fr_pipeline->start ();
}

Pipeline::~Pipeline () {}
} // namespace qfre
