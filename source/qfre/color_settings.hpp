/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <qfre/color_scheme.hpp>
#include <qfre/color_scheme_database.hpp>
#include <QColor>
#include <QObject>
#include <QTimer>
#include <chrono>

namespace qfre
{

/// @brief Color settings
///
class Color_Settings : public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY ( Color_Settings )

  Q_PROPERTY ( QString schemeId READ scheme_id WRITE set_scheme_id NOTIFY
                   scheme_id_changed )
  Q_PROPERTY (
      unsigned int colorCount READ color_count NOTIFY color_count_changed )
  Q_PROPERTY ( int offset READ offset WRITE set_offset NOTIFY offset_changed )
  Q_PROPERTY ( QColor colorEnd READ color_end WRITE set_color_end NOTIFY
                   color_end_changed )
  Q_PROPERTY ( bool colorCycling READ color_cycling WRITE set_color_cycling
                   NOTIFY color_cycling_changed )
  Q_PROPERTY (
      bool colorCyclingForward READ color_cycling_forward WRITE
          set_color_cycling_forward NOTIFY color_cycling_forward_changed )
  Q_PROPERTY ( int colorCyclingCPS READ color_cycling_cps WRITE
                   set_color_cycling_cps NOTIFY color_cycling_cps_changed )

  public:
  // -- Constructors

  Color_Settings ( QObject * parent_n = nullptr );

  ~Color_Settings () = default;

  // -- Color scheme

  Color_Scheme_Reference const &
  scheme () const
  {
    return _scheme;
  }

  void
  set_scheme ( Color_Scheme_Reference const & scheme_n );

  // -- Color scheme id

  QString const &
  scheme_id () const
  {
    return _scheme_id;
  }

  void
  set_scheme_id ( QString const & id_n );

  Q_SIGNAL
  void
  scheme_id_changed ();

  // -- Color count

  std::uint32_t
  color_count () const
  {
    return _color_count;
  }

  Q_SIGNAL
  void
  color_count_changed ();

  // -- Color palette offset

  std::uint32_t
  offset () const
  {
    return _offset;
  }

  void
  set_offset ( std::uint32_t value_n );

  Q_SIGNAL
  void
  offset_changed ();

  // -- End color

  QColor
  color_end () const
  {
    return _color_end;
  }

  void
  set_color_end ( QColor col_n );

  Q_SIGNAL
  void
  color_end_changed ();

  // -- Color cycling

  bool
  color_cycling () const
  {
    return _color_cycling;
  }

  void
  set_color_cycling ( bool flag_n );

  Q_SIGNAL
  void
  color_cycling_changed ();

  // -- Color cycling forward

  bool
  color_cycling_forward () const
  {
    return _color_cycling_forward;
  }

  void
  set_color_cycling_forward ( bool flag_n );

  Q_SIGNAL
  void
  color_cycling_forward_changed ();

  // -- Color cycling color per second

  std::uint32_t
  color_cycling_cps () const
  {
    return _color_cycling_cps;
  }

  void
  set_color_cycling_cps ( std::uint32_t color_cycling_cps_n );

  Q_SIGNAL
  void
  color_cycling_cps_changed ();

  private:
  bool
  update_color_count ();

  /// @return true if the offset changed
  bool
  set_offset_private ( std::uint32_t value_n );

  void
  setup_color_cycling_timer ();

  Q_SLOT
  void
  color_cycling_tick ();

  private:
  Color_Scheme_Database _color_scheme_database;
  Color_Scheme_Reference _scheme;
  QString _scheme_id;
  QColor _color_end = QColor ( 0, 0, 0, 255 );
  std::uint32_t _offset = 0;
  std::uint32_t _color_count = 0;
  bool _color_cycling = false;
  bool _color_cycling_forward = true;
  std::uint32_t _color_cycling_cps = 15;
  double _color_cycling_offset = 0.0;
  QTimer _color_cycling_timer;
  std::chrono::steady_clock::time_point _color_cycling_time_point;
};
} // namespace qfre
