/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/chain.hpp>

namespace qfre
{

/// @brief Event chain
///
class Event_Chain : protected event::Chain
{
  public:
  // -- Constructors

  Event_Chain () = default;

  ~Event_Chain () { clear (); }

  /// @brief Deletes all events in the chain
  void
  clear ();

  template < class T, typename... Args >
  T *
  create_back ( Args &&... args_n )
  {
    T * res = new T ( std::forward< Args > ( args_n )... );
    push_back ( res );
    return res;
  }

  using event::Chain::front;
  using event::Chain::is_empty;

  /// @brief Deletes the first event in the chain
  void
  pop_front ()
  {
    delete pop_front_not_empty ();
  }

  void
  transfer_to ( Event_Chain & chain_n )
  {
    move_append_back ( *this, chain_n );
  }
};
} // namespace qfre
