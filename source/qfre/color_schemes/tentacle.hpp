/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <qfre/color_scheme.hpp>

namespace qfre::color_schemes
{

/// @brief Color palette generatior abstract base class
///
class Tentacle : public Color_Scheme
{
  public:
  // -- Constructors

  Tentacle ();

  // -- Abstract interface

  void
  acquire_colors ( std::uint8_t * rgba_n ) override;
};
} // namespace qfre::color_schemes
