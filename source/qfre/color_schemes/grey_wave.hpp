/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <qfre/color_scheme.hpp>

namespace qfre::color_schemes
{

/// @brief Color palette generatior abstract base class
///
class Grey_Wave : public Color_Scheme
{
  public:
  // -- Constructors

  Grey_Wave ();

  // -- Abstract interface

  void
  acquire_colors ( std::uint8_t * rgba_n ) override;

  private:
  std::uint32_t _intervall_length = 60;
};
} // namespace qfre::color_schemes
