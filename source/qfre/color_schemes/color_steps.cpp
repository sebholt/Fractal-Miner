/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "color_steps.hpp"
#include <qfre/colors.hpp>

namespace qfre::color_schemes
{

Color_Steps::Color_Steps ()
: Color_Scheme ( "color_steps" )
{
  set_num_colors (
      Colors::least_common_multiple ( 25, 360 * ( _intervall_length + 1 ) ) );
}

void
Color_Steps::acquire_colors ( std::uint8_t * rgba_n )
{
  const std::uint32_t iv_length ( _intervall_length );
  const std::uint32_t iv_length_plus ( iv_length + 1 );
  const float iv_length_f ( iv_length );
  const float sat = 1.0f;

  float px_f[ 4 ];
  for ( std::uint32_t ii = 0; ii != num_colors (); ++ii ) {
    const int hue = ( ( ii / iv_length_plus ) * 25 ) % 360;
    const int iter_val = iv_length - ( ii % iv_length_plus );
    const float val = float ( iter_val ) / iv_length_f;
    Colors::hsv_to_rgb ( hue, sat, val, px_f );
    px_f[ 3 ] = 1.0f;
    Colors::rgba_float_to_rgba_bytes ( rgba_n, px_f );
    rgba_n += 4;
  }
}
} // namespace qfre::color_schemes
