/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <qfre/color_scheme.hpp>

namespace qfre::color_schemes
{

/// @brief Color palette generatior abstract base class
///
class Color_Steps : public Color_Scheme
{
  public:
  // -- Constructors

  Color_Steps ();

  // -- Abstract interface

  void
  acquire_colors ( std::uint8_t * rgba_n ) override;

  private:
  std::uint32_t _intervall_length = 8;
};
} // namespace qfre::color_schemes
