/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "tentacle.hpp"
#include <qfre/colors.hpp>

namespace qfre::color_schemes
{

Tentacle::Tentacle ()
: Color_Scheme ( "tentacle" )
{
  set_num_colors ( Colors::least_common_multiple ( 3, 360 * 2 ) );
}

void
Tentacle::acquire_colors ( std::uint8_t * rgba_n )
{
  float px_f[ 4 ];
  for ( std::uint32_t ii = 0; ii != num_colors (); ++ii ) {
    const int hue = ( ( ( ii * 3 ) / 2 ) % 360 );
    const float sat = 1.0f;
    const float tri = Colors::triangle ( ii, 4 );
    const float val = 1.0f - 0.5f * tri;
    Colors::hsv_to_rgb ( hue, sat, val, px_f );
    px_f[ 3 ] = 1.0f;
    Colors::rgba_float_to_rgba_bytes ( rgba_n, px_f );
    rgba_n += 4;
  }
}
} // namespace qfre::color_schemes
