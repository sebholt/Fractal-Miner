/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "color_wave.hpp"
#include <qfre/colors.hpp>

namespace qfre::color_schemes
{

Color_Wave::Color_Wave ()
: Color_Scheme ( "color_wave" )
{
  set_num_colors (
      Colors::least_common_multiple ( 32, 360 * ( _intervall_length + 1 ) ) );
}

void
Color_Wave::acquire_colors ( std::uint8_t * rgba_n )
{
  const std::uint32_t iv_length ( _intervall_length );
  const std::uint32_t iv_length_plus ( iv_length + 1 );
  const float iv_length_f ( iv_length );

  float px_f[ 4 ];
  for ( std::uint32_t ii = 0; ii != num_colors (); ++ii ) {
    const int col_select = ii / iv_length_plus;
    const int hue = ( col_select * 32 ) % 360;
    int iter_val = ii % iv_length_plus;
    if ( ( col_select % 2 ) == 0 ) {
      iter_val = iv_length - iter_val;
    }
    const float val = float ( iter_val ) / iv_length_f;
    float sat = 1.0f;
    if ( val > 0.5f ) {
      sat -= ( val - 0.5f ) * 2.0f;
    }

    Colors::hsv_to_rgb ( hue, sat, val, px_f );
    px_f[ 3 ] = 1.0f;

    Colors::rgba_float_to_rgba_bytes ( rgba_n, px_f );
    rgba_n += 4;
  }
}
} // namespace qfre::color_schemes
