/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "grey_steps.hpp"
#include <qfre/colors.hpp>

namespace qfre::color_schemes
{

Grey_Steps::Grey_Steps ()
: Color_Scheme ( "grey_steps" )
{
  set_num_colors ( _intervall_length + 1 );
}

void
Grey_Steps::acquire_colors ( std::uint8_t * rgba_n )
{
  const std::uint32_t iv_length ( _intervall_length );
  const std::uint32_t iv_length_plus ( iv_length + 1 );
  const float iv_length_f ( iv_length );

  float px_f[ 4 ];
  for ( std::uint32_t ii = 0; ii != num_colors (); ++ii ) {
    const float val ( 1.0f - float ( ii % iv_length_plus ) / iv_length_f );
    px_f[ 0 ] = val;
    px_f[ 1 ] = val;
    px_f[ 2 ] = val;
    px_f[ 3 ] = 1.0f;
    Colors::rgba_float_to_rgba_bytes ( rgba_n, px_f );
    rgba_n += 4;
  }
}
} // namespace qfre::color_schemes
