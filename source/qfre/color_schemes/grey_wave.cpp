/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "grey_wave.hpp"
#include <qfre/colors.hpp>

namespace qfre::color_schemes
{

Grey_Wave::Grey_Wave ()
: Color_Scheme ( "grey_wave" )
{
  set_num_colors ( _intervall_length );
}

void
Grey_Wave::acquire_colors ( std::uint8_t * rgba_n )
{
  const std::uint32_t iv_length ( _intervall_length );

  float px_f[ 4 ];
  for ( std::uint32_t ii = 0; ii != num_colors (); ++ii ) {
    const float val ( 1.0f - Colors::triangle ( ii, iv_length ) );
    px_f[ 0 ] = val;
    px_f[ 1 ] = val;
    px_f[ 2 ] = val;
    px_f[ 3 ] = 1.0f;
    for ( std::uint32_t cc = 0; cc != 4; ++cc ) {
      rgba_n[ cc ] = ( px_f[ cc ] * 255.0f );
    }
    rgba_n += 4;
  }
}
} // namespace qfre::color_schemes
