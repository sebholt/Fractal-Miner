/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <QEvent>
#include <QPointer>

namespace qfre
{

class Qt_Event_Sender
{
  public:
  Qt_Event_Sender ( QObject * receiver_n, int event_type_n )
  : _receiver ( receiver_n )
  , _event_type ( event_type_n )
  {
  }

  void
  operator() () const;

  private:
  QPointer< QObject > _receiver;
  int _event_type;
};
} // namespace qfre
