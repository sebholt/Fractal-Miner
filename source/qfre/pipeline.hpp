/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <QObject>
#include <memory>

// -- Forward declaration
namespace fre
{
class Pipeline;
}

namespace qfre
{

/// @brief Qt fracal QML Item
///
class Pipeline : public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY ( Pipeline )

  public:
  // -- Constructors

  Pipeline ( QObject * parent_n = Q_NULLPTR );

  ~Pipeline ();

  // -- Accessors

  std::shared_ptr< fre::Pipeline >
  pipeline ()
  {
    return _fr_pipeline;
  }

  private:
  // -- Shared resources
  std::shared_ptr< fre::Pipeline > _fr_pipeline;
};
} // namespace qfre
