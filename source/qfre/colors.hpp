/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cmath>
#include <cstdint>

namespace qfre
{

class Colors
{
  public:
  /// @brief A triangle function from 0.0 to 1.0 and back to 0.0
  /// Each interval is length units long
  /// If the length is even 1.0 can be reached reached at ii % length = length /
  /// 2.
  /// If the length is odd the peak will be slightly below 1.0
  static float
  triangle ( int ii, int length )
  {
    const int peak ( 16384 );
    const int l2 ( length / 2 );
    int res;

    ii = ii % length;

    if ( ii <= l2 ) {
      res = ( peak * ii ) * 2 / length;
    } else {
      res = peak - ( peak * ( ii - l2 ) ) * 2 / length;
    }
    return float ( res ) / float ( peak );
  }

  static std::uint32_t
  least_common_multiple ( const std::uint32_t val1, const std::uint32_t val2 );

  /// @brief Converts HSV values to sRGB
  inline static void
  hsv_to_rgb ( const int hue, const float sat, const float val, float * rgb )
  {
    const int h_i = hue / 60;
    const float vf = float ( hue ) / 60.0f - float ( h_i );

    const float vp = val * ( 1.0f - sat );
    const float vq = val * ( 1.0f - sat * vf );
    const float vt = val * ( 1.0f - sat * ( 1.0f - vf ) );

    switch ( h_i ) {
    case 0:
      rgb[ 0 ] = val;
      rgb[ 1 ] = vt;
      rgb[ 2 ] = vp;
      break;
    case 1:
      rgb[ 0 ] = vq;
      rgb[ 1 ] = val;
      rgb[ 2 ] = vp;
      break;
    case 2:
      rgb[ 0 ] = vp;
      rgb[ 1 ] = val;
      rgb[ 2 ] = vt;
      break;
    case 3:
      rgb[ 0 ] = vp;
      rgb[ 1 ] = vq;
      rgb[ 2 ] = val;
      break;
    case 4:
      rgb[ 0 ] = vt;
      rgb[ 1 ] = vp;
      rgb[ 2 ] = val;
      break;
    case 5:
      rgb[ 0 ] = val;
      rgb[ 1 ] = vp;
      rgb[ 2 ] = vq;
      break;
    }
  }

  /// @brief Converts an sRGB value to linear RGB
  inline static void
  sRGB_to_lRGB ( float * sRGB )
  {
    for ( std::uint32_t ii = 0; ii < 3; ++ii ) {
      float cc ( sRGB[ ii ] );
      if ( cc <= 0.04045f ) {
        cc /= 12.92f;
      } else {
        cc += 0.055f;
        cc /= 1.055f;
        cc = std::pow ( cc, 2.4f );
      }
      sRGB[ ii ] = cc;
    }
  }

  /// @brief Converts an linear RGB set to sRGB
  inline static void
  lRGB_to_sRGB ( float * rgb )
  {
    for ( std::uint32_t ii = 0; ii < 3; ++ii ) {
      float cc ( rgb[ ii ] );
      if ( cc <= 0.0031308f ) {
        cc *= 12.92f;
      } else {
        cc = std::pow ( cc, 1.0f / 2.4f );
        cc *= 1.055f;
        cc -= 0.055f;
      }
      rgb[ ii ] = cc;
    }
  }

  inline static void
  rgba_float_to_rgba_bytes ( std::uint8_t * rgb_n, float * rgbf_n )
  {
    for ( std::uint32_t cc = 0; cc != 4; ++cc ) {
      rgb_n[ cc ] = ( rgbf_n[ cc ] * 255.0f );
    }
  }
};
} // namespace qfre
