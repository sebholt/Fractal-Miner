/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <geo/point.hpp>
#include <geo/size.hpp>
#include <algorithm>
#include <cstdint>
#include <ostream>

namespace geo
{

/// @brief A Region in 2D space
///
template < typename Val >
class Region
{
  public:
  // -- Constructors

  /// @brief Default constructor
  constexpr Region () {}

  /// Explicit Initilization constructor
  /// @param pos_n The region position
  /// @param size_n The region size
  constexpr Region ( Point< Val > const & pos_n, Size< Val > const & size_n )
  : _pos ( pos_n )
  , _size ( size_n )
  {
  }

  /// Explicit Initilization constructor
  /// @param x_n Position x value
  /// @param y_n Position y value
  /// @param width_n Size width
  /// @param height_n Size height
  constexpr Region ( Val const & x_n,
                     Val const & y_n,
                     Val const & width_n,
                     Val const & height_n )
  : _pos ( x_n, y_n )
  , _size ( width_n, height_n )
  {
  }

  // -- Position

  /// @brief Returns the position vector
  /// @return The position vector
  constexpr Point< Val > &
  pos_ref ()
  {
    return _pos;
  }

  /// @brief Returns the position vector - const
  /// @return The position vector
  constexpr Point< Val > const &
  pos () const
  {
    return _pos;
  }

  /// @brief Position setter
  /// @param pos_n The new position
  void
  set_pos ( Point< Val > const & pos_n )
  {
    _pos = pos_n;
  }

  /// @brief Position x setter
  /// @param x_n The new x position
  void
  set_x ( Val const & x_n )
  {
    pos_ref ().set_x ( x_n );
  }

  /// @brief Position y setter
  /// @param y_n The new y position
  void
  set_y ( Val const & y_n )
  {
    pos_ref ().set_y ( y_n );
  }

  // -- Size

  /// @brief Returns the size vector
  /// @return The size vector
  constexpr Size< Val > &
  size_ref ()
  {
    return _size;
  }

  /// @brief Returns the size vector - const
  /// @return The size vector
  constexpr Size< Val > const &
  size () const
  {
    return _size;
  }

  /// @brief Size setter
  /// @param size_n The new size
  void
  set_size ( Size< Val > const & size_n )
  {
    _size = size_n;
  }

  /// @brief Height setter
  /// @param width_n The new width
  void
  set_width ( Val const & width_n )
  {
    size_ref ().set_width ( width_n );
  }

  /// @brief Width setter
  /// @param height_n The new height
  void
  set_height ( Val const & height_n )
  {
    size_ref ().set_height ( height_n );
  }

  // -- Readers

  constexpr Val const &
  x () const
  {
    return _pos.x ();
  }

  constexpr Val const &
  y () const
  {
    return _pos.y ();
  }

  constexpr Val const &
  width () const
  {
    return _size.width ();
  }

  constexpr Val const &
  height () const
  {
    return _size.height ();
  }

  constexpr Val
  area () const
  {
    return _size.area ();
  }

  // -- Mixed readers

  constexpr Val
  left () const
  {
    return _pos.x ();
  }

  constexpr Val
  right () const
  {
    return ( _pos.x () + _size.width () - 1 );
  }

  constexpr Val
  bottom () const
  {
    return _pos.y ();
  }

  constexpr Val
  top () const
  {
    return ( _pos.y () + _size.height () - 1 );
  }

  /// @brief Returns the horizontal center of the region
  /// @return The horizontal center
  constexpr Val
  center_x () const
  {
    return _pos.x () + _size.width () / 2;
  }

  /// @brief Returns the vertical center of the region
  /// @return The vertical center
  constexpr Val
  center_y () const
  {
    return _pos.y () + _size.height () / 2;
  }

  /// @brief Returns the center of the region
  /// @return The center
  constexpr Point< Val >
  center () const
  {
    return Point< Val > ( center_x (), center_y () );
  }

  // -- Setting

  void
  reset ()
  {
    _pos.reset ();
    _size.reset ();
  }

  void
  set ( Point< Val > const & pos_n, Size< Val > const & size_n )
  {
    _pos = pos_n;
    _size = size_n;
  }

  void
  set ( Val const & x_n,
        Val const & y_n,
        Val const & width_n,
        Val const & height_n )
  {
    _pos.set ( x_n, y_n );
    _size.set ( width_n, height_n );
  }

  // -- Adjusting

  void
  move ( Point< Val > const & delta )
  {
    _pos.move ( delta );
  }

  void
  move ( Val const & delta_x, Val const & delta_y )
  {
    _pos.add ( delta_x, delta_y );
  }

  void
  move_x ( const Val delta )
  {
    _pos.add_x ( delta );
  }

  void
  move_y ( const Val delta )
  {
    _pos.add_y ( delta );
  }

  // -- Operators

  /// Returns true if both values are equal
  /// @return True on equality
  constexpr bool
  operator== ( const Region< Val > & region ) const
  {
    return ( ( region._pos == _pos ) && ( region._size == _size ) );
  }

  /// Returns true if any value is not equal
  /// @return True on unequality
  constexpr bool
  operator!= ( const Region< Val > & region ) const
  {
    return ( ( region._pos != _pos ) || ( region._size != _size ) );
  }

  // -- Utility

  Region< Val >
  intersected ( Region< Val > const & reg_n ) const;

  private:
  Point< Val > _pos;
  Size< Val > _size;
};

// -- Utility
template < typename Val >
Region< Val >
Region< Val >::intersected ( Region< Val > const & reg_n ) const
{
  Val this_right = right ();
  Val this_top = top ();
  Val reg_right = reg_n.right ();
  Val reg_top = reg_n.top ();
  if ( ( this_right < reg_n.left () ) || ( reg_right < left () ) ||
       ( this_top < reg_n.bottom () ) || ( reg_top < bottom () ) ) {
    return Region< Val > ();
  }

  Val r_left = std::max ( left (), reg_n.left () );
  Val r_right = std::min ( this_right, reg_right );
  Val r_bottom = std::max ( bottom (), reg_n.bottom () );
  Val r_top = std::min ( this_top, reg_top );

  return Region< Val > (
      r_left, r_bottom, ( r_right - r_left ) + 1, ( r_top - r_bottom ) + 1 );
}

// -- std::ostream adaptors
template < typename Val >
inline std::ostream &
operator<< ( std::ostream & os_n, const Region< Val > & region )
{
  return os_n << region.pos () << region.size ();
}

// -- Type definitions
typedef Region< std::int8_t > Int8_Region;
typedef Region< std::uint8_t > UInt8_Region;
typedef Region< std::int16_t > Int16_Region;
typedef Region< std::uint16_t > UInt16_Region;
typedef Region< std::int32_t > Int32_Region;
typedef Region< std::uint32_t > Unt32_Region;
} // namespace geo
