/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include "pair.hpp"
#include "point.hpp"
#include "region.hpp"
#include "size.hpp"
