/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <array>
#include <cstdint>
#include <ostream>

namespace geo
{

/// @brief A 2D size
///
template < typename Val >
class Size
{
  public:
  // -- Constructors

  /// @brief Default constructor
  constexpr Size ()
  : _ll{ { 0, 0 } }
  {
  }

  /// @brief Explicit Initilization constructor
  /// @param width_n The width
  /// @param width_n The height
  constexpr Size ( Val const & width_n, Val const & height_n )
  : _ll{ { width_n, height_n } }
  {
  }

  // -- Clear

  /// @brief Reset to default construction state (all zero)
  void
  reset ()
  {
    _ll[ 0 ] = 0;
    _ll[ 1 ] = 0;
  }

  // -- Accessors

  /// @brief Returns the width
  /// @return The width
  constexpr Val &
  width_ref ()
  {
    return _ll[ 0 ];
  }

  /// @brief Returns the height
  /// @return The height
  constexpr Val &
  height_ref ()
  {
    return _ll[ 1 ];
  }

  /// @brief Returns the width
  /// @return The width
  constexpr Val const &
  width () const
  {
    return _ll[ 0 ];
  }

  /// @brief Returns the height
  /// @return The height
  constexpr Val const &
  height () const
  {
    return _ll[ 1 ];
  }

  /// @brief Returns the area of the rectangle
  /// @return width()*height()
  template < typename O >
  constexpr O
  area () const
  {
    return ( O ( width () ) * O ( height () ) );
  }

  /// @brief Sets the width
  /// @param width The width
  void
  set_width ( Val const & width_n )
  {
    _ll[ 0 ] = width_n;
  }

  /// @brief Sets the height
  /// @param height The height
  void
  set_height ( Val const & height_n )
  {
    _ll[ 1 ] = height_n;
  }

  /// @brief Sets width and height
  /// @param width The width
  /// @param height The height
  void
  set ( Val const & width_n, Val const & height_n )
  {
    _ll[ 0 ] = width_n;
    _ll[ 1 ] = height_n;
  }

  /// @brief Sets width and height equal
  /// @param length The value for width and height
  void
  fill ( Val const & length_n )
  {
    _ll[ 0 ] = length_n;
    _ll[ 1 ] = length_n;
  }

  // -- Adding

  void
  add ( Val const & width_n, Val const & height_n )
  {
    _ll[ 0 ] += width_n;
    _ll[ 1 ] += height_n;
  }

  void
  add_width ( Val const & width_add )
  {
    _ll[ 0 ] += width_add;
  }

  void
  add_height ( Val const & height_add )
  {
    _ll[ 1 ] += height_add;
  }

  void
  sub ( Val const & width_n, Val const & height_n )
  {
    _ll[ 0 ] -= width_n;
    _ll[ 1 ] -= height_n;
  }

  void
  sub_width ( Val const & width_add )
  {
    _ll[ 0 ] -= width_add;
  }

  void
  sub_height ( Val const & height_add )
  {
    _ll[ 1 ] -= height_add;
  }

  // -- Subscript operators

  Val &
  operator[] ( std::size_t index_n )
  {
    return _ll[ index_n ];
  }

  Val const &
  operator[] ( std::size_t index_n ) const
  {
    return _ll[ index_n ];
  }

  // -- Comparison operators

  /// Returns true if both values are equal
  constexpr bool
  operator== ( const Size< Val > & size_n ) const
  {
    return ( ( size_n.width () == width () ) &&
             ( size_n.height () == height () ) );
  }

  /// Returns true if any value is not equal
  constexpr bool
  operator!= ( const Size< Val > & size_n ) const
  {
    return ( ( size_n.width () != width () ) ||
             ( size_n.height () != height () ) );
  }

  /// Returns true if both values are equal to length_n
  constexpr bool
  operator== ( Val const & length_n ) const
  {
    return ( ( length_n == width () ) && ( length_n == height () ) );
  }

  /// Returns true if any value is not equal to length_n
  constexpr bool
  operator!= ( Val const & length_n ) const
  {
    return ( ( length_n != width () ) || ( length_n != height () ) );
  }

  private:
  std::array< Val, 2 > _ll;
};

// -- std::ostream adaptors
inline std::ostream &
operator<< ( std::ostream & os_n, const Size< std::int8_t > & size )
{
  return os_n << '(' << std::int32_t ( size.width () ) << ','
              << std::int32_t ( size.height () ) << ')';
}

inline std::ostream &
operator<< ( std::ostream & os_n, const Size< std::uint8_t > & size )
{
  return os_n << '(' << std::uint32_t ( size.width () ) << ','
              << std::uint32_t ( size.height () ) << ')';
}

template < typename Val >
inline std::ostream &
operator<< ( std::ostream & os_n, const Size< Val > & size )
{
  return os_n << '(' << size.width () << ',' << size.height () << ')';
}

// -- Type definitions
typedef Size< std::int8_t > Int8_Size;
typedef Size< std::uint8_t > UInt8_Size;
typedef Size< std::int16_t > Int16_Size;
typedef Size< std::uint16_t > UInt16_Size;
typedef Size< std::int32_t > Int32_Size;
typedef Size< std::uint32_t > UInt32_Size;
typedef Size< float > Float_Size;
typedef Size< double > Double_Size;
} // namespace geo
