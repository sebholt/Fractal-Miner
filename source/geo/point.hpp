/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <array>
#include <cstdint>
#include <ostream>

namespace geo
{

/// @brief A Point in space
///
template < typename Val >
class Point
{
  public:
  // -- Constructors

  /// @brief Default constructor
  constexpr Point ()
  : _pp{ { 0, 0 } }
  {
  }

  /// @brief Explicit Initilization constructor
  /// @param x_n The new x value
  /// @param y_n The new y value
  constexpr Point ( Val const & x_n, Val const & y_n )
  : _pp{ { x_n, y_n } }
  {
  }

  // -- Clear

  /// @brief Reset to default construction state (all zero)
  void
  reset ()
  {
    _pp[ 0 ] = 0;
    _pp[ 1 ] = 0;
  }

  // -- Accessors

  /// @brief Returns the x value
  /// @return The x value
  constexpr Val &
  x_ref ()
  {
    return _pp[ 0 ];
  }

  /// @brief Returns the y value
  /// @return The y value
  constexpr Val &
  y_ref ()
  {
    return _pp[ 1 ];
  }

  /// @brief Returns the x value
  /// @return The x value
  constexpr Val const &
  x () const
  {
    return _pp[ 0 ];
  }

  /// @brief Returns the y value
  /// @return The y value
  constexpr Val const &
  y () const
  {
    return _pp[ 1 ];
  }

  /// @brief Sets the x value
  /// @param x_n The new x value
  void
  set_x ( Val const & x_n )
  {
    _pp[ 0 ] = x_n;
  }

  /// @brief Sets the y value
  /// @param y_n The new y value
  void
  set_y ( Val const & y_n )
  {
    _pp[ 1 ] = y_n;
  }

  /// @brief Sets the point x and y value
  /// @param x_n The new x value
  /// @param y_n The new y value
  void
  set ( Val const & x_n, Val const & y_n )
  {
    _pp[ 0 ] = x_n;
    _pp[ 1 ] = y_n;
  }

  /// @brief Sets the point x and y value
  /// @param val_n The new value or x and y
  void
  fill ( Val const & val_n )
  {
    _pp[ 0 ] = val_n;
    _pp[ 1 ] = val_n;
  }

  // -- Algebra

  /// @brief Adds the point values to the x/y values
  /// @param move_x_n The x value to add
  /// @param move_y_n The y value to add
  void
  add ( Val const & move_x_n, Val const & move_y_n )
  {
    _pp[ 0 ] += move_x_n;
    _pp[ 1 ] += move_y_n;
  }

  /// @brief Adds the move_x_n values to the x valuey
  /// @param move_x_n The x value to add
  void
  add_x ( Val const & move_x_n )
  {
    _pp[ 0 ] += move_x_n;
  }

  /// @brief Adds the move_y_n values to the x value
  /// @param move_y_n The y value to add
  void
  add_y ( Val const & move_y_n )
  {
    _pp[ 1 ] += move_y_n;
  }

  void
  sub ( Val const & move_x_n, Val const & move_y_n )
  {
    _pp[ 0 ] -= move_x_n;
    _pp[ 1 ] -= move_y_n;
  }

  /// @brief Adds the move_x_n values to the x valuey
  /// @param move_x_n The x value to add
  void
  sub_x ( Val const & move_x_n )
  {
    _pp[ 0 ] -= move_x_n;
  }

  /// @brief Adds the move_y_n values to the x value
  /// @param move_y_n The y value to add
  void
  sub_y ( Val const & move_y_n )
  {
    _pp[ 1 ] -= move_y_n;
  }

  // -- Subscript operators

  Val &
  operator[] ( std::size_t index_n )
  {
    return _pp[ index_n ];
  }

  Val const &
  operator[] ( std::size_t index_n ) const
  {
    return _pp[ index_n ];
  }

  // -- Comparison operators

  /// @brief Returns true if both values are equal
  constexpr bool
  operator== ( Point< Val > const & point_n ) const
  {
    return ( ( point_n.x () == x () ) && ( point_n.y () == y () ) );
  }

  /// @brief Returns true if any value is not equal
  constexpr bool
  operator!= ( Point< Val > const & point_n ) const
  {
    return ( ( point_n.x () != x () ) || ( point_n.y () != y () ) );
  }

  /// @brief Returns true if both values are equal to pos_n
  constexpr bool
  operator== ( Val const & pos_n ) const
  {
    return ( ( pos_n == x () ) && ( pos_n == y () ) );
  }

  /// @brief Returns true if any value is not equal to pos_n
  constexpr bool
  operator!= ( Val const & pos_n ) const
  {
    return ( ( pos_n != x () ) || ( pos_n != y () ) );
  }

  private:
  std::array< Val, 2 > _pp;
};

// -- ostream adaptors
inline std::ostream &
operator<< ( std::ostream & os_n, Point< std::int8_t > const & point_n )
{
  return os_n << '(' << std::int32_t ( point_n.x () ) << ','
              << std::int32_t ( point_n.y () ) << ')';
}

inline std::ostream &
operator<< ( std::ostream & os_n, Point< std::uint8_t > const & point_n )
{
  return os_n << '(' << std::uint32_t ( point_n.x () ) << ','
              << std::uint32_t ( point_n.y () ) << ')';
}

template < typename Val >
inline std::ostream &
operator<< ( std::ostream & os_n, Point< Val > const & point_n )
{
  return os_n << '(' << point_n.x () << ',' << point_n.y () << ')';
}

// -- Type definitions
typedef Point< std::int8_t > Int8_Point;
typedef Point< std::uint8_t > UInt8_Point;
typedef Point< std::int16_t > Int16_Point;
typedef Point< std::uint16_t > UInt16_Point;
typedef Point< std::int32_t > Int32_Point;
typedef Point< std::uint32_t > UInt32_Point;
typedef Point< float > Float_Point;
typedef Point< double > Double_Point;
} // namespace geo
