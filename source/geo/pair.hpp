/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <array>
#include <cstdint>
#include <ostream>

namespace geo
{

/// @brief A Pair in space
///
template < typename Val >
class Pair : public std::array< Val, 2 >
{
  public:
  // -- Constructors

  constexpr Pair ()
  : std::array< Val, 2 >{ { Val ( 0 ), Val ( 0 ) } }
  {
  }

  constexpr Pair ( Val const & first_n, Val const & second_n )
  : std::array< Val, 2 >{ { first_n, second_n } }
  {
  }

  // -- Clear

  /// @brief Reset to default construction state (all zero)
  void
  reset ()
  {
    std::array< Val, 2 >::fill ( Val ( 0 ) );
  }

  // -- Accessors

  constexpr Val const &
  first () const
  {
    return std::array< Val, 2 >::operator[] ( 0 );
  }

  constexpr Val &
  first_ref ()
  {
    return std::array< Val, 2 >::operator[] ( 0 );
  }

  void
  set_first ( Val const & first_n )
  {
    first_ref () = first_n;
  }

  constexpr Val const &
  second () const
  {
    return std::array< Val, 2 >::operator[] ( 1 );
  }

  constexpr Val &
  second_ref ()
  {
    return std::array< Val, 2 >::operator[] ( 1 );
  }

  void
  set_second ( Val const & second_n )
  {
    second_ref () = second_n;
  }

  void
  set ( Val const & first_n, Val const & second_n )
  {
    set_first ( first_n );
    set_second ( second_n );
  }

  // -- Comparison operators

  /// @brief Returns true if both values are equal
  constexpr bool
  operator== ( Pair< Val > const & pair_n ) const
  {
    return ( ( pair_n.first () == first () ) &&
             ( pair_n.second () == second () ) );
  }

  /// @brief Returns true if any value is not equal
  constexpr bool
  operator!= ( Pair< Val > const & pair_n ) const
  {
    return ( ( pair_n.first () != first () ) ||
             ( pair_n.second () != second () ) );
  }
};

// -- ostream adaptors
inline std::ostream &
operator<< ( std::ostream & os_n, Pair< std::int8_t > const & pair_n )
{
  return os_n << '(' << std::int32_t ( pair_n.first () ) << ','
              << std::int32_t ( pair_n.second () ) << ')';
}

inline std::ostream &
operator<< ( std::ostream & os_n, Pair< std::uint8_t > const & pair_n )
{
  return os_n << '(' << std::uint32_t ( pair_n.first () ) << ','
              << std::uint32_t ( pair_n.second () ) << ')';
}

template < typename Val >
inline std::ostream &
operator<< ( std::ostream & os_n, Pair< Val > const & pair_n )
{
  return os_n << '(' << pair_n.first () << ',' << pair_n.second () << ')';
}

// -- Type definitions
typedef Pair< std::int8_t > Int8_Pair;
typedef Pair< std::uint8_t > UInt8_Pair;
typedef Pair< std::int16_t > Int16_Pair;
typedef Pair< std::uint16_t > UInt16_Pair;
typedef Pair< std::int32_t > Int32_Pair;
typedef Pair< std::uint32_t > UInt32_Pair;
typedef Pair< float > Float_Pair;
typedef Pair< double > Double_Pair;
} // namespace geo
