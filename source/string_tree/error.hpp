/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

namespace string_tree
{

/// @brief Path processing error
///
class Error
{
  // Public types
  public:
  /// @brief Rrror type
  enum class Type
  {
    NONE,
    ADDRESS_INVALID,
    NODE_NOT_EXIST,
    NODE_NOT_LEAF,
    NODE_NOT_BRANCH
  };

  // Public attributes
  public:
  // -- Constructors

  Error ()
  : _type ( Type::NONE )
  {
  }

  // -- General

  void
  clear ()
  {
    _type = Type::NONE;
  }

  // -- Type

  Type
  type () const
  {
    return _type;
  }

  void
  set_type ( Type type_n )
  {
    _type = type_n;
  }

  /// @return True if type() != Type::NONE
  bool
  is_valid ()
  {
    return _type != Type::NONE;
  }

  // Private attributes
  private:
  Type _type;
};
} // namespace string_tree
