/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <string_tree/node.hpp>
#include <string_tree/node_ref.hpp>
#include <string_tree/path.hpp>
#include <QMap>

namespace string_tree
{

/// @brief String tree branch
///
class Branch : public Node
{
  public:
  // -- Types

  typedef QMap< QString, Node_Ref > Children;

  public:
  // -- Constructors

  /// @brief Default constructor
  Branch ();

  /// @brief Copy constructor
  Branch ( const Branch & branch_n );

  /// @brief Move constructor
  Branch ( Branch && branch_n );

  ~Branch ();

  // -- Children

  /// @brief Number of child nodes
  std::size_t
  size () const
  {
    return _children.size ();
  }

  /// @brief Returns true if ( size() == 0 )
  bool
  is_empty () const
  {
    return _children.empty ();
  }

  const Children &
  children () const
  {
    return _children;
  }

  /// @brief Deletes all children
  void
  clear ();

  // -- Children

  void
  get_branch_keys ( QStringList & branch_keys_n ) const;

  void
  get_leaf_keys ( QStringList & leaf_keys_n ) const;

  void
  get_split_keys ( QStringList & branch_keys_n,
                   QStringList & leaf_keys_n ) const;

  // -- Children

  /// @brief Finds a direct child by its key
  /// @return Non nullptr on success
  const Node *
  child_const ( const QString & key_n, Error * error_n = nullptr ) const;

  /// @brief Finds a direct child by its key
  /// @return Non nullptr on success
  const Node *
  child ( const QString & key_n, Error * error_n = nullptr ) const
  {
    return child_const ( key_n, error_n );
  }

  /// @brief Finds a direct child by its key
  /// @return Non nullptr on success
  Node *
  child ( const QString & key_n, Error * error_n = nullptr )
  {
    return const_cast< Node * > ( child_const ( key_n, error_n ) );
  }

  /// @brief Finds a direct child leaf by its key
  /// @return Non nullptr on success
  const Leaf *
  child_leaf_const ( const QString & key_n, Error * error_n = nullptr ) const;

  /// @brief Finds a direct child leaf by its key
  /// @return Non nullptr on success
  const Leaf *
  child_leaf ( const QString & key_n, Error * error_n = nullptr ) const
  {
    return child_leaf_const ( key_n, error_n );
  }

  /// @brief Finds a direct child leaf by its key
  /// @return Non nullptr on success
  Leaf *
  child_leaf ( const QString & key_n, Error * error_n = nullptr )
  {
    return const_cast< Leaf * > ( child_leaf_const ( key_n, error_n ) );
  }

  /// @brief Finds a direct child branch by its key
  /// @return Non nullptr on success
  const Branch *
  child_branch_const ( const QString & key_n, Error * error_n = nullptr ) const;

  /// @brief Finds a direct child branch by its key
  /// @return Non nullptr on success
  const Branch *
  child_branch ( const QString & key_n, Error * error_n = nullptr ) const
  {
    return child_branch_const ( key_n, error_n );
  }

  /// @brief Finds a direct child branch by its key
  /// @return Non nullptr on success
  Branch *
  child_branch ( const QString & key_n, Error * error_n = nullptr )
  {
    return const_cast< Branch * > ( child_branch_const ( key_n, error_n ) );
  }

  // -- Children add / remove

  Branch *
  new_child_branch ( const QString & key_n );

  Leaf *
  new_child_leaf ( const QString & key_n, const QString & value = QString () );

  void
  remove_child ( const QString & key_n );

  // -- Child value access

  /// @brief Ensures a child leaf with the given key exists and sets it's value
  void
  set_child_value ( const QString & key_n, const QString & value_n );

  /// @brief Get a child leaf value
  QString
  child_value ( const QString & key_n, Error * error_n = nullptr ) const;

  // -- Recursive node generation

  /// @brief Finds an existing branch node or creates a new one. May return
  /// this.
  /// @return Non nullptr on success
  Branch *
  acquire_branch ( const Path & path_n, Error * error_n = nullptr );

  /// @brief Finds an existing leaf node or creates a new one
  /// @return Non nullptr on success
  Leaf *
  acquire_leaf ( const Path & path_n, Error * error_n = nullptr );

  void
  insert ( const Branch & branch_ext_n );

  /// @brief Insert a copy of tree_n into the branch at address_n
  /// @arg address_n Address of the destination branch
  /// @arg branch_ext_n Remote branch
  /// @return True on success
  bool
  insert_at ( const QString & address_n,
              const Branch & branch_ext_n,
              Error * error_n = nullptr );

  /// @brief Clears the branch and inserts a copy of the remote branch
  void
  clear_and_insert ( const Branch & branch_ext_n );

  // -- Remove nodes recursive

  /// @brief Finds an existing node and removes it recursively.
  /// @return True if the node did exist and was removed
  bool
  remove ( const Path & path_n, Error * error_n = nullptr );

  /// @brief Finds an existing node and removes it recursively.
  /// @return True if the node did exist and was removed
  bool
  remove ( const QString & address_n, Error * error_n = nullptr );

  // -- Node search

  /// @brief Finds a node. May return this.
  /// @return Non nullptr on success
  const Node *
  find_const ( const Path & path_n, Error * error_n ) const;

  /// @brief Finds a node. May return this.
  /// @return Non nullptr on success
  const Node *
  find ( const Path & path_n, Error * error_n ) const
  {
    return find_const ( path_n, error_n );
  }

  /// @brief Finds a node. May return this.
  /// @return Non nullptr on success
  Node *
  find ( const Path & path_n, Error * error_n )
  {
    return const_cast< Node * > ( find_const ( path_n, error_n ) );
  }

  // -- Recursive branch search

  /// @brief Finds this or a sub branch
  /// @return Non nullptr on success
  const Branch *
  find_branch_const ( const Path & path_n, Error * error_n ) const;

  /// @brief Finds this or a sub branch
  /// @return Non nullptr on success
  const Branch *
  find_branch ( const Path & path_n, Error * error_n ) const
  {
    return find_branch_const ( path_n, error_n );
  }

  /// @brief Finds this or a sub branch
  /// @return Non nullptr on success
  Branch *
  find_branch ( const Path & path_n, Error * error_n )
  {
    return const_cast< Branch * > ( find_branch_const ( path_n, error_n ) );
  }

  // -- Sub node search

  /// @brief Finds a sub node. Never returns this.
  /// @return Non nullptr on success
  const Node *
  find_sub_const ( const Path & path_n, Error * error_n ) const;

  /// @brief Finds a sub node. Never returns this.
  /// @return Non nullptr on success
  const Node *
  find_sub ( const Path & path_n, Error * error_n ) const
  {
    return find_sub_const ( path_n, error_n );
  }

  /// @brief Finds a sub node. Never returns this.
  /// @return Non nullptr on success
  Node *
  find_sub ( const Path & path_n, Error * error_n )
  {
    return const_cast< Node * > ( find_sub_const ( path_n, error_n ) );
  }

  /// @brief Finds a sub leaf
  /// @return Non nullptr on success
  const Leaf *
  find_sub_leaf_const ( const Path & path_n, Error * error_n ) const;

  /// @brief Finds a sub leaf
  /// @return Non nullptr on success
  const Leaf *
  find_sub_leaf ( const Path & path_n, Error * error_n ) const
  {
    return find_sub_leaf_const ( path_n, error_n );
  }

  /// @brief Finds a sub leaf
  /// @return Non nullptr on success
  Leaf *
  find_sub_leaf ( const Path & path_n, Error * error_n )
  {
    return const_cast< Leaf * > ( find_sub_leaf_const ( path_n, error_n ) );
  }

  /// @brief Finds a sub branch
  /// @return Non nullptr on success
  const Branch *
  find_sub_branch_const ( const Path & path_n, Error * error_n ) const;

  /// @brief Finds a sub branch
  /// @return Non nullptr on success
  const Branch *
  find_sub_branch ( const Path & path_n, Error * error_n ) const
  {
    return find_sub_branch_const ( path_n, error_n );
  }

  /// @brief Finds a sub branch recursively
  /// @return Non nullptr on success
  Branch *
  find_sub_branch ( const Path & path_n, Error * error_n )
  {
    return const_cast< Branch * > ( find_sub_branch_const ( path_n, error_n ) );
  }

  // -- Recursive value getting / setting

  /// @brief Get a leaf value
  ///
  /// @return Valid string if there was a leaf at the given address
  QString
  value ( const QString & address_n, Error * error_n = nullptr ) const;

  /// @brief Get a leaf value
  ///
  /// @return Valid string if there was a leaf at the given address
  QString
  value ( const Path & path_n, Error * error_n = nullptr ) const;

  /// @brief Get a leaf value
  ///
  /// If there is a leaf at address_n its value is assigned to value_n.
  ///
  /// @return True if there was a leaf at the given address
  bool
  value ( QString & value_n,
          const QString & address_n,
          Error * error_n = nullptr ) const;

  /// @brief Get a leaf value
  ///
  /// If there is a leaf at address_n its value is assigned to value_n.
  ///
  /// @return True if there was a leaf at the given address
  bool
  value ( QString & value_n,
          const Path & path_n,
          Error * error_n = nullptr ) const;

  // -- Set value

  void
  set_value ( const QString & address_n,
              const QString & value_n,
              Error * error_n = nullptr );

  void
  set_value ( const Path & path_n,
              const QString & value_n,
              Error * error_n = nullptr );

  /// @return True if the value was changed
  bool
  set_value_testing ( const QString & address_n,
                      const QString & value_n,
                      Error * error_n = nullptr );

  /// @return True if the value was changed
  bool
  set_value_testing ( const Path & path_n,
                      const QString & value_n,
                      Error * error_n = nullptr );

  // -- Swap

  void
  clear_and_swap_children ( Branch & branch_n );

  void
  swap_children ( Branch & branch_n );

  void
  swap ( Branch & branch_n )
  {
    swap_children ( branch_n );
  }

  // -- Assignment operators

  Branch &
  operator= ( const Branch & branch_n )
  {
    clear_and_insert ( branch_n );
    return *this;
  }

  Branch &
  operator= ( Branch && branch_n )
  {
    clear_and_swap_children ( branch_n );
    return *this;
  }

  private:
  void
  reset_children_parent ();

  private:
  // -- Constructors

  Branch ( Branch * parent_n, const QString & key_n );

  private:
  Children _children;
};

inline const Branch &
Node::as_branch () const
{
  return static_cast< const Branch & > ( *this );
}

inline const Branch &
Node::as_branch_const () const
{
  return static_cast< const Branch & > ( *this );
}

inline Branch &
Node::as_branch ()
{
  return static_cast< Branch & > ( *this );
}
} // namespace string_tree
