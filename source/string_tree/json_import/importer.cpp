/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "importer.hpp"
#include <string_tree/leaf.hpp>
#include <QJsonDocument>
#include <QJsonObject>
#include <memory>
#include <message_streams.hpp>
#include <stack>

namespace string_tree::json_import
{

Importer::Importer () {}

Importer::~Importer () {}

bool
Importer::import ( Branch & branch_n, QByteArray const & utf8_n )
{
  bool success = true;
  {
    branch_n.clear ();
    QJsonDocument jdoc;
    {
      QJsonParseError jerror;
      jdoc = QJsonDocument::fromJson ( utf8_n, &jerror );
      success = ( jerror.error == QJsonParseError::NoError );
    }
    if ( success && jdoc.isObject () ) {
      struct Entry
      {
        Branch * branch;
        QJsonObject object;
        QJsonObject::iterator iter;
      };
      std::stack< std::unique_ptr< Entry > > stack;

      stack.emplace ( std::make_unique< Entry > () );
      {
        auto & top = *stack.top ();
        top.branch = &branch_n;
        top.object = jdoc.object ();
        top.iter = top.object.begin ();
      }

      while ( !stack.empty () ) {
        auto & top = *stack.top ();
        if ( top.iter == top.object.end () ) {
          stack.pop ();
        } else {
          auto const & key = top.iter.key ();
          auto const & value = top.iter.value ();
          if ( top.iter->isBool () ) {
            top.branch->set_child_value ( key, value.toBool () ? "1" : "0" );
          } else if ( top.iter->isDouble () ) {
            top.branch->set_child_value (
                key, QString::number ( value.toDouble () ) );
          } else if ( top.iter->isString () ) {
            top.branch->set_child_value ( key, value.toString () );
          } else if ( top.iter->isObject () ) {
            stack.emplace ( std::make_unique< Entry > () );
            {
              auto & ntop = *stack.top ();
              ntop.branch = top.branch->new_child_branch ( key );
              ntop.object = value.toObject ();
              ntop.iter = ntop.object.begin ();
            }
          }
          ++top.iter;
        }
      }
    }
  }
  return success;
}
} // namespace string_tree::json_import
