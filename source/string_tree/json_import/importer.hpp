/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <string_tree/branch.hpp>
#include <QString>

namespace string_tree::json_import
{

/// @brief Importer
///
class Importer
{
  public:
  // -- Constructors

  Importer ();

  ~Importer ();

  // -- Import

  /// @brief Import utf8 encoded json data
  bool
  import ( Branch & branch_n, QByteArray const & utf8_n );

  private:
};
} // namespace string_tree::json_import
