/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <string_tree/address_iterator.hpp>
#include <string_tree/path.hpp>

namespace string_tree
{

void
Path::clear ()
{
  QStringList::clear ();
  _is_valid = true;
}

bool
Path::append ( const QString & key_n )
{
  if ( !key_n.isEmpty () ) {
    for ( QChar schar : key_n ) {
      if ( !path_key_char_valid ( schar ) ) {
        _is_valid = false;
        return false;
      }
    }
    QStringList::append ( key_n );
    return true;
  }
  return false;
}

QString
Path::to_address () const
{
  QString address;
  // Preallocate
  {
    std::size_t reserve ( size () );
    for ( const QString & key : *this ) {
      reserve += key.size ();
    }
    address.reserve ( reserve );
  }
  // Collect
  for ( const QString & key : *this ) {
    if ( !address.isEmpty () ) {
      address.push_back ( path_key_sep );
    }
    address.append ( key );
  }
  return address;
}

bool
Path::set_address ( const QString & address_n, Error * error_n )
{
  clear ();
  bool success ( false );
  Address_Iterator parser ( address_n );
  while ( true ) {
    switch ( parser.next () ) {
    case Address_Iterator::State::GOOD:
      append ( parser.key () );
      // Continue main loop
      continue;
      break;
    case Address_Iterator::State::INVALID:
      if ( error_n != nullptr ) {
        error_n->set_type ( Error::Type::ADDRESS_INVALID );
      }
      break;
    case Address_Iterator::State::END:
      success = true;
      break;
    }
    // Leave loop by default
    break;
  }
  return success;
}
} // namespace string_tree
