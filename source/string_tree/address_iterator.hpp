/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <QString>

namespace string_tree
{

/// @brief Iterates along and address and extracts the keys
///
class Address_Iterator
{
  // Public types
  public:
  enum class State
  {
    GOOD,
    INVALID,
    END
  };

  typedef QString::const_iterator Iterator;

  // Public methods
  public:
  Address_Iterator ( const QString & address_n );

  State
  next ();

  Iterator
  key_begin () const
  {
    return _it_key;
  }

  Iterator
  key_end () const
  {
    return _it_dot;
  }

  /// @brief Current key
  QString
  key () const
  {
    return QString ( static_cast< const QChar * > ( _it_key ),
                     ( _it_dot - _it_key ) );
  }

  /// @brief Only valid as long as address_n is unchanged
  QString
  raw_key () const
  {
    return QString::fromRawData ( static_cast< const QChar * > ( _it_key ),
                                  ( _it_dot - _it_key ) );
  }

  bool
  is_last () const
  {
    return ( _it_dot == _it_end );
  }

  // Private attributes
  private:
  bool _first;
  Iterator _it_key;
  Iterator _it_dot;
  Iterator _it_end;
};
} // namespace string_tree
