/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <string_tree/node.hpp>

namespace string_tree
{

/// @brief Leaf node
///
class Leaf : public Node
{
  public:
  friend class Branch;

  public:
  // -- Constructors

  /// @brief Copy constructor
  Leaf ( const Leaf & leaf_n ) = delete;

  /// @brief Move constructor
  Leaf ( Leaf && leaf_n ) = delete;

  ~Leaf ();

  // -- Value access

  const QString &
  value () const
  {
    return _value;
  }

  void
  set_value ( const QString & value_n )
  {
    _value = value_n;
  }

  /// @return True if the value changed
  bool
  set_value_testing ( const QString & value_n );

  /// @brief Sets a C locale encoded number string
  void
  set_value_number ( double value_n );

  /// @brief Sets a C locale encoded number string
  void
  set_value_number ( float value_n );

  /// @brief Sets a C locale encoded number string
  void
  set_value_number ( int64_t value_n );

  /// @brief Sets a C locale encoded number string
  void
  set_value_number ( uint64_t value_n );

  /// @brief Converts the C locale encoded value() string to a binary number
  /// @return True on success
  bool
  get_value_number ( double & value_n );

  /// @brief Converts the C locale encoded value() string to a binary number
  /// @return True on success
  bool
  get_value_number ( float & value_n );

  /// @brief Converts the C locale encoded value() string to a binary number
  /// @return True on success
  bool
  get_value_number ( int64_t & value_n );

  /// @brief Converts the C locale encoded value() string to a binary number
  /// @return True on success
  bool
  get_value_number ( uint64_t & value_n );

  // -- Operators

  Leaf &
  operator= ( const Leaf & node_n ) = delete;

  Leaf &
  operator= ( Leaf && node_n ) = delete;

  protected:
  // -- Constructors

  Leaf ( Branch * parent_n,
         const QString & key_n,
         const QString & value_n = QString () );

  private:
  QString _value;
};

inline const Leaf &
Node::as_leaf_const () const
{
  return static_cast< const Leaf & > ( *this );
}

inline const Leaf &
Node::as_leaf () const
{
  return static_cast< const Leaf & > ( *this );
}

inline Leaf &
Node::as_leaf ()
{
  return static_cast< Leaf & > ( *this );
}
} // namespace string_tree
