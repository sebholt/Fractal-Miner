/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <string_tree/branch.hpp>
#include <string_tree/leaf.hpp>
#include <array>
#include <stack>

namespace string_tree
{

// -- Utility

static inline void
error_set_type ( Error * error_n, Error::Type type_n )
{
  if ( error_n != nullptr ) {
    error_n->set_type ( type_n );
  }
}

// -- Class methods

Branch::Branch ( Branch * parent_n, const QString & key_n )
: Node ( Type::BRANCH, parent_n, key_n )
{
}

Branch::Branch ()
: Node ( Type::BRANCH, nullptr, QString () )
{
}

Branch::Branch ( const Branch & branch_n )
: Branch ()
{
  insert ( branch_n );
}

Branch::Branch ( Branch && branch_n )
: Branch ()
{
  _children.swap ( branch_n._children );
  reset_children_parent ();
}

Branch::~Branch ()
{
  clear ();
}

void
Branch::clear ()
{
  while ( !children ().empty () ) {
    Children::iterator it ( _children.begin () );
    Node * child_node ( it.value () );
    switch ( child_node->type () ) {
    case Node::Type::BRANCH: {
      std::stack< Branch * > stack;
      stack.push ( &child_node->as_branch () );
      do {
        Branch * btop ( stack.top () );
        if ( btop->is_empty () ) {
          // Delete only empty branches
          stack.pop ();
          btop->parent ()->remove_child ( btop->key () );
          break;
        } else {
          // Delete child leaf immediately or push a branch
          Children::iterator it_top ( btop->_children.begin () );
          Node * top_child_node ( it_top.value () );
          switch ( top_child_node->type () ) {
          case Node::Type::BRANCH:
            stack.push ( &top_child_node->as_branch () );
            break;
          case Node::Type::LEAF:
            btop->_children.erase ( it_top );
            break;
          }
        }
      } while ( !stack.empty () );
    } break;
    case Node::Type::LEAF:
      _children.erase ( it );
      break;
    }
  }
}

void
Branch::get_branch_keys ( QStringList & branch_keys_n ) const
{
  for ( Children::const_iterator it ( _children.cbegin () );
        it != _children.cend ();
        ++it ) {
    if ( it.value ()->is_branch () ) {
      branch_keys_n.push_back ( it.key () );
      break;
    }
  }
}

void
Branch::get_leaf_keys ( QStringList & leaf_keys_n ) const
{
  for ( Children::const_iterator it ( _children.cbegin () );
        it != _children.cend ();
        ++it ) {
    if ( it.value ()->is_leaf () ) {
      leaf_keys_n.push_back ( it.key () );
      break;
    }
  }
}

void
Branch::get_split_keys ( QStringList & branch_keys_n,
                         QStringList & leaf_keys_n ) const
{
  for ( Children::const_iterator it ( _children.cbegin () );
        it != _children.cend ();
        ++it ) {
    switch ( it.value ()->type () ) {
    case Node::Type::BRANCH:
      branch_keys_n.push_back ( it.key () );
      break;
    case Node::Type::LEAF:
      leaf_keys_n.push_back ( it.key () );
      break;
    }
  }
}

const Node *
Branch::child_const ( const QString & key_n, Error * error_n ) const
{
  Children::const_iterator cit ( _children.find ( key_n ) );
  if ( cit != _children.end () ) {
    return cit.value ();
  }
  error_set_type ( error_n, Error::Type::NODE_NOT_EXIST );
  return nullptr;
}

const Leaf *
Branch::child_leaf_const ( const QString & key_n, Error * error_n ) const
{
  const Node * node ( child_const ( key_n, error_n ) );
  if ( node != nullptr ) {
    if ( node->is_leaf () ) {
      return &node->as_leaf ();
    } else {
      error_set_type ( error_n, Error::Type::NODE_NOT_LEAF );
    }
  }
  return nullptr;
}

const Branch *
Branch::child_branch_const ( const QString & key_n, Error * error_n ) const
{
  const Node * node ( child_const ( key_n, error_n ) );
  if ( node != nullptr ) {
    if ( node->is_branch () ) {
      return &node->as_branch ();
    } else {
      error_set_type ( error_n, Error::Type::NODE_NOT_BRANCH );
    }
  }
  return nullptr;
}

Branch *
Branch::new_child_branch ( const QString & key_n )
{
  Branch * branch ( new Branch ( this, key_n ) );
  _children.insert ( key_n, branch );
  return branch;
}

Leaf *
Branch::new_child_leaf ( const QString & key_n, const QString & value_n )
{
  Leaf * leaf ( new Leaf ( this, key_n, value_n ) );
  _children.insert ( key_n, leaf );
  return leaf;
}

void
Branch::remove_child ( const QString & key_n )
{
  Children::iterator it ( _children.find ( key_n ) );
  if ( it != _children.end () ) {
    _children.erase ( it );
  }
}

void
Branch::set_child_value ( const QString & key_n, const QString & value_n )
{
  Leaf * leaf ( child_leaf ( key_n ) );
  if ( leaf != nullptr ) {
    leaf->set_value ( value_n );
  } else {
    new_child_leaf ( key_n, value_n );
  }
}

QString
Branch::child_value ( const QString & key_n, Error * error_n ) const
{
  const Leaf * leaf ( child_leaf ( key_n, error_n ) );
  if ( leaf != nullptr ) {
    return leaf->value ();
  }
  return QString ();
}

Branch *
Branch::acquire_branch ( const Path & path_n, Error * error_n )
{
  Branch * branch ( this );
  if ( !path_n.empty () ) {
    // Path iterator
    Path::const_iterator pit ( path_n.begin () );
    // Walk over existing nodes
    for ( ; pit != path_n.end (); ++pit ) {
      Node * node ( branch->child ( *pit ) );
      if ( node != nullptr ) {
        // Child with matching key found
        if ( node->is_branch () ) {
          // Continue with this branch
          branch = static_cast< Branch * > ( node );
        } else {
          // Node not a branch. Abort.
          error_set_type ( error_n, Error::Type::NODE_NOT_BRANCH );
          branch = nullptr;
          break;
        }
      } else {
        // No child branch with matching key.
        break;
      }
    }
    // Generate new nodes
    if ( branch != nullptr ) {
      for ( ; pit != path_n.end (); ++pit ) {
        // Generate new branch
        branch = branch->new_child_branch ( *pit );
      }
    }
  }
  return branch;
}

Leaf *
Branch::acquire_leaf ( const Path & path_n, Error * error_n )
{
  Leaf * leaf ( nullptr );
  if ( path_n.empty () ) {
    error_set_type ( error_n, Error::Type::NODE_NOT_LEAF );
  } else {
    Branch * branch ( this );
    // Path iterator
    Path::const_iterator pit ( path_n.begin () );
    Path::const_iterator pit_last ( pit + ( path_n.size () - 1 ) );

    // Walk over existing branches
    for ( ; pit != pit_last; ++pit ) {
      Node * node ( branch->child ( *pit ) );
      if ( node != nullptr ) {
        if ( node->is_branch () ) {
          // Continue with this branch
          branch = static_cast< Branch * > ( node );
        } else {
          // Node not a branch. Error abort.
          error_set_type ( error_n, Error::Type::NODE_NOT_BRANCH );
          branch = nullptr;
          break;
        }
      } else {
        break;
      }
    }
    if ( branch != nullptr ) {
      // Generate missing branches
      for ( ; pit != pit_last; ++pit ) {
        // Generate new branch
        branch = branch->new_child_branch ( *pit );
      }
      // Take or create leaf
      {
        Node * node ( branch->child ( *pit ) );
        if ( node != nullptr ) {
          // Final child with matching key found
          if ( node->is_leaf () ) {
            // Finish: Use existing child leaf
            leaf = static_cast< Leaf * > ( node );
          } else {
            // Node is not a leaf. Error abort.
            error_set_type ( error_n, Error::Type::NODE_NOT_LEAF );
          }
        } else {
          // Leaf does not exist
          leaf = branch->new_child_leaf ( *pit );
        }
      }
    }
  }
  return leaf;
}

void
Branch::insert ( const Branch & branch_ext_n )
{
  // Stack item struct
  struct Stack_Item
  {
    Stack_Item ( Branch * blocal, const Branch * bext )
    : local ( blocal )
    , ext_cit ( bext->children ().cbegin () )
    , ext_end ( bext->children ().cend () )
    {
    }

    Branch * local;
    Branch::Children::const_iterator ext_cit;
    const Branch::Children::const_iterator ext_end;
  };

  // Insert tracking stack
  std::stack< Stack_Item > stack;

  // Push root branches to stack
  stack.emplace ( this, &branch_ext_n );
  while ( !stack.empty () ) {
    Stack_Item & sitem ( stack.top () );
    if ( sitem.ext_cit == sitem.ext_end ) {
      stack.pop ();
    } else {
      // Keep values from iterator
      Branch * branch_own ( sitem.local );
      const QString name_ext ( sitem.ext_cit.key () );
      const Node * child_ext ( sitem.ext_cit.value () );
      // Increment iterator
      ++sitem.ext_cit;

      // Find existing child node
      Node * child_own ( branch_own->child ( name_ext ) );
      if ( child_own != nullptr ) {
        // Named child _does_ exists already
        if ( child_own->type () == child_ext->type () ) {
          switch ( child_ext->type () ) {
          case Node::Type::BRANCH:
            // Push branches to stack
            stack.emplace ( &child_own->as_branch (),
                            &child_ext->as_branch () );
            break;
          case Node::Type::LEAF:
            // Update existing leaf value
            child_own->as_leaf ().set_value ( child_ext->as_leaf ().value () );
            break;
          }
        } else {
          // Type missmatch
        }
      } else {
        // Named child does _not_ exist
        switch ( child_ext->type () ) {
        case Node::Type::BRANCH: {
          // Generate new branch
          Branch * branch_new ( branch_own->new_child_branch ( name_ext ) );
          // Push branches to stack
          stack.emplace ( branch_new, &child_ext->as_branch () );
        } break;
        case Node::Type::LEAF: {
          // Generate new leaf
          Leaf * leaf_new ( branch_own->new_child_leaf ( name_ext ) );
          // Set leaf value
          leaf_new->set_value ( child_ext->as_leaf ().value () );
        } break;
        }
      }
    }
  }
}

bool
Branch::insert_at ( const QString & address_n,
                    const Branch & branch_n,
                    Error * error_n )
{
  bool success ( false );
  {
    Path spath ( address_n, error_n );
    if ( spath ) {
      // Insert into root branch
      Branch * branch ( acquire_branch ( spath, error_n ) );
      if ( branch != nullptr ) {
        branch->insert ( branch_n );
        success = true;
      }
    }
  }
  return success;
}

void
Branch::clear_and_insert ( const Branch & branch_ext_n )
{
  clear ();
  insert ( branch_ext_n );
}

bool
Branch::remove ( const Path & path_n, Error * error_n )
{
  bool res ( false );
  if ( path_n.is_valid () ) {
    Node * node ( find_sub ( path_n, error_n ) );
    if ( node != nullptr ) {
      node->parent ()->remove_child ( node->key () );
      res = true;
    }
  }
  return res;
}

bool
Branch::remove ( const QString & address_n, Error * error_n )
{
  return remove ( Path ( address_n, error_n ) );
}

const Node *
Branch::find_const ( const Path & path_n, Error * error_n ) const
{
  const Node * node ( this );
  for ( const QString & skey : path_n ) {
    if ( node->is_branch () ) {
      const Branch * branch ( static_cast< const Branch * > ( node ) );
      Children::const_iterator cit ( branch->_children.find ( skey ) );
      if ( cit != branch->_children.end () ) {
        node = cit.value ();
      } else {
        // No child node with matching key
        // Error abort
        error_set_type ( error_n, Error::Type::NODE_NOT_EXIST );
        node = nullptr;
        break;
      }
    } else {
      // Node is not a branch
      // Error abort
      error_set_type ( error_n, Error::Type::NODE_NOT_BRANCH );
      node = nullptr;
      break;
    }
  }
  return node;
}

const Branch *
Branch::find_branch_const ( const Path & path_n, Error * error_n ) const
{
  const Node * node ( find_const ( path_n, error_n ) );
  if ( node != nullptr ) {
    if ( node->is_branch () ) {
      return &node->as_branch ();
    } else {
      error_set_type ( error_n, Error::Type::NODE_NOT_BRANCH );
    }
  }
  return nullptr;
}

const Node *
Branch::find_sub_const ( const Path & path_n, Error * error_n ) const
{
  const Node * node ( find ( path_n, error_n ) );
  if ( node == this ) {
    error_set_type ( error_n, Error::Type::ADDRESS_INVALID );
    return nullptr;
  }
  return node;
}

const Leaf *
Branch::find_sub_leaf_const ( const Path & path_n, Error * error_n ) const
{
  const Node * node ( find_sub_const ( path_n, error_n ) );
  if ( node != nullptr ) {
    if ( node->is_leaf () ) {
      return &node->as_leaf ();
    } else {
      error_set_type ( error_n, Error::Type::NODE_NOT_LEAF );
    }
  }
  return nullptr;
}

const Branch *
Branch::find_sub_branch_const ( const Path & path_n, Error * error_n ) const
{
  const Node * node ( find_sub_const ( path_n, error_n ) );
  if ( node != nullptr ) {
    if ( node->is_branch () ) {
      return &node->as_branch ();
    } else {
      error_set_type ( error_n, Error::Type::NODE_NOT_BRANCH );
    }
  }
  return nullptr;
}

QString
Branch::value ( const QString & address_n, Error * error_n ) const
{
  QString res;
  {
    Path spath ( address_n, error_n );
    if ( spath ) {
      const Leaf * leaf ( find_sub_leaf ( spath, error_n ) );
      if ( leaf != nullptr ) {
        res = leaf->value ();
      }
    }
  }
  return res;
}

QString
Branch::value ( const Path & path_n, Error * error_n ) const
{
  QString res;
  if ( path_n ) {
    const Leaf * leaf ( find_sub_leaf ( path_n, error_n ) );
    if ( leaf != nullptr ) {
      res = leaf->value ();
    }
  }
  return res;
}

bool
Branch::value ( QString & value_n,
                const QString & address_n,
                Error * error_n ) const
{
  bool good ( false );
  {
    Path spath ( address_n, error_n );
    if ( spath ) {
      const Leaf * leaf ( find_sub_leaf ( spath, error_n ) );
      if ( leaf != nullptr ) {
        value_n = leaf->value ();
        good = true;
      }
    }
  }
  return good;
}

bool
Branch::value ( QString & value_n, const Path & path_n, Error * error_n ) const
{
  bool good ( false );
  if ( path_n ) {
    const Leaf * leaf ( find_sub_leaf ( path_n, error_n ) );
    if ( leaf != nullptr ) {
      value_n = leaf->value ();
      good = true;
    }
  }
  return good;
}

void
Branch::set_value ( const QString & address_n,
                    const QString & value_n,
                    Error * error_n )
{
  Path spath ( address_n, error_n );
  if ( spath ) {
    Leaf * leaf ( acquire_leaf ( spath, error_n ) );
    if ( leaf != nullptr ) {
      leaf->set_value ( value_n );
    }
  }
}

void
Branch::set_value ( const Path & path_n,
                    const QString & value_n,
                    Error * error_n )
{
  if ( path_n ) {
    Leaf * leaf ( acquire_leaf ( path_n, error_n ) );
    if ( leaf != nullptr ) {
      leaf->set_value ( value_n );
    }
  }
}

bool
Branch::set_value_testing ( const QString & address_n,
                            const QString & value_n,
                            Error * error_n )
{
  bool changed ( false );
  {
    Path spath ( address_n, error_n );
    if ( spath ) {
      Leaf * leaf ( acquire_leaf ( spath, error_n ) );
      if ( leaf != nullptr ) {
        changed = leaf->set_value_testing ( value_n );
      }
    }
  }
  return changed;
}

bool
Branch::set_value_testing ( const Path & path_n,
                            const QString & value_n,
                            Error * error_n )
{
  bool changed ( false );
  if ( path_n ) {
    Leaf * leaf ( acquire_leaf ( path_n, error_n ) );
    if ( leaf != nullptr ) {
      changed = leaf->set_value_testing ( value_n );
    }
  }
  return changed;
}

void
Branch::clear_and_swap_children ( Branch & branch_n )
{
  clear ();
  _children.swap ( branch_n._children );
  reset_children_parent ();
}

void
Branch::swap_children ( Branch & branch_n )
{
  _children.swap ( branch_n._children );
  // Update children's parent pointer#
  reset_children_parent ();
  branch_n.reset_children_parent ();
}

void
Branch::reset_children_parent ()
{
  for ( Children::iterator it ( _children.begin () ); it != _children.end ();
        ++it ) {
    it.value ()->set_parent ( this );
  }
}
} // namespace string_tree
