/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <string_tree/branch.hpp>
#include <string_tree/leaf.hpp>
#include <string_tree/node.hpp>
#include <string_tree/node_ref.hpp>

namespace string_tree
{

Node_Ref::Node_Ref ( Node * node_n )
: _node ( node_n )
{
  if ( _node != nullptr ) {
    _node->ref ();
  }
}

/// @brief Copy constructor
Node_Ref::Node_Ref ( const Node_Ref & ref_n )
: _node ( ref_n._node )
{
  if ( _node != nullptr ) {
    _node->ref ();
  }
}

/// @brief Move constructor
Node_Ref::Node_Ref ( Node_Ref && ref_n )
: _node ( ref_n._node )
{
  if ( _node != nullptr ) {
    ref_n._node = nullptr;
  }
}

void
Node_Ref::clear ()
{
  if ( _node != nullptr ) {
    Node * node_old ( _node );
    _node = nullptr;
    deref_node ( node_old );
  }
}

void
Node_Ref::reset ( Node * node_n )
{
  Node * node_old ( _node );
  _node = node_n;
  if ( _node != nullptr ) {
    _node->ref ();
  }
  if ( node_old != nullptr ) {
    deref_node ( node_old );
  }
}

inline void
Node_Ref::deref_node ( Node * node_n )
{
  if ( !node_n->deref () ) {
    switch ( node_n->type () ) {
    case Node::Type::BRANCH:
      delete &node_n->as_branch ();
      break;
    case Node::Type::LEAF:
      delete &node_n->as_leaf ();
      break;
    }
  }
}

Node_Ref &
Node_Ref::operator= ( Node_Ref && ref_n )
{
  if ( &ref_n != this ) {
    clear ();
    if ( ref_n._node != nullptr ) {
      _node = ref_n._node;
      ref_n._node = nullptr;
    }
  }
  return *this;
}
} // namespace string_tree
