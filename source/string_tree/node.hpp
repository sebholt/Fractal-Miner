/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <string_tree/node_ref.hpp>
#include <QString>
#include <cstdint>

namespace string_tree
{

// -- Forward declaration
class Branch;
class Leaf;

/// @brief Configuration tree node
///
class Node
{
  public:
  // -- Types

  enum class Type : std::uint8_t
  {
    BRANCH,
    LEAF
  };

  // -- Friends
  friend class Node_Ref;
  friend class Branch;
  friend class String_Tree;

  public:
  // -- Constructors

  /// @brief Copy constructor
  Node ( const Node & node_n ) = delete;

  /// @brief Move constructor
  Node ( Node && node_n ) = delete;

  // -- Node type

  Type
  type () const
  {
    return _type;
  }

  bool
  is_branch () const
  {
    return ( _type == Type::BRANCH );
  }

  bool
  is_leaf () const
  {
    return ( _type == Type::LEAF );
  }

  // -- Type casts

  const Branch &
  as_branch_const () const;

  const Branch &
  as_branch () const;

  Branch &
  as_branch ();

  const Leaf &
  as_leaf_const () const;

  const Leaf &
  as_leaf () const;

  Leaf &
  as_leaf ();

  // -- Parent node

  Branch *
  parent ()
  {
    return _parent;
  }

  const Branch *
  parent () const
  {
    return _parent;
  }

  // -- Key / address

  const QString &
  key () const
  {
    return _key;
  }

  QString
  acquire_address () const;

  // -- Assignment operators

  Node &
  operator= ( const Node & node_n ) = delete;

  Node &
  operator= ( Node && node_n ) = delete;

  protected:
  // -- Constructors

  Node ( Type type_n, Branch * parent_n, QString key_n )
  : _type ( type_n )
  , _parent ( parent_n )
  , _key ( key_n )
  , _ref_count ( 0 )
  {
  }

  ~Node (){};

  void
  set_parent ( Branch * parent_n )
  {
    _parent = parent_n;
  }

  private:
  // -- Reference counting

  void
  ref ()
  {
    ++_ref_count;
  }

  bool
  deref ()
  {
    return ( ( --_ref_count ) != 0 );
  }

  // Private attributes
  private:
  Type _type;
  Branch * _parent;
  QString _key;
  std::size_t _ref_count;
};
} // namespace string_tree
