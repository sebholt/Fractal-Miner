/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <string_tree/error.hpp>
#include <QStringList>
#include <array>

namespace string_tree
{

// -- Types and statics

/// @brief Key separator character
const QChar path_key_sep = '.';

/// @brief Invalid characters for child keys
const std::array< QChar, 9 > path_invalid_key_chars{
    { // Puktuation / Separator
      '.',
      '/',
      '\\',
      // Space
      ' ',
      '\f',
      '\n',
      '\r',
      '\t',
      '\v' } };

/// @return True if the char is valid for a key
inline bool
path_key_char_valid ( const QChar char_n )
{
  for ( const QChar icha : path_invalid_key_chars ) {
    if ( char_n == icha ) {
      return false;
    }
  }
  return true;
}

// -- Class

/// @brief Container for split adresses
class Path : private QStringList
{
  // Public methods
  public:
  // -- Constructors

  /// @brief Creates an empty valid path
  Path ()
  : _is_valid ( true )
  {
  }

  /// @brief Copy constructor
  Path ( const Path & path_n ) = default;

  /// @brief Move constructor
  Path ( Path && path_n ) = default;

  Path ( const QString & address_n, Error * error_n = nullptr )
  : Path ()
  {
    set_address ( address_n, error_n );
  }

  // -- Derived forwarding

  using QStringList::const_iterator;
  using QStringList::iterator;

  using QStringList::empty;
  using QStringList::size;

  using QStringList::begin;
  using QStringList::cbegin;
  using QStringList::cend;
  using QStringList::end;

  using QStringList::pop_back;

  // -- Append

  /// @return True if the key was valid
  bool
  append ( const QString & key_n );

  // -- General

  /// @brief Resets to an empty valid path
  void
  clear ();

  /// @brief Invalid path won't be accepted
  ///
  /// Paths become invalid when generated from invalid addresses
  bool
  is_valid () const
  {
    return _is_valid;
  }

  // -- Address

  QString
  to_address () const;

  bool
  set_address ( const QString & address_n, Error * error_n = nullptr );

  // -- Cast operators

  operator bool () const { return _is_valid; }

  // -- Assignment operators

  Path &
  operator= ( const Path & path_n ) = default;

  Path &
  operator= ( Path && path_n )
  {
    QStringList::operator= ( static_cast< QStringList && > ( path_n ) );
    _is_valid = path_n.is_valid ();
    path_n.clear ();
    return *this;
  }

  // Private attributes
  private:
  bool _is_valid;
};
} // namespace string_tree
