/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>

namespace string_tree
{

// -- Forward declaration
class Node;

/// @brief Configuration tree node
///
class Node_Ref
{
  // Public methods
  public:
  // -- Constructors

  /// @brief Init constructor
  Node_Ref ( Node * node_n = nullptr );

  /// @brief Copy constructor
  Node_Ref ( const Node_Ref & ref_n );

  /// @brief Move constructor
  Node_Ref ( Node_Ref && ref_n );

  ~Node_Ref () { clear (); }

  void
  clear ();

  void
  reset ( Node * node_n );

  // -- Cast operators

  operator Node * () const { return _node; }

  Node *
  operator-> () const
  {
    return _node;
  }

  // -- Assignment operators

  Node_Ref &
  operator= ( const Node_Ref & ref_n )
  {
    reset ( ref_n._node );
    return *this;
  }

  Node_Ref &
  operator= ( Node_Ref && ref_n );

  // Private methods
  private:
  static void
  deref_node ( Node * node_n );

  // Private attributes
  private:
  Node * _node;
};
} // namespace string_tree
