/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <string_tree/leaf.hpp>

namespace string_tree
{

Leaf::Leaf ( Branch * parent_n, const QString & key_n, const QString & value_n )
: Node ( Type::LEAF, parent_n, key_n )
, _value ( value_n )
{
}

Leaf::~Leaf () {}

bool
Leaf::set_value_testing ( const QString & value_n )
{
  bool changed ( false );
  if ( _value != value_n ) {
    _value = value_n;
    _value.squeeze ();
    changed = true;
  }
  return changed;
}

void
Leaf::set_value_number ( double value_n )
{
  _value.setNum ( value_n, 'g', std::numeric_limits< double >::max_digits10 );
}

void
Leaf::set_value_number ( float value_n )
{
  _value.setNum ( value_n, 'g', std::numeric_limits< float >::max_digits10 );
}

void
Leaf::set_value_number ( int64_t value_n )
{
  _value.setNum ( value_n );
}

void
Leaf::set_value_number ( uint64_t value_n )
{
  _value.setNum ( value_n );
}

bool
Leaf::get_value_number ( double & value_n )
{
  bool good ( false );
  value_n = _value.toDouble ( &good );
  return good;
}

bool
Leaf::get_value_number ( float & value_n )
{
  bool good ( false );
  value_n = _value.toFloat ( &good );
  return good;
}

bool
Leaf::get_value_number ( int64_t & value_n )
{
  bool good ( false );
  value_n = _value.toLongLong ( &good );
  return good;
}

bool
Leaf::get_value_number ( uint64_t & value_n )
{
  bool good ( false );
  value_n = _value.toULongLong ( &good );
  return good;
}
} // namespace string_tree
