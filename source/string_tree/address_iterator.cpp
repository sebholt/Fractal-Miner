/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <string_tree/address_iterator.hpp>
#include <string_tree/path.hpp>

namespace string_tree
{

Address_Iterator::Address_Iterator ( const QString & address_n )
: _first ( true )
, _it_key ( address_n.begin () )
, _it_dot ( address_n.begin () )
, _it_end ( address_n.end () )
{
}

Address_Iterator::State
Address_Iterator::next ()
{
  // Check if we are at the end
  if ( _it_dot == _it_end ) {
    return State::END;
  }

  if ( !_first ) {
    if ( _it_key == _it_dot ) {
      // dot should be behind key -> zero length key
      return State::INVALID;
    } else {
      // Place both iterators behind current dot
      ++_it_dot;
      _it_key = _it_dot;
    }
  } else {
    _first = false;
  }

  // Move seeker forward
  while ( ( _it_dot != _it_end ) && ( *_it_dot != path_key_sep ) ) {
    // Test for invalid characters
    if ( !path_key_char_valid ( *_it_dot ) ) {
      return State::INVALID;
    }
    ++_it_dot;
  };
  // Test for zero length key
  if ( _it_key == _it_dot ) {
    return State::INVALID;
  }

  return State::GOOD;
}
} // namespace string_tree
