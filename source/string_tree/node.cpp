/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <string_tree/branch.hpp>
#include <string_tree/node.hpp>
#include <string_tree/path.hpp>

namespace string_tree
{

QString
Node::acquire_address () const
{
  QString address ( key () );
  const Node * node ( this );
  while ( true ) {
    // Raise bottom one node upwards
    const Branch * parent ( node->parent () );
    if ( ( parent != nullptr ) && ( parent->parent () != nullptr ) ) {
      address.prepend ( path_key_sep );
      address.prepend ( parent->key () );
      node = parent;
    } else {
      break;
    }
  }
  return address;
}
} // namespace string_tree
