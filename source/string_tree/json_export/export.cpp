/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "export.hpp"
#include <string_tree/leaf.hpp>

namespace string_tree::json_export
{

Char_Escapes char_escapes = { { { '\\', { '\\', '\\' } },
                                { '/', { '\\', '/' } },
                                { '"', { '\\', '"' } },
                                { '\b', { '\\', 'b' } },
                                { '\f', { '\\', 'f' } },
                                { '\n', { '\\', 'n' } },
                                { '\r', { '\\', 'r' } },
                                { '\t', { '\\', 't' } } } };

void
append_escaped ( QString & json_n, const QString & text_n )
{
  json_n.push_back ( '"' );
  for ( QChar schar : text_n ) {
    for ( const Char_Escape & item : char_escapes ) {
      if ( schar == item.cha ) {
        json_n.push_back ( item.esc[ 0 ] );
        json_n.push_back ( item.esc[ 1 ] );
        goto skip;
      }
    }
    json_n.push_back ( schar );
  skip:;
  }
  json_n.push_back ( '"' );
}

QString
export_compressed ( const Branch & root_n )
{
  // -- Types
  typedef Branch::Children::const_iterator Children_Iterator;
  enum class Mode
  {
    DONE,
    UP,
    EVAL,
    NEXT,
    DOWN
  };

  // -- Algorithm

  // Persistent variables
  QString json;
  Mode mode = Mode::UP;
  const Branch * parent ( &root_n );
  Children_Iterator child_it;
  const Node * child ( nullptr );

  while ( mode != Mode::DONE ) {
    switch ( mode ) {
    case Mode::DONE:
      break;

    case Mode::UP:
      json.push_back ( '{' );
      if ( !parent->children ().empty () ) {
        // Select first child
        child_it = parent->children ().begin ();
        child = child_it.value ();
        mode = Mode::EVAL;
      } else {
        // Go down immediately
        mode = Mode::DOWN;
      }
      break;

    case Mode::EVAL:
      // Append key string
      append_escaped ( json, child_it.key () );
      json.push_back ( ':' );
      // Evaluate child node type
      switch ( child->type () ) {
      case Node::Type::LEAF:
        // Append leaf value
        append_escaped ( json, child->as_leaf ().value () );
        mode = Mode::NEXT;
        break;
      case Node::Type::BRANCH:
        // Go up
        parent = &child->as_branch ();
        mode = Mode::UP;
        break;
      }
      break;

    case Mode::NEXT:
      ++child_it;
      if ( child_it != parent->children ().end () ) {
        json.push_back ( ',' );
        child = child_it.value ();
        mode = Mode::EVAL;
      } else {
        mode = Mode::DOWN;
      }
      break;

    case Mode::DOWN:
      json.push_back ( '}' );
      if ( parent != &root_n ) {
        child = parent;
        parent = child->parent ();
        {
          Children_Iterator it ( parent->children ().begin () );
          Children_Iterator it_end ( parent->children ().end () );
          for ( ; ( it != it_end ) && ( it.value () != child ); ++it ) {
          }
          child_it = it;
        }
        mode = Mode::NEXT;
      } else {
        mode = Mode::DONE;
      }
      break;
    }
  }

  return json;
}
} // namespace string_tree::json_export
