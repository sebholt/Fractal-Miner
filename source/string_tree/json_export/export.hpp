/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <string_tree/branch.hpp>
#include <QString>

namespace string_tree::json_export
{

// -- Types

struct Char_Escape
{
  char cha;
  char esc[ 2 ];
};

typedef std::array< Char_Escape, 8 > Char_Escapes;

// -- Variables
extern Char_Escapes char_escapes;

// -- Functions

extern void
append_escaped ( QString & json_n, const QString & text_n );

extern QString
export_compressed ( const Branch & root_n );
} // namespace string_tree::json_export
