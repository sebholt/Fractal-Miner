/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <string_tree/branch.hpp>
#include <QString>

namespace string_tree::json_export
{

/// @brief Serializes a branch to a UTF-8 encoded Json object string
///
class Exporter
{
  public:
  // -- Constructors

  Exporter ();

  ~Exporter ();

  // -- Feed

  /// @brief Serializes the tree to utf8 json
  QString
  serialize ( const Branch & branch_n );

  // -- Indentation string

  QString
  indentation () const
  {
    return _indentation;
  }

  void
  set_indentation ( const QString & string_n );

  private:
  QString _indentation;
};
} // namespace string_tree::json_export
