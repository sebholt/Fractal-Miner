/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "exporter.hpp"
#include <string_tree/json_export/export.hpp>
#include <string_tree/leaf.hpp>

namespace string_tree::json_export
{

// -- Static functions

static void
append_indent ( QString & json_n,
                const QString & indent_n,
                unsigned int depth_n )
{
  for ( unsigned int ii = 0; ii != depth_n; ++ii ) {
    json_n.append ( indent_n );
  }
}

// -- Class methods

Exporter::Exporter ()
: _indentation ( "\t" )
{
}

Exporter::~Exporter () {}

QString
Exporter::serialize ( const Branch & root_n )
{
  // -- Types
  typedef Branch::Children::const_iterator Children_Iterator;
  enum class Mode
  {
    DONE,
    UP,
    EVAL,
    NEXT,
    DOWN
  };

  // -- Algorithm
  // Persistent variables
  QString json;
  Mode mode = Mode::UP;
  std::size_t depth ( 0 );
  const Branch * parent ( &root_n );
  Children_Iterator child_it;
  const Node * child ( nullptr );

  while ( mode != Mode::DONE ) {
    switch ( mode ) {
    case Mode::DONE:
      break;

    case Mode::UP:
      ++depth;
      json.push_back ( '{' );
      json.push_back ( '\n' );
      if ( !parent->children ().empty () ) {
        // Select first child
        child_it = parent->children ().begin ();
        child = child_it.value ();
        mode = Mode::EVAL;
      } else {
        // Go down immediately
        mode = Mode::DOWN;
      }
      break;

    case Mode::EVAL:
      // Append key string
      append_indent ( json, _indentation, depth );
      append_escaped ( json, child_it.key () );
      json.push_back ( ':' );
      json.push_back ( ' ' );
      // Evaluate child node type
      switch ( child->type () ) {
      case Node::Type::LEAF:
        // Append leaf value
        append_escaped ( json, child->as_leaf ().value () );
        mode = Mode::NEXT;
        break;
      case Node::Type::BRANCH:
        // Go up
        parent = &child->as_branch ();
        mode = Mode::UP;
        break;
      }
      break;

    case Mode::NEXT:
      ++child_it;
      if ( child_it != parent->children ().end () ) {
        json.push_back ( ',' );
        json.push_back ( '\n' );
        child = child_it.value ();
        mode = Mode::EVAL;
      } else {
        json.push_back ( '\n' );
        mode = Mode::DOWN;
      }
      break;

    case Mode::DOWN:
      --depth;
      append_indent ( json, _indentation, depth );
      json.push_back ( '}' );
      if ( parent != &root_n ) {
        child = parent;
        parent = child->parent ();
        {
          Children_Iterator it ( parent->children ().begin () );
          Children_Iterator it_end ( parent->children ().end () );
          for ( ; ( it != it_end ) && ( it.value () != child ); ++it ) {
          }
          child_it = it;
        }
        mode = Mode::NEXT;
      } else {
        mode = Mode::DONE;
      }
      break;
    }
  }
  json.push_back ( '\n' );

  return json;
}

void
Exporter::set_indentation ( const QString & string_n )
{
  _indentation = string_n;
}
} // namespace string_tree::json_export
