/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <atomic>
#include <thread>

namespace event
{

/// @brief Spinlock
///
class Spinlock
{
  public:
  // -- Constructors

  Spinlock () = default;

  ~Spinlock () = default;

  /// @brief Tries to lock the spinlock and returns immediately
  /// @return True if the spinlock was locked
  bool
  try_lock () noexcept
  {
    return !_locked.test_and_set ( std::memory_order_acquire );
  }

  /// @brief Lock the spinlock
  ///
  void
  lock () noexcept
  {
    do {
      for ( unsigned int ii = 4096; ii != 0; --ii ) {
        if ( try_lock () ) {
          return;
        }
      }
      std::this_thread::yield ();
    } while ( true );
  }

  void
  unlock () noexcept
  {
    _locked.clear ( std::memory_order_release );
  }

  private:
  std::atomic_flag _locked = ATOMIC_FLAG_INIT;
};
} // namespace event
