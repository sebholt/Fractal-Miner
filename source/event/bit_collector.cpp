/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "bit_collector.hpp"

namespace event
{

Bit_Collector::~Bit_Collector () {}

void
Bit_Collector::clear_events ()
{
  // Default implementation
  {
    std::lock_guard< std::mutex > lock ( mutex () );
    events_ref () = 0;
  }
}

void
Bit_Collector::clear_events ( Int_Type events_n )
{
  // Default implementation
  Int_Type mask = ~events_n;
  {
    std::lock_guard< std::mutex > lock ( mutex () );
    events_ref () &= mask;
  }
}

void
Bit_Collector::fetch_events ( Int_Type & events_n )
{
  // Default implementation
  {
    std::lock_guard< std::mutex > lock ( mutex () );
    events_n = events_ref ();
    events_ref () = 0;
  }
}

void
Bit_Collector::read_events ( Int_Type & events_n )
{
  {
    std::lock_guard< std::mutex > lock ( mutex () );
    events_n = events_ref ();
  }
}
} // namespace event
