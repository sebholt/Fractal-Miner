/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/event.hpp>
#include <debug_assert.hpp>
#include <utility>

namespace event
{

/// @brief Simple event chain tracker
///
class Chain
{
  public:
  // -- Constructors

  Chain ()
  : _front ( nullptr )
  , _back ( nullptr )
  {
  }

  Chain ( const Chain & chain_n ) = delete;

  Chain ( Chain && chain_n )
  : _front ( chain_n._front )
  , _back ( chain_n._back )
  {
    chain_n._front = nullptr;
    chain_n._back = nullptr;
  }

  ~Chain () {}

  // -- Size

  /// @return True if the chain is empty
  bool
  is_empty () const noexcept
  {
    return ( _front == nullptr );
  }

  /// @return True if there is exactly on event in the chain
  bool
  single_event () const
  {
    return ( !is_empty () && ( _front == _back ) );
  }

  /// @brief Counts the events in the queue
  std::size_t
  count_events () const noexcept;

  // -- Ends

  /// @return Event at the front of the chain
  Event *
  front () const noexcept
  {
    return _front;
  }

  /// @return Event at the back of the chain
  Event *
  back () const noexcept
  {
    return _back;
  }

  // -- Push

  void
  push_back ( Event * event_n ) noexcept
  {
    DEBUG_ASSERT ( event_n != nullptr );
    DEBUG_ASSERT ( !event_n->is_chained () );
    if ( _back != nullptr ) {
      event_n->chain_link_ref ().prev = _back;
      event_n->chain_link_ref ().next = event_n;
      _back->chain_link_ref ().next = event_n;
      _back = event_n;
    } else {
      // First and only event
      event_n->chain_link_ref ().prev = event_n;
      event_n->chain_link_ref ().next = event_n;
      _front = event_n;
      _back = event_n;
    }
  }

  void
  push_front ( Event * event_n ) noexcept
  {
    DEBUG_ASSERT ( event_n != nullptr );
    DEBUG_ASSERT ( !event_n->is_chained () );
    if ( _front != nullptr ) {
      event_n->chain_link_ref ().prev = event_n;
      event_n->chain_link_ref ().next = _front;
      _front->chain_link_ref ().prev = event_n;
      _front = event_n;
    } else {
      // First and only event
      event_n->chain_link_ref ().prev = event_n;
      event_n->chain_link_ref ().next = event_n;
      _front = event_n;
      _back = event_n;
    }
  }

  // -- Pop

  /// @brief Assumes !is_empty()
  /// @return Pointer to the popped front event
  Event *
  pop_front_not_empty () noexcept
  {
    Event * res = _front;
    Event * const enext ( _front->chain_link ().next );
    // Update chain end pointers
    if ( _front != enext ) {
      _front = enext;
      _front->chain_link_ref ().prev = _front;
    } else {
      // Next pointer to self -> last event
      _front = nullptr;
      _back = nullptr;
    }
    res->chain_clear ();
    return res;
  }

  /// @brief Tries to pop the front event
  /// @return Not nullptr to the popped front event if the chain wasn't empty
  Event *
  pop_front () noexcept
  {
    if ( !is_empty () ) {
      return pop_front_not_empty ();
    }
    return nullptr;
  }

  /// @brief Assumes !is_empty()
  Event *
  pop_back_not_empty () noexcept
  {
    Event * res = _back;
    Event * const eprev ( _back->chain_link ().prev );
    // Update chain end pointers
    if ( _back != eprev ) {
      _back = eprev;
      _back->chain_link_ref ().next = _back;
    } else {
      // Prev pointer to self -> last event
      _front = nullptr;
      _back = nullptr;
    }
    res->chain_clear ();
    return res;
  }

  /// @brief Tries to pop the back event
  // @return Not nullptr if the chain wasn't empty
  Event *
  pop_back () noexcept
  {
    if ( !is_empty () ) {
      return pop_back_not_empty ();
    }
    return nullptr;
  }

  // -- Remove

  /// @brief Removes event_n from the chain.
  /// @arg event_n must be in the chain!
  void
  remove ( Event * event_n );

  // -- Swapping / moving

  void
  swap ( Chain & chain_n ) noexcept
  {
    std::swap ( _front, chain_n._front );
    std::swap ( _back, chain_n._back );
  }

  /// @brief Moves to this
  void
  move_assign ( Chain && chain_n ) noexcept
  {
    // Copy from source
    _front = chain_n._front;
    _back = chain_n._back;
    // Clear source
    chain_n._front = nullptr;
    chain_n._back = nullptr;
  }

  /// @brief Moves src_n events to the back of chain_n
  static void
  move_append_back ( Chain & src_n, Chain & dst_n ) noexcept;

  // -- Assignment operators

  Chain &
  operator= ( const Chain & chain_n ) = delete;

  Chain &
  operator= ( Chain && chain_n ) noexcept
  {
    move_assign ( std::move ( chain_n ) );
    return *this;
  }

  private:
  Event * _front;
  Event * _back;
};

/// @brief Chain walker
///
class Chain_Forward_Walker
{
  public:
  Chain_Forward_Walker ( Chain const & chain_n )
  : _event ( chain_n.front () )
  {
  }

  bool
  operator++ ()
  {
    Event * next ( _event->chain_link ().next );
    if ( _event != next ) {
      _event = next;
      return true;
    }
    return false;
  }

  Event *
  event () const
  {
    return _event;
  }

  operator Event * () { return _event; }

  Event *
  operator-> ()
  {
    return _event;
  }

  private:
  Event * _event;
};
} // namespace event
