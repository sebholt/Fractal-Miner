/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/event.hpp>
#include <debug_assert.hpp>
#include <utility>

namespace event
{

/// @brief Simple event stack tracker
///
class Stack
{
  public:
  // -- Constructors

  Stack ()
  : _top ( nullptr )
  {
  }

  Stack ( const Stack & stack_n ) = delete;

  Stack ( Stack && stack_n )
  : _top ( stack_n._top )
  {
    stack_n._top = nullptr;
  }

  ~Stack (){};

  // -- Ends

  Event *
  top () const noexcept
  {
    return _top;
  }

  bool
  is_empty () const noexcept
  {
    return ( _top == nullptr );
  }

  /// @brief Counts the events in the queue
  std::size_t
  count_events () const noexcept;

  // -- Push

  void
  push ( Event * event_n ) noexcept
  {
    DEBUG_ASSERT ( event_n != nullptr );
    DEBUG_ASSERT ( !event_n->is_chained () );
    if ( _top != 0 ) {
      event_n->chain_link_ref ().prev = _top;
      _top = event_n;
    } else {
      // First and only event
      event_n->chain_link_ref ().prev = event_n;
      _top = event_n;
    }
  }

  // -- Pop

  /// @brief Assumes !is_empty()
  /// @return Pointer to the popped top event
  Event *
  pop_not_empty () noexcept
  {
    Event * res ( _top );
    Event * const eprev ( _top->chain_link ().prev );
    // Update top pointer
    if ( _top != eprev ) {
      _top = eprev;
    } else {
      _top = nullptr;
    }
    res->chain_clear ();
    return res;
  }

  /// @brief Tries to pop the top event
  // @return Not nullptr if the stack wasn't empty
  Event *
  pop () noexcept
  {
    if ( !is_empty () ) {
      return pop_not_empty ();
    }
    return nullptr;
  }

  // -- Moving

  void
  swap ( Stack & stack_n ) noexcept
  {
    std::swap ( _top, stack_n._top );
  }

  void
  move_assign ( Stack && stack_n ) noexcept
  {
    // Copy from source
    _top = stack_n._top;
    // Clear source
    stack_n._top = nullptr;
  }

  // -- Operators

  Stack &
  operator= ( const Stack & stack_n ) = delete;

  Stack &
  operator= ( Stack && stack_n ) noexcept
  {
    move_assign ( std::move ( stack_n ) );
    return *this;
  }

  private:
  Event * _top;
};
} // namespace event
