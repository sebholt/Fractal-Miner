/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "client.hpp"
#include "queue.hpp"

namespace event::timer_queue
{

template < class CLK >
Client< CLK >::Client ()
{
}

template < class CLK >
Client< CLK >::Client ( Queue * queue_n )
{
  set_queue ( queue_n );
}

template < class CLK >
Client< CLK >::Client ( Queue * queue_n, const Callback & callback_n )
{
  set_queue ( queue_n );
  set_callback ( callback_n );
}

template < class CLK >
Client< CLK >::Client ( Queue * queue_n, Callback && callback_n )
{
  set_queue ( queue_n );
  set_callback ( std::move ( callback_n ) );
}

template < class CLK >
Client< CLK >::Client ( const Callback & callback_n )
{
  set_callback ( callback_n );
}

template < class CLK >
Client< CLK >::Client ( Callback && callback_n )
{
  set_callback ( std::move ( callback_n ) );
}

template < class CLK >
Client< CLK >::~Client ()
{
  clear_queue ();
}

template < class CLK >
bool
Client< CLK >::set_queue ( Queue * queue_n )
{
  if ( _queue != queue_n ) {
    clear_queue ();

    if ( queue_n != nullptr ) {
      queue_n->client_register ();
      _queue = queue_n;
    }
  }
  return queue_is_valid ();
}

template < class CLK >
void
Client< CLK >::clear_queue ()
{
  if ( queue_is_valid () ) {
    stop ();
    _queue->client_unregister ();
    _queue = nullptr;
  }
}

template < class CLK >
void
Client< CLK >::set_callback ( const Callback & callback_n )
{
  stop ();
  _callback = callback_n;
}

template < class CLK >
void
Client< CLK >::set_callback ( Callback && callback_n )
{
  stop ();
  _callback = std::move ( callback_n );
}

template < class CLK >
void
Client< CLK >::set_point ( const Time_Point & time_point_n )
{
  stop ();
  _point = time_point_n;
}

template < class CLK >
void
Client< CLK >::adjust_point ( const Duration & duration_n )
{
  stop ();
  _point += duration_n;
}

template < class CLK >
void
Client< CLK >::set_duration ( const Duration & duration_n )
{
  stop ();
  _duration = duration_n;
}

template < class CLK >
void
Client< CLK >::start_single_point ()
{
  if ( queue_is_valid () ) {
    stop ();
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_point ( const Time_Point & time_point_n )
{
  if ( queue_is_valid () ) {
    stop ();
    _point = time_point_n;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_point_now ()
{
  if ( queue_is_valid () ) {
    stop ();
    _point = _queue->now ();
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_period ( const Time_Point & time_point_n )
{
  if ( queue_is_valid () ) {
    stop ();
    _point = time_point_n;
    _point += _duration;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_period_next ()
{
  if ( queue_is_valid () ) {
    stop ();
    _point += _duration;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_period_next ( const Duration & duration_n )
{
  if ( queue_is_valid () ) {
    stop ();
    _duration = duration_n;
    _point += _duration;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_period_now ()
{
  if ( queue_is_valid () ) {
    stop ();
    _point = _queue->now ();
    _point += _duration;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_period_now ( const Duration & duration_n )
{
  if ( queue_is_valid () ) {
    stop ();
    _duration = duration_n;
    _point = _queue->now ();
    _point += _duration;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_periodical_at_point ()
{
  if ( queue_is_valid () ) {
    stop ();
    _run_state = Client_Run_State::PERIOD;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_periodical_at ( const Time_Point & time_point_n )
{
  if ( queue_is_valid () ) {
    stop ();
    _point = time_point_n;
    _run_state = Client_Run_State::PERIOD;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_periodical_next ()
{
  if ( queue_is_valid () ) {
    stop ();
    _point += _duration;
    _run_state = Client_Run_State::PERIOD;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_periodical_now ( const Time_Point & time_now_n )
{
  if ( queue_is_valid () ) {
    stop ();
    _point = time_now_n;
    _point += _duration;
    _run_state = Client_Run_State::PERIOD;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_periodical_now ()
{
  if ( queue_is_valid () ) {
    stop ();
    _point = _queue->now ();
    _point += _duration;
    _run_state = Client_Run_State::PERIOD;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::stop ()
{
  if ( is_running () ) {
    _run_state = Client_Run_State::IDLE;
    _queue->client_stop ( this );
  }
}

// -- Instantiation
template class Client< std::chrono::steady_clock >;
template class Client< std::chrono::system_clock >;
} // namespace event::timer_queue
