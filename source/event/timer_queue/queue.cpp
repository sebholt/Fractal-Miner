/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "queue.hpp"
#include "client.hpp"

namespace event::timer_queue
{

/// @brief Entry comparison function for lowest element first sorting
template < class CLK >
static inline bool
client_entry_compare ( const typename Queue< CLK >::Client_Entry & ena,
                       const typename Queue< CLK >::Client_Entry & enb )
{
  return ( ena.first > enb.first );
}

template < class CLK >
Queue< CLK >::Queue ()
: _reference_count ( 0 )
{
}

template < class CLK >
Queue< CLK >::~Queue ()
{
  assert ( _reference_count == 0 );
}

template < class CLK >
void
Queue< CLK >::ensure_minimum_capacity ( std::size_t capacity_n )
{
  if ( _heap.capacity () < capacity_n ) {
    _heap.reserve ( capacity_n );
  }
}

template < class CLK >
std::size_t
Queue< CLK >::process_timeouts ( Time_Point time_now_n )
{
  std::size_t res ( 0 );
  while ( !_heap.empty () ) {
    const auto & ifront ( _heap.front () );
    if ( ifront.first <= time_now_n ) {
      {
        // Pop client from queue
        Client< CLK > * client ( ifront.second );
        std::pop_heap ( _heap.begin (), _heap.end () );
        _heap.pop_back ();
        // Process client timeout
        process_timeout ( client );
      }
      ++res;
    } else {
      // Lowest time point is in the future
      break;
    }
  }
  return res;
}

template < class CLK >
inline void
Queue< CLK >::process_timeout ( Client< CLK > * client_n )
{
  // -- Update client run state
  switch ( client_n->run_state () ) {
  case Client_Run_State::SINGLE:
    // Stop client
    client_n->_run_state = Client_Run_State::IDLE;
    break;
  case Client_Run_State::PERIOD:
    // Register client for another period
    client_n->_point += client_n->_duration;
    _heap.emplace_back ( client_n->_point, client_n );
    std::push_heap (
        _heap.begin (), _heap.end (), &client_entry_compare< CLK > );
    break;
  default:
    break;
  }

  // Notify client callback as last action of the client
  // (in case it gets destroyed)
  client_n->notify_callback ();
}

template < class CLK >
void
Queue< CLK >::set_callback_front ( const Callback & callback_n )
{
  _callback_front = callback_n;
}

template < class CLK >
void
Queue< CLK >::set_callback_front ( Callback && callback_n )
{
  _callback_front = std::move ( callback_n );
}

template < class CLK >
void
Queue< CLK >::client_register ()
{
  ensure_minimum_capacity ( _reference_count + 1 );
  ++_reference_count;
}

template < class CLK >
void
Queue< CLK >::client_unregister ()
{
  // Remove client from registers
  DEBUG_ASSERT ( _reference_count != 0 );
  --_reference_count;
}

template < class CLK >
void
Queue< CLK >::client_start ( Client< CLK > * client_n )
{
  // Insert client into timeout heap
  _heap.emplace_back ( client_n->point (), client_n );
  std::push_heap ( _heap.begin (), _heap.end (), &client_entry_compare< CLK > );
  // If this became the front time notify the front change callback
  if ( _heap.front ().second == client_n ) {
    if ( callback_front () ) {
      callback_front () ();
    }
  }
}

template < class CLK >
void
Queue< CLK >::client_stop ( Client< CLK > * client_n )
{
  bool was_first ( false );
  // Remove client from heap
  {
    auto const itb = _heap.begin ();
    auto const ite = _heap.end ();
    auto itc = itb;
    for ( ; itc != ite; ++itc ) {
      if ( itc->second == client_n ) {
        if ( itc == itb ) {
          was_first = true;
        }
        _heap.erase ( itc );
        break;
      }
    }
  }
  // Restore heap
  std::make_heap ( _heap.begin (), _heap.end (), &client_entry_compare< CLK > );
  // If this was the front time notify the front change callback
  if ( was_first ) {
    if ( callback_front () ) {
      callback_front () ();
    }
  }
}

// -- Instantiation
template class Queue< std::chrono::steady_clock >;
template class Queue< std::chrono::system_clock >;
} // namespace event::timer_queue
