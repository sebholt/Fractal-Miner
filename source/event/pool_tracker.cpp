/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "pool_tracker.hpp"
#include "pool_base.hpp"
#include <algorithm>

namespace event
{

Pool_Tracker::Pool_Tracker () {}

Pool_Tracker::~Pool_Tracker ()
{
  DEBUG_ASSERT ( _pool_register.empty () );
}

bool
Pool_Tracker::all_home () const
{
  for ( const auto & entry : _pool_register ) {
    if ( !entry.second->all_home () ) {
      return false;
    }
  }
  return true;
}

bool
Pool_Tracker::all_empty () const
{
  for ( const auto & entry : _pool_register ) {
    if ( !entry.second->is_empty () ) {
      return false;
    }
  }
  return true;
}

bool
Pool_Tracker::all_not_empty () const
{
  for ( const auto & entry : _pool_register ) {
    if ( entry.second->is_empty () ) {
      return false;
    }
  }
  return true;
}

std::size_t
Pool_Tracker::size_outside () const
{
  std::size_t res ( 0 );
  for ( const auto & entry : _pool_register ) {
    res += entry.second->size_outside ();
  }
  return res;
}

void
Pool_Tracker::clear_all_instances () const
{
  for ( const auto & entry : _pool_register ) {
    entry.second->clear ();
  }
}

void
Pool_Tracker::set_all_capacities ( std::size_t capacity_n ) const
{
  for ( const auto & entry : _pool_register ) {
    entry.second->set_capacity ( capacity_n );
  }
}

void
Pool_Tracker::clear_all_capacities () const
{
  for ( const auto & entry : _pool_register ) {
    entry.second->clear_capacity ();
  }
}

event::Pool_Base *
Pool_Tracker::pool_by_event_type ( std::uint32_t event_type_n )
{
  Pool_Register::iterator it ( _pool_register.find ( event_type_n ) );
  if ( it != _pool_register.end () ) {
    return it->second;
  }
  return nullptr;
}

bool
Pool_Tracker::casted_reset_release ( event::Event * event_n )
{
  Pool_Register::iterator it ( _pool_register.find ( event_n->type () ) );
  if ( it != _pool_register.end () ) {
    it->second->casted_reset_release_virtual ( event_n );
    return true;
  }
  return false;
}

void
Pool_Tracker::pool_register ( std::uint32_t event_type_n, Pool_Base * pool_n )
{
  // Detect doubles in debug mode
  DEBUG_ASSERT ( pool_by_event_type ( event_type_n ) == nullptr );
  _pool_register.emplace ( event_type_n, pool_n );
}

void
Pool_Tracker::pool_unregister ( Pool_Base * pool_n )
{
  Pool_Register::iterator it (
      std::find_if ( _pool_register.begin (),
                     _pool_register.end (),
                     [ pool_n ] ( const Pool_Register::value_type & entry ) {
                       return entry.second == pool_n;
                     } ) );
  if ( it != _pool_register.end () ) {
    _pool_register.erase ( it );
  }
}
} // namespace event
