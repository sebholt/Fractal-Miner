/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <event/stack.hpp>
#include <algorithm>

namespace event
{

std::size_t
Stack::count_events () const noexcept
{
  std::size_t res ( 0 );
  if ( !is_empty () ) {
    event::Event * itc ( _top );
    do {
      ++res;
      {
        event::Event * const itprev ( itc->chain_link ().prev );
        if ( itc == itprev ) {
          break;
        }
        itc = itprev;
      }
    } while ( true );
  }
  return res;
}
} // namespace event
