/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>
#include <mutex>

namespace event
{

/// @brief Thread safe bit collector
///
class Bit_Collector
{
  public:
  // -- Types
  typedef std::uint32_t Int_Type;
  /// @brief Abstract event bit notification function type
  ///
  typedef void ( *Func_Notify ) ( Bit_Collector * collector_n,
                                  Int_Type event_bits_n );

  public:
  // -- Constructors

  virtual ~Bit_Collector ();

  // -- Abstract interface

  /// @brief Notifies the event waiter of new events
  ///
  /// @arg events_n The event bits
  virtual void
  notify_events ( Int_Type events_n ) = 0;

  /// @brief Anonymous event bit notification function to call with this
  /// notifier as argument.
  ///
  /// @return The notification function
  virtual Func_Notify
  notify_function () = 0;

  /// @brief Clears all event flags
  ///
  /// Call to ensure there's no pending event.
  virtual void
  clear_events ();

  /// @brief Clears the givven event bits in case they were set before.
  ///
  /// Call to ensure there's no pending event of a a certain type.
  virtual void
  clear_events ( Int_Type events_n );

  /// @brief Fetches all events, sets the new mask and returns immediately
  ///
  /// @arg events_n Incoming notifications will be or'ed to this respecting the
  /// mask
  virtual void
  fetch_events ( Int_Type & events_n );

  // -- Utility interface

  /// @brief Reads the events without changing them
  ///
  void
  read_events ( Int_Type & events_n );

  protected:
  // -- Constructors

  Bit_Collector ( Int_Type events_n = 0 )
  : _events ( events_n )
  {
  }

  // -- Accessors

  std::mutex &
  mutex ()
  {
    return _mutex;
  }

  Int_Type &
  events_ref ()
  {
    return _events;
  }

  private:
  std::mutex _mutex;
  Int_Type _events;
};
} // namespace event
