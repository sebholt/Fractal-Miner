/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "bit_notifier.hpp"
#include "bit_collector.hpp"

namespace event
{

Bit_Notifier::Bit_Notifier ( Int_Type event_bits_n,
                             Bit_Collector * event_collector_n )
: _event_bits ( event_bits_n )
, _collector ( event_collector_n )
, _func_notify ( nullptr )
{
  if ( _collector != nullptr ) {
    _func_notify = _collector->notify_function ();
  }
}

void
Bit_Notifier::set_collector ( Bit_Collector * event_collector_n )
{
  _collector = event_collector_n;
  if ( _collector != nullptr ) {
    _func_notify = _collector->notify_function ();
  } else {
    _func_notify = nullptr;
  }
}

void
Bit_Notifier::clear_collector ()
{
  _collector = nullptr;
  _func_notify = nullptr;
}

void
Bit_Notifier::setup ( Int_Type event_bits_n, Bit_Collector * event_collector_n )
{
  set_event_bits ( event_bits_n );
  set_collector ( event_collector_n );
}
} // namespace event
