/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/pool_tracker.hpp>
#include <event/stack.hpp>
#include <debug_assert.hpp>

namespace event
{

// -- Forward declaration
class Pool_Tracker;

/// @brief Abstract event pool base class
///
class Pool_Base
{
  public:
  // -- Types
  static const std::uint32_t capacity_default = 8;

  public:
  // -- Constructors

  Pool_Base ( std::uint32_t event_type_n, Pool_Tracker & tracker_n );

  Pool_Base ( const Pool_Base & pool_n ) = delete;

  Pool_Base ( Pool_Base && pool_n ) = delete;

  virtual ~Pool_Base ();

  // -- Tracker

  Pool_Tracker &
  tracker ()
  {
    return _tracker;
  }

  const Pool_Tracker &
  tracker () const
  {
    return _tracker;
  }

  // -- Sizes

  /// @return Number of items available in the pool
  ///
  std::uint32_t
  size () const
  {
    return _size;
  }

  /// @return Number of items that were allocated and constructed
  ///
  std::uint32_t
  size_owned () const
  {
    return _size_owned;
  }

  /// @brief Returns true if ( size() == 0 )
  bool
  is_empty () const
  {
    return _stack.is_empty ();
  }

  /// @brief Returns true if ( size() == capacity() )
  bool
  is_full () const
  {
    return ( _size == _capacity );
  }

  /// @brief Returns true if ( size() == size_owned() )
  bool
  all_home () const
  {
    return ( _size == _size_owned );
  }

  /// @brief Returns ( size_owned() - size() )
  std::uint32_t
  size_outside () const
  {
    return ( _size_owned - _size );
  }

  /// @brief Returns ( size_owned() == capacity() )
  bool
  size_owned_eq_capacity () const
  {
    return ( _size_owned == _capacity );
  }

  /// @brief Returns ( size_owned() >= capacity() )
  bool
  size_owned_ge_capacity () const
  {
    return ( _size_owned >= _capacity );
  }

  /// @return Maximum number of events the pool can keep available
  ///
  std::uint32_t
  capacity () const
  {
    return _capacity;
  }

  // -- Abstract interface

  /// @brief Clears all instances
  virtual void
  clear () = 0;

  /// @brief Sets the capacity
  ///
  /// @return True on success
  virtual void
  set_capacity ( std::uint32_t capacity_n ) = 0;

  /// @brief Sets the capacity to 0
  ///
  virtual void
  clear_capacity () = 0;

  /// @brief Static casts to T calls T::reset() and release()s the event
  virtual void
  casted_reset_release_virtual ( Event * event_n ) = 0;

  protected:
  Stack _stack;
  std::uint32_t _size;
  std::uint32_t _size_owned;
  std::uint32_t _capacity;
  Pool_Tracker & _tracker;
};
} // namespace event
