/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include "pool_base.hpp"

namespace event
{

/// @brief Event pool
///
template < class T >
class Pool : public Pool_Base
{
  public:
  // -- Types
  static const std::uint32_t etype = static_cast< std::uint32_t > ( T::etype );

  public:
  // -- Constructors

  Pool ( Pool_Tracker & tracker_n )
  : Pool_Base ( etype, tracker_n )
  {
  }

  Pool ( const Pool & pool_n ) = delete;

  Pool ( Pool && pool_n ) = delete;

  ~Pool () override;

  // -- Capacity

  void
  set_capacity ( std::uint32_t capacity_n ) override;

  void
  clear_capacity () override;

  void
  ensure_minimum_capacity ( std::uint32_t capacity_n )
  {
    if ( capacity () < capacity_n ) {
      set_capacity ( capacity_n );
    }
  }

  void
  ensure_maximum_capacity ( std::uint32_t capacity_n )
  {
    if ( capacity () > capacity_n ) {
      set_capacity ( capacity_n );
    }
  }

  // -- Size and resize

  /// @brief Clears all instaces
  ///
  void
  clear () override;

  /// @brief Ensure there are exactly size_n objects available
  ///
  template < typename... Args >
  void
  set_size ( std::uint32_t size_n, Args &&... args_n );

  /// @brief Ensure there are at least size_n objects available
  ///
  template < typename... Args >
  void
  ensure_minimum_size ( std::uint32_t size_n, Args &&... args_n );

  /// @brief Ensure there are at maximum size_n objects available
  ///
  template < typename... Args >
  void
  ensure_maximum_size ( std::uint32_t size_n );

  /// @brief Allocates and constructs a new event and
  ///        adds it to the pool
  ///
  template < typename... Args >
  void
  emplace ( Args &&... args_n );

  /// @brief Allocates and constructs a number of new events and
  ///        adds it to the pool
  ///
  template < typename... Args >
  void
  emplace_num ( std::uint32_t num_n, Args &&... args_n );

  // -- Pop / release

  /// @brief Takes an object from the pool or allocates and constructs an new
  /// one on demand
  ///
  template < typename... Args >
  T *
  acquire ( Args &&... args_n )
  {
    if ( !is_empty () ) {
      return pop_not_empty ();
    }
    return new_event ( std::forward< Args > ( args_n )... );
  }

  /// @brief Pops an event and returns it in event_n
  /// @return True when the pool was not empty and an event was returned
  bool
  try_pop ( T ** event_n )
  {
    if ( !is_empty () ) {
      *event_n = pop_not_empty ();
      return true;
    }
    return false;
  }

  /// @brief Pops an event and returns it
  ///
  /// Assumes that !is_empty()
  T *
  pop_not_empty ();

  /// @brief Pops an event and deletes it
  void
  drop_not_empty ();

  /// @brief Pops a number of events and deletes them
  void
  drop_num_not_empty ( std::uint32_t num_n );

  /// @brief Pushes the object to the pool unless is_full(). Deletes it
  /// otherwise.
  ///
  void
  release ( T * event_n );

  /// @brief Calls T::reset() on the event an release()es it
  void
  reset_release ( T * event_n );

  /// @brief Static casts to T  and release()s the event
  void
  casted_release ( Event * event_n )
  {
    release ( static_cast< T * > ( event_n ) );
  }

  /// @brief Static casts to T calls T::reset() and release()s the event
  void
  casted_reset_release ( Event * event_n )
  {
    reset_release ( static_cast< T * > ( event_n ) );
  }

  /// @brief Static casts to T calls T::reset() and release()s the event
  void
  casted_reset_release_virtual ( Event * event_n ) override;

  // -- Assignhment operators

  Pool &
  operator= ( const Pool & pool_n ) = delete;

  Pool &
  operator= ( Pool && pool_n ) = delete;

  private:
  // -- Utility

  void
  release_inline ( T * event_n )
  {
    DEBUG_ASSERT ( size () < size_owned () );
    if ( !is_full () ) {
      _stack.push ( static_cast< Event * > ( event_n ) );
      ++_size;
    } else {
      delete_event ( event_n );
    }
  }

  /// @brief Allocates and constructs a new event
  ///
  /// @return True on allocation success
  template < typename... Args >
  T *
  new_event ( Args &&... args_n )
  {
    T * res ( new T ( std::forward< Args > ( args_n )... ) );
    ++_size_owned;
    return res;
  }

  /// @brief Destructs and deallocates an object
  void
  delete_event ( T * pointer_n )
  {
    DEBUG_ASSERT ( _size_owned != 0 );
    --_size_owned;
    delete pointer_n;
  }
};

template < class T >
Pool< T >::~Pool ()
{
  DEBUG_ASSERT ( all_home () );
  clear ();
}

template < class T >
void
Pool< T >::clear ()
{
  drop_num_not_empty ( _size );
}

template < class T >
void
Pool< T >::set_capacity ( std::uint32_t capacity_n )
{
  // Destroy overcounting items
  if ( size () > capacity_n ) {
    drop_num_not_empty ( size () - capacity_n );
  }
  _capacity = capacity_n;
}

template < class T >
void
Pool< T >::clear_capacity ()
{
  if ( capacity () != 0 ) {
    clear ();
    _capacity = 0;
  }
}

template < class T >
template < typename... Args >
void
Pool< T >::set_size ( std::uint32_t size_n, Args &&... args_n )
{
  if ( size () != size_n ) {
    if ( size () < size_n ) {
      emplace_num ( ( size_n - size () ), std::forward< Args > ( args_n )... );
    } else {
      drop_num_not_empty ( size () - size_n );
    }
  }
}

template < class T >
template < typename... Args >
void
Pool< T >::ensure_minimum_size ( std::uint32_t size_n, Args &&... args_n )
{
  if ( _size < size_n ) {
    emplace_num ( ( size_n - _size ), std::forward< Args > ( args_n )... );
    // Adjust capacity
    _capacity = std::max ( _capacity, _size );
  }
}

template < class T >
template < typename... Args >
void
Pool< T >::ensure_maximum_size ( std::uint32_t size_n )
{
  if ( _size > size_n ) {
    drop_num_not_empty ( _size - size_n );
  }
}

template < class T >
template < typename... Args >
void
Pool< T >::emplace ( Args &&... args_n )
{
  T * event ( new_event ( std::forward< Args > ( args_n )... ) );
  _stack.push ( event );
  // Adjust size and capacity
  ++_size;
  _capacity = std::max ( _capacity, _size );
}

template < class T >
template < typename... Args >
void
Pool< T >::emplace_num ( std::uint32_t num_n, Args &&... args_n )
{
  for ( ; num_n != 0; --num_n ) {
    emplace ( std::forward< Args > ( args_n )... );
  }
}

template < class T >
T *
Pool< T >::pop_not_empty ()
{
  DEBUG_ASSERT ( _size != 0 );
  T * res ( static_cast< T * > ( _stack.top () ) );
  _stack.pop_not_empty ();
  --_size;
  return res;
}

template < class T >
void
Pool< T >::drop_not_empty ()
{
  DEBUG_ASSERT ( _size != 0 );
  event::Event * event ( _stack.top () );
  _stack.pop_not_empty ();
  --_size;
  delete_event ( static_cast< T * > ( event ) );
}

template < class T >
void
Pool< T >::drop_num_not_empty ( std::uint32_t num_n )
{
  for ( ; num_n != 0; --num_n ) {
    drop_not_empty ();
  }
}

template < class T >
void
Pool< T >::release ( T * event_n )
{
  release_inline ( event_n );
}

template < class T >
void
Pool< T >::reset_release ( T * event_n )
{
  event_n->reset ();
  release_inline ( event_n );
}

template < class T >
void
Pool< T >::casted_reset_release_virtual ( Event * event_n )
{
  DEBUG_ASSERT ( event_n->type () == etype );
  T * cevent ( static_cast< T * > ( event_n ) );
  cevent->reset ();
  release_inline ( cevent );
}
} // namespace event
