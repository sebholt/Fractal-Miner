/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>

namespace event
{

// -- Forward declaration
class Bit_Collector;

/// @brief Notifies an abstract bit collector
///
class Bit_Notifier
{
  public:
  // -- Types

  typedef std::uint32_t Int_Type;
  typedef void ( *Func_Notify ) ( Bit_Collector * collector_n,
                                  Int_Type event_n );

  public:
  // -- Constructors

  Bit_Notifier ()
  : _event_bits ( 0 )
  , _collector ( nullptr )
  , _func_notify ( nullptr )
  {
  }

  Bit_Notifier ( Int_Type event_bits_n, Bit_Collector * bit_collector_n );

  Bit_Notifier ( const Bit_Notifier & notifier_n )
  : _event_bits ( notifier_n._event_bits )
  , _collector ( notifier_n._collector )
  , _func_notify ( notifier_n._func_notify )
  {
  }

  // -- Event bits

  /// @brief Event bits to set in the collector when notify() is called.
  Int_Type
  event_bits () const
  {
    return _event_bits;
  }

  void
  set_event_bits ( Int_Type event_bits_n )
  {
    _event_bits = event_bits_n;
  }

  // -- Bit collector connection

  /// @brief Bit collector
  ///
  Bit_Collector *
  collector () const
  {
    return _collector;
  }

  void
  set_collector ( Bit_Collector * collector_n );

  void
  clear_collector ();

  // -- Setup state

  /// @brief Clears the event bits and the bit collector reference
  void
  clear ()
  {
    _event_bits = 0;
    _collector = nullptr;
    _func_notify = nullptr;
  }

  /// @brief Sets the event bits and the bit collector reference
  void
  setup ( Int_Type event_bits_n, Bit_Collector * bit_collector_n );

  /// @return True if the event bits and the collector reference are valid
  bool
  is_ready () const
  {
    return ( ( _event_bits != 0 ) && ( _collector != nullptr ) );
  }

  // -- Bit collector nit´otification

  void
  notify () const
  {
    if ( _func_notify != nullptr ) {
      _func_notify ( _collector, _event_bits );
    }
  }

  // -- Operators

  Bit_Notifier &
  operator= ( const Bit_Notifier & notifier_n )
  {
    _event_bits = notifier_n._event_bits;
    _collector = notifier_n._collector;
    _func_notify = notifier_n._func_notify;
    return *this;
  }

  private:
  Int_Type _event_bits;
  Bit_Collector * _collector;
  Func_Notify _func_notify;
};
} // namespace event
