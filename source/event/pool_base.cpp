/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "pool_base.hpp"
#include "pool_tracker.hpp"

namespace event
{

Pool_Base::Pool_Base ( std::uint32_t event_type_n, Pool_Tracker & tracker_n )
: _size ( 0 )
, _size_owned ( 0 )
, _capacity ( capacity_default )
, _tracker ( tracker_n )
{
  _tracker.pool_register ( event_type_n, this );
}

Pool_Base::~Pool_Base ()
{
  DEBUG_ASSERT ( all_home () );
  DEBUG_ASSERT ( is_empty () );
  _tracker.pool_unregister ( this );
}
} // namespace event
