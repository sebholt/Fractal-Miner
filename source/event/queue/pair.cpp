/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <event/queue/pair.hpp>

namespace event::queue
{

Pair::Pair ()
: _first ( Reference::created () )
, _second ( Reference::created () )
{
}

Pair::~Pair () {}
} // namespace event::queue