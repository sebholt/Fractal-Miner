/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <utility>

namespace event::queue
{

// -- Forward declaration
class Queue;

/// @brief Reference to a shared queue
///
/// The last destroyed reference deletes the shared queue.
class Reference
{
  public:
  // -- Constructors

  /// @brief Constructs an unconnected reference
  Reference ();

  /// @brief Makes this another reference to the queue
  Reference ( Queue * queue_n );

  /// @brief Makes this another reference to the queue
  Reference ( const Reference & ref_n );

  /// @brief Takes over the reference and leaves it in an unconnected state
  Reference ( Reference && ref_n );

  ~Reference () { clear (); }

  // -- Queue accessors

  /// @return True if connected to a queue
  bool
  is_valid () const
  {
    return ( _queue != nullptr );
  }

  Queue *
  get () const
  {
    return _queue;
  }

  /// @brief Drops and resets the queue reference
  void
  reset ( Queue * queue_n );

  // -- Create / clear

  /// @brief Generates a new queue
  static Reference
  created ();

  void
  clear ();

  /// @brief Moves the reference to this
  void
  move_assign ( Reference && ref_n );

  // -- Type cast operators

  operator Queue * () const { return _queue; }

  Queue *
  operator-> () const
  {
    return _queue;
  }

  // -- Comparison operators

  bool
  operator== ( const Reference & ref_n ) const
  {
    return ( _queue == ref_n._queue );
  }

  bool
  operator!= ( const Reference & ref_n ) const
  {
    return ( _queue != ref_n._queue );
  }

  bool
  operator== ( Queue * queue_n ) const
  {
    return ( _queue == queue_n );
  }

  bool
  operator!= ( Queue * queue_n ) const
  {
    return ( _queue != queue_n );
  }

  // -- Assignment operators

  /// @brief Makes this another reference to the queue
  Reference &
  operator= ( const Reference & ref_n )
  {
    reset ( ref_n._queue );
    return *this;
  }

  /// @brief Takes over the reference and leaves it in an unconnected state
  Reference &
  operator= ( Reference && ref_n )
  {
    move_assign ( std::move ( ref_n ) );
    return *this;
  }

  /// @brief Makes this another reference to the queue
  Reference &
  operator= ( Queue * queue_n )
  {
    reset ( queue_n );
    return *this;
  }

  private:
  static void
  decrement_ref_count ( Queue * queue_n );

  private:
  Queue * _queue;
};

// -- Type definitions
typedef std::pair< Reference, Reference > Reference_Pair;
} // namespace event::queue