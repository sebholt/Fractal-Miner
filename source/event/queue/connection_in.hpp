/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_notifier.hpp>
#include <event/queue/connection.hpp>

namespace event::queue
{

/// @brief Push connection to a event::Queue with local event chain
///
class Connection_In : public Connection
{
  public:
  // -- Constructors

  Connection_In ();

  Connection_In ( const Connection_In & connection_n ) = delete;

  Connection_In ( Connection_In && connection_n ) = delete;

  ~Connection_In ();

  // -- Accessors

  using Connection::is_empty;

  // -- Setup

  /// @brief Disconnects and clears the bit notifier and event chain
  ///
  void
  clear ();

  // -- Bit notifier

  /// @brief The bit notifier gets passed to the queue for new event callback
  ///
  const Bit_Notifier &
  bit_notifier () const
  {
    return _bit_notifier;
  }

  void
  set_bit_notifier ( const Bit_Notifier & notifier_n );

  void
  set_bit_notifier ( std::uint32_t event_bits_n,
                     Bit_Collector * bit_collector_n )
  {
    set_bit_notifier ( Bit_Notifier ( event_bits_n, bit_collector_n ) );
  }

  void
  clear_bit_notifier ();

  // -- Queue connection

  using Connection::is_connected;
  using Connection::queue;

  /// @brief Connects to a given queue
  ///
  /// @return True on success
  bool
  connect ( const Reference & reference_n );

  bool
  connect ( Reference && reference_n );

  void
  disconnect ();

  // -- Queue reading

  /// @return True if events were fetched
  bool
  read_from_queue ();

  /// @brief Reads from the queue if any event bit
  ///        from bit_notifier() is set in events_n
  ///
  /// The bit_notifier() event bits get cleared() in events_n.flags()
  /// @return True if events were fetched
  bool
  read_from_queue_checked ( Bit_Notifier::Int_Type & events_n );

  // -- Single event processing

  event::Event *
  front () const
  {
    return chain ().front ();
  }

  event::Event *
  pop ()
  {
    return chain_ref ().pop_front ();
  }

  event::Event *
  pop_not_empty ()
  {
    return chain_ref ().pop_front_not_empty ();
  }

  /// @brief Push back to the front of the qeueue
  void
  repush ( event::Event * event_n )
  {
    chain_ref ().push_front ( event_n );
  }

  // -- Operators

  Connection_In &
  operator= ( const Connection_In & connection_n ) = delete;

  Connection_In &
  operator= ( Connection_In && connection_n ) = delete;

  private:
  Bit_Notifier _bit_notifier;
};
} // namespace event::queue