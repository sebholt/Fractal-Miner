/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/chain.hpp>
#include <event/queue/reference.hpp>
#include <array>

namespace event::queue
{

/// @brief Base class for a connection to a event::Queue with
///        a local event chain
///
class Connection
{
  public:
  // -- Local event chain

  const Chain &
  chain () const
  {
    return _chain;
  }

  bool
  is_empty () const
  {
    return _chain.is_empty ();
  }

  // -- Queue

  const Reference &
  queue () const
  {
    return _queue;
  }

  bool
  is_connected () const
  {
    return _queue.is_valid ();
  }

  // -- Operators

  Connection &
  operator= ( const Connection & connection_n ) = delete;

  Connection &
  operator= ( Connection && connection_n ) = delete;

  protected:
  // -- Construcctors

  Connection ();

  Connection ( const Connection & connection_n ) = delete;

  Connection ( Connection && connection_n ) = delete;

  ~Connection ();

  // -- Accessors

  Chain &
  chain_ref ()
  {
    return _chain;
  }

  Reference &
  queue_ref ()
  {
    return _queue;
  }

  private:
  Chain _chain;
  Reference _queue;
};
} // namespace event::queue