/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/queue/reference.hpp>

namespace event::queue
{

// -- Forward declaration
class Queue;

/// @brief Event queue pair
///
class Pair
{
  public:
  // -- Constructors

  /// @brief Creates two queues
  ///
  Pair ();

  /// @brief Disable copy constructor
  ///
  Pair ( const Pair & queues_n ) = delete;

  /// @brief Move constructor
  ///
  Pair ( Pair && queues_n ) = default;

  ~Pair ();

  // -- Queues

  const Reference &
  first () const
  {
    return _first;
  }

  const Reference &
  second () const
  {
    return _second;
  }

  Reference_Pair
  end_a () const
  {
    return Reference_Pair ( _first, _second );
  }

  Reference_Pair
  end_b () const
  {
    return Reference_Pair ( _second, _first );
  }

  // -- Operators

  Pair &
  operator= ( const Pair & queues_n ) = delete;

  Pair &
  operator= ( Pair && queues_n ) = default;

  private:
  Reference _first;
  Reference _second;
};
} // namespace event::queue