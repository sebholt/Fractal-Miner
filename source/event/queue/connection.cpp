/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <event/queue/connection.hpp>

namespace event::queue
{

Connection::Connection () {}

Connection::~Connection ()
{
  DEBUG_ASSERT ( is_empty () );
}
} // namespace event::queue