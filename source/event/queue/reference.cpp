/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <event/queue/queue.hpp>
#include <event/queue/reference.hpp>

namespace event::queue
{

Reference::Reference ()
: Reference ( nullptr )
{
}

Reference::Reference ( Queue * queue_n )
: _queue ( queue_n )
{
  if ( _queue != nullptr ) {
    _queue->ref_count_add ();
  }
}

Reference::Reference ( const Reference & ref_n )
: _queue ( ref_n._queue )
{
  // This is another reference
  if ( _queue != nullptr ) {
    _queue->ref_count_add ();
  }
}

Reference::Reference ( Reference && ref_n )
: _queue ( ref_n._queue )
{
  // We took over the source's reference
  if ( _queue != nullptr ) {
    ref_n._queue = nullptr;
  }
}

void
Reference::clear ()
{
  if ( _queue != nullptr ) {
    decrement_ref_count ( _queue );
    _queue = nullptr;
  }
}

Reference
Reference::created ()
{
  return Reference ( new Queue );
}

void
Reference::reset ( Queue * queue_n )
{
  Queue * queue_prev ( _queue );
  _queue = queue_n;
  if ( _queue != nullptr ) {
    _queue->ref_count_add ();
  }
  if ( queue_prev != nullptr ) {
    decrement_ref_count ( queue_prev );
  }
}

void
Reference::move_assign ( Reference && ref_n )
{
  if ( &ref_n != this ) {
    clear ();
    if ( ref_n._queue != nullptr ) {
      _queue = ref_n._queue;
      ref_n._queue = nullptr;
    }
  }
}

void
Reference::decrement_ref_count ( Queue * queue_n )
{
  if ( queue_n->ref_count_sub () == 0 ) {
    delete queue_n;
  }
}
} // namespace event::queue