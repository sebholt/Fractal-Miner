/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <event/queue/connection_out.hpp>
#include <event/queue/queue.hpp>
#include <debug_assert.hpp>

namespace event::queue
{

Connection_Out::Connection_Out ()
: _empty_push ( true )
{
}

Connection_Out::~Connection_Out () {}

void
Connection_Out::clear ()
{
  disconnect ();
  _push_bit_notifier.clear ();
  _pop_bit_notifier.clear ();
}

void
Connection_Out::set_push_bit_notifier ( const Bit_Notifier & notifier_n,
                                        bool call_on_demand_n )
{
  _push_bit_notifier = notifier_n;
  if ( !_empty_push && call_on_demand_n ) {
    _push_bit_notifier.notify ();
  }
}

void
Connection_Out::set_pop_bit_notifier ( const Bit_Notifier & notifier_n )
{
  _pop_bit_notifier = notifier_n;
  if ( is_connected () ) {
    queue ()->set_pop_notifier ( _pop_bit_notifier );
  }
}

bool
Connection_Out::connect ( Queue * queue_n )
{
  disconnect ();
  if ( queue_n != nullptr ) {
    Connection::queue_ref ().reset ( queue_n );
    queue ()->set_pop_notifier ( _pop_bit_notifier );
    return true;
  }
  return false;
}

void
Connection_Out::disconnect ()
{
  if ( is_connected () ) {
    queue ()->clear_pop_notifier ();
    Connection::queue_ref ().clear ();
  }
}

bool
Connection_Out::feed_into_queue ()
{
  if ( is_connected () ) {
    if ( !_empty_push ) {
      _empty_push = true;
      queue ()->push_chain ( Connection::chain_ref () );
      return true;
    }
  }
  return false;
}

bool
Connection_Out::feed_into_queue_checked ( Bit_Notifier::Int_Type & events_n )
{
  if ( is_connected () ) {
    if ( !_empty_push ) {
      Bit_Notifier::Int_Type const nbits = _push_bit_notifier.event_bits ();
      if ( !_empty_push && ( ( events_n & nbits ) != 0 ) ) {
        events_n &= ( ~nbits );
        _empty_push = true;
        queue ()->push_chain ( Connection::chain_ref () );
        return true;
      }
    }
  }
  return false;
}
} // namespace event::queue