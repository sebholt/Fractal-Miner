/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/queue/connection_in.hpp>
#include <event/queue/connection_out.hpp>

namespace event::queue
{

/// @brief Pop and out connection pair for one end of a
///        event::Queue_Pair < N >
///
class Connections_IO
{
  public:
  // -- Constructors

  Connections_IO ();

  Connections_IO ( const Connections_IO & connection_n ) = delete;

  Connections_IO ( Connections_IO && connection_n ) = delete;

  ~Connections_IO ();

  // -- Accessors in

  Connection_In &
  in ()
  {
    return _in;
  }

  const Connection_In &
  in () const
  {
    return _in;
  }

  // -- Accessors out

  Connection_Out &
  out ()
  {
    return _out;
  }

  const Connection_Out &
  out () const
  {
    return _out;
  }

  // -- Setup

  bool
  all_empty () const;

  /// @brief Clears in() and out()
  ///
  void
  clear ();

  // -- Connection

  /// @brief Connects to the given queues
  ///
  /// @return True on success
  bool
  connect ( Reference_Pair queues_n );

  /// @brief Disconnects in() and out()
  ///
  void
  disconnect ();

  bool
  all_connected () const;

  // -- Incoming queue

  /// @return True if any events were fetched
  ///         ring().size() was increased by this.
  bool
  read_from_queue ()
  {
    return _in.read_from_queue ();
  }

  void
  read_from_queue_checked ( Bit_Notifier::Int_Type & events_n )
  {
    _in.read_from_queue_checked ( events_n );
  }

  // -- Outgoing queue

  /// @brief Non blocking queue feeding
  ///
  /// @return True if any event was fed to the queue
  bool
  feed_into_queue ()
  {
    return _out.feed_into_queue ();
  }

  /// @brief Non blocking queue feeding
  ///
  /// @return True if any event was fed to the queue
  bool
  feed_into_queue_checked ( Bit_Notifier::Int_Type & events_n )
  {
    return _out.feed_into_queue_checked ( events_n );
  }

  // -- Operators

  Connections_IO &
  operator= ( const Connections_IO & connection_n ) = delete;

  Connections_IO &
  operator= ( Connections_IO && connection_n ) = delete;

  private:
  Connection_In _in;
  Connection_Out _out;
};
} // namespace event::queue