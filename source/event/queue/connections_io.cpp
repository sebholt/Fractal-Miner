/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <event/queue/connections_io.hpp>

namespace event::queue
{

Connections_IO::Connections_IO () {}

Connections_IO::~Connections_IO () {}

bool
Connections_IO::all_empty () const
{
  return ( _in.is_empty () && _out.is_empty () );
}

void
Connections_IO::clear ()
{
  in ().clear ();
  out ().clear ();
}

bool
Connections_IO::connect ( Reference_Pair queues_n )
{
  const bool res ( _in.connect ( queues_n.first ) &&
                   _out.connect ( queues_n.second ) );
  // Disconnect both on any error
  if ( !res ) {
    disconnect ();
  }
  return res;
}

void
Connections_IO::disconnect ()
{
  in ().disconnect ();
  out ().disconnect ();
}

bool
Connections_IO::all_connected () const
{
  return ( _in.is_connected () && _out.is_connected () );
}
} // namespace event::queue