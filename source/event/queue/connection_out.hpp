/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_notifier.hpp>
#include <event/queue/connection.hpp>

namespace event::queue
{

/// @brief Push connection to a event::Queue with local event chain
///
class Connection_Out : public Connection
{
  public:
  // -- Constructors

  Connection_Out ();

  Connection_Out ( const Connection_Out & connection_n ) = delete;

  Connection_Out ( Connection_Out && connection_n ) = delete;

  ~Connection_Out ();

  using Connection::is_connected;
  using Connection::is_empty;
  using Connection::queue;

  // -- Clear

  /// @brief Disconnects from the queue and clears the push_bit_notifier() and
  ///        pop_bit_notifier()
  void
  clear ();

  // -- Bit notifiers

  /// @brief Gets notified on the first push to the local chain
  ///
  const Bit_Notifier &
  push_bit_notifier () const
  {
    return _push_bit_notifier;
  }

  void
  set_push_bit_notifier ( const Bit_Notifier & notifier_n,
                          bool call_on_demand_n = true );

  void
  set_push_bit_notifier ( std::uint32_t event_bits_n,
                          Bit_Collector * bit_collector_n,
                          bool call_on_demand_n = true )
  {
    set_push_bit_notifier ( Bit_Notifier ( event_bits_n, bit_collector_n ),
                            call_on_demand_n );
  }

  /// @brief Gets notified when events were fetched from the shared queue
  ///
  const Bit_Notifier &
  pop_bit_notifier () const
  {
    return _pop_bit_notifier;
  }

  void
  set_pop_bit_notifier ( const Bit_Notifier & notifier_n );

  void
  set_pop_bit_notifier ( std::uint32_t event_bits_n,
                         Bit_Collector * bit_collector_n )
  {
    set_pop_bit_notifier ( Bit_Notifier ( event_bits_n, bit_collector_n ) );
  }

  // -- Connection

  /// @brief Connects to a given queue
  ///
  /// @return True on success
  bool
  connect ( Queue * queue_n );

  void
  disconnect ();

  // -- Queue feeding

  /// @brief Non blocking queue feeding
  ///
  /// @return True if events were fed to the queue
  bool
  feed_into_queue ();

  /// @brief Non blocking queue feeding
  ///
  /// If events_n contain any bit from push_bit_notifier().event_bits(),
  /// then all bits from push_bit_notifier().event_bits() are cleared in
  /// events_n and feed_into_queue() is called.
  ///
  /// @arg events_n Event bit buffer
  /// @return True if events were fed to the queue
  bool
  feed_into_queue_checked ( Bit_Notifier::Int_Type & events_n );

  // -- Single event processing

  void
  push ( event::Event * event_n )
  {
    Connection::chain_ref ().push_back ( event_n );
    // Notify callback if this was the first push
    if ( _empty_push ) {
      _empty_push = false;
      _push_bit_notifier.notify ();
    }
  }

  // -- Operators

  Connection_Out &
  operator= ( const Connection_Out & connection_n ) = delete;

  Connection_Out &
  operator= ( Connection_Out && connection_n ) = delete;

  private:
  bool _empty_push;
  Bit_Notifier _push_bit_notifier;
  Bit_Notifier _pop_bit_notifier;
};
} // namespace event::queue