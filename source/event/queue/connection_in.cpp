/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <event/queue/connection_in.hpp>
#include <event/queue/queue.hpp>

namespace event::queue
{

Connection_In::Connection_In () {}

Connection_In::~Connection_In ()
{
  disconnect ();
}

void
Connection_In::clear ()
{
  disconnect ();
  _bit_notifier.clear ();
}

void
Connection_In::set_bit_notifier ( const Bit_Notifier & notifier_n )
{
  _bit_notifier = notifier_n;
  if ( is_connected () ) {
    queue ()->set_push_notifier ( _bit_notifier );
  }
}

void
Connection_In::clear_bit_notifier ()
{
  if ( is_connected () ) {
    queue ()->clear_push_notifier ();
  }
  _bit_notifier.clear ();
}

bool
Connection_In::connect ( const Reference & reference_n )
{
  disconnect ();
  if ( reference_n.is_valid () ) {
    Connection::queue_ref () = reference_n;
    queue ()->set_push_notifier ( _bit_notifier );
    return true;
  }
  return false;
}

bool
Connection_In::connect ( Reference && reference_n )
{
  disconnect ();
  if ( reference_n.is_valid () ) {
    Connection::queue_ref () = std::move ( reference_n );
    queue ()->set_push_notifier ( _bit_notifier );
    return true;
  }
  return false;
}

void
Connection_In::disconnect ()
{
  if ( is_connected () ) {
    queue ()->clear_push_notifier ();
    Connection::queue_ref ().clear ();
  }
}

bool
Connection_In::read_from_queue ()
{
  if ( is_connected () ) {
    return queue ()->try_pop_chain ( Connection::chain_ref () );
  }
  return false;
}

bool
Connection_In::read_from_queue_checked ( Bit_Notifier::Int_Type & events_n )
{
  if ( is_connected () ) {
    auto const nbits = _bit_notifier.event_bits ();
    if ( ( events_n & nbits ) != 0 ) {
      events_n &= ( ~nbits );
      return queue ()->try_pop_chain ( Connection::chain_ref () );
    }
  }
  return false;
}
} // namespace event::queue
