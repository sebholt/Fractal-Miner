/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <event/queue/queue.hpp>
#include <debug_assert.hpp>

namespace event::queue
{

// -- Queue

Queue::Queue ()
: _ref_count ( 0 )
{
}

Queue::~Queue () {}

bool
Queue::is_empty ()
{
  _chain_lock.lock ();
  bool res = _chain.is_empty ();
  _chain_lock.unlock ();
  return res;
}

void
Queue::set_push_notifier ( const Bit_Notifier & notifier_n,
                           bool notify_on_demand_n )
{
  _chain_lock.lock ();
  bool const not_empty = !_chain.is_empty ();
  _push_lock.lock ();
  _chain_lock.unlock ();
  _push_notifier = notifier_n;
  if ( not_empty && notify_on_demand_n ) {
    try {
      _push_notifier.notify ();
    }
    catch ( ... ) {
      _push_lock.unlock ();
      throw;
    }
  }
  _push_lock.unlock ();
}

void
Queue::clear_push_notifier ()
{
  _push_lock.lock ();
  _push_notifier.clear ();
  _push_lock.unlock ();
}

void
Queue::set_pop_notifier ( const Bit_Notifier & notifier_n )
{
  _pop_lock.lock ();
  _pop_notifier = notifier_n;
  _pop_lock.unlock ();
}

void
Queue::clear_pop_notifier ()
{
  _pop_lock.lock ();
  _pop_notifier.clear ();
  _pop_lock.unlock ();
}

void
Queue::push ( Event * event_n )
{
  _chain_lock.lock ();
  bool const notify ( _chain.is_empty () );
  _chain.push_back ( event_n );
  if ( notify ) {
    _push_lock.lock ();
  }
  _chain_lock.unlock ();
  if ( notify ) {
    try {
      _push_notifier.notify ();
    }
    catch ( ... ) {
      _push_lock.unlock ();
      throw;
    }
    _push_lock.unlock ();
  }
}

void
Queue::push_chain ( Chain & chain_n )
{
  if ( !chain_n.is_empty () ) {
    _chain_lock.lock ();
    bool const notify ( _chain.is_empty () );
    Chain::move_append_back ( chain_n, _chain );
    if ( notify ) {
      _push_lock.lock ();
    }
    _chain_lock.unlock ();
    if ( notify ) {
      try {
        _push_notifier.notify ();
      }
      catch ( ... ) {
        _push_lock.unlock ();
        throw;
      }
      _push_lock.unlock ();
    }
  }
}

bool
Queue::try_pop_chain ( Chain & chain_n )
{

  _chain_lock.lock ();
  if ( !_chain.is_empty () ) {
    Chain::move_append_back ( _chain, chain_n );
    _pop_lock.lock ();
    _chain_lock.unlock ();
    try {
      _pop_notifier.notify ();
    }
    catch ( ... ) {
      _pop_lock.unlock ();
      throw;
    }
    _pop_lock.unlock ();
    return true;
  } else {
    _chain_lock.unlock ();
  }
  return false;
}
} // namespace event::queue