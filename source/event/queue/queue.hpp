/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_notifier.hpp>
#include <event/chain.hpp>
#include <event/event.hpp>
#include <event/spinlock.hpp>
#include <atomic>

namespace event::queue
{

// -- Forward declaration
class Reference;
class Connection_In;
class Connection_Out;

/// @brief Shared event queue
///
class Queue
{
  public:
  // -- Friends
  friend class Reference;
  friend class Connection_In;
  friend class Connection_Out;

  public:
  /// @ return True if all chains are empty
  bool
  is_empty ();

  // -- Push

  void
  push ( Event * event_n );

  /// @brief Whole chains push
  void
  push_chain ( Chain & chain_n );

  // -- Pop

  /// @brief Non blocking whole chain pop
  ///
  /// The queue is empty afterwards.
  /// @return True if events were fetched
  bool
  try_pop_chain ( Chain & chain_n );

  // -- Assignment operators

  /// @brief Disable copy assignment
  ///
  Queue &
  operator= ( const Queue & queue_n ) = delete;

  /// @brief Disable move assignment
  ///
  Queue &
  operator= ( Queue && queue_n ) = delete;

  private:
  // -- Constructors

  Queue ();

  /// @brief Disable copy contructor
  ///
  Queue ( const Queue & queue_n ) = delete;

  /// @brief Disable move contructor
  ///
  Queue ( Queue && queue_n ) = delete;

  ~Queue ();

  void
  set_push_notifier ( const Bit_Notifier & notifier_n,
                      bool notify_on_demand_n = true );

  void
  clear_push_notifier ();

  void
  set_pop_notifier ( const Bit_Notifier & notifier_n );

  void
  clear_pop_notifier ();

  // -- Reference counting

  void
  ref_count_add ()
  {
    ++_ref_count;
  }

  /// @return Reference count afterwards
  std::uint32_t
  ref_count_sub ()
  {
    return --_ref_count;
  }

  private:
  event::Spinlock _chain_lock;
  Chain _chain;
  /// @brief Gets notified when events were pushed into an empty queue
  event::Spinlock _push_lock;
  Bit_Notifier _push_notifier;
  /// @brief Gets notified when events were fetched from a non empty queue
  event::Spinlock _pop_lock;
  Bit_Notifier _pop_notifier;
  /// @brief reference count
  std::atomic_uint _ref_count;
};
} // namespace event::queue