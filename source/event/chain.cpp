/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include "chain.hpp"
#include <algorithm>

namespace event
{

std::size_t
Chain::count_events () const noexcept
{
  std::size_t res ( 0 );
  if ( !is_empty () ) {
    event::Event * itc ( _front );
    do {
      ++res;
      {
        event::Event * const itnext ( itc->chain_link ().next );
        if ( itc == itnext ) {
          break;
        }
        itc = itnext;
      }
    } while ( true );
  }
  return res;
}

void
Chain::remove ( Event * event_n )
{
  if ( _front == _back ) {
    // The only element
    _front = nullptr;
    _back = nullptr;
  } else {
    // Not the only element
    if ( event_n == _back ) {
      // Last element
      _back = _back->chain_link ().prev;
      _back->chain_link_ref ().next = _back;
    } else if ( event_n == _front ) {
      // First element
      _front = _front->chain_link ().next;
      _front->chain_link_ref ().prev = _front;
    } else {
      // Middle element
      event_n->chain_link ().prev->chain_link_ref ().next =
          event_n->chain_link ().next;
      event_n->chain_link ().next->chain_link_ref ().prev =
          event_n->chain_link ().prev;
    }
  }
  // Clear chain pointers
  event_n->chain_clear ();
}

void
Chain::move_append_back ( Chain & src_n, Chain & dst_n ) noexcept
{
  if ( !src_n.is_empty () ) {
    if ( dst_n.is_empty () ) {
      // Copy to target
      dst_n._front = src_n._front;
      dst_n._back = src_n._back;
    } else {
      // Link front and back events
      src_n._front->chain_link_ref ().prev = dst_n._back;
      dst_n._back->chain_link_ref ().next = src_n._front;
      // Update target back pointer
      dst_n._back = src_n._back;
    }
    // Clear source
    src_n._front = nullptr;
    src_n._back = nullptr;
  }
}
} // namespace event
