/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>
#include <map>

namespace event
{

// -- Forward declaration
class Event;
class Pool_Base;

/// @brief Simple event chain tracker
///
class Pool_Tracker
{
  public:
  // -- Types
  typedef std::map< std::uint32_t, Pool_Base * > Pool_Register;
  // -- Friends
  friend class Pool_Base;

  public:
  // -- Constructors

  Pool_Tracker ();

  Pool_Tracker ( const Pool_Tracker & chain_n ) = delete;

  Pool_Tracker ( Pool_Tracker && chain_n ) = delete;

  ~Pool_Tracker ();

  // -- Pools statistics / manipulation

  /// @brief Returns true if all pools' events are back home
  bool
  all_home () const;

  /// @brief Returns true if all pools are empty
  bool
  all_empty () const;

  /// @brief Returns true if all pools are not empty
  bool
  all_not_empty () const;

  /// @brief Sum of all events not home in the pools
  std::size_t
  size_outside () const;

  void
  clear_all_instances () const;

  void
  set_all_capacities ( std::size_t capacity_n ) const;

  void
  clear_all_capacities () const;

  // -- Abstract event release

  event::Pool_Base *
  pool_by_event_type ( std::uint32_t event_type_n );

  /// @brief Releases the event to a pool identified by the event's event_type()
  /// @return True if the event was released
  bool
  casted_reset_release ( event::Event * event_n );

  // -- Assignment operators

  Pool_Tracker &
  operator= ( const Pool_Tracker & chain_n ) = delete;

  Pool_Tracker &
  operator= ( Pool_Tracker && chain_n ) = delete;

  private:
  // -- Pool registration

  void
  pool_register ( std::uint32_t event_type_n, Pool_Base * pool_n );

  void
  pool_unregister ( Pool_Base * pool_n );

  private:
  Pool_Register _pool_register;
};
} // namespace event
