/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_collector.hpp>
#include <functional>

namespace event::bit_collectors
{

/// @brief Notifies a callback on events
///
class Callback : public Bit_Collector
{
  public:
  // -- Types
  typedef std::function< void () > Callback_Function;

  public:
  // -- Constructors

  Callback ();

  Callback ( const Callback_Function & function_n );

  Callback ( Callback_Function && function_n );

  ~Callback ();

  // -- Callback

  /// @brief The installed callback
  ///
  const Callback_Function &
  callback () const
  {
    return _callback;
  }

  /// @brief Installed a callback
  ///
  /// @arg callback_n The callback
  /// @arg call_on_demand_n Selects whether the callback can be called
  /// immediately
  /// by this method
  void
  set_callback ( Callback_Function && callback_n,
                 bool call_on_demand_n = true );

  void
  set_callback ( const Callback_Function & callback_n,
                 bool call_on_demand_n = true )
  {
    set_callback ( Callback_Function ( callback_n ), call_on_demand_n );
  }

  void
  clear_callback ();

  // -- Abstract interface

  /// @see Bit_Collector::notify_event
  void
  notify_events ( Int_Type events_n ) override;

  /// @see Bit_Collector::notify_function
  static void
  notify_event_callback ( Bit_Collector * collector_n, Int_Type event_bits_n );

  /// @see Bit_Collector::notify_function
  typename Bit_Collector::Func_Notify
  notify_function () override;

  private:
  void
  notify_events_inline ( Int_Type events_n );

  private:
  Callback_Function _callback;
};
} // namespace event::bit_collectors
