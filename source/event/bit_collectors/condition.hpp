/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <event/bit_collector.hpp>
#include <chrono>
#include <condition_variable>

namespace event::bit_collectors
{

/// @brief Supports waiting at a std::condition_variable
///
class Condition : public Bit_Collector
{
  public:
  // -- Constructors

  Condition ();

  ~Condition ();

  // -- Abstract interface

  /// @see Bit_Collector::notify_event
  void
  notify_events ( Int_Type events_n ) override;

  /// @see Bit_Collector::notify_function
  static void
  notify_event_callback ( Bit_Collector * collector_n, Int_Type event_bits_n );

  /// @see Bit_Collector::notify_function
  typename Bit_Collector::Func_Notify
  notify_function () override;

  // -- Event waiting

  /// @brief Waits for any event notification
  ///
  void
  wait_for_events ();

  /// @brief Waits for any event notification
  ///
  /// - Shared events extended by events_n.flags()
  /// - Shared mask set to events_n.mask()
  /// - Wait until any shared events appear that pass the shared mask
  /// - events_n.flags() set to shared flags
  /// - Shared flags cleared
  ///
  /// @arg events_n flags() gets extended by incoming events
  void
  wait_for_events ( Int_Type & events_n );

  /// @brief Same as wait_for_events() but with additional timeout
  ///
  /// If a the time of time_point_n was met true is returned and
  /// time_now_n is set to Clock::now().
  ///
  /// @return True if time_point_n time was met
  bool
  wait_for_events_until (
      Int_Type & events_n,
      const std::chrono::steady_clock::time_point & time_point_n );

  /// @brief Same as wait_for_events() but with additional timeout
  ///
  /// If a the time of time_point_n was met true is returned and
  /// time_now_n is set to Clock::now().
  ///
  /// @return True if time_point_n time was met
  bool
  wait_for_events_until (
      Int_Type & events_n,
      const std::chrono::system_clock::time_point & time_point_n );

  private:
  void
  notify_events_inline ( Int_Type events_n );

  template < class Clock >
  bool
  wait_for_events_until_inline (
      Int_Type & events_n, const typename Clock::time_point & time_point_n );

  private:
  std::condition_variable _cond;
  std::uint32_t _num_waiters;
};
} // namespace event::bit_collectors
