/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <event/bit_collectors/callback.hpp>

namespace event::bit_collectors
{

Callback::Callback () {}

Callback::Callback ( const Callback_Function & callback_n )
: _callback ( callback_n )
{
}

Callback::Callback ( Callback_Function && callback_n )
: _callback ( std::move ( callback_n ) )
{
}

Callback::~Callback () {}

void
Callback::set_callback ( Callback_Function && callback_n,
                         bool call_on_demand_n )
{
  bool any_flags ( false );
  {
    std::lock_guard< std::mutex > mlock ( mutex () );
    if ( call_on_demand_n ) {
      any_flags = ( events_ref () != 0 );
    }
    _callback = std::move ( callback_n );
    if ( any_flags && _callback ) {
      _callback ();
    }
  }
}

void
Callback::clear_callback ()
{
  std::lock_guard< std::mutex > mlock ( mutex () );
  _callback = Callback_Function ();
}

void
Callback::notify_events_inline ( Int_Type events_n )
{
  if ( events_n != 0 ) {
    std::lock_guard< std::mutex > mlock ( mutex () );
    Int_Type const flags_before = events_ref ();
    events_ref () |= events_n;
    // Notify callback on demand
    if ( ( flags_before == 0 ) && _callback ) {
      _callback ();
    }
  }
}

void
Callback::notify_events ( Int_Type events_n )
{
  notify_events_inline ( events_n );
}

void
Callback::notify_event_callback ( Bit_Collector * collector_n,
                                  Int_Type events_n )
{
  typedef Callback * PType;
  PType inst ( static_cast< PType > ( collector_n ) );
  inst->notify_events_inline ( events_n );
}

typename Bit_Collector::Func_Notify
Callback::notify_function ()
{
  return &Callback::notify_event_callback;
}
} // namespace event::bit_collectors
