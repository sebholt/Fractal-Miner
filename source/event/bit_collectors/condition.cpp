/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#include <event/bit_collectors/condition.hpp>

namespace event::bit_collectors
{

Condition::Condition ()
: _num_waiters ( 0 )
{
}

Condition::~Condition () {}

void
Condition::notify_events_inline ( Int_Type events_n )
{
  if ( events_n != 0 ) {
    bool notify ( false );
    {
      std::lock_guard< std::mutex > lock ( mutex () );
      notify = ( ( events_ref () == 0 ) && ( _num_waiters != 0 ) );
      events_ref () |= events_n;
    }
    if ( notify ) {
      _cond.notify_all ();
    }
  }
}

void
Condition::notify_events ( Int_Type events_n )
{
  notify_events_inline ( events_n );
}

void
Condition::notify_event_callback ( Bit_Collector * collector_n,
                                   Int_Type events_n )
{
  typedef Condition * PType;
  PType inst ( static_cast< PType > ( collector_n ) );
  inst->notify_events_inline ( events_n );
}

Bit_Collector::Func_Notify
Condition::notify_function ()
{
  return &Condition::notify_event_callback;
}

void
Condition::wait_for_events ()
{
  {
    std::unique_lock< std::mutex > lock ( mutex () );
    if ( events_ref () == 0 ) {
      ++_num_waiters;
      do {
        _cond.wait ( lock );
      } while ( events_ref () == 0 );
      --_num_waiters;
    }
    events_ref () = 0;
  }
}

void
Condition::wait_for_events ( Int_Type & events_n )
{
  {
    std::unique_lock< std::mutex > lock ( mutex () );
    events_ref () |= events_n;
    if ( events_ref () == 0 ) {
      ++_num_waiters;
      do {
        _cond.wait ( lock );
      } while ( events_ref () == 0 );
      --_num_waiters;
    }
    events_n = events_ref ();
    events_ref () = 0;
  }
}

bool
Condition::wait_for_events_until (
    Int_Type & events_n,
    const std::chrono::steady_clock::time_point & time_point_n )
{
  return wait_for_events_until_inline< std::chrono::steady_clock > (
      events_n, time_point_n );
}

bool
Condition::wait_for_events_until (
    Int_Type & events_n,
    const std::chrono::system_clock::time_point & time_point_n )
{
  return wait_for_events_until_inline< std::chrono::system_clock > (
      events_n, time_point_n );
}

template < class Clock >
bool
Condition::wait_for_events_until_inline (
    Int_Type & events_n, const typename Clock::time_point & time_point_n )
{
  bool res ( false );
  bool immediate ( false );
  {
    std::unique_lock< std::mutex > lock ( mutex () );
    events_ref () |= events_n;
    if ( events_ref () == 0 ) {
      ++_num_waiters;
      do {
        if ( _cond.wait_until ( lock, time_point_n ) ==
             std::cv_status::timeout ) {
          // Timeout expired
          res = true;
          break;
        }
      } while ( events_ref () == 0 );
      --_num_waiters;
    } else {
      // Events found initially.
      immediate = true;
    }
    events_n = events_ref ();
    events_ref () = 0;
  }

  // Perform the timeout check manually on demand
  if ( immediate ) {
    if ( Clock::now () >= time_point_n ) {
      res = true;
    }
  }
  return res;
}
} // namespace event::bit_collectors
