/// Fractal-Miner2: Fractal image rendering program.
/// \copyright See LICENSE.txt file.
#pragma once

#include <cstdint>

namespace event
{

// -- Forward declaration
class Chain;
class Stack;

/// @brief Event base class with type id and chaining support
///
class Event
{
  public:
  // -- Types
  struct Chain_Link
  {
    Event * prev;
    Event * next;
  };
  // -- Friends
  friend class event::Chain;
  friend class event::Stack;

  public:
  // -- Constructors

  Event ( std::uint32_t type_n = 0 )
  : _type ( type_n )
  , _chain_link{ nullptr, nullptr }
  {
  }

  ~Event () {}

  // -- Event type

  /// @brief The event type
  /// @return The event type
  std::uint32_t
  type () const
  {
    return _type;
  }

  // -- Event type

  /// @brief Reset to construction state (dummy implementation)
  ///
  /// This is used by event event::Pool::reset_release().
  void
  reset ()
  {
  }

  // -- Chain

  const Chain_Link &
  chain_link () const
  {
    return _chain_link;
  }

  /// @return True if ( chain().prev != nullptr )
  bool
  is_chained () const
  {
    return ( _chain_link.prev != nullptr );
  }

  private:
  // -- Chain

  Chain_Link &
  chain_link_ref ()
  {
    return _chain_link;
  }

  /// @brief Clear the chain references
  void
  chain_clear ()
  {
    _chain_link.prev = nullptr;
    _chain_link.next = nullptr;
  }

  private:
  std::uint32_t _type;
  Chain_Link _chain_link;
};
} // namespace event
