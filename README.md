# Fractal-Miner


## Description

Fractal-Miner is an interactive fractal image viewer.

It shows the famous Mandelbrot fractal and allows
zooming and paning in the visible area.

The user interface is built upon the Qt5 libraries.
Internally a custom built multithreaded pipeline engine distributes the
fractal iteration among all available CPU cores.
Colorization is performed on the GPU which allows
realtime color palette cycling.


## Building

Fractal-Miner uses the CMake build system. To build the application call

```
mkdir build
cd build
cmake fractal-miner-source-dir
make
```


## Download

The Fractal-Miner source code is hosted at gitlab.com:

https://gitlab.com/sebholt/Fractal-Miner


## COPYING

Fractal-Miner is distributed under the terms in the
the [LICENSE.txt](LICENSE.txt) file.


## Author

Fractal-Miner was written by:

Sebastian Holtermann <sebholt@web.de>
